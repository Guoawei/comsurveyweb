<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Tools Class
 *
 * @package
 * @subpackage
 * @category	back-end manager
 * @author		Will Tsai
 * @link		http://sweea.com
 */
class Tools
{

	/**
	 * get_thumb_path()
	 * 轉成縮圖檔名
	 * @access	public
	 * @param	array $files 要轉的檔案路徑
	 * @return	string
	 */
	public function get_thumb_path($files)
	{
		if (file_exists($files))
		{
			$fileParts = pathinfo($files);
			$subname = $fileParts['extension'];
			//附檔名

			return str_replace('.' . $subname, '_thumb.' . $subname, $files);
		}
		else
		{

			return false;
		}
	}

	/**
	 * get_search_key_word($str)
	 * 取得搜尋得字串，去掉空白與逗號，並加入%字元
	 * @param int $str 要處理的字串
	 * @return string
	 */
	public function get_search_key_word($str)
	{
		$final = '';
		$str = str_replace(" ", "|", $str);
		$str = str_replace(",", "|", $str);
		$str = str_replace("+", "|", $str);
		$ep = explode("|", $str);
		foreach ($ep as $key=>$value)
		{
			if ($str)
			{
				$final .= $value;
				if ($key < (count($ep) - 1))
					$final .= "%";
			}
		}
		return $final;
	}

	/**
	 * 修正 pathinfo 中文問題
	 * uploadify()
	 */
	function fix_pathinfo($path)
	{
		$dirname = '';
		$basename = '';
		$extension = '';
		$filename = '';

		$pos = strrpos($path, '/');

		if ($pos !== false)
		{
			$dirname = substr($path, 0, strrpos($path, '/'));
			$basename = substr($path, strrpos($path, '/') + 1);
		}
		else
		{
			$basename = $path;
		}

		$ext = strrchr($path, '.');
		if ($ext !== false)
		{
			$extension = substr($ext, 1);
		}

		$filename = $basename;
		$pos = strrpos($basename, '.');
		if ($pos !== false)
		{
			$filename = substr($basename, 0, $pos);
		}

		return array(
			'dirname'=>$dirname,
			'basename'=>$basename,
			'extension'=>$extension,
			'filename'=>$filename
		);
	}
	
	/**
	 * get_taiwan_numbedr($str)
	 * 取得中華名國日期
	 *
	 * @param $str 含數字的字串
	 * @return boolean, string
	 */
	public function get_taiwan_numbedr($str)
	{
		$ch_num = array(
			'Ｏ',
			'一',
			'二',
			'三',
			'四',
			'五',
			'六',
			'七',
			'八',
			'九'
		);
		
		$word = $this->utf8_str_split($str);
		$new_str = '';
		$new_replace_str = '';
		//取得數字
		$i = 0;
		foreach ($word as $key => $row) {
			if(is_numeric($row))
			{
				$number[$i][] = array('num'=>$row,'index'=>$key);
				$new_str = $new_str.$ch_num[$row];
			}
			else 
			{
				$i++;	
				$new_str = $new_str.$row;
			}
		}
		foreach ($number as $key => $value) {
			if(count($value) == 2)
			{
				//取得要被置換的字串
				$sp_num[0] = $ch_num[mb_substr($str,$value[0]['index'],1)];
				$sp_num[1] = $ch_num[mb_substr($str,$value[1]['index'],1)];
				$plus = $sp_num[0].$sp_num[1];
				if($sp_num[0] == 'Ｏ')
				{
					$new_replace_str = $sp_num[1];
				}
				else 
				{
					switch ($plus) {
						case '一Ｏ':
							$new_replace_str = "十";
							break;
						case '二Ｏ':
							$new_replace_str = "二十";
							break;
						case '三Ｏ':
							$new_replace_str = "三十";
							break;
						case '四Ｏ':
							$new_replace_str = "四十";
							break;
						case '五Ｏ':
							$new_replace_str = "五十";
							break;
						case '六Ｏ':
							$new_replace_str = "六十";
							break;
						case '七Ｏ':
							$new_replace_str = "七十";
							break;
						case '八Ｏ':
							$new_replace_str = "八十";
							break;
						case '九Ｏ':
							$new_replace_str = "九十";
							break;
						default:
							
							switch ($sp_num[0]) {
								case '一':
									$new_replace_str = "十".$sp_num[1];
									break;
								default:
									$new_replace_str = $sp_num[0]."十".$sp_num[1];
									break;
							}
							break;
					}
				}
				
				//置換兩位數數字
				//echo $new_replace_str."<br/>";
				if($new_replace_str != '')
				{
					$new_str = str_replace($plus, $new_replace_str, $new_str);
				}
			}
		}
		return $new_str;
	}
	
	/**
	 * get_taiwan_date($date)
	 * 取得中華名國日期
	 *
	 * @param $date 日期 2013-11-12
	 * @return boolean, string
	 */
	public function get_taiwan_date($date)
	{
		$ch_num = array(
			'Ｏ',
			'一',
			'二',
			'三',
			'四',
			'五',
			'六',
			'七',
			'八',
			'九'
		);
		$split = substr($date, 4, 1);
		$sp_date = explode($split, $date);
		if (count($sp_date) == 3)
		{
			$sp_date[0] = $sp_date[0] - 1911;
			//轉成民國年
			for ($i = 2; $i >= 0; $i--)
			{
				$sp = str_split($sp_date[$i]);
				$str = '';
				for ($j = count($sp) - 1; $j >= 0; $j--)
				{
					if ($i == 0)
					{
						$str = $ch_num[$sp[$j]] . $str;
					}
					else
					{
						$str = $ch_num[$sp[$j]] . $str;
						if(mb_strlen($str,'UTF-8') == 2)
						{
							$sp_date2 = $this->utf8_str_split($str);
							if($sp_date2[0] == 'Ｏ')
							{
								$str = mb_substr($str, 1,1,'UTF-8');
							}
							else 
							{
								switch ($str) {
									case '一Ｏ':
										$str = "十";
										break;
									case '二Ｏ':
										$str = "二十";
										break;
									case '三Ｏ':
										$str = "三十";
										break;
									default:
										$ten = mb_substr($str, 0,1,'UTF-8');
										switch ($ten) {
											case '一':
												$str = "十".mb_substr($str, 1,1,'UTF-8');
												break;
											case '二':
												$str = $ten."十".mb_substr($str, 1,1,'UTF-8');
												break;
											case '三':
												$str = $ten."十".mb_substr($str, 1,1,'UTF-8');
												break;
											default:
												
												break;
										}
										break;
								}
							}
						}
					}
				}
				$sp_date[$i] = $str;
			}
			return $sp_date[0].'年'.$sp_date[1].'月'.$sp_date[2].'日';
		}
		else
		{
			return FALSE;
		}
	}
	
	function utf8_str_split($str, $split_len = 1)
	{
	    if (!preg_match('/^[0-9]+$/', $split_len) || $split_len < 1)
	        return FALSE;
	 
	    $len = mb_strlen($str, 'UTF-8');
	    if ($len <= $split_len)
	        return array($str);
	 
	    preg_match_all('/.{'.$split_len.'}|[^\x00]{1,'.$split_len.'}$/us', $str, $ar);
	 
	    return $ar[0];
	}

	/**
	 * get_chinese_number($str,$money = false)
	 * 取得中文的字母2
	 *
	 * @param $str 為字串
	 * @param $money 是否為錢
	 * @return boolean string
	 */
	public function get_chinese_number($insertStr, $money = false)
	{
		if ($money)
		{
			if (is_numeric($insertStr))
			{
				$ch_num = array(
					'零',
					'壹',
					'貳',
					'參',
					'肆',
					'伍',
					'陸',
					'柒',
					'捌',
					'玖'
				);
				$ch_num_unit = array(
					"",
					"拾",
					"佰",
					"仟",
					"萬",
					"拾",
					"佰",
					"仟",
					"億",
					"拾",
					"佰",
					"仟",
					"兆"
				);
				$sp_money = str_split($insertStr);
				$j = 0;
				$str = '';
				for ($i = strlen($insertStr) - 1; $i >= 0; $i--)
				{
					if ($ch_num[$sp_money[$i]] != '零')
						$str = $ch_num[$sp_money[$i]] . "" . $ch_num_unit[$j] . "" . $str;
					$j++;
				}

				return $str;
			}
		}
		else
		{
			$ch_num = array(
				'零',
				'一',
				'二',
				'三',
				'四',
				'五',
				'六',
				'七',
				'八',
				'九'
			);
			$sp_str = str_split($insertStr);
			$str = '';
			for ($i = strlen($insertStr) - 1; $i >= 0; $i--)
			{
				if (is_numeric($sp_str[$i]))
					$str = $ch_num[$sp_str[$i]] . $str;
				else
					$str = $sp_str[$i] . $str;
			}
			return $str;
		}
	}

}

/* End of file Someclass.php */

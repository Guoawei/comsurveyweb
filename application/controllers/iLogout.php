<?php
    /**
     * 
     */
    class iLogout extends CI_Controller {
        
        function iLogout() {
            parent::__construct();
			$this->load->helper('url');
			$this->load->library('session');
        }
		
		function index()
		{
			$this->doLogout();
			redirect('iLogin', 'refresh');
		}
		
		function doLogout()
		{
			$loginData = $this->session->all_userdata();
			$this->session->unset_userdata($loginData);	
		}
    }
    
?>
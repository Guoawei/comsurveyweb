<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Uploadify Class
 *
 * @package		多多 文章管理系統 dodo
 * @subpackage	上傳檔案 Uploadify
 * @category	back-end manager
 * @author		Will Tsai
 * @link		http://sweea.com
 */
class Uploader extends CI_Controller
{
	/**
	 * @link
	 * @see
	 * @uses 取得每個控制所需要的預設資料
	 * @var array
	 * @access protected
	 */
	protected $default_data = array();
	/**
	 * @link
	 * @see
	 * @uses 設定特殊允許的html標籤
	 * @var array
	 * @access protected
	 */
	protected $specialchars;
	/**
	 * @link
	 * @see
	 * @uses 設定rootpath給上傳檔案用
	 * @var array
	 * @access protected
	 */
	protected $rootpath;
	/**
	 * __construct 建構子
	 * 取得預設所要的相關資料
	 */
	public function __construct()
	{
		parent::__construct();

		//取得根目錄
		$this->rootpath = str_ireplace('index.php', '', $_SERVER['SCRIPT_FILENAME']);

	}

	/**
	 * 管理介面的第一頁索引頁
	 */
	public function index()
	{

	}

	/**
	 * 產生亂數密碼
	 *
	 * @access	public
	 * @param	int $len 要產生的密碼字數長度
	 * @return	string 產出的密碼
	 */
	function generator_pw($len)
	{
		$password_len = $len;
		$password = '';

		// remove o,0,1,l
		$word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
		$len = strlen($word);
		for ($i = 0; $i < $password_len; $i++)
		{
			$password .= $word[rand() % $len];
		}
		return $password;
	}

	/**
	 * 給取的主機的作業系統
	 * getServerOS()
	 *
	 */
	function getServerOS()
	{
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			return 'win';
		}
		else
		{
			return 'linux';
		}
	}

	/**
	 * 給uploadify用的 檢查檔案有無存在 check-exists.php
	 * uploadCheckExists()
	 *
	 */
	public function uploadCheckExists()
	{
		if (file_exists($this->rootpath . $this->input->get('targetFolder') . '/' . $this->input->post('filename')))
		{
			echo 1;
		}
		else
		{
			echo 0;
		}
	}

	
	/**
	 * 給uploadify用的 取代 uploder.php
	 * uploadify()
	 *
	 */
	public function uploadify()
	{
		$this->load->library('tools');
		
		//作業系統 與刪除檔案的編碼有關係 win or linux
		$os = $this->getServerOS();

		date_default_timezone_set('Asia/Taipei');

		$targetFolder = $this->input->post('targetFolder');
		if (!empty($_FILES))
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];

			$targetPath = $this->rootpath . $targetFolder;

			$fileParts = $this->tools->fix_pathinfo($_FILES['Filedata']['name']);
			$subname = strtolower($fileParts['extension']);
			//附檔名轉小寫

			// 取得附檔名是否為圖檔
			$fileTypes = array(
				'jpg',
				'jpeg',
				'gif',
				'png'
			);
			// File extensions
			$videoTypes = array(
				'mp4',
				'flv'
			);
			// video File extensions

			//取得原先檔名
			$oldname = $fileParts['basename'];

			//不要改檔名
			if ($this->input->get('not_change_name'))
			{
				//處理符號與空白
				$filename = str_replace(array(
					" ",
					",",
					"(",
					")",
					"-"
				), "_", trim($fileParts['filename']));
			}
			else
			{
				$filename = date("YmdHis") . "_" . time() . "_" . $this->generator_pw(10);
			}

			//執行圖片改名
			$targetFile = rtrim($targetPath, '/') . '/' . $filename . "." . $subname;

			switch ($_FILES['Filedata']['error'])
			{
				case 0:
					$msg = "No Error";
					// comment this out if you don't want a message to appear on success.
					break;
				case 1:
					$msg = "The file is bigger than this PHP installation allows";
					break;
				case 2:
					$msg = "The file is bigger than this form allows";
					break;
				case 3:
					$msg = "Only part of the file was uploaded";
					break;
				case 4:
					$msg = "No file was uploaded";
					break;
				case 6:
					$msg = "Missing a temporary folder";
					break;
				case 7:
					$msg = "Failed to write file to disk";
					break;
				case 8:
					$msg = "File upload stopped by extension";
					break;
				default:
					$msg = "unknown error " . $_FILES['Filedata']['error'];
					break;
			}

			$json['oldname'] = str_replace("." . $subname, '', $oldname);
			if ($_FILES['Filedata']['error'] != 0)
			{
				$json['msg'] = $msg;
			}
			else
			{
				//沒有錯誤就搬移檔案，並回傳資訊
				if ($os == 'win')
				{
					//$targetFile= mb_convert_encoding($targetFile,"big5","utf8");
				}
				if (move_uploaded_file($tempFile, $targetFile))
				{
					//完整元檔名稱位址
					$json['original'] = $targetFolder . "/" . $filename . "." . $subname;

					//如果沒有傳送不要縮圖
					if (!$this->input->get('not_zip_img'))
					{
						//如果為圖檔就壓縮
						if (in_array($subname, $fileTypes))
						{
							//預設的縮圖高跟寬，若有傳get來，就取得值
							$default_thumb_w = ($this->input->get('thumb_w')) ? $this->input->get('thumb_w') : 200;
							$default_thumb_h = ($this->input->get('thumb_h')) ? $this->input->get('thumb_h') : 200;
							//執行縮圖
							$config_1['image_library'] = 'gd2';
							$config_1['create_thumb'] = TRUE;
							$config_1['maintain_ratio'] = TRUE;
							$config_1['source_image'] = $targetFile;
							$config_1['width'] = $default_thumb_w;
							$config_1['height'] = $default_thumb_h;
							$this->image_lib->initialize($config_1);
							if ($this->image_lib->resize())
							{
								$this->image_lib->clear();
							}
							else
							{
								$json['msg'] = $this->image_lib->display_errors();
							}
							//壓縮原圖
							$getImgInfo = getimagesize($targetFile);
							//$json['msg']=$getImgInfo[0];
							//如果原圖的寬大於900就壓縮到900
							//原圖的高跟寬，若有傳get來，就取得值
							$default_original_w = ($this->input->get('original_w')) ? $this->input->get('original_w') : 900;
							$default_origina_h = ($this->input->get('original_h')) ? $this->input->get('original_h') : 900;
							if ($getImgInfo[0] > $default_original_w)
							{
								$config_2['image_library'] = 'gd2';
								$config_2['maintain_ratio'] = TRUE;
								$config_2['create_thumb'] = FALSE;
								$config_2['master_dim'] = 'width';
								$config_2['source_image'] = $targetFile;
								$config_2['width'] = $default_original_w;
								$config_2['height'] = $default_origina_h;

								$this->image_lib->initialize($config_2);
								if ($this->image_lib->resize())
								{
									$this->image_lib->clear();
								}
								else
								{
									$json['msg'] = $this->image_lib->display_errors();
								}
							}
							else
							{
								//如果沒有大於900就壓縮至80%品質
								$config_2['image_library'] = 'gd2';
								$config_2['maintain_ratio'] = TRUE;
								$config_2['create_thumb'] = FALSE;
								$config_2['master_dim'] = 'width';
								$config_2['source_image'] = $targetFile;
								$config_2['width'] = $getImgInfo[0];
								$config_2['height'] = $getImgInfo[0];

								$this->image_lib->initialize($config_2);
								if ($this->image_lib->resize())
								{
									$this->image_lib->clear();
								}
								else
								{
									$json['msg'] = $this->image_lib->display_errors();
								}
							}

							$json['thumb'] = $targetFolder . "/" . $filename . "_thumb" . "." . $subname;
							//最後取得圖檔的大小
							$getNewImgInfo = getimagesize($targetFile);
							$json['resolution'] = $getNewImgInfo[0] . "x" . $getNewImgInfo[1];
						}
					}

					$json['result'] = TRUE;
					$json['msg'] = '上傳成功';
					$json['msg2'] = $_FILES['Filedata'];
					$json['filename'] = '';
				}
				else
				{
					$json['result'] = FALSE;
					$json['msg'] = '上傳失敗';
				}
				//輸出json格式
				$this->output->set_content_type('application/json')->set_output(json_encode($json));
			}
		}
	}

	/**
	 * 給uploadify用的 取代 uploder.php
	 * uploadify()
	 * @param string $img_url 圖片位置
	 * @param string $crop_info 要裁切的資訊
	 */
	public function crop()
	{
		//取得要裁切的資訊
		// formatter:off
		$config = array(
			'image_library'=>'gd2',
			'maintain_ratio'=>FALSE,
			'source_image'=>$this->input->post('source_image'),
			'width'=>$this->input->post('w'),
			'height'=>$this->input->post('h'),
			'x_axis'=>$this->input->post('x'),
			'y_axis'=>$this->input->post('y')
		);
		// formatter:on
		$this->image_lib->initialize($config);
		if (!$this->image_lib->crop())
		{
			$json['result'] = FALSE;
			$json['msg'] = "圖片裁切失敗：" . $this->image_lib->display_errors();
		}
		else
		{
			$this->image_lib->clear();

			//如果圖片是大於200就縮成200寬
			if ($this->input->post('type') == 'my_photo')
			{
				//如果型態為 大頭貼就判別是否大於200寬，是就縮成200
				if ($this->input->post('w') > 200)
				{

					// @formatter:off
					$config = array(
			                  'image_library'   => 'gd2',
			                  'maintain_ratio'  =>  TRUE,
			                  'create_thumb'	=>	FALSE,
			                  'master_dim'		=>	'width',
			                  'source_image'    =>  $this->input->post('source_image'),
			                  'width'           =>  200,
			                  'height'          =>  200
					);
					// @formatter:on

					$this->image_lib->initialize($config);
					if ($this->image_lib->resize())
					{
						$this->image_lib->clear();
					}
					else
					{
						$json['msg'] = $this->image_lib->display_errors();
					}
				}
			}
			if ($this->input->post('type') == 'article_thumb')
			{
				//先刪除舊縮圖
				unlink($this->post_m->get_thumb_path($this->input->post('source_image')));

				//如果型態為文章縮圖，就將裁切後的，在產生新的縮圖。
				// @formatter:off
				$config = array(
		                  'image_library'   => 'gd2',
		                  'maintain_ratio'  =>  TRUE,
		                  'new_image'		=>	$this->post_m->get_thumb_path($this->input->post('source_image')),
		                  'source_image'    =>  $this->input->post('source_image'),
		                  'width'           =>  200,
		                  'height'          =>  200
				);
				// @formatter:on

				$this->image_lib->initialize($config);
				if ($this->image_lib->resize())
				{
					$this->image_lib->clear();
				}
				else
				{
					$json['msg'] = $this->image_lib->display_errors();
				}

			}
			$json['result'] = TRUE;
			$json['msg'] = '圖片裁切成功';
		}

		//輸出json格式
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	/**
	 * 刪除檔案
	 * delfile()
	 * @param string $img_url 圖片位置
	 * @param string $crop_info 要裁切的資訊
	 */
	public function delfile()
	{
		$json = array(
			'result'=>FALSE,
			'msg'=>'無法刪除'
		);

		//先取得看有無縮圖
		$thumb = $this->get_thumb_path($this->input->post('file'));

		//刪除原檔
		if (unlink($this->input->post('file')))
		{
			$json = array(
				'result'=>TRUE,
				'msg'=>'刪除成功'
			);
		}

		//刪除縮圖檔
		if ($thumb)
		{
			if (unlink($thumb))
			{
				$json = array(
					'result'=>TRUE,
					'msg'=>'原檔與縮圖皆刪除成功'
				);
			}
		}
		//輸出json格式
		$this->output->set_content_type('application/json')->set_output(json_encode($json));
	}

	/**
	 * get_thumb_path()
	 * 轉成縮圖檔名
	 * @access	public
	 * @param	array $files 要轉的檔案路徑
	 * @return	string
	 */
	public function get_thumb_path($files)
	{
		if (file_exists($files))
		{
			$fileParts = pathinfo($files);
			$subname = $fileParts['extension'];
			//附檔名

			return str_replace('.' . $subname, '_thumb.' . $subname, $files);
		}
		else
		{
			return false;
		}
	}

}

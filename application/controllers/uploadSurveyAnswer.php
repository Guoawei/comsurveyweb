<?php
    /**
     * 
     */
    class uploadSurveyAnswer extends CI_Controller {
        
        function uploadSurveyAnswer() {
            parent::__construct();
			$this->load->model("survey_model");
        }
		
		function index()
		{
			//$json =  '{"mail": 12345,"aa":[11,"99"]}';
			// echo $json;
			
			// $json2 = $this->input->get_post('answerJson',true); 			
			// $obj = json_decode($json);
			// $obj2 = json_decode($json2);
			// print $obj->{'mail'}; // 12345
			// print $obj2->{'car'};
			
			// $json = '{"SurveyGuid":"dfd"
					 // ,"aa":32
					 // ,"bb":"bb"}';
			// $obj = json_decode($json);
			// echo $obj->{'bb'};
			
			// $json =  '{"SurveyGuid":"df3b2e687af5377f749cb895b034198d",
					   // "InterviewerGuid":"i332zcd3",
					   // "SampleGuid":"sii3fc",
			           // "AnswerList":{"SubjectNumber":"A1","InputAnswer":["1","3"],
									// ["SubjectNumber":"A2","InputAnswer":"730627"],
									// ["SubjectNumber":"A3","InputAnswer":"25"],
									// ["SubjectNumber":"A4","InputAnswer":"1"]}';
			$printCode = 0;
			$printCode = $this->input->get_post('test',true); 	 
			
			if ($printCode == 1) {
				//Test Json
				$surveyGuid = "3fba9a60f401e35406c2d78340484caa";
				$interviewerGuid = "e078a972f5131eed29c0b12ca596e95c";
				$sampleGuid = "c2caa1d3beeb3a917926b9ade137ef31";
				$subjectNumber = array("A1","A2","A3","A4");
				$inputAnswer = array("1", "測試中文,3","1","8");
				$AnswerList = array('SubjectNumber'=>$subjectNumber,'InputAnswer'=>$inputAnswer);
				$json = json_encode(array('SurveyGuid' => $surveyGuid,
			    		 								'InterviewerGuid' => $interviewerGuid,
			    		 								'SampleGuid' => $sampleGuid,
														'AnswerList'=> $AnswerList
														));
			
				//Output JSON
				$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output(json_encode(array('SurveyGuid' => $surveyGuid,
		    		 								'InterviewerGuid' => $interviewerGuid,
		    		 								'SampleGuid' => $sampleGuid,
													'AnswerList'=> $AnswerList
													)));
			} else {
				$json = file_get_contents('php://input');
				if ($json) {
					$data = json_decode($json);
					$surveyGuid = $data->{'SurveyGuid'};
					$interviewerGuid = $data->{'InterviewerGuid'};
					$sampleGuid = $data->{'SampleGuid'};
					$answerList = $data->{'AnswerList'};
					$subjectNumber = $answerList->{'SubjectNumber'};
					$inputAnswer = $answerList->{'InputAnswer'}; 
				}else {
					$printCode = 1;
					echo "No JSON";
				}

			}
			

			if ($printCode == 0) {
				//建立答案資料表
				$survey = new survey_model();
				$survey->setGuid($surveyGuid);
				$survey = $survey->getSurvey($survey);
				$answerTableName = "survey_answer_".$surveyGuid."_".$survey->getVersion()."";
				
				if (!$this->db->table_exists($answerTableName)) {
					$createTableSql = "create table ".$answerTableName." (id int auto_increment primary key,
																		interviewerGuid varchar(32),
																		sampleGuid varchar(32),
																		cDateTime dateTime);";
					$createTableQuery = $this->db->query($createTableSql);
					
					//查欄位名稱及數量並新增欄位
					$subjectNumberArray = array();
					$sql = "SELECT `subjectNumber` FROM survey_question WHERE surveyGuid='".$surveyGuid."' order by `priority` asc";
					$query = $this->db->query($sql);
					foreach ($query->result() as $row) {
						$addSubjectNumberColumnSql = "ALTER TABLE `".$answerTableName."` ADD COLUMN `".$row->subjectNumber."` text ";
						$this->db->query($addSubjectNumberColumnSql);
					}
	
				}
				
				$sample = new sample_model();
				$sample->setGuid($sampleGuid);
				$sample->getSample($sample);
		
				$sql = "SELECT sampleGuid FROM ".$answerTableName." where sampleGuid = '".$sampleGuid."'";
				$result = $this->db->query($sql);
				//隨時都可以更新的功能在2012/6/18時修改需求時增加了。
				if ($result->num_rows()==0) {
					$sql = "insert into ".$answerTableName." (interviewerGuid,sampleGuid,cDateTime)
													    values ('".$interviewerGuid."','".$sampleGuid."',now())";
					$this->db->query($sql);
					for ($i=0; $i < count($subjectNumber); $i++) { 
						$sql = "UPDATE ".$answerTableName." SET `".$subjectNumber[$i]."` = '".urldecode($inputAnswer[$i])."' where sampleGuid = '".$sampleGuid."'";
						$this->db->query($sql);
					}
					
					$sql = "SELECT sampleGuid FROM ".$answerTableName." where sampleGuid = '".$sampleGuid."'";
					$result = $this->db->query($sql);

					//回傳成功、失敗
					if ($result->num_rows()==1) {
						echo $sample->getName().":Success";	
					}else {
						echo $sample->getName().":Failure";	
					}
				}else {
					//回傳已經有記錄
					for ($i=0; $i < count($subjectNumber); $i++) { 
						$sql = "UPDATE ".$answerTableName." SET `".$subjectNumber[$i]."` = '".urldecode($inputAnswer[$i])."' where sampleGuid = '".$sampleGuid."'";
						$this->db->query($sql);
					}
					
					$sql = "SELECT sampleGuid FROM ".$answerTableName." where sampleGuid = '".$sampleGuid."'";
					$result = $this->db->query($sql);
					
					echo $sample->getName().":Uploaded";	
				}
				
				//20131123 珂：開啟了資料庫快取，因此有新的答案進去後，都要清除快取，好讓網頁版可以取得最新的樣本答案進度。
				$this->db->cache_delete_all();
			}	

		}

		

    }
    
	
?>
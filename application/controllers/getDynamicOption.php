<?php
    /**
     * 
     */
    class getDynamicOption extends CI_Controller {
        
        function getDynamicOption() {
            parent::__construct();
			$this->load->model("dynamicoption_model");		
        }
		
		function index()
		{
			$dyOption = new dynamicoption_model();			
			
			//取得參數
			$dyOptionGuid =  $this->input->get_post('dyoptionguid',true);
			
			$dyOption->setGuid($dyOptionGuid);

			//取得該選項要與它對應的key
			$dyOption = $dyOption->getDynamicOption($dyOption);

			//從DB撈資料
			$data = $dyOption->getDynamicOptionList($dyOption);
			if (count($data)>0) {
				
				//樣本人數
				$dyOptionCount = count($data);
				//集合成Array
				for ($i=0; $i < $dyOptionCount; $i++) { 
					$loaddyOption = new dynamicoption_model();
					$loaddyOption = $data[$i];
					$loaddyOption->setAccordingKey($dyOption->getAccordingKey());
					$dyOptionDataA = array('accordingValue' =>$loaddyOption->getAccordingValue(),
										   'index'=>$loaddyOption->getOptionIndex(),
										   'text'=>$loaddyOption->getOptionText(),
										   'value'=>$loaddyOption->getOptionValue());
					$dyOptionData[] = $dyOptionDataA;  
				}
				
					//Output JSON
					$this->output
		    			 ->set_content_type('application/json')
		    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
		    			 												  'Doc' => '讀取正常'),
														'Body' => array('getDynamicOption' => array(
																		'subject_accordingKey' => $dyOption->getAccordingKey(),
																		'subject_subjectIndex' => $dyOption->getSubjectIndex(),
																		'subject_accordingValue' => $dyOptionData
																		 )))));	
								
			} else {
					//帳號密碼錯誤
					//Output JSON
					$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
			}
			
		}
    }
    
?>
<?php 

class Show extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	function Show(){
		parent::__construct();
		$this->load->model("survey_model");
		$this->load->model("question_model");
		// $this->load->model("questionnaire_model");
		// $this->load->model("qtype_model");
		$this->load->model("interviewer_model");
		$this->load->helper('url');
	}
	
	public function index()
	{
		// $result = $this->db->simple_query("select * from survey_sample");
		// echo $this->db->simple_query("select * from survey_sample");
	
		//Survey
		// $survey = new survey_model();
		// $survey->setGreetingText("本問卷目的欲了解玩家玩線上遊戲時，對遊戲的期望以及加入公會或在公會中的經驗是否會影響玩線上遊戲的樂趣。因此，希望您能撥出一點時間，幫忙填答此問卷，您的資料僅供學術用途，絕不會對外公開，請您安心作答。");
		// $survey->setThankText("在問卷回收完畢後會抽出五位幸運玩家，分別贈送魔獸世界點數卡150點，如欲參加抽獎，也請留下您的e-mail 以便通知。謝謝各位的參與！");
		// $survey->setTitle("線上遊戲玩家的遊戲期望與公會經驗對遊戲忠誠度之影響─以魔獸世界玩家為例。");
		// $survey->setAuthor("姜 研究生");
		// $survey->setVersion(3);
		// $interviewersArray = array("97937bc41c7092874d1543479c2499da","07f4d88ba70704573d2f52c53fc42de4");
		// $survey->setInterviewersArray($interviewersArray);
		// $sampleArray = array("c2caa1d3beeb3a917926b9ade137ef31","9274ff8bc29fabab6d2ee9754766033b","e178fc7cabef1bbe5b09d719ae2bb43b","954e2f6a2e40abdb07066b55f7c5ea8a");
		// $survey->setSampleArray($sampleArray);		
		// $survey->addSurvey($survey);
		// $survey->addInterviewers($survey);
		// $survey->addSamples($survey);
// 		
		// $quest = new questionnaire_model();
		// $quest->setSubject("My First Subject");
		// echo "B";
		// $QType = new qtype_model();
		// echo "3";
		// $quest->setType($QType->getQtype()->qtypeM);
		// // echo $QType->getQtype()->qtypeF;
		// // echo $quest->getType();
		// echo "C";
		// $option = new option_model();
		// echo "D";
		// $option->setId(1);
		// $option->setText("Option 1");
		// $option->setOptionNO(3);
		// $quest->setOption($option);
		// // $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
		// // echo json_encode($arr);
// 		
		//new interviewer_model();
		// $interviewer = new interviewer_model();
		// $interviewer->setName("TestQ2");
		// $interviewer->setEmail("Test@gmail.com");
		// $interviewer->setPwd("abcdef");
		// // $interviewer->addInterviewer($interviewer);
		
		
			// echo "1";
			// foreach ($result->result() as $row) {
				// $sample->setGuid($row->guid);
				// $sample->setName($row->name);
				// $sample->setGender($row->gender);
				// $sample->setAge($row->age);
				// $sample->setAddress($row->address);
				// $sample->setZipCode($row->zipcode);
				// $sample->setMobilePhone($row->mobilephone);	
				// echo "2";
			// }
		
		
		//new sample_model();
		 // $sample = new sample_model();
		// $sample->setName("郭威廷");
		// $sample->setGender(1);
		// $sample->setAge(28);
		// $sample->setAddress("台南市永康區");
		// $sample->setZipCode("710");
		// $sample->setMobilePhone("0982430627");
		//$sample->addSample($sample);
		// $sample->setGuid("33");

		// $sample = $sample->getSample($sample);
		// echo "   Show:".$sample->getSample($sample)->getGuid();
		// echo $sample->getGuid();
		// echo $sample->getName();
		// echo $sample->getAddress();
		
		// $survey = new survey_model();
		 // $survey->getSampleList("302ec78994b35865f4b1r11fc15e9754", "07f4d88ba70704573d2f52c53fc42de4");
		
		// echo "B";
		// $data['subject'] = $quest->getSubject();
		// $data['option'] = $quest->getOption()->getText();
		// $data['header'] = "SubjectView";
		
		// $this->load->view('subject_view',$data);
		
		// $question = new question_model();
		// $question->setGuid("56448f030c87fe1e62580dfb04a808bc");
		// $question->setSubjectNumber("TestA1");
		// $question->setSubject("This is Test A1");
		// $question->setOptionGuid("optionGuid");
		// $question->setType("T");
		// $question->setGroupOptionGuid("groupOptionGuid");
		// $question->addQuestion($question,"5ed97bcd27704b953d3b3b0064a8800c");
		// $question->updateQuestion($question, "5ed97bcd27704b953d3b3b0064a8800c");
		// $question->deleteQuestion($question);
		
		$option = new option_model();
		
		// $option->setSurveyGuid("5ed97bcd27704b953d3b3b0064a8800c");
		// $option->setOptionIndex(1);
		// $option->setTitle("Test3");
		// $option->setOptionValue(1);
		// $option->setIsShowTextView(0);
		// $option->setTextViewPosition("back");
		// $option->addOption($option);
		

		$option->setGuid("4e15f6e8ab6ed1671cedc652e5ab0c9f");
		$optionArray = $option->getOptionArray($option);
		// $optionArray[0]->setTitle("Test2");
		$optionArray[0]->setTitle("Test1");
		$optionArray[0]->setTextViewPosition("");
		$option->updateOption($optionArray[0]);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
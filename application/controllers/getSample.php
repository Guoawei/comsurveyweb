<?php
    /**
     * 
     */
    class getSample extends CI_Controller {
        
        function getSample() {
            parent::__construct();
			$this->load->model("interviewer_model");
			$this->load->model("survey_model");		
			$this->load->model("sample_model");	
        }
		
		function index()
		{	
			//取得參數
			$surveyGuid =  $this->input->get_post('surveyguid',true);
			$interviewerGuid = $this->input->get_post('interviewerguid',true);
			//20131117增加取得單一樣本資訊的參數
			$sampleGuid = $this->input->get_post('sampleguid',true);
			
			//判別是否是取得單一 sample 資訊
			if(!$sampleGuid)
			{
				//此為取得該訪員在該問卷的所有樣本
				$interviewer = new interviewer_model();
				$survey = new survey_model();
				
				$interviewer->setGuid($surveyGuid);
	
				//從DB撈資料
				$data = $survey->getSamples($surveyGuid, $interviewerGuid);
				if (count($data)>0) {
					
					//樣本人數
					$sampleCount = count($data);
					//集合成Array
					for ($i=0; $i < $sampleCount; $i++) { 
						$loadSample = new sample_model();
						$loadSample = $data[$i];
						// echo $loadSample->getAddress();
						$sampleDataA = array("SampleGuid"=>$loadSample->getGuid(),"Name"=>$loadSample->getName()
											,"Birthday"=>$loadSample->getBirthday(),"ZipCode"=>$loadSample->getZipCode()
											,"Address"=>$loadSample->getAddress(),"Phone"=>$loadSample->getMobilePhone()
	                                        ,"MainPriority"=>$loadSample->getMainPriority()
	                                        ,"SubPriority"=>$loadSample->getSubPriority()
											,"Locked"=>$loadSample->getLocked());
						$sampleData[] = $sampleDataA;  
					}
					
						//Output JSON
						$this->output
			    			 ->set_content_type('application/json')
			    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
			    			 												  'Doc' => '讀取正常'),
															'Body' => array('getSample' => array(
																			'SurveyGuid' => $surveyGuid,
																			'SampleRows' => $sampleCount,
																			'Sample' => $sampleData
																			 )))));	
									
				} else {
						//帳號密碼錯誤
						//Output JSON
						$this->output
		    			 ->set_content_type('application/json')
		    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
		    			 												  'Doc' => '讀取失敗'),
														'Body' => NULL
																		 )));	
				}
			}
			else 
			{
				//為取得單一樣本資訊的方法
				$sample = new sample_model();
				$sample->setGuid($sampleGuid);
				$sample = $sample->getSample($sample);
				//Output JSON
				$this->output
    			 ->set_content_type('application/json')
    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
    			 												  'Doc' => '讀取正常'),
												'Body' => array('getSample' => array("SampleGuid"=>$sample->getGuid(),"Name"=>$sample->getName()
																					,"Birthday"=>$sample->getBirthday(),"ZipCode"=>$sample->getZipCode()
																					,"Address"=>$sample->getAddress(),"Phone"=>$sample->getMobilePhone()
											                                        ,"MainPriority"=>$sample->getMainPriority()
											                                        ,"SubPriority"=>$sample->getSubPriority()
																					,"Locked"=>$sample->getLocked())))));	
			}
			
		}
    }
    
?>
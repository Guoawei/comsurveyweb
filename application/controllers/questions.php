<?php
	/**
	 * 
	 */
	class questions extends CI_Controller {
		
		private $limit = 50;
		
		
        function samples() {
            parent::__construct();
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->helper('form');
			$this->load->model('question_model');
		}
		
		function index()
		{
			$this->sampleList();
		}
		
		function doCheckIsLogin()
		{			
			return $this->session->userdata('admin_logged_in');
		}
		
		function doSetSessionPageRecord($pageRecord)
		{
			$pageData = array(
				'pageRecord' => $pageRecord
			);
			$this->session->set_userdata($pageData);
		}
		
		function doGetSessionPageRecord()
		{
			return $this->session->userdata('pageRecord') ;
		}
		
		public function questionList()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
		}

		public function view($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
		}
		
		public function add()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
						
		}
		
		public function addQuestion()
		{
			
		}
		
		public function update($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
		}
		
		public function updateSample()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			
		}
		
		public function delete($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
	
		}
				

}
		
?>
<?php
    /**
     * 
     */
    class getQuestion extends CI_Controller {
        
        function getQuestion() {
            parent::__construct();
			$this->load->model("question_model");
        }
		
		function index()
		{
			
			$question = new question_model();
			
			//取得參數
			$surveyGuid =  $this->input->get_post('surveyguid',true);
			// $interviewerGuid = $this->input->get_post('interviewerguid',true);
			// $interviewer->setGuid($surveyGuid);

			//從DB撈資料
			$data = $question->getQuestionListBySurveyGuid($surveyGuid);
			if (count($data)>0) {
				$metaData = array();
				
				//問卷題目數量
				$questionCount = count($data);
				//集合成Array
				for ($i=0; $i < $questionCount ; $i++) {
					//getQuestion 
					$question->setGuid($data[$i]);
					$question = $question->getSingleQuestion($question);
					
					//getOption
					$optionData = NULL;
					$option = new option_model();
					$option->setGuid($question->getOptionGuid());
					
					//getPass
					$singlePassObj = NULL;
					$groupPassObj = NULL;
					if ($question->getPassGuid()!=NULL) {
						$pass = new pass_model();
						$pass->setGuid($question->getPassGuid());
						$pass = $pass->getPass($pass);

						$singlePassObj = array("answer" => $pass->getAnswerArray(), 
												  "jumpTo" => $pass->getJumpToArray(),//給單一跳題，且每個選項有獨立的跳題邏輯用的 by Awei 2013/04/13
//                                                    "jumpTo" => $pass->getJumpTo(),
												  "select" => $pass->getSelect()); 
					}
					
					if ($question->getGroupPassGuid()!=NULL) {
						$pass = new pass_model();
						$pass->setGuid($question->getGroupPassGuid());
						$pass = $pass->getPass($pass);

												
						$groupPassObj = array("subject" => $pass->getGroupSubjectArray(),
												  "answer" => $pass->getAnswerArray(), 
												  "jumpTo" => $pass->getJumpTo());
					}
					

					
					//getTips
					$tipData = NULL;
					if ($question->getTipsGuid()!=NULL){
						$tips = new tips_model();
						$tips->setTipsGuid($question->getTipsGuid());
						// $tipsArray = array(); 
						$tipsArray = $tips->getTipsArray($tips);
						if (count($tipsArray)>0) {
							for ($j=0; $j < count($tipsArray); $j++) {
								 $tempTips = new tips_model(); 							
								 $tempTips = $tipsArray[$j];
								 $tipsDataA = array("tips_index" => $tempTips->getTipsIndex(),
												   "tips_text" => $tempTips->getTipsText(),
												   "tips_image" => $tempTips->getTipsImage());
								 $tipData[] = $tipsDataA;						
							}						
						} else {
							$tipData = NULL;
						}
					}
					
					//Generate Array
					$questionDataA = array("subject_index" => $question->getSubjectNumber()
									      ,"subject_type" => $question->getType()
										  ,"subject_title" => $question->getSubject()
										  ,"subject_option" => $question->getOptionGuid()
										  ,"subject_groupOption" => $question->getGroupOptionGuid()
										  ,"subject_multiSelectedLimit" => $question->getMultiSelectedLimit()
										  ,"subject_isRandom" => $question->getIsRandom()
										  ,"subject_optionOutputForm" => $question->getOptionOutputForm() 
										  ,"subject_singlePass" => $singlePassObj
										  ,"subject_groupPass" => $groupPassObj
										  ,"subject_tips" => $tipData
										  ,"subject_dynamicOption" => $question->getDynamicOptionGuid()
										  ,"subject_dynamicOptionAnswer" => $question->getDynamicOptionAnswerGuid()
										  );
					$questionData[] = $questionDataA;  
				}
				
				//$tipsData = array("1","2");
				
				
					//Decode Json
					$decodeJson = json_encode(array('Header' => array('Status' => "0",
		    			 											  'Doc' => '讀取正常'),
													'Body' => array('getQuestion' => array("Subject" => $questionData,
																	)
													)));
				
					//Output JSON
					$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output($decodeJson);
		    		 
					 // $file = "./question/".$surveyGuid.".json";
					// //using the FILE_APPEND flag to append the content.
					 // $fd = fopen($file, "a"); // a for append, append text to file
// 
					    // fwrite($fd, $decodeJson);
					    // fclose($fd);	
// 													
													
								
			} else {
					//帳號密碼錯誤
					//Output JSON
					$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
			}
			
		}
    }
    
?>
<?php
	/**
	 * 
	 */
	class samples extends CI_Controller {
		
		private $limit = 50;
		private $title = "科技部傳播調查資料庫";
		
        function samples() {
            parent::__construct();
			$this->load->library('session');
			$this->load->model("survey_model");
			// $this->load->model("sample_model");
			$this->load->model("samplelogic_model");
			$this->load->model("interviewer_model");
			$this->load->helper('url');
			$this->load->helper('form');
		
		}
		
		function index()
		{
			$this->sampleList();
		}
		
		function doCheckIsLogin()
		{			
			return $this->session->userdata('admin_logged_in');
		}
		
		function doGetUserName()
		{			
			return $this->session->userdata('username');
		}
		
		function doSetSessionSurveyGuid($surveyGuid)
		{
			$data = array(
				'surveyGuid' => $surveyGuid
			);
			$this->session->set_userdata($data);
		}
		function doGetSessionSurveyGuid()
		{
			return $this->session->userdata('surveyGuid') ;
		}
		
		function doSetSessionPageRecord($pageRecord)
		{
			$pageData = array(
				'pageRecord' => $pageRecord
			);
			$this->session->set_userdata($pageData);
		}
		
		function doGetSessionPageRecord()
		{
			return $this->session->userdata('pageRecord') ;
		}
		
		public function sampleList()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}

            $selectAllCheckBoxData = array(
                'name'        => 'selectAll',
                'id'          => 'selectAll',
                'value'       => 1,
                'checked'     => FALSE
            );
            $selecAllJs = 'onClick="some_function()"';
			$this->load->library('pagination');
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading(form_checkbox($selectAllCheckBoxData),'NO','編輯','姓名','性別','生日','訪員','功能');
			
			// offset
        	$uri_segment = 4;
        	$offset = $this->uri->segment($uri_segment);
			$pageRecord = $this->uri->segment(4);
			if($pageRecord == NULL){
				$pageRecord = 0;
			}
			$this->doSetSessionPageRecord($pageRecord);

			//load Data
			$sampleModel = new sample_model();
            $countAll = $sampleModel->getCountAll($this->doGetSessionSurveyGuid());
			$sampleArray = $sampleModel->getSampleWithPage($this->doGetSessionSurveyGuid(),$this->limit, $offset)->result();

			$i = 0 + $offset;
			
			//load data interviewer
			$sampleLogicModel = new samplelogic_model();
			
			foreach ($sampleArray as $row) {
				$tempSample = new sample_model();
				$tempSample = $row;
		
				$interviewer = new interviewer_model();
				$interviewer->setGuid($sampleLogicModel->getWhoInterviewSetOnSample($row->guid));				
				$interviewer = $interviewer->getInterviewerByGuid($interviewer);

                $checkData = array(
                    'name'        => 'sampleSelected[]',
                    'id'          => 'sampleSelected[]',
                    'value'       => $row->guid,
                    'checked'     => FALSE
                );
				$this->table->add_row(
                array('data'=>form_checkbox($checkData),'style'=>'width:1%;'),
				array('data'=>++$i,'style'=>'width:3%;'),
				anchor('samples/update/'.$row->guid,'編輯',array('class'=>'edit'))
				,array('data'=>$row->name,'style'=>'width:110px;')
				,$row->gender?'男':'女',$row->birthday
				,$interviewer->getName()
				,anchor('samples/delete/'.$row->guid,'刪除',array('class'=>'delete','onclick'=>"return confirm('確定刪除此樣本？')")));
			}
			
			$submitData = array(
			    'name'        => "mysubmit",
			    'value'       => "刪除樣本",
			    'onClick'     => "return confirm('確定刪除選擇的樣本?');"
			);
			
			//分頁功能
			$uri_segment = 4;
			
			$config['base_url'] = site_url('samples/sampleList/index/');
			$config['total_rows'] = $countAll;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;
			$config['num_links'] = 7;
			$config['first_link'] = '[第1頁]';
			$config['last_link'] = '[最後頁]';
			$config['next_link'] = '下一頁 >';
			$config['prev_link'] = '< 上一頁';			
			$this->pagination->initialize($config);
			
			//目前的調查問卷
			$surveyObject = new survey_model();
			$surveyObject->setGuid($this->doGetSessionSurveyGuid());
			$surveyObject = $surveyObject->getSurvey($surveyObject);
			
			$data['pagination'] = $this->pagination->create_links();
			$data['form'] = form_open('samples/deleteSelectedSample');
			$data['formSubmit'] = form_submit($submitData);

			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['headerTitle'] = $surveyObject->getTitle();
			$data['surveyGuid'] = $this->doGetSessionSurveyGuid();
			$data['username'] = $this->doGetUserName();
			$data['add'] = anchor("samples/add/", '<img src="'.base_url().'/style/images/addSample.png" alt="Delete" />');
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            $data['toSampleGroupView'] = anchor("samplegroups/sampleGroupList/", '<img src="'.base_url().'/style/images/sampleGroup.png" alt="Delete" />')
                ."   ".anchor("samples/sampleList/", '<img src="'.base_url().'/style/images/inSample.png" alt="Delete" />')
                ."   ".anchor("samplegroups/add/", '<img src="'.base_url().'/style/images/addSampleGroup.png" alt="Delete" />');
            // $this->load->view('sample_list_view', $data);
			$this->load->view('survey_detail_view', $data);
		}

		public function view($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			$pageRecord = $this->doGetSessionPageRecord();
			$sampleModel = new sample_model();
			$sampleObject = new sample_model();
			$sampleObject->setGuid($guid);
			$data['sample'] = $sampleModel->getSample($sampleObject);
			$data['title'] = $this->title;
			$data['headerTitle'] = "樣本資料";
			$data['link_back'] = anchor('samples/sampleList/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('sample_view', $data);
		}
		
		public function add()
		{
            $pageRecord = $this->doGetSessionPageRecord();
			$sampleModel = new sample_model();
			$sampleObject = new sample_model();
			$data['sample'] = $sampleObject;
			$data['title'] = $this->title;
			$data['headerTitle'] = "新增樣本";
			$data['message']="";
			$data['action'] = site_url('samples/addSample');
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('samples/sampleList/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('sample_edit', $data);
		}
		
		public function addSample()
		{
            $pageRecord = $this->doGetSessionPageRecord();
            $name = $this->input->post('name');
            if(trim($name)==''){
                $addResult = "請輸入樣本名稱";
            }else {
                $sampleModel = new sample_model();
                $sampleObject = new sample_model();
                $sampleObject->setGuid($this->input->post('guid'));
                $sampleObject->setSurveyGuid($this->doGetSessionSurveyGuid());
                $sampleObject->setName($this->input->post('name'));
                $sampleObject->setGender($this->input->post('gender'));
                $sampleObject->setBirthday($this->input->post('birthday'));
                $sampleObject->setZipCode($this->input->post('zipcode'));
                $sampleObject->setAddress($this->input->post('address'));
                $sampleObject->setMainPriority($this->input->post('mainpriority'));
                $sampleObject->setSubPriority($this->input->post('subpriority'));
                if($sampleModel->addSample($sampleObject)){
                    $addResult = "新增樣本 ".$sampleObject->getName()." 成功";
                }else {
                    $addResult = "新增樣本 ".$sampleObject->getName()." 失敗,有重覆的樣本名稱";
                }
            }

			$newSample = new sample_model();
			$data['sample'] = $newSample;
			$data['title'] = $this->title;
			$data['headerTitle'] = "新增樣本";
			$data['message']= $addResult;
			$data['action'] = site_url('samples/addSample');
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('samples/sampleList/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('sample_edit', $data);
		}
		
		public function update($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
			$pageRecord = $this->doGetSessionPageRecord();
			$sampleModel = new sample_model();
			$sampleObject = new sample_model();
			$sampleObject->setGuid($guid);
			$data['sample'] = $sampleModel->getSample($sampleObject);
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "編輯樣本";
			$data['message']="";
			$data['action'] = site_url('samples/updateSample');
			$data['link_back'] = anchor('samples/sampleList/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('sample_edit', $data);
		}
		
		public function updateSample()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			$pageRecord = $this->doGetSessionPageRecord();

            $sampleModel = new sample_model();
            $sampleObject = new sample_model();
            $sampleObject->setGuid($this->input->post('guid'));
            $sampleObject->setSurveyGuid($this->doGetSessionSurveyGuid());
            $sampleObject->setName($this->input->post('name'));
            $sampleObject->setGender($this->input->post('gender'));
            $sampleObject->setBirthday($this->input->post('birthday'));
            $sampleObject->setZipCode($this->input->post('zipcode'));
            $sampleObject->setAddress($this->input->post('address'));
            $sampleObject->setMainPriority($this->input->post('mainpriority'));
            $sampleObject->setSubPriority($this->input->post('subpriority'));

            $name = $this->input->post('name');
            if(trim($name)==''){
                $updateResult = "請輸入樣本名稱";
            }else {

                if($sampleModel->updateSample($sampleObject)){
                    $updateResult = "更新樣本 ".$sampleObject->getName()." 成功";
                }else {
                    $updateResult = "更新樣本 ".$sampleObject->getName()." 失敗,有重覆的樣本名稱";
                }
            }
			$data['message'] = $updateResult;
            $data['sample'] = $sampleModel->getSample($sampleObject);
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "編輯樣本";
			$data['action'] = site_url('samples/updateSample');
			$data['link_back'] = anchor('samples/sampleList/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('sample_edit', $data);
//            redirect('samples/sampleList/index/'.$pageRecord,'refresh');
		}
		
		public function delete($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			$pageRecord = $this->doGetSessionPageRecord();
			$sampleModel = new sample_model();
			$sampleLogicModel = new samplelogic_model();
			$sampleObject = new sample_model();
			$sampleObject->setGuid($guid);
			$sampleModel->deleteSample($sampleObject);
			$sampleLogicModel->deleteSampleGuid($sampleObject->getGuid());
			
			redirect('samples/index/'.$pageRecord,'refresh');	
		}
				
		public function deleteSelectedSample()
		{
			$confirmDataArray = array();
			$pageRecord = $this->doGetSessionPageRecord();
			
			$sampleObject = new sample_model();
			$sampleModel = new sample_model();
			$sampleLogicModel = new samplelogic_model();
			
			$sampleChkbox = $this->input->post('sampleSelected');
			if(!empty($sampleChkbox)){
                    for (reset($sampleChkbox); $k=key($sampleChkbox); next($sampleChkbox)){
                        $sampleArray[] = $sampleChkbox[$k];
                    }
								
				//Confirm View Data
				foreach ($sampleChkbox as $row) {
					$sampleObject = new sample_model();
					$sampleObject->setGuid($row);
					$confirmDataArray[] = $sampleModel->getSample($sampleObject);
					$sampleModel->deleteSample($sampleObject);
					$sampleLogicModel->deleteSampleGuid($sampleObject->getGuid());
				}
				$message = '刪除了'.count($sampleChkbox).'樣本';
			}else {
				$message = '沒選擇任何樣本';
			}		
			
			$data['message']=$message;
			// $data['interviewer'] = $interviewerObject;
			$data['confirmDataArray'] = $confirmDataArray;			
			$data['title'] = $this->title;
			$data['headerTitle'] = "刪除樣本名單";
			// $data['doDeleteAction'] = anchor('samples/doDeleteSelectedSample','回到列表',array('class'=>'back'));
			$hidden = array('confirmDataArray'=>$confirmDataArray);			
			$data['link_back'] = anchor('samples/index/'.$pageRecord,'回到列表',array('class'=>'back'));

            $pageRecord = $this->doGetSessionPageRecord();
            redirect('samples/index/'.$pageRecord,'refresh');
		}

		public function doDeleteSelectedSample($dataArray)
		{
			$confirmDataArray = $dataArray;
			// $confirmDataArray = $this->input->post('confirmDataArray');
			$pageRecord = $this->doGetSessionPageRecord();
			
			$sampleModel = new sample_model();
			
			foreach ($confirmDataArray as $row) {
				$sampleObject = new sample_model();
				$sampleObject = $row;
				$sampleModel->deleteSample($sampleObject);
			}
			
			redirect('samples/index/'.$pageRecord,'refresh');
			
		}
        
        public function sampleImport()
        {
        	//目前的調查問卷
			
            $surveyObject = new survey_model();
            $surveyObject->setGuid($this->doGetSessionSurveyGuid());
            $surveyObject = $surveyObject->getSurvey($surveyObject);
            if($surveyObject)
            {
                $data['message'] = '';
                $data['title'] = $this->title;
                $data['headerTitle'] = "匯入訪員與樣本資料至 ".$surveyObject->getTitle(). " 問卷";
                $data['username'] = $this->doGetUserName();
                $data['link_back'] = anchor('samples/sampleList/index/'.$this->doGetSessionPageRecord(),'前往樣本列表',array('class'=>'back'));
                $this->load->view('sample_import', $data);
            }
            else{
                echo "yoo~~404 無此問卷";
            }
        }
        
        public function doSampleImport()
        {
            $this->load->model("interviewer_logic_model");
            $this->load->model("samplegroup_model");
			$this->load->model("sample_model");
           
            if(file_exists($this->input->post('file')))
            {
                //問卷id
                $surveyGuid = $this->doGetSessionSurveyGuid();
                $this->load->library('csvreader');
                $csv = $this->csvreader->parse_file($this->input->post('file'));
                //取得所有訪員與分組名稱
                $interVeiwers = array();
                foreach ($csv as $row)
                {
                    //處理空白
                    foreach ($row as $key => $value) {
                        //不是密碼才處理
                        if($key!='interViewerPwd')
                            $row[$key] = mb_convert_encoding(str_replace(array(" ",'.'),array('','_'),trim($value)),"UTF-8", "BIG5");
                            //$row[$key] = str_replace(array(" ",'.'),array('','_'),trim($value));
                    }
                    if(!in_array($row['interViewerName'],$interVeiwers))
                    {
                        //不再已有的訪員中
                        $data['interViewer'][$row['interViewerAcc']]['name']=$row['interViewerName'];
                        $data['interViewer'][$row['interViewerAcc']]['email']=$row['interViewerAcc'];
                        $data['interViewer'][$row['interViewerAcc']]['pwd']=$row['interViewerPwd'];
                        $data['interViewer'][$row['interViewerAcc']]['sampleGroupName']=$row['sampleGroupName'];
                        $data['interViewer'][$row['interViewerAcc']]['samples'][] = array(
                            'name'=>($row['sampleName']=='')?NULL:$row['sampleName'],
                            'gender'=>($row['gender']=='')?NULL:$row['gender'],
                            'birthday'=>($row['birthday']=='')?NULL:$row['birthday'],
                            'address'=>($row['address']=='')?NULL:$row['address'],
                            'zipcode'=>($row['zipcode']=='')?NULL:$row['zipcode'],
                            'mainpriority'=>($row['mainpriority']=='')?0:$row['mainpriority'],
                            'subpriority'=>($row['subpriority']=='')?0:$row['subpriority']
                        );
                    }
                    else
                    {
                        //一樣的訪員，繼續加入
                        $data['interViewer'][$row['interViewerAcc']]['samples'][] = array(
                            'name'=>($row['sampleName']=='')?NULL:$row['sampleName'],
                            'gender'=>($row['gender']=='')?NULL:$row['gender'],
                            'birthday'=>($row['birthday']=='')?NULL:$row['birthday'],
                            'address'=>($row['address']=='')?NULL:$row['address'],
                            'zipcode'=>($row['zipcode']=='')?NULL:$row['zipcode'],
                            'mainpriority'=>($row['mainpriority']=='')?0:$row['mainpriority'],
                            'subpriority'=>($row['subpriority']=='')?0:$row['subpriority']
                        );
                    }
                }
                
                //初始化訊息
                $json['msg'] = "共".count($data['interViewer'])."位訪員，".count($csv)."筆樣本。";
                //根據所有訪員取進行資料輸入
                //初始化成功或失敗
                $ok = array();
				$no = array();
				
                foreach ($data['interViewer'] as $row)
                {
                    usleep(10000);
                    //先新增訪員
                    $interviewer = new interviewer_model();
                    $interviewer->setName($row['name']);
                    $interviewer->setEmail($row['email']);
                    $interviewer->setPwd($row['pwd']);
					//判別該訪員是否已經存在
					if($interviewer->exists_user())
					{
						//訪員已經存在只取得訪員資料
						$interviewer = $interviewer->getInterviewerByEmail($interviewer);
						//判別是否已經在該問卷中
						$hasSurveys = $this->interviewer_logic_model->getInterviewerOwnSurveyGuid($interviewer);
						$hasSurveysGuid = array();
						//將問卷guid取出
						foreach ($hasSurveys as $key => $value) {
							$hasSurveysGuid[] = $value->getGuid();
						}
						if(!in_array($surveyGuid,$hasSurveysGuid))
						{
							//如果不在此問卷，才新增關連。
							$this->interviewer_logic_model->addInterviewerSurveyGuid($interviewer,$surveyGuid);
						}
					}
					else
					{
						//無該訪員就新增
	                    $this->interviewer_model->addInterviewer($interviewer);
						//新增訪員關連
						$this->interviewer_logic_model->addInterviewerSurveyGuid($interviewer,$surveyGuid);
					}
					//新增該訪員的樣本後，再加入分群中
                    //清除要加入的樣本
                    $selectSampleArray = array();
                 	foreach ($row['samples'] as $key => $a_sample) {
                        usleep(10000);
                        $sampleModel = new sample_model();
                        $sampleModel->setSurveyGuid($surveyGuid);
                        $sampleModel->setName($a_sample['name']);
                        $sampleModel->setGender($a_sample['gender']);
                        $sampleModel->setBirthday($a_sample['birthday']);
                        $sampleModel->setZipCode($a_sample['zipcode']);
                        $sampleModel->setAddress($a_sample['address']);
                        $sampleModel->setMainPriority($a_sample['mainpriority']);
                        $sampleModel->setSubPriority($a_sample['subpriority']);
                        if($this->sample_model->addSample($sampleModel)){
                            $ok[] = $a_sample;
							//要加入分群的樣本們    
                        	$selectSampleArray[] = $sampleModel->getGuid();
                        }else {
                        	$no[] = array(
                        		'interviewer'=>$row['name']."(".$row['email'].") 的 {$row['sampleGroupName']} 分群。",
                        		'sample'=>$a_sample
							);
                        }
                    }
                    
                    //將所有沒問題的樣本加入樣本分群中
                    if(count($ok)>0)
					{
						//如果該分群已經存在該問卷，就只更新分群中的樣本
						$this->db->select('survey_samplegroup_data.*');
						$this->db->join('survey_samplegroup_data','survey_samplegroup_data.sampleGroupGuid = survey_survey_own_sample.groupSampleGuid','left');
						$query = $this->db->get_where('survey_survey_own_sample',array(
							'survey_samplegroup_data.sampleGroupName' => $row['sampleGroupName'],
							'survey_survey_own_sample.surveyGuid' => $surveyGuid,
							'survey_survey_own_sample.interviewerGuid' => $interviewer->getGuid()
						));
						if($query->num_rows() == 1)
						{
							$samplegroup_data = $query->row_array();
							//更新分群中的樣本
							foreach ($selectSampleArray as $key => $value) {
								$sql = "SELECT guid from survey_samplegroup where guid = '".$samplegroup_data['sampleGroupGuid']."' and sampleGuid = '".$value."'";
				                $result = $this->db->query($sql);
				                if ($result->num_rows()==0) {
				                    if($value!=""){
				                        $sql2 = "INSERT INTO survey_samplegroup (guid, sampleGuid) VALUES ('".$samplegroup_data['sampleGroupGuid']."', '".$value."')";
				                        $this->db->query($sql2);
				                    }
				                }
							}
						}
						else
						{
						    usleep(10000);
							//沒有存在就新增
							$sampleGroupObject = new samplegroup_model();
		                    $sampleGroupObject->setSampleGroupName($row['sampleGroupName']);
		                    $sampleGroupObject->setSampleArray($selectSampleArray);
							
		                    $this->samplegroup_model->addSampleToGroup($sampleGroupObject);
							//並指定分群給訪員
		                    $this->interviewer_logic_model->addSampleToInterviewer($interviewer,$surveyGuid,$sampleGroupObject->getSampleGroupGuid());
						}
					}
                }
               
                $json['result'] = TRUE;
            	$json['msg'] .= "成功匯入".count($ok)."筆樣本";
				if(count($no) > 0)
				{
					$json['msg'].="，但有".count($no)."筆樣本失敗。失敗樣本如下：<br/>";
					$json['msg'].="<ul>";
					foreach ($no as $row) {
						$json['msg'].="<li class='red'>{$row['interviewer']} {$row['sample']['name']} 已存在此樣本，無法儲存，請更改樣本名稱。</li>";
					}
					$json['msg'].="</ul>";
				}
				else
				{
					$json['msg'].="。";
				}
				
            }
            else
            {
                $json['result'] = FALSE;
                $json['msg'] = '無此檔案：'.$this->input->post('file');
            }
            //輸出json格式
            $this->output->set_content_type('application/json')->set_output(json_encode($json));
        }

	public function lists($s_guid = null)
	{
		
		if($s_guid)
		{
			$this->db->order_by('survey_sample.mainpriority ASC, survey_sample.subpriority ASC');
			$this->db->select('survey_sample.*');
			$this->db->join('survey_sample','survey_sample.guid = survey_answer_' . $s_guid . '_1.sampleGuid','left');
			$this->db->where('survey_sample.surveyGuid', $s_guid);
			$query = $this->db->get('survey_answer_' . $s_guid . '_1');
			
			/*
			$this->db->order_by('survey_sample.mainpriority ASC, survey_sample.subpriority ASC');
			$this->db->select('survey_sample.*');
			$this->db->join('survey_samplegroup','survey_samplegroup.sampleGuid = survey_sample.guid','left');
			$this->db->join('survey_survey_own_sample','survey_survey_own_sample.groupSampleGuid = survey_samplegroup.guid','left');
			
			$this->db->where('survey_survey_own_sample.surveyGuid', $s_guid);
			$query = $this->db->get('survey_sample');
			 * 
			 */
			//echo $this->db->last_query();
			echo "共（" . $query->num_rows() . "）筆";
			echo "<style>table th{background-color:#333;color:#fff;border:solid 1px #aaa;} table td{border:solid 1px #aaa;}</style>";
			echo "<table><tr><th></th><th>姓名</th><th>住址</th></tr>";
			foreach ($query->result_array() as $key => $value) {
				//echo mb_strpos($value['name'], "測試") ."<br>";
				if(!(mb_strpos($value['name'], "測試")))
				{
					echo "<tr><td>" . ($key+1) . "</td><td>{$value['name']}</td><td>{$value['address']}</td></tr>";
				}
			}
			echo "</table>";
		}
	}
}
		
?>
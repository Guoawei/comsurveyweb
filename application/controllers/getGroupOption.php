<?php
    /**
     * 
     */
    class getGroupOption extends CI_Controller {
        
        function getGroupOption() {
            parent::__construct();
			$this->load->model('groupoption_model');
        }
		
		function index()
		{
			$groupOption = new groupoption_model();
			
			//取得參數
			$groupOptionGuid =  $this->input->get_post('groupoptionguid',true);
			
			$groupOption->setGuid($groupOptionGuid);
			
			$groupOptionArray = $groupOption->getGroupOptionList($groupOption);
			
			if (count($groupOptionArray)>0) {
				//集合成Array
				for ($i=0; $i < count($groupOptionArray); $i++) { 
					$gOption = new groupoption_model();
					$gOption = $groupOptionArray[$i];
					$gOptionDataA = array('id' => $gOption->getId(),
										   'type' =>$gOption->getType(),
										   'index'=>$gOption->getIndex(),
										   'text'=>$gOption->getText(),
										   'value'=>$gOption->getValue(),
										   'isShowTextView'=>$gOption->getIsShowTextView(),
										   'textViewPosition'=>$gOption->getTextViewPosition(),
                                           'optionTips'=>$gOption->getOptionTips());
					$gOptionData[] = $gOptionDataA;  
				}
				
				//Output JSON
				$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output(json_encode(array('Header' => array('Status' => "0",
		    			 												  'Doc' => '讀取正常'),
														'Body' => array('getGroupOption' => $gOptionData
																		 ))));	
				
			} else {
				//帳號密碼錯誤
				//Output JSON
				$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
			}
		}
    }
    
?>
<?php
    /**
     * 
     */
    class uploadSurveySituation extends CI_Controller {
        
        function uploadSurveySituation() {
            parent::__construct();
			$this->load->model("sample_model");
			$this->load->model("surveysituation_model");
        }
		
		function unicodToUTF8($chrCode) { 
		    if(!is_integer($chrCode))return $chrCode; 
		    elseif($chrCode<0x80){ // 單一字元 [0xxxxxxx] 
		        return chr($chrCode); 
		    }elseif($chrCode>=0x80 && $chrCode<=0x07ff){        // 雙字元 [110xxxxx][10xxxxxx] 
		        $bin = sprintf('%011s', decbin($chrCode)); 
		        $chrs = chr(intVal('110'.substr($bin, 0, 5), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 5), 2)); 
		    }elseif($chrCode>=0x800 && $chrCode<=0xFFFF){        // 三字元 [1110xxxx][10xxxxxx][10xxxxxx] 
		        $bin = sprintf('%016s', decbin($chrCode)); 
		        $chrs = chr(intVal('1110'.substr($bin, 0, 4), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 4, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 10), 2)); 
		    }elseif($chrCode>=0x10000 && $chrCode<=0x1FFFFF){     // 四字元 [11110xxx][10xxxxxx][10xxxxxx][10xxxxxx] 
		        $bin = sprintf('%021s', decbin($chrCode)); 
		        $chrs = chr(intVal('11110'.substr($bin, 0, 3), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 3, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 9, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 15), 2)); 
		    }elseif($chrCode>=0x200000 && $chrCode<=0x3FFFFFF){    // 五字元 [111110xx][10xxxxxx][10xxxxxx][10xxxxxx][10xxxxxx] 
		        $bin = sprintf('%026s', decbin($chrCode)); 
		        $chrs = chr(intVal('111110'.substr($bin, 0, 2), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 2, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 8, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 14, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 20), 2)); 
		    }elseif($chrCode>=0x4000000 && $chrCode<=0x7FFFFFFF){    // 六字元 [1111110x][10xxxxxx][10xxxxxx][10xxxxxx][10xxxxxx][10xxxxxx] 
		        $bin = sprintf('%031s', decbin($chrCode)); 
		        $chrs = chr(intVal('1111110'.substr($bin, 0, 1), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 1, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 7, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 13, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 19, 6), 2)); 
		        $chrs.= chr(intVal('10'.substr($bin, 25), 2)); 
		    }else{ // 錯誤處理 
		        return "{[U?$chrCode]}"; 
		    } 
		    return $chrs; 

		} 
		
		function unichr($u) {
		    return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
		}
		
		function index()
		{
			$printCode = 0;
			$printCode = $this->input->get_post('test',true); 	 
			
			if ($printCode == 1) {
				//Test Json
				$sampleGuid = "116100311";
				$interviewerGuid = "334cd1a5189c06320b718c2d43c40f19";
				$surveyGuid = "3fba9a60f401e35406c2d78340484caa";
				$situationCode = 2;
				$cDateTime = "2012-06-15 01:30:28";
				
				$situationArray = array("SituationCode"=>"1","Comment"=>"","CDateTime"=>"2012-06-14 13:32:46");
				$situationArray2 = array("SituationCode"=>"98","Comment"=>"說明用測試1","CDateTime"=>"2012-06-15 01:30:28");
				$situationArray3 = array("SituationCode"=>"22","Comment"=>"","CDateTime"=>"2012-06-16 01:30:28");
				$situationList = array($situationArray,$situationArray2,$situationArray3);
				// $timeList = array('SubjectNumber'=>$subjectNumber,'InputAnswer'=>$inputAnswer);
				$json = json_encode(array('SampleGuid' => $sampleGuid,
			    		 				  'InterviewerGuid' => $interviewerGuid,
			    		 				  'SurveyGuid' => $surveyGuid,			
										  'SituationList'=> $situationList,
														));
			
				//Output JSON
				$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output(json_encode(array('SampleGuid' => $sampleGuid,
			    		 				  'InterviewerGuid' => $interviewerGuid,
			    		 				  'SurveyGuid' => $surveyGuid,			
										  'SituationList'=> $situationList,
														)));
														
														
				$sit = new surveysituation_model();
				$sit->setSurveyGuid('3fba9a60f401e35406c2d78340484caa');
				$sit->setInterviewerGuid('a46853d6fe01b79be4aa232c45123a96');
				$sit->setSampleGuid('110103256');
				$sitList = $sit->getSurveySituation($sit);
				foreach ($sitList as $row) {
					$ss = new surveysituation_model();
					$ss = $row;
					// echo urldecode($ss->getComment());
				}
				
				
			}else {
					// header('Content-Type: text/html; charset=UTF-8');
				    // $json = file_get_contents('./style/myfile.json');
				    
					// $json = $_POST['php://input'];
					 // $json = iconv('UTF-8','Big5', file_get_contents("./files/myfile.json"));
					// $this->load->helper('file');
// 					
					// // You can test for valid json like this:
					// $is_valid_json = json_decode($json) !== NULL;
// 					
					// if ( ! write_file('./style/myfile.json', $json)) {
					    // echo 'Unable to write the file';
					// }
					// else {
					    // echo 'File written!';
					// }
				$json = file_get_contents('php://input');
				if ($json) {
					$data = json_decode($json);
					$sampleGuid = $data->{'SampleGuid'};
					$interviewerGuid = $data->{'InterviewerGuid'};
					$surveyGuid = $data->{'SurveyGuid'};
					$SituationList = $data->{'SituationList'};
					
					// $printCode == 2; 
				}else {
					$printCode = 1;
					echo "No JSON";
				}

			}
			

			if ($printCode == 0) {
				//建立答案資料表
				$surveySituation = new surveysituation_model();
				$surveySituation->setSampleGuid($sampleGuid);
				$surveySituation->setInterviewerGuid($interviewerGuid);
				$surveySituation->setSurveyGuid($surveyGuid);
				$temp ="";
				$count = 1;
				foreach ($SituationList as $row) {
				    $temp = $temp.$row->SituationCode.$count;
					 // $tempComment = stripSlashes($row->Comment);
					// $tempComment = addSlashes($row->Comment);
					// $tempComment = $this->unicodToUTF8($row->Comment);
 
					$surveySituation->setSituationCode($row->SituationCode);
					// $comment = iconv("utf-8","big5",$row->Comment);
					$surveySituation->setComment(urldecode($row->Comment));
					$surveySituation->setCDateTime($row->CDateTime);
					// $surveySituation->setComment(count($SituationList));
					$sample = new sample_model();
					$sample->setGuid($sampleGuid);
					$sample->getSample($sample);
					
					$isAlreadyHave = $surveySituation->checkSurveySituationAlreadyHave($surveySituation);
					// echo $isAlreadyHave;
					if ($isAlreadyHave==0) {

						$surveySituation->addSurveySituation($surveySituation);
						
						//檢查是否上傳成功，並回傳成功、失敗
						$isAlreadyHave = $surveySituation->checkSurveySituationAlreadyHave($surveySituation);
						if ($isAlreadyHave==0) {
							echo $sample->getName().":Failure";	
						}else {
							echo $sample->getName().":Success";	
						}
					}else {
						//回傳已經有記錄
						echo $sample->getName().":Success";	
					}
					$count ++;
				}
			}	

		}

		

    }
    
	
?>
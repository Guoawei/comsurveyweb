<?php
    /**
     * 
     */
    class login extends CI_Controller {
        
        function login() {
            parent::__construct();
			$this->load->model("interviewer_model");
			$this->load->library('session');
        }
		
		function index()
		{
			$interviewer = new interviewer_model();
			
			//取得參數
			$inputEmail =  $this->input->get_post('email',true);
			$inputPwd =  $this->input->get_post('pwd',true);
			
			$interviewerAccount = new account_model();
			$interviewerAccount->setAccount($inputEmail);
			$interviewerAccount->setPassword($inputPwd);
			//從DB撈資料
			$interviewerAccount = $interviewer->user_login($interviewerAccount);
			if ($interviewerAccount->getAccount()!=NULL) {
				//帳號密碼正確
				
				$interviewer->setEmail($inputEmail);
				$interviewer = $interviewer->getInterviewerByEmail($interviewer);
				
				//20131117 將使用者資訊存入session中，給前台使用的
				//寫入訪員的session
				$loginData = array(
               		'username'  => $interviewer->getName(),
               		'account'   => $interviewer->getEmail(),
               		'guid'     => $interviewer->getGuid(),
               	    'interviewer_logged_in' => TRUE
        		);

				$this->session->set_userdata($loginData);
				
				//Output JSON
				$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
	    			 												  'Doc' => '登入成功'),
													'Body' => array('login' => array('Guid' => $interviewer->getGuid(),
																 	'Name' => $interviewer->getName(),
																	'Email' => $interviewer->getEmail()
																	 )))));					
			} else {
				//帳號密碼錯誤
				//Output JSON
				$this->output
    			 ->set_content_type('application/json')
    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
    			 												  'Doc' => '帳號密碼錯誤'),
												'Body' => NULL
																 )));	
			}
			
			
		}
    }
    
?>
<?php
    /**
     * 
     */
    class getSurveyList extends CI_Controller {
        
        function getSurveyList() {
            parent::__construct();
			$this->load->model("interviewer_model");
			$this->load->model("survey_model");
        }
		
		function index()
		{
			$interviewer = new interviewer_model();
			$survey = new survey_model();
			//取得參數
			$surveyGuid =  $this->input->get_post('guid',true);
			$interviewer->setGuid($surveyGuid);
			
			//從DB撈資料
			$data = $interviewer->getSurveyList($interviewer);
			if ($data->num_rows() > 0) {
				$metaData = array();
				foreach ($data->result() as $row) {
					$guid = $row->surveyGuid;
					$survey->setGuid($guid);
					$survey = $survey->getSurvey($survey);
					$metaDataA = array("SurveyGuid"=>$survey->getGuid(),"Title"=>$survey->getTitle()
										,"GreetingText"=>$survey->getGreetingText(),"ThankText"=>$survey->getThankText()
										,"TotalRows"=>$survey->getTotalRows(),"Ver"=>$survey->getVersion()
										,"CDateTime"=>$survey->getCDateTime(),"UDateTime"=>$survey->getUDateTime());
					$metaData[] = $metaDataA;  
				}
					//Output JSON
					$this->output
		    			 ->set_content_type('application/json')
		    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
		    			 												  'Doc' => '讀取正常'),
														'Body' => array('getSurveyList' => array(
																		'MetaData' => $metaData
																		 )))));	
								
			} else {
					//帳號密碼錯誤
					//Output JSON
					$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
			}
			
		}
    }
    
?>
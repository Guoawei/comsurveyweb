<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * front 首頁控制器
 *
 * @package
 * @subpackage
 * @category
 * @author      Will Tsai
 * @link        http://sweea.com
 */

class Front extends CI_Controller
{

    /**
     * __construct 建構子
     * 取得預設所要的相關資料
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'interviewer_model',
            'samplelogic_model',
            'surveyanswer_model',
            'survey_model',
            'front_answer_model'
        ));
        $this->load->library(array(
            'session',
            'table'
        ));
        $this->load->helper(array(
            'security',
            'url',
            'html'
        ));
    }

    public function index()
    {
        //判別是否已經登入
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //是，進入問卷列表頁。
            redirect('front/survey_list', 'reflash');
        }
        else
        {
            //否，進入登入頁
            $this->login();
        }
    }

    /**
     * 登入頁面
     * @param
     * @return html
     */
    public function login()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //有登入就進入管理頁預設的 article_list
            redirect('front/survey_list', 'reflash');
        }
        else
        {
            //如果沒有登入，進入登入頁

            //取得預設值
            $data = null;

            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/header');
            $this->load->view('front/login', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');
        }
    }

    /**
     * survey_list()
     * 問卷列表頁面，根據登入的使用者取得所屬的問卷。
     * @param
     * @return html
     */
    public function survey_list()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //如果有登入，就取得session值
            $session = $this->session->all_userdata();

            //取得登入者資訊
            $data['interviewer'] = array(
                'name'=>$session['username'],
                'account'=>$session['account']
            );
            //echo $session['guid'];
            //取得該訪員的所有問卷
            $url = base_url('getSurveyList?guid=' . $session['guid']);
            $surveys = json_decode(file_get_contents($url));

            //print_r($surveys);

            //設定表格

            $tmpl = array('table_open'=>'<table class="lists">');
            $this->table->set_empty("&nbsp;");
            $this->table->set_template($tmpl);
            $this->table->set_heading('問卷', '填寫人數', '調查人數', '版本', '發佈日期');

            if ($surveys->Header->Status == 0)
            {
                //表讀取成功 0 有問卷
                foreach ($surveys->Body->getSurveyList->MetaData as $row)
                {
                    //取得 填寫人數
                    $surveyAnswerModel = new surveyanswer_model();
                    $surveyAnswerAlreadyUploadCount = $surveyAnswerModel->getAlreadyUploadCount($session['guid'], $row->SurveyGuid, $row->Ver);

                    //取得 調查人數
                    $sampleLogicModel = new samplelogic_model();
                    //取得樣本清單
                    $sampleArray = $sampleLogicModel->getSampleInGroup($row->SurveyGuid, $session['guid']);
                    //總調查數
                    $sampleTotal = count($sampleArray);

                    //print_r($row->SurveyGuid);
                    $this->table->add_row(array(
                        'data'=>anchor("front/sample_list/?sguid=" . $row->SurveyGuid, $row->Title, "class='mask'"),
                        'class'=>''
                    ), array(
                        'data'=>$surveyAnswerAlreadyUploadCount,
                        'class'=>'text-center'
                    ), array(
                        'data'=>$sampleTotal,
                        'class'=>'text-center'
                    ), array(
                        'data'=>$row->Ver,
                        'class'=>'text-center'
                    ), array(
                        'data'=>$row->CDateTime,
                        'class'=>'text-center'
                    ));
                }
            }
            else
            {
                //表讀取失敗 1 沒有問卷
                $this->table->add_row(array(
                    'data'=>'此訪員尚未有問卷',
                    'colspan'=>5
                ));
            }

            //產生表格
            $data['list_table'] = $this->table->generate();
            $this->table->clear();

            //重新給予網頁meta描述
            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/header', $data);
            $this->load->view('front/survey_list', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');

        }
        else
        {
            //否，進入登入頁
            $this->login();
        }
    }

    /**
     * sample_list()
     * 樣本列表頁面，根據登入的使用者取得選取的問卷。
     * @param
     * @return html
     */
    public function sample_list()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //如果有登入，就取得session值
            $session = $this->session->all_userdata();

            //載入 localstorage jquery plugin
            $data['js'] = '<script src="' . base_url("style/front/js/jquery.storageapi.min.js") . '"></script>';

            //取得登入者資訊
            $data['interviewer'] = array(
                'name'=>$session['username'],
                'account'=>$session['account']
            );

            //取得目前問卷的資訊
            $surveyGuid = $this->input->get_post('sguid', TRUE);

            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);
            
            $result_url = "<a href='".base_url('front/sample_list_result/?sguid='.$surveyGuid)."' style='color:#fff'>」</a>";
            
            $data['survey'] = array(
                'guid'=>$surveyObject->getGuid(),
                'title'=>$surveyObject->getTitle().$result_url,
                'ver'=>$surveyObject->getVersion()
            );

            //取得該訪員的在該問卷底下的樣本
            $url = base_url('getSample?surveyguid=' . $surveyGuid . "&interviewerguid=" . $session['guid']);
            //echo $url;
            $samples = json_decode(file_get_contents($url));

            //print_r($samples);

            //設定表格

            $tmpl = array('table_open'=>'<table class="lists">');
            $this->table->set_empty("&nbsp;");
            $this->table->set_template($tmpl);
            $this->table->set_heading('樣本', '進度', '已鎖定', '版本', '作答日期', '動作');

            if ($samples->Header->Status == 0)
            {
                //產生物件，並設定值，用在非ajax讀取時，取得進度與作答時間
                /*
                $frontAnswerObject = new front_answer_model();
                $frontAnswerObject->setInterViewerGuid($session['guid']);
                $frontAnswerObject->setSurveyGuid($surveyGuid);
                $frontAnswerObject->setVersion($surveyObject->getVersion());
                //取得所有問卷題目數量
                $all = $frontAnswerObject->getSurveyQuestionNumber();*/
                //表讀取成功 0 有問卷
                foreach ($samples->Body->getSample->Sample as $row)
                {
                    //進度、作答日期 產生 div 與 span 標籤，在讓 front.js去ajax取得值在算出，已將降低網頁讀取時間。
                    /*
                    $frontAnswerObject->setSampleGuid($row->SampleGuid);
            
                    //取得該樣本的作答狀況
                    $ProgressAndTime = $frontAnswerObject->getSampleProgressAndTime();
                    $percent = round(($ProgressAndTime[0] / $all) * 100);
                    $percent = "width:{$percent};";
                    $noyet = '';
                    if($percent<100)
                        $noyet = 'notyet';
                    */
                    //先清空百分比與是否還沒完成，因為要透過front.js ajax動態取得
                    $percent = "";
                    $noyet='';
                    //鎖定
                    if ($row->Locked)
                    {
                        $action = "已鎖定，無法作答";
                        $locked = "<img src='" . base_url('style/front/images/base/lock.gif') . "'/>";
                    }
                    else
                    {
                        $action = anchor("front/sample_status/?spguid=" . $row->SampleGuid . "&sguid=" . $surveyGuid, '開始(繼續)作答', "class='btn btn-green startAnswer' data-sguid='{$surveyGuid}'") . nbs(2) . "<a href='javascript:void(0);' class='btn lockSample' data-guid='" . $row->SampleGuid . "'>鎖定</a>";
                        $locked = '';
                    }

                    //print_r($row->SurveyGuid);
                    $this->table->add_row(array(
                        'data'=>$row->Name,
                        //'data'=>anchor("front/sample_status/?spguid=" . $row->SampleGuid . "&sguid=" . $surveyGuid, $row->Name, "class='startAnswer' data-sguid='{}'"),
                        'class'=>'text-center'
                    ), array(
                        'data'=>'<div class="sampleProgress" data-spguid="' . $row->SampleGuid . '" data-sguid="' . $surveyGuid . '" data-ivguid="' . $session['guid'] . '" data-ver="' . $surveyObject->getVersion() . '"><span style="'.$percent.'" class="'.$noyet.'"></span></div>',
                        'class'=>'text-center'
                    ), array(
                        'data'=>$locked,
                        'class'=>'text-center'
                    ), array(
                        'data'=>$data['survey']['ver'],
                        'class'=>'text-center'
                    ), array(
                        'data'=>'',
                        'class'=>'text-center doAnswerDate'
                    ), array(
                        'data'=>$action,
                        'class'=>'text-center'
                    ));

                }
            }
            else
            {
                //表讀取失敗 1 沒有問卷
                $this->table->add_row(array(
                    'data'=>'此訪員這問卷尚未指定樣本',
                    'colspan'=>5
                ));
            }

            //產生表格
            $data['list_table'] = $this->table->generate();
            $this->table->clear();

            //重新給予網頁meta描述
            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/header', $data);
            $this->load->view('front/sample_list', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');
            //20131122 珂： 因為有開啟資料庫快取，所以有更新或新增樣本答案時，都需清掉前台的樣本快取，這樣user在loader資料才會是最新的 
            //$this->db->cache_delete('front','sample_list');
        }
        else
        {
            //否，進入登入頁
            $this->login();
        }
    }

    /**
     * sample_list_result()
     * 樣本列表頁面，根據登入的使用者取得選取的問卷，顯示目前作答情況。
     * @param
     * @return html
     */
    public function sample_list_result()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //如果有登入，就取得session值
            $session = $this->session->all_userdata();

            //載入 localstorage jquery plugin
            $data['js'] = '<script src="' . base_url("style/front/js/jquery.storageapi.min.js") . '"></script>';

            //取得登入者資訊
            $data['interviewer'] = array(
                'name'=>$session['username'],
                'account'=>$session['account']
            );

            //取得目前問卷的資訊
            $surveyGuid = $this->input->get_post('sguid', TRUE);

            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            $data['survey'] = array(
                'guid'=>$surveyObject->getGuid(),
                'title'=>$surveyObject->getTitle()."(".$surveyGuid.")",
                'ver'=>$surveyObject->getVersion()
            );
            
            $data['list_table'] = '';
            
            $table_name = "survey_answer_{$data['survey']['guid']}_{$data['survey']['ver']}";
            
            //取得該訪員在該問卷的樣本            
            $query = $this->db->get_where('survey_survey_own_sample',array('interviewerGuid' => $session['guid'], 'surveyGuid'=>$surveyGuid));
            
            if($query->num_rows()>0)
            {
                $tmpl = array('table_open'=>'<table class="lists results">');
                $this->table->set_empty("&nbsp;");
                $this->table->set_template($tmpl);
                $i = 0;
                $noshow = array('id','interviewerGuid','sampleGuid');
                foreach($query->result_array() as $row)
                {
                    
                    //根據每一個樣本群組找出樣本
                    $this->db->order_by('survey_sample.mainpriority ASC, survey_sample.subpriority ASC');
                    $this->db->select('survey_sample.*');
                    $this->db->join('survey_sample','survey_sample.guid = survey_samplegroup.sampleGuid','left');
                    $query = $this->db->get_where('survey_samplegroup',array('survey_samplegroup.guid' => $row['groupSampleGuid']));
                    if($query->num_rows()>0)
                    {
                        foreach ($query->result_array() as $row2) {
                            //根據每一個樣本取得，已經做的答案
                            $query = $this->db->get_where($table_name,array('interviewerGuid'=> $session['guid'],'sampleGuid' => $row2['guid']));
                            unset($header);
                            unset($rowAnwser);
                            if($query->num_rows() == 1)
                            {
                                $a_sample = $query->row_array();
                                //如果為0就製作表頭
                                if($i == 0)
                                {
                                    $header[] = '樣本名稱';
                                    foreach($a_sample as $key=>$value)
                                    {
                                        if(!in_array($key,$noshow))
                                        {
                                            $header[] = $key;
                                        }
                                    }
                                    $this->table->set_heading($header);
                                    $i++;
                                }
                                foreach ($noshow as $removeKey) {
                                    unset($a_sample[$removeKey]);
                                }
                                //print_r($row2);
                                $rowAnwser[] = $row2['name']."<br/>".$row2['guid'];
                                foreach ($a_sample as $anwsers) {
                                    if($anwsers === null)
                                        $anwsers = '<span style="color:#ddd">NULL</span>';
                                    if($anwsers === '')
                                        $anwsers = '<span style="color:#d2d2d2">跳過</span>';
                                    $rowAnwser[] = $anwsers;
                                }
                                $this->table->add_row($rowAnwser);
                                //echo $row2['name'];
                                //print_r($query->row_array());
                            }
                            else 
                            {
                                 //echo $row2['name']."尚未作答";
                            }                            
                        }
                    }
                }
            }
            
            //產生表格
            $data['list_table'] = $this->table->generate();
            $this->table->clear();
            
            //重新給予網頁meta描述
            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/header', $data);
            $this->load->view('front/sample_list', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');
            //20131122 珂： 因為有開啟資料庫快取，所以有更新或新增樣本答案時，都需清掉前台的樣本快取，這樣user在loader資料才會是最新的 
            //$this->db->cache_delete('front','sample_list');
        }
        else
        {
            //否，進入登入頁
            $this->login();
        }
    }

    /**
     * sample_status()
     * 樣本狀態，開始做問卷前，會先詢問 繼續訪問、需在訪運、不住原址...等問題
     * 當結束樣本時，也會出現此狀態
     * @param
     * @return html
     */
    public function sample_status()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //如果有登入，就取得session值
            $session = $this->session->all_userdata();

            //載入 localstorage jquery plugin
            $data['js'] = '';

            //取得登入者資訊
            $data['interviewer'] = array(
                'name'=>$session['username'],
                'account'=>$session['account']
            );

            $sampleGuid = $this->input->get_post('spguid', TRUE);
            $surveyGuid = $this->input->get_post('sguid', TRUE);

            //取得目前問卷的資訊
            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            $data['survey'] = array(
                'guid'=>$surveyObject->getGuid(),
                'title'=>$surveyObject->getTitle(),
                'ver'=>$surveyObject->getVersion()
            );

            //取得該樣本的資訊
            $url = base_url('getSample?sampleguid=' . $sampleGuid);
            //echo $url;
            $sample = json_decode(file_get_contents($url));
            $data['sample'] = array(
                'guid'=>$sampleGuid,
                'name'=>$sample->Body->getSample->Name,
            );

            //將問卷相關guid存入session中
            $guids = array(
                'surveyGuid'=>$surveyGuid,
                'ver'=>$surveyObject->getVersion(),
                'sampleGuid'=>$sampleGuid
            );
            $this->session->set_userdata($guids);

            //載入 front_answer_model
            $front_answer = new front_answer_model();

            //取得該樣本的開始或停止的問題內容，並產生 ul 以列印出

            $situation = $front_answer->getStatusQuestions();

            $data['situation'] = '<ul class="situation">';

            foreach ($situation as $key=>$row)
            {

                if ($key == 0)
                {
                    $data['situation'] .= "<li>" . anchor("front/answering/", $row['title'], " class='intoAnswer' data-code='{$row['code']}' data-sguid='{$surveyGuid}'") . "</li>";
                }
                else
                {
                    $data['situation'] .= "<li><a href='javascript:void(0);' class='dropdown' data-code='{$row['code']}'>{$row['title']}</a>";
                    $options = array();
                    foreach ($row['options'] as $val)
                    {

                        if ($val['code'] == 23 || $val['code'] == 410)
                        {
                            $options[] = "{$val['title']}:<input type='text' class='situationComment{$val['code']} width50' placeholder='請填入原因'/> <button type='button' class='stopSurvey' data-code='{$val['code']}'>確定並離開</button>";
                        }
                        else
                        {
                            $options[] = "<a href='javascript:void(0);' class='stopSurvey' data-code='{$val['code']}'>{$val['title']}</a>";
                        }
                    }

                    $attributes = array(
                        'class'=>'',
                        'id'=>''
                    );

                    $data['situation'] .= ul($options, $attributes) . "</li>";
                }

            }
            $data['situation'] .= '</ul>';

            /*

             $data['situation'] = ul($options,$attributes);
             */
            //重新給予網頁meta描述
            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/answer_header', $data);
            $this->load->view('front/answering', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');

        }
        else
        {
            //否，進入登入頁
            $this->login();
        }

    }

    /**
     * answering()
     * 完成受訪詢問後，進入作答畫面。
     * @param
     * @return html
     */
    public function answering()
    {
        if ($this->session->userdata('interviewer_logged_in'))
        {
            //如果有登入，就取得session值
            $session = $this->session->all_userdata();

            //載入 localstorage jquery plugin
            $data['js'] = '<script src="' . base_url("style/front/js/jquery.storageapi.min.js") . '"></script>
                           <script src="' . base_url("style/front/js/jquery.twzipcode.min.js") . '"></script>';

            //取得登入者資訊
            $data['interviewer'] = array(
                'name'=>$session['username'],
                'account'=>$session['account']
            );
            //print_r($session);
            $sampleGuid = $session['sampleGuid'];
            $surveyGuid = $session['surveyGuid'];

            //取得目前問卷的資訊
            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            $data['survey'] = array(
                'guid'=>$surveyObject->getGuid(),
                'title'=>$surveyObject->getTitle(),
                'ver'=>$surveyObject->getVersion(),
                'greetingText'=>$surveyObject->getGreetingText(),
                'thankText'=>$surveyObject->getThankText()
            );

            //取得該樣本的資訊
            $url = base_url('getSample?sampleguid=' . $sampleGuid);
            //echo $url;
            $sample = json_decode(file_get_contents($url));
            $data['sample'] = array(
                'guid'=>$sampleGuid,
                'name'=>$sample->Body->getSample->Name
            );

            //重新給予網頁meta描述
            $this->load->view('front/htmlstart');
            $this->load->view('front/head', $data);
            $this->load->view('front/answer_header', $data);
            $this->load->view('front/answering', $data);
            $this->load->view('front/footer');
            $this->load->view('front/htmlend');

        }
        else
        {
            //否，進入登入頁
            $this->login();
        }
    }
    
    
    /**
     * updateStartTime()
     * 更新問卷開始時間
     * @param
     * @return json
     */
    public function updateSurveyStartTime()
    {
       
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);

        $frontAnswerObject->saveSurveyTime('update');
         
        $json = array(
            'result'=>TRUE,
            'msg'=>'更新成功'
        );
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    
    /**
     * uploadSurveySituation()
     * 上傳問卷狀態，並判別開始或結束時間，以記錄
     * @param
     * @return html
     */
    public function uploadSurveySituation()
    {
        $code = $this->input->post('c', TRUE);
        $comment = $this->input->post('m', TRUE);

        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setSituationCode($code);
        $frontAnswerObject->setSituationComment($comment);

        //儲存情況
        if ($frontAnswerObject->saveSituation())
        {
            //如果狀態儲存成功，就繼續儲存開始或結束時間
            if ($code == 1)
            {
                //如果 code 為 1 則為開始做問卷，記錄開始的時間與狀態
                //如果沒有已經有開始的時間id，就進行開始時間的儲存。有時間記錄，就不管，繼續作答
                if (!$this->session->userdata('surveyTimeID'))
                {
                    $frontAnswerObject->saveSurveyTime('start');
                }
                //開始狀態與時間儲存完後，進入作答頁面
                $json['redirect'] = base_url('front/answering');
            }
            else
            {
                //done就是完成問卷
                //如果不是1，若已經開始，就記錄結束時間，沒有開始就不記錄。
                if ($this->session->userdata('surveyTimeID'))
                {
                    $frontAnswerObject->saveSurveyTime('end');
                }
                //離開狀態與時間儲存完後，回到樣本列表
                $json['redirect'] = base_url('front/sample_list/?sguid=' . $session['surveyGuid']);
            }
            $json['result'] = TRUE;
            $json['msg'] = '狀態記錄成功';
        }
        else
        {
            $json = array(
                'result'=>FALSE,
                'msg'=>'狀態儲存失敗'
            );
        }
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /**
     * lockSample()
     * 上傳問卷狀態，並判別開始或結束時間，以記錄
     * @param
     * @return html
     */
    public function lockSample()
    {

        //取得樣本id
        $sampleGuid = $this->input->post('s');
        if ($sampleGuid)
        {
            //產生物件，設定值後
            $frontAnswerObject = new front_answer_model();
            $frontAnswerObject->setSampleGuid($sampleGuid);
            if ($frontAnswerObject->changeLockSample(1))
            {
                $json = array(
                    'result'=>TRUE,
                    'msg'=>'鎖定成功'
                );
            }
            else
            {
                $json = array(
                    'result'=>FALSE,
                    'msg'=>'鎖定失敗，資料庫有問題'
                );
            }
        }
        else
        {
            $json = array(
                'result'=>FALSE,
                'msg'=>'鎖定失敗，沒有樣本id'
            );
        }

        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

    /**
     * getSampleProgress()
     * 取得樣本的進度
     * @param
     * @return html
     */
    public function getSampleProgress()
    {
        //取得post值
        $posts = $this->input->post(NULL, TRUE);
        //產生物件，並設定值
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($posts['iv']);
        $frontAnswerObject->setSampleGuid($posts['sp']);
        $frontAnswerObject->setSurveyGuid($posts['s']);
        $frontAnswerObject->setVersion($posts['v']);
        //取得所有問卷題目數量
        $all = $frontAnswerObject->getSurveyQuestionNumber();

        //取得該樣本的作答狀況
        $ProgressAndTime = $frontAnswerObject->getSampleProgressAndTime();

        //回傳計算後的百分比。
        if($ProgressAndTime[0])
        {
            $json = array(
                'result'=>TRUE,
                'percentage'=>round(($ProgressAndTime[0] / $all) * 100),
                'cDateTime'=>$ProgressAndTime[1]
            );
        }
        else
        {
            $json = array(
                'result'=>TRUE,
                'percentage'=>0,
                'cDateTime'=>''
            );
        }
        
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));

    }

    /**
     * checkCurrentQuestion()
     * 檢查此樣本目前做到哪一題題目
     * @param
     * @return html
     */
    public function checkSampleCurrentQuestion()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        //因為localstore記錄的index從0開始，所以結果要少1
        $subjectArrayIndex = $frontAnswerObject->checkOnWhichQuestion() - 1;
        if($subjectArrayIndex<0)
            $subjectArrayIndex = 0;
        
        //回傳計算後的百分比。
        $json = array(
            'result'=>TRUE,
            'subjectArrayIndex'=>$subjectArrayIndex,
            'msg'=>'讀取成功'
        );
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }
    
    /**
     * checkHaveSelect()
     * 檢查此題目是否已經有選答案了
     * @param
     * @return html
     */
    public function checkHaveSelect()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        //取得是否有答案
        $value = $frontAnswerObject->checkHaveValue((string)$this->input->post('sn',TRUE));
        
        //回傳計算後的百分比。
        $json = array(
            'result'=>TRUE,
            'value'=>$value,
            'msg'=>'讀取成功'
        );
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }
    
    /**
     * getGroupPassAnswer()
     * 取得群組跳題的答案
     * @param
     * @return josn
     */
    public function getGroupPassAnswer()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        //取得是否有答案
        $anwser = $frontAnswerObject->gerQuestionAnswer($this->input->post('n',TRUE));
        
        //回傳計算後的百分比。
        $json = array(
            'result'=>TRUE,
            'anwser'=>$anwser,
            'msg'=>'讀取成功'
        );
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }

    /**
     * creatAnswerTable()
     * 建立答案表格
     * @param
     * @return josn
     */
    public function creatAnswerTable()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        
        //檢查有無表格，沒有就建立
        if($frontAnswerObject->creatAnswerTable($this->input->post('i',TRUE))){
            
            //回傳儲存結果
            $json = array(
                'result'=>TRUE,
                'msg'=>'檢查與建立完畢'
            );
        }
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }

    /**
     * saveAnswer()
     * 儲存答案
     * @param
     * @return josn
     */
    public function saveAnswer()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        
        //儲存答案
        $anwser = $frontAnswerObject->saveAnswer($this->input->post('n'), $this->input->post('a'));
        
        //回傳儲存結果
        $json = array(
            'result'=>TRUE,
            'anwser'=>$this->input->post('n')." => ".join("," ,$this->input->post('a')),
            'msg'=>$anwser
        );
        
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }
    
    /**
     * saveAnswer()
     * 取得之前題目的答案，作為標題置換用
     * @param
     * @return josn
     */
    public function getPrevAnswer()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        
        //取得該答案
        $getAnwser = $frontAnswerObject->getPrevAnswer($this->input->post('sn',TRUE));
        
        //回傳儲存結果
        $json = array(
            'result'=>TRUE,
            'anwser'=>$getAnwser,
            'msg'=>'讀取成功'
        );
        
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }
    
    /**
     * clearJumpAnswer()
     * 清圖
     * @param
     * @return josn
     */
    public function clearJumpAnswer()
    {
        //取得session值
        $session = $this->session->all_userdata();

        //產生物件，設定值後
        $frontAnswerObject = new front_answer_model();
        $frontAnswerObject->setInterViewerGuid($session['guid']);
        $frontAnswerObject->setSampleGuid($session['sampleGuid']);
        $frontAnswerObject->setSurveyGuid($session['surveyGuid']);
        $frontAnswerObject->setVersion($session['ver']);
        
        
        //取得該答案
        $result = $frontAnswerObject->clearJumpAnswer($this->input->post('c',TRUE));
        
        //回傳儲存結果
        $json = array(
            'result'=>TRUE,
            'msg'=>$result
        );
        
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
        
    }
    
    /**
     * cleanSampleRecode()
     * 清圖
     * @param
     * @return josn
     */
    public function cleanSampleRecode($sampleGUID)
    {
        $this->db->delete('survey_surveysituation',array('sampleGuid' => $sampleGUID));
        $this->db->delete('survey_surveytime',array('sampleGuid' => $sampleGUID));
        $json = array('msg'=>$sampleGUID .' 樣本的狀態與時間已刪除，需在刪除survey_answer...的資料');
        //輸出json格式
        //$this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    
    
    /**
     * cleanSurveyData()
     * 清除問卷底下的資料，包含其樣本的相關記錄
     * @param
     * @return josn
     */
    public function cleanSurveyData($surveyGUID,$ver)
    {
        $table = "survey_answer_{$surveyGUID}_{$ver}";
        if($this->db->table_exists($table))
        {
            //取得該問卷底下的所有樣本
            $query = $this->db->get($table);
            if($query->num_rows()>0)
            {
                foreach ($query->result_array() as $row) {
                    //清除該樣本資料
                    $this->cleanSampleRecode($row['sampleGuid']);
                }
                //最後清除該表格中資料
                $this->load->dbforge();
                $this->dbforge->drop_table($table);
                $json = array('msg'=>$surveyGUID."_".$ver .' 的樣本已刪除。 All sample has delete.');
            
            }
            else
            {
                //最後清除該表格中資料
                $this->load->dbforge();
                $this->dbforge->drop_table($table);
                
                $json = array('msg'=>$surveyGUID."_".$ver .' 資料表中沒有作答記錄，已刪除表格');
            }
            //以及清除快取
            $this->db->cache_delete('front','getSampleProgress');
        }
        else 
        {
            $json = array('msg'=>$surveyGUID."_".$ver .' 資料表不存在 無作答記錄。');
        }
        
        //輸出json格式
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    
    public function removehtml(){
        $query = $this->db->get_where('survey_option',array('surveyGuid' => 'feeb24ec9d36780895de11ee8b1ba38f'));
        foreach ($query->result_array() as $row) {
            $removedTitle = str_replace(array("<br>","<br/>","<b>","</b>"), '',$row['title']);
            //$this->db->where('id',$row['id']);
            //$this->db->update('survey_option',array('title'=>$removedTitle));
            //echo htmlspecialchars($row['title'])." = ".$removedTitle;
            //echo "<hr>";
        }
        $query = $this->db->get_where('survey_question',array('surveyGuid' => 'feeb24ec9d36780895de11ee8b1ba38f'));
        foreach ($query->result_array() as $row) {
            $removedSubject = str_replace(array("<br>","<br/>","<b>","</b>"), '', $row['subject']);
            //$this->db->where('id',$row['id']);
            //$this->db->update('survey_question',array('subject'=>$removedSubject));
            //echo htmlspecialchars($row['subject'])." = ".$removedSubject;
            //echo "<hr>";
        }
    }
    
    /**
     * 給取的主機的作業系統
     * getServerOS()
     *
     */
    function getServerOS()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
        {
            return 'win';
        }
        else
        {
            return 'linux';
        }
    }
    
    public function db_backup()
    {
        //修改php.ini 設定
        ini_set('memory_limit', '10240M');
        ini_set('max_execution_time', 0);
        
        //檔名設定
        $db_name = 'backup_on_'.date("Y_m_d").".sql.gz";
        $del_name = 'backup_on_'.date("Y_m_d",strtotime("-29 day")).".sql.gz";
        //要上傳的資料夾
        $ftp_backup_folder = 'capi.nctu.edu.tw';
        if($this->getServerOS() == 'linux')
        {
            $local_backup_folder = str_replace('application/controllers', '', dirname(__FILE__)).'db_backup/';
        }
        else
        {
            $local_backup_folder = str_replace('application\controllers', '', dirname(__FILE__)).'db_backup\\';
        }
        
        $save = $local_backup_folder.$db_name;
        
        
        //開始使用資料庫工具
        $this->load->dbutil();      
        //備份設定並建立備份檔
        $prefs = array('format'=>'gzip',
                       'filename'=>'capi_db_backup.sql',
                       'foreign_key_checks' => FALSE);
        $backup = $this->dbutil->backup($prefs);
        
        //將備份資料庫存入本機端
        $this->load->helper('file');
        write_file($save, $backup);
        
        //檢查本機端是否有超過28天以上的檔案，有就刪除
        $locallist = scandir($local_backup_folder);
        if(in_array($del_name,$locallist)){
            unlink($local_backup_folder.$del_name);
        }
        
        //上傳ftp
        $this->load->library('ftp');
        $config['hostname'] = 'workshop.sweea.com';
        $config['username'] = 'db_backup';
        $config['password'] = 'ej03xu3admin';
        $config['port']     = 2121; 
        $config['passive']  = true; //非預設的21port 就要啟用被動式的 21port 就用false
        $config['debug']    = TRUE;
        
        $this->ftp->connect($config);
        
        //檢查有無備份的資料夾，沒有就新增
        $list = $this->ftp->list_files('/');
        
        if(!in_array("/".$ftp_backup_folder,$list)){
            $this->ftp->mkdir('/'.$ftp_backup_folder,DIR_WRITE_MODE);
        }
        //檢查有無超過28天以上的檔案，有就刪除
        $list = $this->ftp->list_files('/'.$ftp_backup_folder);
        if(in_array($del_name,$list)){
            $this->ftp->delete_file("/{$ftp_backup_folder}/{$del_name}");
        }
        
        $this->ftp->upload($save,"/{$ftp_backup_folder}/".$db_name,'auto',0755);
        
        $this->ftp->close();
		
        $url = 'http://workshop.sweea.com/case_backup/index.php';
		$data = array(
			"note" => "{$ftp_backup_folder}/{$db_name}",
			"domain" => $_SERVER['HTTP_HOST'],
			"status" => "upload done"
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $data )); 
		curl_exec($ch); 
		curl_close($ch);
        //立即下載
        //$this->load->helper('download');
        //force_download($db_name, $backup);
        //echo 'done <a href="http://140.113.53.105/db_backup/'.$db_name.'">download '.$db_name.'</a>';
        
        //完成後發信給我
        //寄發新密碼
        /*
        $message = "
                <body style='background-color:#eee'><p>&nbsp;</p>
                
                <div style='width:500px;margin:0 auto;padding:10px;color:#454545;font-size:10pt;background:#fff;border-bottom:#5d1e0b solid 1px;border-left:#5d1e0b solid 1px;border-right:#5d1e0b solid 1px;'>
                <br />
                <p>hi 管理者 您好！</p>
                <p>
                多多系統 {$db_name} 資料庫已備份完畢
                </p>
                
                </div>
                </body>";

        $subject = "多多".date("Y/m/d").'的資料庫備份通知。';
        //載入發信模組
        $this->load->model('sendmail_m');
        $send = $this->sendmail_m->sendmail('mktsai@sweea.com', $subject, $message);
        if ($send)
        {
            echo '備份完畢，並發送通知給管理者';
        }
        else
        {
            echo '備份完畢，但信件發送失敗。';
        }*/
    }

    function test()
    {
        echo "<meta charset='utf-8'>";
        /*
        echo "問卷題目";
        $this->db->order_by('priority','ASC');
        $query = $this->db->get_where('survey_question',array('surveyGuid' => '591c4bd60fc60256b3a2a0ace24d577f'));
        $result = $query->result_array();
        $keys = 0;
        $subjectNumber = array();
        foreach ($result as $key => $value) {
            $keys++;
            echo $value['subjectNumber']."<br/>";
            $subjectNumber[] = $value['subjectNumber'];
        }
        echo '$keys='.$keys;
        echo "<hr>";
        $query = $this->db->get_where('survey_answer_591c4bd60fc60256b3a2a0ace24d577f_1',array('id' =>2 ));
        $keys = 0;
        
        echo "已作答問卷題目";
        $result = $query->row_array();
        $i = 0;
        foreach ($result as $key => $value) {
            if($i > 3)
            {
                 $keys++;
                echo $key."=>".$value."<br/>";
                if(!in_array($key,$subjectNumber))
                {
                    echo "<span style='color:red;'>".$key . " 不存在目前的題目當中</span><br/>";
                }
            }
            $i++;
        }
        echo '$keys='.$keys;
        */
        $interviwer = array("TTR004","TTR025","TTR026","TTR027","TTR028","TTR029","TTR030","TTR031","TTR032","TTR033","TTR034","TTR035","TTR036","TTR037","TTR038","TTR039","TTR040","TTR050");
        foreach($interviwer as $row)
        {
            $query = $this->db->get_where('survey_interviewer',array('name' => $row));
            if($query->num_rows() > 0)
            {
                $a_interviewer = $query->row_array();
                //刪除 survey_interviewer_own_surveys
                $query = $this->db->get_where('survey_interviewer_own_surveys',array('interviewerGuid' => $a_interviewer['guid'],'surveyGuid'=>''));
                if($query->num_rows() > 0)
                {
                    foreach($query->result_array() as $row2)
                    {
                        //$this->db->delete('survey_interviewer_own_surveys',array('id' => $row2['id']));
                        echo "需要刪除的：";
                        echo $row2['id'];
                        echo "<hr>";
                    } 
                }
                
                //刪除 survey_survey_own_sample
                $query = $this->db->get_where('survey_survey_own_sample',array('surveyGuid'=>'', 'interviewerGuid' => $a_interviewer['guid']));
                if($query->num_rows() > 0)
                {
                    foreach($query->result_array() as $row2)
                    {
                        //$this->db->delete('survey_survey_own_sample',array('id' => $row2['id']));
                        echo "需要刪除的：";
                        echo $row2['id'].":".$row2['groupSampleGuid'];
                        echo "<hr>";
                        
                        //刪除 樣本與樣本群組
                       // $this->db->delete('survey_samplegroup_data',array('sampleGroupGuid' => $row2['groupSampleGuid']));
                        //$this->db->delete('survey_samplegroup',array('guid' => $row2['groupSampleGuid']));
                    } 
                }
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

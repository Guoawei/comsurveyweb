<?php

	/**
	 * 
	 */
	class interviewers extends CI_Controller {
		
		private $limit = 30;
		private $title = "科技部傳播調查資料庫";
		
        function interviewers() {
            parent::__construct();
			$this->load->library('session');
			$this->load->model("survey_model");
			$this->load->model("interviewer_model");
			$this->load->model("interviewer_logic_model");
			$this->load->model("sample_model");
			$this->load->model("samplelogic_model");
            $this->load->model("samplegroup_model");
			$this->load->helper('url');
            $this->load->helper('form');
		}	
			
		function index()
		{
			$this->interviewerList();
		}
		
		function doCheckIsLogin()
		{			
			return $this->session->userdata('admin_logged_in');
		}
		
		function doCheckInterviewerIsLogin()
		{			
			return $this->session->userdata('interviewer_logged_in');
		}
		
		function doGetUserName()
		{			
			return $this->session->userdata('username');
		}
		
		function doSetSessionSurveyGuid($surveyGuid)
		{
			$data = array(
				'surveyGuid' => $surveyGuid
			);
			$this->session->set_userdata($data);
		}
		
		function doGetSessionSurveyGuid()
		{
			return $this->session->userdata('surveyGuid') ;
		}
		
		function doSetSessionPageRecord($pageRecord)
		{
			$pageData = array(
				'pageRecord' => $pageRecord
			);
			$this->session->set_userdata($pageData);
		}
		
		function doGetSessionPageRecord()
		{
			return $this->session->userdata('pageRecord') ;
		}
		

		public function interviewerList()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}

			$this->load->library('pagination');
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading('NO','編輯','訪員的樣本分群','姓名','帳號','功能');
			
			// offset
        	$uri_segment = 4;
        	$offset = $this->uri->segment($uri_segment);
			
			$pageRecord = $this->uri->segment(3);
			if($pageRecord == NULL){
				$pageRecord = 0;
			}
			$this->doSetSessionPageRecord($pageRecord);
			$surveyGuid = $this->doGetSessionSurveyGuid();
			
			//load Data
			$sampleLogicModel = new samplelogic_model();
			$interviewerModel = new interviewer_model();
            $interviewerLogicModle = new interviewer_logic_model();
			$countAll =$interviewerModel->getCountAll($surveyGuid);

			$interviewerArray = $interviewerModel->getInterviewerWithPage($surveyGuid, $this->limit, $offset)->result();
			$i = 0 + $offset;
			foreach ($interviewerArray as $row) {
				$tempInterviewer = new interviewer_model();
				$tempInterviewer->setGuid($row->guid);
				// $sampleListArray['Guid'][] = $row->getGuid();
				// $sampleListArray['Name'][] = $row->getName();
				// $sampleListArray['Gender'][] = $row->getGender();
				// $sampleListArray['Birthday'][] = $row->getBirthday();
				// $sampleListArray['Address'][] = $row->getAddress();
				// $sampleListArray['ZipCode'][] = $row->getZipCode();
				$selectedSampleListCount = $sampleLogicModel->getAllCountSampleInGroup($row->guid);
                $sampleGroupObject = new samplegroup_model();
                $sampleGroupGuid = $interviewerLogicModle->getSampleGroupGuidToInterviewer($tempInterviewer);
                if($sampleGroupGuid != ""){
                    $sampleGroupObject = $sampleGroupObject->getSampleGroupBySampleGroupGuid($sampleGroupGuid);
                }else {
                    $sampleGroupObject->setSampleGroupName('(未設定)');
                }
				$this->table->add_row(++$i
				,anchor('interviewers/update/'.$row->guid,'編輯',array('class'=>'edit'))	// ,anchor('samples/view/'.$row->getGuid(),$row->getName(),array('class'=>'viewNoImage'))	,$row->name
//				,anchor('interviewers/assignSurvey/'.$row->guid,'設定問卷',array('class'=>'edit'))
//				,anchor('interviewers/ownSurveyList/'.$row->guid,'設定樣本名單('.$selectedSampleListCount.')',array('class'=>'edit')),$row->name,$row->email
//                ,anchor('interviewers/assignSampleGroup/'.$surveyGuid.'/'.$row->guid,'設定樣本名單('.$selectedSampleListCount.')',array('class'=>'edit')),$row->name,$row->email
                ,anchor('interviewers/assignSampleGroup/'.$surveyGuid.'/'.$row->guid,'設定樣本名單-'.$sampleGroupObject->getSampleGroupName(),array('class'=>'edit')),$row->name,$row->email
				,anchor('interviewers/delete/'.$row->guid,'刪除',array('class'=>'delete','onclick'=>"return confirm('確定刪除此訪問員？')")));
				
			}
			//分頁功能
			$uri_segment = 4;
			
			$config['base_url'] = site_url('interviewers/interviewerList/index/');
			$config['total_rows'] = $countAll;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;
			$config['num_links'] = 7;
			$config['first_link'] = '[第1頁]';
			$config['last_link'] = '[最後頁]';
			$config['next_link'] = '下一頁 >';
			$config['prev_link'] = '< 上一頁';
			
			//目前的調查問卷
			$surveyObject = new survey_model();
			$surveyObject->setGuid($surveyGuid);
			$surveyObject = $surveyObject->getSurvey($surveyObject);
			
			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['headerTitle'] = $surveyObject->getTitle();
			$data['surveyGuid'] = $surveyGuid;
			$data['username'] = $this->doGetUserName();
			$data['add'] = anchor("interviewers/add/", '<img src="'.base_url().'/style/images/addInterviewer.png" alt="Delete" />');
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            $data['toSampleGroupView'] = "";
            $data['form'] = "";
            $data['formSubmit'] = "";
			$this->load->view('survey_detail_view', $data);
		}

		public function add()
		{
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
						
			$data['interviewer'] = $interviewerObject;
			$data['title'] = $this->title;
			$data['headerTitle'] = "新增訪問員";
			$data['message']="";
			$data['action'] = site_url('interviewers/addInterviewer');
			$data['link_back'] = anchor('interviewers/index/','回到列表',array('class'=>'back'));
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_edit', $data);
		}
		
		public function addInterviewer()
		{
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
            $interviewerLogicModel = new interviewer_logic_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($this->input->post('guid'));
			$interviewerObject->setName($this->input->post('name'));
			$interviewerObject->setEmail($this->input->post('email'));
			$interviewerObject->setPwd($this->input->post('pwd'));
			$msg = '';
			if($interviewerObject->exists_user())
			{
				//如果已經存在，就讀取出來。
				$interviewerObject = $interviewerModel->getInterviewerByEmail($interviewerObject);
				//判別是否已經在該問卷中
				$hasSurveys = $interviewerLogicModel->getInterviewerOwnSurveyGuid($interviewerObject);
				$hasSurveysGuid = array();
				//將問卷guid取出
				foreach ($hasSurveys as $key => $value) {
					$hasSurveysGuid[] = $value->getGuid();
				}
				if(!in_array($this->doGetSessionSurveyGuid(),$hasSurveysGuid))
				{
					//如果不在此問卷，才新增關連。
					$interviewerLogicModel->addInterviewerSurveyGuid($interviewerObject,$this->doGetSessionSurveyGuid());
					$msg = $interviewerObject->getEmail().'此帳號歸'.$interviewerObject->getName()."所有，已將".$interviewerObject->getName()."新增至此問卷中。";
				}
				else
				{
					$msg = $interviewerObject->getEmail().'此帳號歸'.$interviewerObject->getName()."所有，且已在此問卷中了";
				}
			}
			else
			{
				//沒有才新增
				$interviewerModel->addInterviewer($interviewerObject);
				$interviewerLogicModel->addInterviewerSurveyGuid($interviewerObject,$this->doGetSessionSurveyGuid());
			}
			$newInterviewer = new interviewer_model();
			$data['interviewer'] = $newInterviewer;
			$data['title'] = $this->title;
			$data['headerTitle'] = "新增訪員";
			$data['message']=($msg!='')?$msg:"新增訪員 ".$interviewerObject->getName()." 成功";
			$data['action'] = site_url('interviewers/addInterviewer');
			$data['link_back'] = anchor('interviewers/index/','回到列表',array('class'=>'back'));
			$data['link_updatePwd'] = anchor('interviewers/updatePwd/'.$interviewerObject->getGuid(),'修改密碼',array('class'=>'viewNoImage'));
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_edit', $data);
		}
		
		public function update($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
			// $pageRecord = $this->input->post('pageRecord');
			
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
						
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['title'] = $this->title;
			$data['headerTitle'] = "編輯訪員資料";
			$data['message']="";
			$data['action'] = site_url('interviewers/updateInterviewer/');
			$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$data['link_updatePwd'] = anchor('interviewers/updatePwd/'.$interviewerObject->getGuid(),'修改密碼',array('class'=>'viewNoImage'));
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_edit', $data);
		}
		
		public function updateInterviewer()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($this->input->post('guid'));
			$interviewerObject->setName($this->input->post('name'));
			$interviewerObject->setEmail($this->input->post('email'));
			$interviewerObject->setPwd($this->input->post('pwd'));
			$interviewerModel->updateInterviewer($interviewerObject);
			$surveyChkbx = $this->input->post('surveyChkbx');	
			
			$data['message']="更新成功";
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['title'] = $this->title;
			$data['headerTitle'] = "編輯訪員資料";
			$data['action'] = site_url('interviewers/updateInterviewer');
			$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$data['link_updatePwd'] = anchor('interviewers/updatePwd/'.$interviewerObject->getGuid(),'修改密碼',array('class'=>'viewNoImage'));
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_edit', $data);
		}
		
		public function updatePwd($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
			
			//取得擁有的問卷物件
			$interviewerLogic = new interviewer_logic_model();
			$ownSurveyList = $interviewerLogic->getInterviewerOwnSurveyGuid($interviewerObject);
			
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['ownSurveyList'] = $ownSurveyList;
			$data['title'] = $this->title;
			$data['headerTitle'] = "修改訪員密碼";
			$data['whoLogin'] = "admin";
			$data['message']="";
			$data['action'] = site_url('interviewers/updateInterviewerPwd');
			$data['link_back'] = anchor('interviewers/update/'.$interviewerObject->getGuid(),'回編輯頁',array('class'=>'back'));
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_pwd_edit', $data);
		}
		
		public function iUpdatePwd($guid)
		{
			if (!$this->doCheckInterviewerIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
			
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['title'] = $this->title;
			$data['headerTitle'] = "修改訪員密碼";
			$data['whoLogin'] = "interviewer";
			$data['message']="";
			$data['action'] = site_url('interviewers/iUpdateInterviewerPwd');
			$data['link_back'] = "";
			$data['username'] = $this->doGetUserName();
			$this->load->view('interviewer_pwd_edit', $data);
		}
		
		public function updateInterviewerPwd()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($this->input->post('guid'));
			$pageRecord = $this->doGetSessionPageRecord();
			if ($this->input->post('firstpwd') == $this->input->post('secondpwd')) {

				$interviewerObject->setPwd($this->input->post('secondpwd'));
				$interviewerModel->updateInterviewerPwd($interviewerObject);
			
				$data['message']="密碼更新成功";
				$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
				$data['title'] = $this->title;
				$data['headerTitle'] = "編輯訪員資料";
				$data['whoLogin'] = "admin";
				$data['action'] = site_url('interviewers/updateInterviewer');
				$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
				$data['link_updatePwd'] = anchor('interviewers/updatePwd/'.$interviewerObject->getGuid(),'修改密碼',array('class'=>'viewNoImage'));
				$data['username'] = $this->doGetUserName();
				$this->load->view('interviewer_edit', $data);
			} else {
				$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
				$data['title'] = $this->title;
				$data['headerTitle'] = "修改訪員密碼";
				$data['message']="輸入的密碼不一致，請新重輸入";
				$data['whoLogin'] = "admin";
				$data['action'] = site_url('interviewers/updateInterviewerPwd');
				$data['link_back'] = anchor('interviewers/update/'.$interviewerObject->getGuid(),'回編輯頁',array('class'=>'back'));
				$data['username'] = $this->doGetUserName();
				$this->load->view('interviewer_pwd_edit', $data);
				

			}

		}
		
		public function iUpdateInterviewerPwd()
		{
			if (!$this->doCheckInterviewerIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
						$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($this->input->post('guid'));
			if ($this->input->post('firstpwd') == $this->input->post('secondpwd')) {

				$interviewerObject->setPwd($this->input->post('secondpwd'));
				$interviewerModel->updateInterviewerPwd($interviewerObject);
			
				$data['message']="密碼更新成功";
				$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
				$data['title'] = $this->title;
				$data['headerTitle'] = "編輯訪員資料";
				$data['whoLogin'] = "interviewer";
				$data['action'] = site_url('interviewers/iUpdateInterviewerPwd');
				$data['link_back'] = "";
				$data['username'] = $this->doGetUserName();
				$this->load->view('interviewer_pwd_edit', $data);
			} else {
				$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
				$data['title'] = $this->title;
				$data['headerTitle'] = "修改訪員密碼";
				$data['message']="輸入的密碼不一致，請新重輸入";
				$data['whoLogin'] = "interviewer";
				$data['action'] = site_url('interviewers/iUpdateInterviewerPwd');
				$data['link_back'] = "";
				$data['username'] = $this->doGetUserName();
				$this->load->view('interviewer_pwd_edit', $data);

			}
		}

		public function delete($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
			$interviewerLogicModel = new interviewer_logic_model();
			$sampleLogic = new samplelogic_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
			//如果只有在一份問卷中，才砍掉該訪員
			$hasSurvery = $interviewerLogicModel->getInterviewerOwnSurveyGuid($interviewerObject);
			if(count($hasSurvery) == 1)
			{
				$interviewerModel->deleteInterviewer($interviewerObject);	
			}
			$interviewerLogicModel->deleteInterviewerSurveyGuid($interviewerObject,$this->doGetSessionSurveyGuid());

			$oldSampleGroupGuid = $interviewerLogicModel->getSampleGroupGuidToInterviewer($interviewerObject,$this->doGetSessionSurveyGuid());
			$sampleLogic->deleteSampleGroup($oldSampleGroupGuid);
//			$interviewerLogicModel->deleteSampleToInterviewer($interviewerObject);
            $interviewerLogicModel->updateSampleToInterviewer(null,$this->doGetSessionSurveyGuid(),$oldSampleGroupGuid,$oldSampleGroupGuid);
			
			redirect('interviewers/index/'.$pageRecord,'refresh');	
		}
		
		public function ownSurveyList($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			$this->load->library('pagination');
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading("訪員的調查");
			
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
			
			//取得擁有的問卷物件
			$interviewerLogic = new interviewer_logic_model();
			$ownSurveyList = $interviewerLogic->getInterviewerOwnSurveyGuid($interviewerObject);
			if (count($ownSurveyList)>0) {
				foreach ($ownSurveyList as $row) {
					$tempSurvey = new survey_model();
					$tempSurvey = $row;
					// $sampleListArray['Guid'][] = $row->getGuid();
					// $sampleListArray['Name'][] = $row->getName();
					// $sampleListArray['Gender'][] = $row->getGender();
					// $sampleListArray['Birthday'][] = $row->getBirthday();
					// $sampleListArray['Address'][] = $row->getAddress();
					// $sampleListArray['ZipCode'][] = $row->getZipCode();
					$this->table->add_row(
					anchor('interviewers/assignSample/'.$tempSurvey->getGuid().'/'.$guid,$row->getTitle(),array('class'=>'edit')));	// ,anchor('samples/view/'.$row->getGuid(),$row->getName(),array('class'=>'viewNoImage'))	,$row->name
				}	
			}else {
				$this->table->add_row('尚未設定問卷，請回上頁設定問卷');
			}
			
			//分頁功能
			$uri_segment = 3;
			$countAll = 2;
			$config['base_url'] = site_url('interviewers/index/');
			$config['total_rows'] = $countAll;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			//$data['pagination'] = $this->pagination->create_links();
			$data['pagination'] = '';  //樣本資料多，若讀取較久，請稍候。
			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('interviewers','回到列表',array('class'=>'back'));
			$data['headerTitle'] = "已設定問卷";
			
			$this->load->view('interviewer_ownsurveylist_view', $data);
		}
		
		public function assignSample($surveyGuid,$guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			$sampleModel = new sample_model();
			$sampleLogicModel = new samplelogic_model();
			$sampleObject = new sample_model();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			
			$interviewerObject->setGuid($guid);
			$interviewerObject = $interviewerModel->getInterviewerByGuid($interviewerObject);
			//取得所有未被勾選的樣本to-do
			$allSampleList = $sampleModel->getList();
			//1.取得已勾選的 2.取得別人選走的(並顯示由誰選走) 3.取得未勾選的 
			//取全部 - 1 - 2 = 3;
			//已勾選
			// $selectedSampleList = $sampleLogicModel->getSampleInGroup($surveyGuid, $guid);
			$selectedSampleList = $sampleLogicModel->getSamplesInterviewer($surveyGuid);
			
			$data['title'] = $this->title;
			$data['headerTitle'] = "設定樣本名單-".$interviewerObject->getName();
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['allSampleList'] = $allSampleList;
			$data['selectedSampleList'] = $selectedSampleList;
			$data['surveyGuid'] = $surveyGuid;
			$data['message']="";
			$data['username'] = $this->doGetUserName();
			$data['action'] = site_url('interviewers/doAssignSample');
			$data['link_back'] = anchor('interviewers/ownSurveyList/'.$guid,'回到列表',array('class'=>'back'));
			$this->load->view('interviewer_assign_sample', $data);
		}

		public function assignSampleTwo($surveyGuid,$guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			
			$this->load->library('pagination');
			// offset
			$localLimit = 200;
        	$uri_segment = 5;
        	$offset = $this->uri->segment($uri_segment);
			if ($offset == NULL) {
				$offset = 0;
			}
			
			$sampleModel = new sample_model();
			$sampleLogicModel = new samplelogic_model();
			$sampleObject = new sample_model();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			
			$interviewerObject->setGuid($guid);
			$interviewerObject = $interviewerModel->getInterviewerByGuid($interviewerObject);
			//取得所有未被勾選的樣本to-do
			$countAll = count($sampleModel->getList());
			$allSampleList = $sampleModel->getListWithPage($localLimit,$offset);
			//1.取得已勾選的 2.取得別人選走的(並顯示由誰選走) 3.取得未勾選的 
			//取全部 - 1 - 2 = 3;
			//已勾選
			// $selectedSampleList = $sampleLogicModel->getSampleInGroup($surveyGuid, $guid);
			$selectedSampleList = $sampleLogicModel->getSamplesInterviewer($surveyGuid);
			//分頁功能
			$uri_segment = 5;
			
			$config['base_url'] = site_url('interviewers/assignSampleTwo/'.$this->uri->segment(3).'/'.$this->uri->segment(4));
			$config['total_rows'] = $countAll;
			$config['per_page'] = $localLimit;
			$config['uri_segment'] = 5;
			$config['first_link'] = '[第1頁]';
			$config['last_link'] = '[最後頁]';
			$config['next_link'] = '下一頁 >';
			$config['prev_link'] = '< 上一頁';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			$data['title'] = $this->title;
			$data['headerTitle'] = "設定樣本名單-".$interviewerObject->getName();
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['allSampleList'] = $allSampleList;
			$data['selectedSampleList'] = $selectedSampleList;
			$data['surveyGuid'] = $surveyGuid;
			$data['message']="";
			$data['action'] = site_url('interviewers/doAssignSampleTwo');
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('interviewers/ownSurveyList/'.$guid,'回到列表',array('class'=>'back'));
			
			$this->load->view('interviewer_assign_two_sample', $data);
		}
		
		public function doAssignSample()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			$pageRecord = $this->doGetSessionPageRecord();
			$surveyGuid = $this->input->post('surveyGuid');
			$confirmDataArray = array();
			$message = '';
			
			$interviewerModel = new interviewer_model();
			$interviewerLogic = new interviewer_logic_model();
			$interviewerObject = new interviewer_model();
			$sampleModel = new sample_model();
			$sampleLogic = new samplelogic_model();
			$interviewerObject->setGuid($this->input->post('interviewerGuid'));
			$interviewerObject = $interviewerModel->getInterviewerByGuid($interviewerObject);
			
			//取得舊有的sampleGroupGuid
			$oldSampleGroupGuid = $interviewerLogic->getSampleGroupGuidToInterviewer($interviewerObject);
			$sampleLogic->deleteSampleGroup($oldSampleGroupGuid);
			$interviewerLogic->deleteSampleToInterviewer($interviewerObject);
			$sampleChkbox = $this->input->post('sampleChkbox');	
			if(!empty($sampleChkbox)){
				// for (reset($sampleChkbox); $k=key($sampleChkbox); next($sampleChkbox)){
					// $sampleArray[] = $sampleChkbox[$k];
				// }
				$sampleArray = array();
				foreach($sampleChkbox as $key => $value){
					$sampleArray[] = $value;	
				}
				$newSampleGroupGuid = $sampleLogic->addSampleGroup($sampleArray);
				$interviewerLogic->addSampleToInterviewer($interviewerObject, $surveyGuid,$newSampleGroupGuid);
				
				//Confirm View Data
				foreach ($sampleArray as $row) {
					$sampleObject = new sample_model();
					$sampleObject->setGuid($row);
					$confirmDataArray[] = $sampleModel->getSample($sampleObject);
				}
				$message = '設定樣本名單-樣本更新成功';
			}else {
				$message = '設定樣本名單-該訪員已無調查樣本';
			}		
			
			$data['message']=$message;
			$data['interviewer'] = $interviewerObject;
			$data['confirmDataArray'] = $confirmDataArray;			
			$data['title'] = $this->title;
			$data['headerTitle'] = "設定樣本名單-".$interviewerObject->getName();
			$data['action'] = site_url('interviewers/updateInterviewer');
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('interviewer_assign_sample_confirm_view', $data);
		}

		public function doAssignSampleTwo()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			
			$surveyGuid = $this->input->post('surveyGuid');
			$confirmDataArray = array();
			$message = '';
			
			$interviewerModel = new interviewer_model();
			$interviewerLogic = new interviewer_logic_model();
			$interviewerObject = new interviewer_model();
			$sampleModel = new sample_model();
			$sampleLogic = new samplelogic_model();
			$interviewerObject->setGuid($this->input->post('interviewerGuid'));
			$interviewerObject = $interviewerModel->getInterviewerByGuid($interviewerObject);
			
			//取得舊有的sampleGroupGuid
			$oldSampleGroupGuid = $interviewerLogic->getSampleGroupGuidToInterviewer($interviewerObject);
			// $sampleLogic->deleteSampleGroup($oldSampleGroupGuid);
			// $interviewerLogic->deleteSampleToInterviewer($interviewerObject);
			$sampleChkbox = $this->input->post('sampleChkbox');	
			if(!empty($sampleChkbox)){
				for (reset($sampleChkbox); $k=key($sampleChkbox); next($sampleChkbox)){
					$sampleArray[] = $sampleChkbox[$k];
				}
				if ($oldSampleGroupGuid == "") {
					$newSampleGroupGuid = $sampleLogic->addSampleGroup($sampleArray);
					$interviewerLogic->addSampleToInterviewer($interviewerObject, $surveyGuid,$newSampleGroupGuid);	
				}else {
					$sampleLogic->addSampleGroupByOldGroupGuid($sampleArray,$oldSampleGroupGuid);
				}
				
				
				//Confirm View Data
				foreach ($sampleChkbox as $row) {
					$sampleObject = new sample_model();
					$sampleObject->setGuid($row);
					$confirmDataArray[] = $sampleModel->getSample($sampleObject);
				}
				$message = '設定樣本名單-樣本更新成功';
			}else {
				$sampleLogic->deleteSampleGroup($oldSampleGroupGuid);
				$interviewerLogic->deleteSampleToInterviewer($interviewerObject);
				
				$message = '設定樣本名單-該訪員已無調查樣本';
			}		
			
			$data['message']=$message;
			$data['interviewer'] = $interviewerObject;
			$data['confirmDataArray'] = $confirmDataArray;			
			$data['title'] = $this->title;
			$data['headerTitle'] = "設定樣本名單-".$interviewerObject->getName();
			$data['action'] = site_url('interviewers/updateInterviewer');
			$data['username'] = $this->doGetUserName();
			$data['link_back'] = anchor('interviewers/interviewerList/','回到列表',array('class'=>'back'));
			$this->load->view('interviewer_assign_sample_confirm_view', $data);
		}


		public function assignSurvey($guid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}			
			$pageRecord = $this->doGetSessionPageRecord();
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$interviewerObject->setGuid($guid);
			
			//取得擁有的問卷物件
			$interviewerLogic = new interviewer_logic_model();
			$ownSurveyList = $interviewerLogic->getInterviewerOwnSurveyGuid($interviewerObject);
			//取得所有問卷
			$surveyObj = new survey_model();
			$surveyObj->setAuthor('schooladmin');
			$allSurveyList = $surveyObj->getAllSurveyListByAuthor($surveyObj);
			
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['allSurveyList'] = $allSurveyList;
			$data['ownSurveyList'] = $ownSurveyList;
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "設定問卷-".$interviewerObject->getName();
			$data['message']="";
			$data['action'] = site_url('interviewers/doAssignSurvey');
			$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('interviewer_assign_survey', $data);
		}
		
		public function doAssignSurvey()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}	
			$pageRecord = $this->doGetSessionPageRecord();
			$message = '';
			
			$interviewerModel = new interviewer_model();
			$interviewerObject = new interviewer_model();
			$sampleLogic = new samplelogic_model();
			$guid = $this->input->post('guid');
			$interviewerObject->setGuid($guid);
			$surveyChkbx = $this->input->post('surveyChkbx');
			//取得所有問卷
			$surveyObj = new survey_model();
			$surveyObj->setAuthor('schooladmin');
			$allSurveyList = $surveyObj->getAllSurveyListByAuthor($surveyObj);
			
			$interviewerLogic = new interviewer_logic_model();
			
			
			if(!empty($surveyChkbx)){
				// for (reset($surveyChkbx); $k=key($surveyChkbx); next($surveyChkbx)){
				$interviewerLogic->deleteAllInterviewerSurveyGuid($interviewerObject);
				foreach($surveyChkbx as $key => $value){
					$interviewerLogic->addInterviewerSurveyGuid($interviewerObject, $value);
					// $interviewerLogic->addSampleToInterviewer($interviewerObject, $surveyChkbx[$k]);
				}				
				$message = '設定問卷-設定問卷成功';
				$data['link_assignSample'] = anchor('interviewers/ownSurveyList/'.$interviewerObject->getGuid(),'繼續設定樣本名單');	
			}else{ 
				//先刪除訪員擁有的問卷列表
				$interviewerLogic->deleteAllInterviewerSurveyGuid($interviewerObject);
				//刪除sampleGroup
				$oldSampleGroupGuid = $interviewerLogic->getSampleGroupGuidToInterviewer($interviewerObject);
				$sampleLogic->deleteSampleGroup($oldSampleGroupGuid);
				//刪除訪員樣本問卷的關係列表
				$interviewerLogic->deleteSampleToInterviewer($interviewerObject, $surveyObj->getGuid());	
					
				$message = '設定問卷-該訪員已無任何問卷';
				$data['link_assignSample'] = '';
				
			}
			
			
			//取得擁有的問卷物件
			
			$ownSurveyList = $interviewerLogic->getInterviewerOwnSurveyGuid($interviewerObject);
			
			$data['message']=$message;
			$data['interviewer'] = $interviewerModel->getInterviewerByGuid($interviewerObject);
			$data['confirmDataArray'] = $ownSurveyList;
			$data['ownSurveyList'] = $ownSurveyList;			
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "設定問卷-".$interviewerObject->getName();
			$data['action'] = site_url('interviewers/updateInterviewer');
			$data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
			$this->load->view('interviewer_assign_survey_confirm_view', $data);
		}

        public function assignSampleGroup($surveyGuid,$interviewerGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $this->load->library('pagination');
            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('','NO','樣本分群名稱','所屬訪員');

            // offset
            $uri_segment = 6;
            $offset = $this->uri->segment($uri_segment);
            $pageRecord = $this->uri->segment(4);
            if($pageRecord == NULL){
                $pageRecord = 0;
            }
            $this->doSetSessionPageRecord($pageRecord);

            //load Data
            $oldSampleGroupGuid = "";
            $sampleGroupModel = new samplegroup_model();
//            $countAll = $sampleGroupModel->getCountAll($surveyGuid);
            $countAll = $sampleGroupModel->getCountAllSampleGroup($surveyGuid);
            $sampleGroupArray = $sampleGroupModel->getSampleGroupWithPage($surveyGuid,$this->limit, $offset)->result();
            $i = 0 + $offset;
            foreach ($sampleGroupArray as $row) {

                $interviewer = new interviewer_model();
                $interviewer->setGuid($row->interviewerGuid);
                $interviewer = $interviewer->getInterviewerByGuid($interviewer);

                if($row->interviewerGuid == $interviewerGuid){
                    $oldSampleGroupGuid = $row->sampleGroupGuid;
                    $radioData = array(
                        'id' => 'sampleGroupChkbox[]',
                        'name' => 'sampleGroupChkbox[]',
                        'value' => $row->sampleGroupGuid,
                        'checked' => TRUE,
                    );

                }else if($row->interviewerGuid != ""){
                    $radioData = array(
                        'id' => 'sampleGroupChkbox[]',
                        'name' => 'sampleGroupChkbox[]',
                        'value' => $row->sampleGroupGuid,
                        'checked' => FALSE,
                        'disabled' => 'disabled'
                    );

                }else {
                    $radioData = array(
                        'id' => 'sampleGroupChkbox[]',
                        'name' => 'sampleGroupChkbox[]',
                        'value' => $row->sampleGroupGuid,
                    );

                }
                $this->table->add_row(
//                    form_radio('sampleGroupChkbox[]',$row->sampleGroupGuid,$checkIsSelf,disabled)
                    form_radio($radioData)
                    ,++$i
                    ,$row->sampleGroupName
                    ,$interviewer->getName()
                    );
            }

            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "送出"
            );

            $config['base_url'] = site_url('interviewers/assignSampleGroup/'.$surveyGuid.'/'.$interviewerGuid.'/index/');
            $config['total_rows'] = $countAll;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['num_links'] = 7;
            $config['first_link'] = '[第1頁]';
            $config['last_link'] = '[最後頁]';
            $config['next_link'] = '下一頁 >';
            $config['prev_link'] = '< 上一頁';
            $this->pagination->initialize($config);

            //目前的調查問卷
            $surveyObject = new survey_model();
            $surveyObject->setGuid($this->doGetSessionSurveyGuid());
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            $data['pagination'] = $this->pagination->create_links();
            $data['form'] = form_open('interviewers/doAssignSampleGroup/'.$this->doGetSessionSurveyGuid().'/'.$interviewerGuid.'/'.$oldSampleGroupGuid);
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['title'] = $this->title;
            $data['headerTitle'] = $surveyObject->getTitle();
            $data['surveyGuid'] = $this->doGetSessionSurveyGuid();
            $data['username'] = $this->doGetUserName();
            $data['toSampleGroupView'] = anchor("samplegroups/sampleGroupList/", '樣本分群列表')
                ."   ".anchor("samples/sampleList/", '樣本列表')
                ."   ".anchor("samplegroups/add/", '建立樣本分群');
            $data['link_back'] = anchor('interviewers/index/'.$pageRecord,'回到列表',array('class'=>'back'));
            $this->load->view('interviewer_assign_sampleGroup', $data);
        }

        public function doAssignSampleGroup($surveyGuid,$interviewerGuid,$oldSampleGroupGuid = "")
        {
            $interviewerLogicModel = new interviewer_logic_model();
            $interviewerObject = new interviewer_model();
            $sampleGroupChkbox = $this->input->post('sampleGroupChkbox');
            //Confirm View Data
            if(!empty($sampleGroupChkbox)){
                foreach($sampleGroupChkbox as $newSampleGroupGuid){
                    $interviewerObject->setGuid($interviewerGuid);
                    if($oldSampleGroupGuid!=""){
                        $interviewerLogicModel->updateSampleSetInterviewerEmpty($surveyGuid,$oldSampleGroupGuid);
                    }
                    $interviewerLogicModel->updateSampleToInterviewer($interviewerObject,$surveyGuid,$newSampleGroupGuid,$newSampleGroupGuid);
                }
                $message = '設定樣本名單-樣本更新成功';
            }else {
                $message = '設定樣本名單失敗';
            }
            redirect('interviewers/','refresh');

        }
	}
	    
    
?>
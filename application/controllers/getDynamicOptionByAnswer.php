<?php
    /**
     * 
     */
    class getDynamicOptionByAnswer extends CI_Controller {
        
        function getDynamicOptionByAnswer() {
            parent::__construct();
			$this->load->model("dynamicoptionbyanswer_model");
        }
		
		function index(){
			$dyAnswer = new dynamicoptionbyanswer_model();
			$dyAnswerGuid = $this->input->get_post('dyaguid',TRUE);
			$dyAnswer->setGuid($dyAnswerGuid);
			$dyAnswerArray = NULL;
			$dyAnswerArray = $dyAnswer->getDynamicOptionArrayByAnswer($dyAnswer);
			if (count($dyAnswerArray)>0){
				for ($i=0; $i < count($dyAnswerArray); $i++) { 
					$tempdyAnswer = new dynamicoptionbyanswer_model();
					$tempdyAnswer = $dyAnswerArray[$i];
					$dyOptionDataA = array('AndLogic'=> array(
                                               'subjectNumber' =>$tempdyAnswer->getBeforeSubjectNumber(),
                                               'targetAnswer'=>$tempdyAnswer->getTargetAnswer()),
                                           'OrLogic'=> array(
                                               'subjectNumber' =>$tempdyAnswer->getOrBeforeSubjectNumber(),
                                               'targetAnswer'=>$tempdyAnswer->getOrTargetAnswer()),
										   'isShow'=>$tempdyAnswer->getIsShow(),
										   'targetOptionIndex'=>$tempdyAnswer->getTargetOption());
					$dyOptionData[] = $dyOptionDataA;  
				}

					//Output JSON
					$this->output
		    			 ->set_content_type('application/json')
		    			 ->set_output(json_encode(array('Header' => array('Status' => "0",
		    			 												  'Doc' => '讀取正常'),
														'Body' => array('getDynamicOptionByAnswer' => 
																		$dyOptionData
																		 ))));	
				
			}else {
					//沒資料或guid錯誤
					//Output JSON
					$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
				
			}
		}
    }
    
?>
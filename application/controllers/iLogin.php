<?php
    /**
     * 
     */
    class iLogin extends CI_Controller {
        
        function iLogin() {
            parent::__construct();
			$this->load->model('interviewer_model');
			$this->load->helper('url');
			$this->load->library('session');
			$this->load->helper('security');
        }
		
		function index()
		{

			$this->load->view('ilogin_view');
			
		}
		
		public function doLogin()
		{
			$interviewer = new interviewer_model();
			
			//取得參數
			$inputUsername =  $this->input->get_post('username',true);
			$inputPassword =  $this->input->get_post('password',true);
			
			//從DB撈資料
			$hashPwd = do_hash($inputPassword,'md5');
			$interviewer->setEmail($inputUsername);
			$interviewer->setPwd($inputPassword);
			
			$data = $interviewer->user_login($interviewer);
			if ($data > 0) {
				//帳號密碼正確
				$interviewer->setEmail($inputUsername);
				$interviewer = $interviewer->getInterviewerByEmail($interviewer);
			
				//寫入訪員的session
				$loginData = array(
                   'username'  => $inputUsername,
                   'password'     => $inputPassword,
                   'interviewer_logged_in' => TRUE
            	);

				$this->session->set_userdata($loginData);
				redirect('interviewers/iUpdatePwd/'.$interviewer->getGuid(), 'refresh');
			} else {
				//帳號密碼錯誤
				echo "帳號密碼錯誤";
				// $this->load->view('admin');
			}
			
		}
    }
    
?>
<?php
    /**
     * 
     */
    ini_set('memory_limit', '256M');

    class surveys extends CI_Controller {

        private $limit = 70;
        private $title = "科技部傳播調查資料庫";
        function surveys() {
            parent::__construct();
            $this->load->model("survey_model");
            $this->load->model("question_model");
            $this->load->model("option_model");
            $this->load->model("optionPicker_model");
            $this->load->model("groupoption_model");
            $this->load->model("dynamicoptionbyanswer_model");
            $this->load->model("pass_model");
            $this->load->model("tips_model");
            $this->load->library('session');
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->helper('security');
        }
        
        function index()
        {
            // $this->sampleList();
            $this->surveylist();
        }
        
        function doCheckIsLogin()
        {           
            return $this->session->userdata('admin_logged_in');
        }
        
        function doGetUserName()
        {           
            return $this->session->userdata('username');
        }
        
        function doSetSessionSurveyGuid($surveyGuid)
        {
            $data = array(
                'surveyGuid' => $surveyGuid
            );
            $this->session->set_userdata($data);
        }
        
        function doGetSessionSurveyGuid()
        {
            return $this->session->userdata('surveyGuid') ;
        }
        
        function doSetSessionPageRecord($pageRecord)
        {
            $pageData = array(
                'pageRecord' => $pageRecord
            );
            $this->session->set_userdata($pageData);
        }
        
        function doGetSessionPageRecord()
        {
            return $this->session->userdata('pageRecord') ;
        }
        
        //問卷調查的清單頁-(登入後的首頁)
        public function surveylist()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
            
            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('調查問卷標題','修改日期','版本','修改 / 設計 / 回覆','動作');
            
            $surveyModel = new survey_model();
            $surveyObject = new survey_model();
            $surveyObject->setAuthor($this->doGetUserName());
            $tempArray = $surveyModel->getAllSurveyListByAuthor($surveyObject);
            foreach ($tempArray as $row) {
                $tempSurvey = new survey_model();
                $tempSurvey = $row;
                $surveyListArray['Guid'][] = $row->getGuid();
                $surveyListArray['Title'][] = $row->getTitle();
                $surveyListArray['Author'][] = $row->getAuthor();
                
                $this->table->add_row(array(
                    'data' => anchor('surveys/editorFull/'.$row->getGuid(),$row->getTitle(),array('class'=>'viewNoImage')),
                    'class' => 'td_title'
                ), array(
                    'data' => $row->getUDateTime(),
                    'class' => 'td_date'
                ), array(
                    'data' => $row->getVersion(),
                    'class' => 'td_ver'
                ), array(
                    'data' => anchor('surveys/updateSurvey/'.$row->getGuid(),'修改調查 ',array('class'=>'edit'))
                            .anchor('surveys/editorFull/'.$row->getGuid() ,'設計調查問卷 ',array('class'=>'edit'))
            //              .anchor('answer/getCsvSpss/'.$row->getGuid().'/'.$row->getVersion().'/view','下載回覆',array('class'=>'update'))
                            .anchor('answer/getAllOptionColumnCSV/'.$row->getGuid().'/'.$row->getVersion(),'下載回覆',array('class'=>'update')),
                    'class' => 'td_download'
                ), array(
                    'data' => anchor('surveys/delete/'.$row->getGuid(),'刪除問卷 ',array('class'=>'delete','onclick'=>"return confirm('確定刪除此份調查？')"))
                            .anchor('front/cleanSurveyData/'.$row->getGuid().'/'.$row->getVersion(),'清除作答',array('class'=>'delete delete_result'))
                            .anchor('surveys/duplicateSurvey/'.$row->getGuid(),'複製問卷  ',array('class'=>'copy')),
                    'class' => 'td_action'
                ));    
            }
            $data['pagination'] = "";
            $data['table'] = $this->table->generate();
            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            
            $data['headerTitle'] = "問卷列表";
            $this->load->view('survey_list_view', $data);
        }

        //問卷調查的Detail頁
        public function surveyDetail($surveyGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);
            $this->doSetSessionSurveyGuid($surveyGuid);
            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = $surveyObject->getTitle();
            $data['surveyGuid'] = $surveyGuid;
            $this->load->view('survey_detail_view', $data);
        }

        public function view($guid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
        }
        
        public function add()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $surveyModel = new survey_model();
            $surveyObject = new survey_model();
    
            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('','','','','');
            $this->table->add_row('調查名稱',form_input('surveyTitle','','size="70"'));
            $this->table->add_row('訪前說明',form_textarea('surveyGreetingText','','style="width: 372px; height: 100px;"'));
            $this->table->add_row('訪後感謝語',form_textarea('surveyThankText','','style="width: 372px; height: 100px;"'));
            $this->table->add_row('維護者',$this->doGetUserName(),form_hidden('surveyAuthor',$this->doGetUserName()));

            //Set Form  
            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "儲存",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );
            
            
            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = "建立調查";
            
            $data['survey'] = $surveyObject;
            $data['message']="";
            $data['form'] = form_open('surveys/doAddSurvey/');
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['action'] = site_url('surveys/doAddSurvey/');
            $data['link_back'] = anchor('surveys','回到列表',array('class'=>'back'));
            $this->load->view('survey_edit_setting', $data);
            

        }
        
        public function doAddSurvey()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $surveyModel = new survey_model();
            $newSurvey = new survey_model();

            $newSurvey->setTitle($this->input->post('surveyTitle'));
            $newSurvey->setGreetingText($this->input->post('surveyGreetingText'));
            $newSurvey->setThankText($this->input->post('surveyThankText'));
            $newSurvey->setAuthor($this->input->post('surveyAuthor'));
            $newSurvey->setVersion('1');
            $newSurvey->addSurvey($newSurvey);
            
            if ($surveyModel->getSurvey($newSurvey)->getGuid()==$newSurvey->getGuid()) {
                //新增成功，導去新增題目頁
                redirect('surveys/editorFull/'.$newSurvey->getGuid(), 'refresh');
            } else {
                //新增失敗，導去失敗頁
                $data['title'] = "國科會傳播調查資料庫";
                $data['headerTitle'] = "新增樣本";
                
                $data['survey'] = $newSurvey;
                $data['message']="";
                $data['action'] = site_url('surveys/addSurvey/');
                $data['link_back'] = anchor('surveys/surveyList/','回到列表',array('class'=>'back'));
                $this->load->view('survey_edit_setting', $data);

            }
            
        }
        
        public function addQuestion()
        {
            
            $surveyModel = new survey_model();
            $surveyObject = new survey_model();
    
            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('','');
            $this->table->add_row('調查名稱',form_input('surveyTitle','','size="100"'));
            $this->table->add_row('訪前說明',form_textarea('surveyGreetingText','','rows="100"'));
            $this->table->add_row('訪後感謝語',form_textarea('surveyThankText'));
            $this->table->add_row('維護者','schooladmin',form_hidden('surveyAuthor','schooladmin'));

            //Set Form  
            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "送出",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );
            
            
            $data['title'] = "國科會傳播調查資料庫";
            $data['headerTitle'] = "新增調查";
            $data['survey'] = $surveyObject;
            $data['message']="";
            $data['form'] = form_open('surveys/addSurvey/');
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['action'] = site_url('surveys/addSurvey/');
            $data['link_back'] = anchor('surveys/surveyList/','回到列表',array('class'=>'back'));
            $this->load->view('survey_edit_setting', $data);
            

        }

        public function showTypeChinese($type)
        {
            if ($type == 'S') {
                return '單選題';
            }else if($type == 'M') {
                return '多選題';
            }else if($type == 'G') {
                return '群組題';
            }else if($type == 'T'){
                return '文字輸入題';
            }else if($type == 'FO'){
                return '四分題';
            }else if($type == 'FI'){
                return '五分題';
            }else if($type == 'P'){
                return '下拉式清單';
            }
            
        }

        public function editorFull($guid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
            $allQuestionChiTypeArray = array();
            $questionModel = new question_model();  
            $allQuestionArray = $questionModel->getAllQuestionsBySurveyGuid($guid);
            $this->doSetSessionSurveyGuid($guid);
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('No.','題號','問題標題','題型','編輯','刪除');
            
            for ($i=0; $i < count($allQuestionArray) ; $i++) {
                $questionObject = $allQuestionArray[$i];
                $cell = array('class' => 'index','data'=>$i+1);
                $this->table->add_row($cell,$questionObject->getSubjectNumber(),$questionObject->getSubject(),$this->showTypeChinese($questionObject->getType())
                ,anchor('/surveys/editQuestion/'.$questionObject->getGuid() ,'編輯',array('class'=>'edit'))
                //,'編輯'
                ,anchor('/surveys/deleteQuestion/'.$questionObject->getGuid() ,'刪除',array('class'=>'delete','onclick'=>"return confirm('確定刪除題目？')"))
                );

                //getChiType
                $allQuestionChiTypeArray[] = $this->showTypeChinese($questionObject->getType());

            }
            //改變題目順序用的id
            $tableTempl = array(
                'table_open'  => '<table id="sort" class="grid">',
//                'row_start'   => '<tr id="itemGuid">',
//                'row_end'     => '</tr>',
//                'cell_alt_start'      => '<td class="index">',
//                'cell_alt_end'        => '</td>',
                'table_close' => '</table>'
                );
            $this->table->set_template($tableTempl);
            //目前的調查問卷
            $surveyObject = new survey_model();
            $surveyObject->setGuid($guid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            
            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = $surveyObject->getTitle();
            $data['guid']=$guid;
            $data['allQuestionsArray'] = $allQuestionArray;
            $data['allQuestionChiTypeArray'] = $allQuestionChiTypeArray;
            $data['message']="";
            $data['table'] = $this->table->generate();
            $data['surveyGuid'] = $guid;
            $data['action'] = site_url('/surveyapp/surveys/addQuestion/'.$guid);
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            $this->load->view('survey_question_editor_full', $data);
            
        }
        
        public function questionBuilder($guid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }           
            
            $submitData = array(
                'name'        => "update",
                'value'       => "更新",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );
            
            $questionObject = new question_model();
            $questionArray = $questionObject->getAllQuestionsBySurveyGuid($guid);
            $data['guid'] = $guid;
            $data['allQuestionArray'] = $questionArray; 
            $this->load->view('survey_question_editor',$data);
        }

        public function newQuestionView()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $submitData = array(
                'name'        => "update",
                'value'       => "更新",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );
            $surveyGuid = $this->doGetSessionSurveyGuid();
            $newQuestion = new question_model();
            $questionGuid = $newQuestion->getGuid();
            $questionArray = $newQuestion->getAllQuestionsBySurveyGuid($surveyGuid);


            //-----給View的資料-----

            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = "新題目";
            $data['message']="";
            $data['surveyGuid'] = $surveyGuid;
            $data['questionGuid'] = $questionGuid;
            $data['allQuestionArray'] = $questionArray;

            $this->load->view('survey_question_editor_new',$data);
        }

        function _getQuestion($questionGuid)
        {
            //GetQuestion
            $questionObject = new question_model();
            $questionObject->setGuid($questionGuid);
            $questionObject = $questionObject->getSingleQuestion($questionObject);
            return $questionObject;
        }

        function _getAllQuestionArray($questionObject,$surveyGuid)
        {
            $questionArray = $questionObject->getAllQuestionsModifySubjectTitleBySurveyGuid($surveyGuid);// 20130506 Show Max 25s Chinese Text
            return $questionArray;
        }

        function _getOption($optionGuid)
        {
            $optionObject = new option_model();
            $optionObject->setGuid($optionGuid);
            return $optionObject->getOptionArray($optionObject);
        }

        function _getOptionText($optionObjectArray)
        {
            $optionText = "";
            for ($i=0; $i<count($optionObjectArray); $i++){
                $tempOption = $optionObjectArray[$i];
                $optionText .= $tempOption->getTitle();
            }
            return $optionText;
        }

        function _getPTypeOption($surveyGuid,$questionObject,$optionObjectArray)
        {
            $questionModel = new question_model();
            $pickerLogic = new optionpicker_model();
            $pickerLogicArray = array();
            $questionArrayNoSelf = array();
            for ($i=0; $i<count($optionObjectArray); $i++){
                $tempOption = $optionObjectArray[$i];
                if ($questionObject->getType()=='P') {
                    //GetPickerLogic
                    $pickerLogic->setGuid($tempOption->getPickerValueArrayGuid());
                    $pickerLogicArray[] = $pickerLogic->getOptionPickerLogic($pickerLogic);
                    $questionArrayNoSelf = $questionModel->getAllQuestionsNotInThisQuestionBySurveyAndQuestionGuidForPType($surveyGuid,$questionObject->getGuid());
                }
            }
            return array("PickerLogic"=>$pickerLogic,"PickerLogicArray"=>$pickerLogicArray,"QuestionArrayNoSelfArray"=>$questionArrayNoSelf);
        }

        function _getGTypeOption($groupOptionGuid)
        {
            $groupMainOptionArray = array();
            $groupSubOptionArray = array();
            $groupOptionObject = new groupoption_model();
            $groupOptionObject->setGuid($groupOptionGuid);
            $tempMainArray = $groupOptionObject->getGroupOptionList($groupOptionObject);
            for ($i=0; $i < count($tempMainArray); $i++) {
                $groupOptionObject = $tempMainArray[$i];
                if ($groupOptionObject->getType()=='0') {
                    $groupMainOptionArray[] = $groupOptionObject;
                } else {
                    $groupSubOptionArray[] = $groupOptionObject;
                }
            }
            return array("GroupMainOptionArray"=>$groupMainOptionArray,"GroupSubOptionArray"=>$groupSubOptionArray);
        }

        function _getTips($tipsGuid)
        {
            $tipsObject = new tips_model();
            return $tipsObject->getTipsByGuid($tipsGuid);
        }

        public function editQuestion($questionGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }           
            
            $submitData = array(
                'name'        => "update",
                'value'       => "更新",
            );

//            $data['questionArrayNoSelf'] = $questionArrayNoSelf;
//            $data['optionText'] = $optionText;
//            $data['optionObjectArray'] = $optionObjectArray;
//            $data['groupMainOptionArray'] = $groupMainOptionArray;
//            $data['groupSubOptionArray'] = $groupSubOptionArray;
//            $data['pickerLogic'] = $pickerLogic;
//            $data['pickerLogicArray'] = $pickerLogicArray;

            //Init
            $surveyGuid = $this->doGetSessionSurveyGuid();
            $questionArrayNoSelf = "";

            //GetQuestion
            $questionObject = $this->_getQuestion($questionGuid);

            //GetTips
            $tipsObject = $this->_getTips($questionObject->getTipsGuid());

            //GetAllQuestionArray
            $questionArray = $this->_getAllQuestionArray($questionObject,$surveyGuid);


            //GetOption
            $groupMainOptionArray = array();
            $groupSubOptionArray = array();
            $optionText = "";
            $optionObjectArray = array();
            $pickerLogic = new optionpicker_model();
            $pickerLogicArray = array();
            if ($questionObject->getType()=='G') {
                $groupOptionArray = $this->_getGTypeOption($questionObject->getGroupOptionGuid());
                $groupMainOptionArray = $groupOptionArray["GroupMainOptionArray"];
                $groupSubOptionArray = $groupOptionArray["GroupSubOptionArray"];
            }else {
                $optionObjectArray = $this->_getOption($questionObject->getOptionGuid());
                $optionText = $this->_getOptionText($optionObjectArray);
                //For P Type Object
                $pTypeOptionArray = $this->_getPTypeOption($surveyGuid,$questionObject,$optionObjectArray);
                $pickerLogic = $pTypeOptionArray["PickerLogic"];
                $pickerLogicArray = $pTypeOptionArray["PickerLogicArray"];
                $questionArrayNoSelf = $pTypeOptionArray["QuestionArrayNoSelfArray"];
            }
            
            //GetSinglePass
            if ($questionObject->getPassGuid()) {
                $passObject = new pass_model();
                $passObject->setGuid($questionObject->getPassGuid());
                $passObject = $passObject->getPass($passObject);
                $data['passObject'] = $passObject;
                $data['passAnswerArray'] = $passObject->getAnswerArray();
                $data['passJumpToArray'] = $passObject->getJumpToArray();
                $data['singlePassSelectStatus'] = $passObject->getSelect();
                $data['questionHasSinglePass'] = 1;
            }else {
                $data['passObject'] = "";
                $data['passAnswerArray'] = "";
                $data['passJumpToArray'] = "";
                $data['questionHasSinglePass'] = 0;
                $data['singlePassSelectStatus'] = 0;
            }

            //GetGroupPass
            if ($questionObject->getGroupPassGuid()) {
                $groupPassObject = new pass_model();
                $groupPassObject->setGuid($questionObject->getGroupPassGuid());
                $groupPassObject = $groupPassObject->getPass($groupPassObject);

                $groupPassSubjectArray = $groupPassObject->getGroupSubjectArray();
                $groupPassAnswerArray = $groupPassObject->getAnswerArray();
                $groupPassAnswerArrayFormat = "";
                foreach($groupPassAnswerArray as $index=>$value){
                    if($index==count($groupPassAnswerArray)-1){
                        $groupPassAnswerArrayFormat .= $value;
                    }else {
                        $groupPassAnswerArrayFormat .= $value.'@';
                    }
                }

                $data['groupPassObject'] = $groupPassObject;
                $data['groupPassSubjectArray'] = $groupPassSubjectArray;
                $data['groupPassAnswerArray'] = $groupPassAnswerArray;
                $data['groupPassAnswerArrayFormat'] = $groupPassAnswerArrayFormat;
                $data['questionHasGroupPass'] = 1;
            }else {
                $groupPassObject = new pass_model();
                $data['groupPassObject'] = "";
                $data['groupPassSubjectArray'] = "";
                $data['groupPassAnswerArray'] = "";
                $data['groupPassAnswerArrayFormat'] = "";
                $data['questionHasGroupPass'] = 0;
            }


            //GetDynamicOptionAnswer
            if ($questionObject->getDynamicOptionAnswerGuid()) {
                $dyAnswer = new dynamicoptionbyanswer_model();
                $dyAnswerGuid = $questionObject->getDynamicOptionAnswerGuid();
                $dyAnswer->setGuid($dyAnswerGuid);
                $dyAnswerArray = $dyAnswer->getDynamicOptionArrayByAnswer($dyAnswer);
                foreach ($dyAnswerArray as $tempdyAnswer) {
                    $subjectNumberArray = $tempdyAnswer->getBeforeSubjectNumber();
                    $targetAnswerArray = $tempdyAnswer->getTargetAnswer();
                    $orSubjectNumberArray = $tempdyAnswer->getOrBeforeSubjectNumber();
                    $orTargetAnswerArray = $tempdyAnswer->getOrTargetAnswer();
                    $dyAnswerIsShow = $tempdyAnswer->getIsShow();
                }
				
				
                $data['dy_SubjectNumber'] = array_merge($subjectNumberArray,$orSubjectNumberArray);
                $data['dy_TargetAnswer'] = array_merge($targetAnswerArray,$orTargetAnswerArray);
                $data['dy_IsShow'] = $dyAnswerIsShow;
                $data['isDyPassLogic'] = 1;

                $dyPassAnswerArray = array_merge($tempdyAnswer->getTargetAnswer(),$tempdyAnswer->getOrTargetAnswer());
                $dyPassAnswerArrayFormat = "";
                foreach($dyPassAnswerArray as $index=>$value){
                    if($index==count($dyPassAnswerArray)-1){
                        $dyPassAnswerArrayFormat .= $value;
                    }else {
                        $dyPassAnswerArrayFormat .= $value.'@';
                    }
                }
                $totalArrayCount = count($subjectNumberArray)+count($orSubjectNumberArray);
                for ($i=0;$i<$totalArrayCount;$i++){
                    if($i>=count($subjectNumberArray)){
                        $isOrLogicArray[$i] = 1;
                    }else {
                        $isOrLogicArray[$i] = 0;
                    }
                }

                $data['isOrAnswer'] = $isOrLogicArray;
                $data['dyPassAnswerArrayFormat'] = $dyPassAnswerArrayFormat;

            }else {
                $isOrLogicArray[0] = 0;
                $data['dy_SubjectNumber'] = "";
                $data['dy_TargetAnswer'] = "";
                $data['dy_IsShow'] = 0;
                $data['isDyPassLogic'] = 0;
                $data['isOrAnswer'] = $isOrLogicArray;
                $data['dyPassAnswerArrayFormat'] = "";
            }

            //-----給View的資料-----
            $data['questionObject'] = $questionObject;
            $data['questionType_chi'] = $this->showTypeChinese($questionObject->getType());
            $data['allQuestionArray'] = $questionArray;
            $data['questionArrayNoSelf'] = $questionArrayNoSelf;
            $data['optionText'] = $optionText;
            $data['optionObjectArray'] = $optionObjectArray;
            $data['groupMainOptionArray'] = $groupMainOptionArray;
            $data['groupSubOptionArray'] = $groupSubOptionArray;
            $data['pickerLogic'] = $pickerLogic;
            $data['pickerLogicArray'] = $pickerLogicArray;
            $data['tipsObject'] = $tipsObject;
             
            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = $questionObject->getSubject(); 
            $data['message']="";
            $data['surveyGuid'] = $surveyGuid;
            $data['action'] = site_url('/surveyapp/surveys/updateQuestion/'.$questionGuid);
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            
            $this->load->view('survey_question_editor_edit',$data);
        }

        function splitOptionFromTextArea($optionTextArea)
        {
            $optionTextArea = rtrim($optionTextArea);
            $optionTextArea = nl2br($optionTextArea);
            $optionTextArea = explode("<br />", $optionTextArea);
            $optionTextArea = preg_replace('/[\s\n\r\t]/', '', $optionTextArea);
            return $optionTextArea;
        }
        
        public function addNewQuestion($guid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
                        
            $surveyGuid = $guid;
            //取得題號
            $questionSubjectNumber = $this->input->post('q_subjectNumber');
            //取得題目
            $questionSubject = $this->input->post('q_subject');
            //取得問題小幫手 2014/04/03-Awei
            $tipsText = $this->input->post('q_tipsText');
            //取得型態
            $questionType = $this->input->post('q_type');
            //取得欄位呈現方式
            $questionOptionOutputForm = $this->input->post('q_optionOutputForm'.$questionType);
            //取得選項
            $questionOption = $this->input->post('optionArea'.$questionType);

            //取得S&M題型
            $smOptionArray = array();
            $smOptionValueArray = array();
            $smOptionIsShowTextViewCKBoxArray = array();
            $smOptionTipsArray = array();
            for ($typeSMsn=1; $typeSMsn <= 100; $typeSMsn++) {
                //
                $optionAreaSM = $this->input->post('optionArea'.$questionType.$typeSMsn);
                if($optionAreaSM != ""){
                    $smOptionArray[] = $optionAreaSM;
                }
                $optionValueSM = $this->input->post('optionValue'.$questionType.$typeSMsn);
                if($optionValueSM != ""){
                    $smOptionValueArray[] = $optionValueSM;
                }
                $optionIsShowTextViewCKBoxSM = $this->input->post('optionIsShowTextViewCKBox'.$questionType.$typeSMsn);
                if(!empty($optionIsShowTextViewCKBoxSM)){
                        $smOptionIsShowTextViewCKBoxArray[] = $optionIsShowTextViewCKBoxSM;
                }else {
                    $smOptionIsShowTextViewCKBoxArray[] = 0;
                }
                $smOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSMsn);

            }

            //取得T題型
            $tOptionArray = array();
            $tOptionTextViewPositionArray = array();
            $tOptionTipsArray = array();
            for ($typeTsn=1; $typeTsn <= 10; $typeTsn++) {
                // $tIndexOptionArray[] = $this->input->post('optionAreaTIndex'.$typeTsn);
                $optionAreaT = $this->input->post('optionAreaT'.$typeTsn);
                $optionTextViewPositionT = $this->input->post('optionTextViewPosition'.$typeTsn);
                if($optionAreaT != ""){
                    $tOptionArray[] = $optionAreaT;
                    $tOptionTextViewPositionArray[] = $optionTextViewPositionT;
                }
                $tOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeTsn);
            }
            //取得P題型的啟始、結束、階層值
            $pOptionArray = array();
            $pDyPickerSubjectArray = array();
            $pickerTypeValueStart = array();
            $pickerTypeValueEnd = array();
            $pickerTypeValueTick = array();
            $isDyPickerCheck = array();
            $pOptionTipsArray = array();
            for ($typePsn=1; $typePsn <= 10; $typePsn++) {
                $optionAreaP = $this->input->post('optionAreaP'.$typePsn);
                if($optionAreaP !=""){
                    $pOptionArray[] = $this->input->post('optionAreaP'.$typePsn);
                    $pickerTypeValueStart[] = $this->input->post('type-P-value-start'.$typePsn);
                    $pickerTypeValueEnd[] = $this->input->post('type-P-value-end'.$typePsn);
                    $pickerTypeValueTick[] = $this->input->post('type-P-value-tick'.$typePsn);
//                    $pDyPickerSubjectArray[] = $this->input->post('dyPickerSelect'.$typePsn);
                    if($this->input->post('isDyPickerCheck'.$typePsn)==true){
                        $isDyPickerCheck[] = "1";
                        $pDyPickerSubjectArray[] = $this->input->post('dyPickerSelect'.$typePsn);
                    }else {
                        $isDyPickerCheck[] = "0";
                        $pDyPickerSubjectArray[] = "";
                    }
                    $pOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typePsn);
                }
            }
            //取得FO題型的選項
            $foOptionArray = array();
            $fiOptionArray = array();
            $foOptionTipsArray = array();
            $fiOptionTipsArray = array();
            for ($typeSn=1; $typeSn <= 4; $typeSn++) {
                $foOptionArray[] = $this->input->post('optionAreaFO'.$typeSn);
                $foOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSn);
            }
            for ($typeSn=1; $typeSn <= 5; $typeSn++) {
                $fiOptionArray[] = $this->input->post('optionAreaFI'.$typeSn);
                $fiOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSn);
            }

            //取得G題型的選項
            $groupMainObjectArray = array();
            $groupSubObjectArray = array();
            $groupSubTypeObjectArray = array();
            $groupSubTextAreaObjectArray = array();
            $groupSubValueObjectArray = array();
            $gOptionIsShowTextViewCKBoxArray = array();
            $gMainOptionTipsArray = array();
            $gSubOptionTipsArray = array();
            for ($typeGsn=1; $typeGsn <= 20; $typeGsn++) {
                //Main
                $groupMainObjectArray[] = $this->input->post('type-G-main-group'.$typeGsn);
                $gMainOptionTipsArray[] = $this->input->post('optionGMain-Tips'.$typeGsn);
                //Sub
                $typeGSubCount = 40;
                for ($typeGSubsn=1; $typeGSubsn <= $typeGSubCount; $typeGSubsn++) {
                    //Type
                    $groupSubTypeObjectArray[$typeGsn][$typeGSubsn] = $typeGsn;
                    //Text
                    $groupSubTextAreaObjectArray[$typeGsn][$typeGSubsn] = $this->input->post('optionAreaG'.$typeGsn.'_'.$typeGSubsn);
                    //value
                    $groupSubValueObjectArray[$typeGsn][$typeGSubsn] = $this->input->post('optionValueG'.$typeGsn.'_'.$typeGSubsn);
                    //TextBox
                    $optionIsShowTextViewCKBoxG = $this->input->post('optionIsShowTextViewCKBoxG'.$typeGsn.'_'.$typeGSubsn);
                    if(!empty($optionIsShowTextViewCKBoxG)){
                        $gOptionIsShowTextViewCKBoxArray[$typeGsn][$typeGSubsn] = $optionIsShowTextViewCKBoxG;
                    }else {
                        $gOptionIsShowTextViewCKBoxArray[$typeGsn][$typeGSubsn] = 0;
                    }
                    $gSubOptionTipsArray[$typeGsn][$typeGSubsn] = $this->input->post('optionGSub-Tips'.$typeGsn.'_'.$typeGSubsn);
                }
            }
            $groupSubObjectArray['Type'] = $groupSubTypeObjectArray;
            $groupSubObjectArray['Text'] = $groupSubTextAreaObjectArray;
            $groupSubObjectArray['Value'] = $groupSubValueObjectArray;
            $groupSubObjectArray['IsShowTextView'] = $gOptionIsShowTextViewCKBoxArray;

            //取得單題跳題的選項
            $isSinglePassLogic = "";
            $isGroupPassLogic = "";
            $isDyPassLogic = "";
            $singlePassAnswerValue = "";
            $singlePassSelectStatus = "1";
            $groupPassAnswerArray = array();
            $groupPassSubjectArray = array();
            $singleJumpToSubjectNumber = "";
            $groupJumpToSubjectNumber = "";
            $dyPassSubjectArray = array();
            $dyPassAnswerArray = array();
            $orDyPassSubjectArray = array();
            $orDyPassAnswerArray = array();

            $dyIsShow = "";

            //題目順序
            $questionObject = new question_model();
            $priority = $questionObject->getNewPriorityNumber($surveyGuid);

            $this->makeQuestion($surveyGuid,$questionSubjectNumber, $questionSubject, $tipsText, $questionType, $questionOptionOutputForm,
                $smOptionArray,$smOptionValueArray,$smOptionIsShowTextViewCKBoxArray , $pOptionArray,$pickerTypeValueStart,
                $pickerTypeValueEnd, $pickerTypeValueTick, $pDyPickerSubjectArray, $isDyPickerCheck, $tOptionArray, $tOptionTextViewPositionArray, $foOptionArray, $fiOptionArray,
             $groupMainObjectArray, $groupSubObjectArray, $isSinglePassLogic, $isGroupPassLogic, $isDyPassLogic,
                $singlePassAnswerValue,$groupPassAnswerArray, $singleJumpToSubjectNumber, $groupJumpToSubjectNumber,$singlePassSelectStatus,
                $groupPassSubjectArray,$dyPassSubjectArray,$dyPassAnswerArray,$orDyPassSubjectArray,$orDyPassAnswerArray,$dyIsShow,
                $priority,$smOptionTipsArray,$tOptionTipsArray,$pOptionTipsArray,$foOptionTipsArray,$fiOptionTipsArray,$gMainOptionTipsArray,$gSubOptionTipsArray);
            $this->updateSurveyUpdateTime($surveyGuid);
            redirect('surveys/editorFull/'.$surveyGuid, 'refresh');
        }

        public function updateQuestion($questionGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
            $this->doDeleteQuestion($questionGuid);
            //真奇怪
            $surveyGuid = $this->doGetSessionSurveyGuid();
            //取得題號
            $questionSubjectNumber = $this->input->post('q_subjectNumber');
            //取得題目
            $questionSubject = $this->input->post('q_subject');
            //取得問題小幫手 2014/04/03-Awei
            $tipsText = $this->input->post('q_tipsText');
            //取得型態
            $questionType = $this->input->post('q_type');
            //取得欄位呈現方式
            $questionOptionOutputForm = $this->input->post('q_optionOutputForm'.$questionType);
            //取得選項
            $questionOption = $this->input->post('optionArea'.$questionType);

            //取得S&M題型
            $smOptionArray = array();
            $smOptionValueArray = array();
            $smOptionIsShowTextViewCKBoxArray = array();
            $smOptionTipsArray = array();
            for ($typeSMsn=1; $typeSMsn <= 100; $typeSMsn++) {
                //
                $optionAreaSM = $this->input->post('optionArea'.$questionType.$typeSMsn);
                if($optionAreaSM != ""){
                    $smOptionArray[] = $optionAreaSM;
                }
                $optionValueSM = $this->input->post('optionValue'.$questionType.$typeSMsn);
                if($optionValueSM != ""){
                    $smOptionValueArray[] = $optionValueSM;
                }
                $optionIsShowTextViewCKBoxSM = $this->input->post('optionIsShowTextViewCKBox'.$questionType.$typeSMsn);
                if(!empty($optionIsShowTextViewCKBoxSM)){
                    $smOptionIsShowTextViewCKBoxArray[] = $optionIsShowTextViewCKBoxSM;
                }else {
                    $smOptionIsShowTextViewCKBoxArray[] = 0;
                }
                $smOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSMsn);
            }

            //取得T題型
            $tOptionArray = array();
            $tOptionTextViewPositionArray = array();
            $tOptionTipsArray = array();
            for ($typeTsn=1; $typeTsn <= 10; $typeTsn++) {
                // $tIndexOptionArray[] = $this->input->post('optionAreaTIndex'.$typeTsn);
                $optionAreaT = $this->input->post('optionAreaT'.$typeTsn);
                $optionTextViewPositionT = $this->input->post('optionTextViewPosition'.$typeTsn);
                if($optionAreaT != ""){
                    $tOptionArray[] = $optionAreaT;
                    $tOptionTextViewPositionArray[] = $optionTextViewPositionT;
                }
                $tOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeTsn);
            }

            //取得P題型
            $pOptionArray = array();
            $pDyPickerSubjectArray = array();
            $pickerTypeValueStart = array();
            $pickerTypeValueEnd = array();
            $pickerTypeValueTick = array();
            $isDyPickerCheck = array();
            $pOptionTipsArray = array();
            for ($typePsn=1; $typePsn <= 10; $typePsn++) {
                $optionAreaP = $this->input->post('optionAreaP'.$typePsn);
                if($optionAreaP !=""){
                    $pOptionArray[] = $this->input->post('optionAreaP'.$typePsn);
                    //取得P題型的啟始、結束、階層值
                    $pickerTypeValueStart[] = $this->input->post('type-P-value-start'.$typePsn);
                    $pickerTypeValueEnd[] = $this->input->post('type-P-value-end'.$typePsn);
                    $pickerTypeValueTick[] = $this->input->post('type-P-value-tick'.$typePsn);
                    if($this->input->post('isDyPickerCheck'.$typePsn)==true){
                        $isDyPickerCheck[] = "1";
                        $pDyPickerSubjectArray[] = $this->input->post('dyPickerSelect'.$typePsn);
                    }else {
                        $isDyPickerCheck[] = "0";
                        $pDyPickerSubjectArray[] = "";
                    }
                    $pOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typePsn);
                }
            }

            //取得FO FI題型的選項
            $foOptionArray = array();
            $foOptionTipsArray = array();
            $fiOptionArray = array();
            $fiOptionTipsArray = array();
            for ($typeSn=1; $typeSn <= 4; $typeSn++) {
                $foOptionArray[] = $this->input->post('optionAreaFO'.$typeSn);
                $foOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSn);
            }
            for ($typeSn=1; $typeSn <= 5; $typeSn++) {
                $fiOptionArray[] = $this->input->post('optionAreaFI'.$typeSn);
                $fiOptionTipsArray[] = $this->input->post('option'.$questionType.'-Tips'.$typeSn);
            }

            //取得G題型的選項
            $groupMainObjectArray = array();
            $groupSubObjectArray = array();
            $groupSubTypeObjectArray = array();
            $groupSubTextAreaObjectArray = array();
            $groupSubValueObjectArray = array();
            $gOptionIsShowTextViewCKBoxArray = array();
            $gMainOptionTipsArray = array();
            $gSubOptionTipsArray = array();
            for ($typeGsn=1; $typeGsn <= 20; $typeGsn++) {
                //Main
                $groupMainObjectArray[] = $this->input->post('type-G-main-group'.$typeGsn);
                $gMainOptionTipsArray[] = $this->input->post('optionGMain-Tips'.$typeGsn);
                //Sub
                $typeGSubCount = 40;
                for ($typeGSubsn=1; $typeGSubsn <= $typeGSubCount; $typeGSubsn++) {
                    //Type
                    $groupSubTypeObjectArray[$typeGsn][$typeGSubsn] = $typeGsn;
                    //Text
                    $groupSubTextAreaObjectArray[$typeGsn][$typeGSubsn] = $this->input->post('optionAreaG'.$typeGsn.'_'.$typeGSubsn);
                    //value
                    $groupSubValueObjectArray[$typeGsn][$typeGSubsn] = $this->input->post('optionValueG'.$typeGsn.'_'.$typeGSubsn);
                    //TextBox
                    $optionIsShowTextViewCKBoxG = $this->input->post('optionIsShowTextViewCKBoxG'.$typeGsn.'_'.$typeGSubsn);
                    if(!empty($optionIsShowTextViewCKBoxG)){
                        $gOptionIsShowTextViewCKBoxArray[$typeGsn][$typeGSubsn] = $optionIsShowTextViewCKBoxG;
                    }else {
                        $gOptionIsShowTextViewCKBoxArray[$typeGsn][$typeGSubsn] = 0;
                    }
                    $gSubOptionTipsArray[$typeGsn][$typeGSubsn] = $this->input->post('optionGSub-Tips'.$typeGsn.'_'.$typeGSubsn);
//                    echo $typeGsn.'@'.$typeGSubsn.':'.$this->input->post('optionAreaG'.$typeGsn.'_'.$typeGSubsn).':'.$this->input->post('optionValueG'.$typeGsn.'_'.$typeGSubsn).'<br>';
                }
            }
            $groupSubObjectArray['Type'] = $groupSubTypeObjectArray;
            $groupSubObjectArray['Text'] = $groupSubTextAreaObjectArray;
            $groupSubObjectArray['Value'] = $groupSubValueObjectArray;
            $groupSubObjectArray['IsShowTextView'] = $gOptionIsShowTextViewCKBoxArray;


            //單一條件跳題邏輯
            $isSinglePassLogic = $this->input->post('isSinglePassLogic');
            $singlePassSelectStatus = $this->input->post('singlePassSelectStatus');
            for ($singlePassSn=1; $singlePassSn<=10 ; $singlePassSn++){

                $singlePassAnswerValueArray[] = $this->input->post('passAnswer'.$singlePassSn);
                $singleJumpToSubjectNumberArray[] = $this->input->post('singleJumpToSubjectNumber'.$singlePassSn);
            }
            if(count($singleJumpToSubjectNumberArray) == 0 && count($singlePassAnswerValueArray) == 0)
            {
                $isSinglePassLogic = 0;
            }
//            echo $isSinglePassLogic.':'.$singlePassSelectStatus.':'.$singlePassAnswerValueArray[0].','.$singlePassAnswerValueArray[1].':'.$singleJumpToSubjectNumberArray[0].','.$singleJumpToSubjectNumberArray[1];
            //多條件跳題邏輯
            $isGroupPassLogic = $this->input->post('isGroupPassLogic');

            $groupPassSubjectArray = array();
            $groupPassAnswerArray = array();
            for ($groupPassSn=1; $groupPassSn<=10 ; $groupPassSn++){
                $tempGroupPassSubject = $this->input->post('groupPassSubjectNumber'.$groupPassSn);
                //groupPassSubjectNumber的value有2個值用'@'隔開
                if($tempGroupPassSubject != ""){
                    $afterExplode = explode('@',$tempGroupPassSubject);
                    $groupPassSubjectArray[] = $afterExplode[1];
                    $groupPassAnswerArray[] = $this->input->post('groupPassAnswerNumber'.$groupPassSn);
//                    echo $isGroupPassLogic;
                }
            }
            if(count($groupPassSubjectArray) == 1 && count($groupPassAnswerArray) == 1){
                $isGroupPassLogic = 0;
            }
            $groupJumpToSubjectNumber = $this->input->post('groupJumpToSubjectNumber');


            /*
             * DyPassAnswer 2013/05/08 add by Awei
             */
            $isDyPassLogic = $this->input->post('isDyPassLogic');
            $dyIsShow = $this->input->post('dyIsShow');
            $allDyPassSubjectArray = array();
            $allDyPassAnswerArray = array();
            $dyPassSubjectArray = array();
            $dyPassAnswerArray = array();
            $orDyPassSubjectArray = array();
            $orDyPassAnswerArray = array();
            for ($dyPassSn=1;$dyPassSn<=10;$dyPassSn++){
                $tempDyPassSubject = $this->input->post('dyPassSubjectNumber'.$dyPassSn);
                //value有2個值用'@'隔開
                if($tempDyPassSubject != ""){
                    $afterExplode = explode('@',$tempDyPassSubject);
                    $allDyPassSubjectArray[] = $afterExplode[1];
                    $allDyPassAnswerArray[] = $this->input->post('dyPassAnswerNumber'.$dyPassSn);
                }
                if($this->input->post('isOrAnswer'.$dyPassSn)=='1'){
                    $tempIsOrAnswerArray[$dyPassSn-1] = $this->input->post('isOrAnswer'.$dyPassSn);
                }else {
                    $tempIsOrAnswerArray[$dyPassSn-1] = '0';
                }

            }
            for($k =0; $k<count($allDyPassAnswerArray);$k++){
                if($tempIsOrAnswerArray[$k]=='1'){
                    $orDyPassSubjectArray[] = $allDyPassSubjectArray[$k];
                    $orDyPassAnswerArray[] = $allDyPassAnswerArray[$k];
                }else {
                    $dyPassSubjectArray[] = $allDyPassSubjectArray[$k];
                    $dyPassAnswerArray[] = $allDyPassAnswerArray[$k];
                }

            }

            //取得題目順序
            $priority = $this->input->post('priority');

            //寫入資料庫
            $this->makeQuestion($surveyGuid,$questionSubjectNumber,$questionSubject,$tipsText,$questionType,$questionOptionOutputForm,
                $smOptionArray,$smOptionValueArray,$smOptionIsShowTextViewCKBoxArray,$pOptionArray,
                $pickerTypeValueStart,$pickerTypeValueEnd,$pickerTypeValueTick,$pDyPickerSubjectArray,$isDyPickerCheck,$tOptionArray,$tOptionTextViewPositionArray,$foOptionArray,$fiOptionArray,
                $groupMainObjectArray,$groupSubObjectArray,$isSinglePassLogic, $isGroupPassLogic, $isDyPassLogic, $singlePassSelectStatus,
                $singlePassAnswerValueArray,$groupPassAnswerArray, $singleJumpToSubjectNumberArray, $groupJumpToSubjectNumber,
                $groupPassSubjectArray,$dyPassSubjectArray,$dyPassAnswerArray,$orDyPassSubjectArray,$orDyPassAnswerArray,$dyIsShow,
                $priority,$smOptionTipsArray,$tOptionTipsArray,$pOptionTipsArray,$foOptionTipsArray,$fiOptionTipsArray,$gMainOptionTipsArray,$gSubOptionTipsArray);
            $this->updateSurveyUpdateTime($surveyGuid);

            redirect('surveys/editorFull/'.$surveyGuid, 'refresh');
        }

        public function makeQuestion($guid,$questionSubjectNumber,$questionSubject,$tipsText,$questionType,$questionOptionOutputForm,
            $smOptionArray,$smOptionValueArray,$smOptionIsShowTextViewCKBoxArray,$pOptionArray,
            $pickerTypeValueStart,$pickerTypeValueEnd,$pickerTypeValueTick,$pDyPickerSubjectArray,$isDyPickerCheck,$tOptionArray,$tOptionTextViewPositionArray,$foOptionArray,$fiOptionArray,
            $groupMainObjectArray,$groupSubObjectArray,$isSinglePassLogic, $isGroupPassLogic, $isDyPassLogic,$singlePassSelectStatus,
            $singlePassAnswerValueArray,$groupPassAnswerArray, $singleJumpToSubjectNumberArray, $groupJumpToSubjectNumber,
            $groupPassSubjectArray,$dyPassSubjectArray,$dyPassAnswerArray,$orDyPassSubjectArray,$orDyPassAnswerArray,$dyIsShow,
            $priority,$smOptionTipsArray,$tOptionTipsArray,$pOptionTipsArray,$foOptionTipsArray,$fiOptionTipsArray,$gMainOptionTipsArray,$gSubOptionTipsArray)
        {
            
            $optionArray = array();
            $optionValueArray = array();
            $optionIsShowTextArray = array();
            $optionTipsArray = array();

            $surveyGuid = $guid;
            $questionObject = new question_model();
            $optionObject = new option_model();

            //去多餘空白
            $questionSubjectNumber = trim($questionSubjectNumber);
            $questionSubjectNumber = preg_replace('/\s(?=\s)/', '', $questionSubjectNumber);
            $questionSubjectNumber = preg_replace('/[\n\r\t]/', '', $questionSubjectNumber);

            //取得G題型
            $groupOptionGuid = do_hash(microtime(),'md5');
            $groupSubTypeObjectArray = $groupSubObjectArray['Type'];
            $groupSubTextAreaObjectArray = $groupSubObjectArray['Text'];
            $groupSubValueObjectArray = $groupSubObjectArray['Value'];
            $gOptionIsShowTextViewCKBoxArray = $groupSubObjectArray['IsShowTextView'];

            for ($typeGsn=1; $typeGsn <= count($groupMainObjectArray); $typeGsn++) {
                if($groupMainObjectArray[$typeGsn-1] != ""){
                    $mainGroupOption = new groupoption_model();
                    $mainGroupOption->setGuid($groupOptionGuid);
                    $mainGroupOption->setType('0');
                    $mainGroupOption->setIndex($typeGsn);
                    $mainGroupOption->setText($groupMainObjectArray[$typeGsn-1]);
                    $mainGroupOption->setValue($typeGsn);
                    $mainGroupOption->setIsShowTextView('0');
                    $mainGroupOption->setTextViewPosition(NULL);
                    $mainGroupOption->setOptionTips($gMainOptionTipsArray[$typeGsn-1]);
                    $mainGroupOption->add($mainGroupOption);
                }
                for ($typeGSubsn=1; $typeGSubsn <= count($groupSubTextAreaObjectArray); $typeGSubsn++) {
//                    echo $groupSubTextAreaObjectArray[0][1];
                    if ($groupSubTextAreaObjectArray[$typeGsn][$typeGSubsn] != "") {
                            $subGroupOption = new groupoption_model();
                            $subGroupOption->setGuid($groupOptionGuid);
                            $subGroupOption->setType($groupSubTypeObjectArray[$typeGsn][$typeGSubsn]);
                            $subGroupOption->setIndex($typeGSubsn);
                            $subGroupOption->setText($groupSubTextAreaObjectArray[$typeGsn][$typeGSubsn]);
                            $subGroupOption->setValue($groupSubValueObjectArray[$typeGsn][$typeGSubsn]);
                            $subGroupOption->setIsShowTextView($gOptionIsShowTextViewCKBoxArray[$typeGsn][$typeGSubsn]);
                            $subGroupOption->setTextViewPosition(NULL);
                            $subGroupOption->setOptionTips($gSubOptionTipsArray[$typeGsn][$typeGSubsn]);
                            $subGroupOption->add($subGroupOption);
                    }
                }
            }

            //取得選項
            if ($questionType == 'S' || $questionType == 'M') {
                for ($i=0; $i < count($smOptionArray); $i++) {
                    if ($smOptionArray[$i] != "") {
                        $optionArray[$i] = $smOptionArray[$i];
                        $optionValueArray[$i] = $smOptionValueArray[$i];
                        $optionIsShowTextArray[$i] = $smOptionIsShowTextViewCKBoxArray[$i];
                        $optionTipsArray[$i] = $smOptionTipsArray[$i];
                    }
                }
            }else if ($questionType == 'T') {
                for ($i=0; $i < count($tOptionArray); $i++) {
                    if ($tOptionArray[$i] != "") {
                        $optionArray[$i] = $tOptionArray[$i];
                        $optionTipsArray[$i] = $tOptionTipsArray[$i];
                    } 
                }
            }else if ($questionType == 'P') {
                for ($i=0; $i < count($pOptionArray); $i++) {
                    if ($pOptionArray[$i] != "") {
                        $optionArray[$i] = $pOptionArray[$i];
                        $optionTipsArray[$i] = $pOptionTipsArray[$i];
                    } 
                }
            }else if ($questionType == 'FO') {
                $optionArray = $foOptionArray;
                $optionTipsArray = $foOptionTipsArray;
            }else if ($questionType == 'FI') {
                $optionArray = $fiOptionArray;
                $optionTipsArray = $fiOptionTipsArray;
            }else if ($questionType == 'G') {
                $optionArray = array();
            }
//            else {
//              //從TextArea取得選項
//              $optionArray = $this->splitOptionFromTextArea($questionOption);
//          }
            
            
            //寫入Option---Start
            $optionGuid = $optionObject->getGuid();
            
            for ($i=0 ;$i<count($optionArray);$i++){
                $indexAndValue = $i+1;
                $optionObject->setGuid($optionGuid);
                $optionObject->setSurveyGuid($surveyGuid);
                $optionObject->setOptionIndex($indexAndValue);
                $optionObject->setTitle($optionArray[$i]);
                $optionObject->setOptionTips($optionTipsArray[$i]);

                if($questionType == 'S' || $questionType == 'M'){
                    $optionObject->setOptionValue($optionValueArray[$i]);
                    $optionObject->setIsShowTextView($optionIsShowTextArray[$i]);
                }else if ($questionType == 'P'){  //下拉式清單題型輸入範圍

                    $optionPickerObject = new optionPicker_model();
            

                    if($isDyPickerCheck[$i]=="1"){
                        $optionObject->setDyPickerMin($pickerTypeValueStart[$i]);
                        $optionObject->setDyPickerTick($pickerTypeValueTick[$i]);
                        $optionObject->setDyPickerSubject($pDyPickerSubjectArray[$i]);
                        $optionObject->setDyPickerMax($pDyPickerSubjectArray[$i]);
                        $optionObject->setPickerValueArrayGuid("");
                    }else {
                        //取得P題型的啟始、結束、階層值
                        $optionPickerObject->setStartValue($pickerTypeValueStart[$i]);
                        $optionPickerObject->setEndValue($pickerTypeValueEnd[$i]);
                        $optionPickerObject->setTickValue($pickerTypeValueTick[$i]);

                        $pickerValueArray = array();
                        for ($pickerScope = $pickerTypeValueStart[$i]; $pickerScope <= $pickerTypeValueEnd[$i] ; $pickerScope += $pickerTypeValueTick[$i]){
                            $pickerValueArray[] = $pickerScope;
                        }
                        $optionPickerValueArrayGuid = $optionObject->addPickerValueArray($pickerValueArray,$optionPickerObject);
                        $optionObject->setDyPickerMin(NULL);
                        $optionObject->setDyPickerTick(NULL);
                        $optionObject->setDyPickerSubject(NULL);
                        $optionObject->setDyPickerMax(NULL);
                        $optionObject->setPickerValueArrayGuid($optionPickerValueArrayGuid);
                    }

                }else if($questionType == 'T'){
                    $optionObject->setTextViewPosition($tOptionTextViewPositionArray[$i]);
                }else {
                    $optionObject->setOptionValue($indexAndValue);
                    $optionObject->setIsShowTextView(0);
                }
                
                $optionObject->addOption($optionObject);
            }
            //---End

            //寫入SinglePass---Start
            if($singleJumpToSubjectNumberArray != "" && $singlePassAnswerValueArray!="" && $isSinglePassLogic) {
                $passAnswerArray[] = $singlePassAnswerValueArray;

                $singlePassObject = new pass_model();
                $singlePassObject->setSubjectNumber($questionSubjectNumber);
                $singlePassObject->setJumpTo($this->_addCommaToString($singleJumpToSubjectNumberArray));
                $singlePassObject->setAnswerArray($singlePassAnswerValueArray);
                $singlePassObject->setGroupSubjectArray($groupPassSubjectArray);
                $singlePassObject->setIsGroupPass('0');
                $singlePassObject->setSelect($singlePassSelectStatus);
                $singlePassObject->add($singlePassObject);
                $questionObject->setPassGuid($singlePassObject->getGuid());
            }
            //---End

            //寫入GroupPass---Start
            if($groupJumpToSubjectNumber != "" && count($groupPassAnswerArray) > 0 && $isGroupPassLogic) {
                $groupPassObject = new pass_model();
                $groupPassObject->setSubjectNumber($questionSubjectNumber);
                $groupPassObject->setJumpTo($groupJumpToSubjectNumber);
                $groupPassObject->setAnswerArray($groupPassAnswerArray);
                $groupPassObject->setGroupSubjectArray($groupPassSubjectArray);
                $groupPassObject->setIsGroupPass('1');
                $groupPassObject->setSelect('1');
                $groupPassObject->add($groupPassObject);
                $questionObject->setGroupPassGuid($groupPassObject->getGuid());
            }
            //---End

            //寫入DyPass(前置跳題)---Start
            if(count($dyPassSubjectArray) > 0 && count($dyPassAnswerArray) > 0 && $isDyPassLogic) {
                $dyPassTargetOptionArray[] = $questionSubjectNumber;
                $dyPassObject = new dynamicoptionbyanswer_model();
                $dyPassObject->setBeforeSubjectNumber($dyPassSubjectArray);
                $dyPassObject->setTargetAnswer($dyPassAnswerArray);
                $dyPassObject->setOrBeforeSubjectNumber($orDyPassSubjectArray);
                $dyPassObject->setOrTargetAnswer($orDyPassAnswerArray);
                $dyPassObject->setTargetOption($dyPassTargetOptionArray);
                $dyPassObject->setIsShow($dyIsShow);
                $dyPassObject = $dyPassObject->add($dyPassObject);
                $questionObject->setDynamicOptionAnswerGuid($dyPassObject->getGuid());
            }

            //---End


            //寫入問題小幫手---Start
            $newTipsObject = new tips_model();
            $newTipsObject->setTipsText($tipsText);
            $newTipsObject->setTipsIndex(1);
            $newTipsGuid = $newTipsObject->insertTips();
            $questionObject->setTipsGuid($newTipsGuid);
            //---End


            //寫入Question---Start
            $questionObject->setSubjectNumber($questionSubjectNumber);
            $questionObject->setSubject($questionSubject);
            $questionObject->setType($questionType);
            $questionObject->setOptionGuid($optionGuid);
            $questionObject->setMultiSelectedLimit(1);
            $questionObject->setIsRandom(0);
            $questionObject->setPriority($priority);
            $questionObject->setOptionOutputForm($questionOptionOutputForm);
            if ($questionType == 'G') {
                $questionObject->setGroupOptionGuid($groupOptionGuid);
            }
            $questionObject->addQuestion($questionObject, $surveyGuid);
            //---End

            
        }

        function _addCommaToArray($array)
        {
            $resultArray = array();
            $string = "";
            for($i=0 ;$i<count($array);$i++)
            {
                if($array[$i]!=Null){
                    if($i!=0){
                        $string .= ','.$array[$i];
                    }else {
                        $string .= $array[$i];
                    }
                }
            }
            $resultArray[] = $string;
            return $resultArray;
        }

        function _addCommaToString($array)
        {
            $string = "";
            for($i=0 ;$i<count($array);$i++)
            {
                if($array[$i]!=Null){
                    if($i!=0){
                        $string .= ','.$array[$i];
                    }else {
                        $string .= $array[$i];
                    }
                }

            }
            return $string;
        }

        public function updateSurveyUpdateTime($surveyGuid)
        {
            $surveyModel = new survey_model();
            $surveyModel->setGuid($surveyGuid);
            $surveyModel->getSurvey($surveyModel);
            $surveyModel->updateSurvey($surveyModel);
        }
        
        public function updateSurvey($surveyGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('','','','','');
            $this->table->add_row('調查名稱',form_input('surveyTitle',$surveyObject->getTitle(),'size="70"'));
            $this->table->add_row('訪前說明',form_textarea('surveyGreetingText',$surveyObject->getGreetingText(),'style="width: 372px; height: 100px;"'));
            $this->table->add_row('訪後感謝語',form_textarea('surveyThankText',$surveyObject->getThankText(),'style="width: 372px; height: 100px;"'));
            $this->table->add_row('建立時間',form_input('cDateTime',$surveyObject->getCDateTime(),'size="70" disabled="disabled"'));
            $this->table->add_row('修改時間',form_input('uDateTime',$surveyObject->getUDateTime(),'size="70" disabled="disabled"'));
            $this->table->add_row('版本',form_input('version',$surveyObject->getVersion(),'onkeyup="this.value=this.value.replace(/\D/g,\'\')"'));

            $hidden = array('surveyGuid' => $surveyGuid, 'author' => $surveyObject->getAuthor());

            //Set Form
            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "儲存",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );


            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = "修改調查";

            $data['survey'] = $surveyObject;
            $data['message']="";
            $data['form'] = form_open('surveys/doUpdateSurvey/','',$hidden);
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['action'] = site_url('surveys/doUpdateSurvey/');
            $data['link_back'] = anchor('surveys','回到列表',array('class'=>'back'));
            $this->load->view('survey_edit_setting', $data);

        }
        
        public function doUpdateSurvey()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }
            $surveyObject = new survey_model();
            $surveyObject->setGuid($this->input->post('surveyGuid'));
            $surveyObject->setTitle($this->input->post('surveyTitle'));
            $surveyObject->setGreetingText($this->input->post('surveyGreetingText'));
            $surveyObject->setThankText($this->input->post('surveyThankText'));
            $surveyObject->setCDateTime($this->input->post('cDateTime'));
            $surveyObject->setUDateTime($this->input->post('uDateTime'));
            $surveyObject->setVersion($this->input->post('version'));
            $surveyObject->setAuthor($this->input->post('author'));
            $surveyObject->updateSurvey($surveyObject);

            redirect('surveys','refresh');
        }
        
        public function delete($guid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

//          $surveyModel = new survey_model();
//          $surveyModel->setGuid($guid);
//          $surveyModel->deleteSurvey($surveyModel);
//            $optionModel = new option_model();
//            $optionModel->deleteOptionFromSurveyGuid($guid);

            $surveyModel = new survey_model();
            $surveyModel->setGuid($guid);
            $surveyModel->closeSurvey($surveyModel);

            redirect('surveys', 'refresh');
            
        }
                
        public function deleteQuestion($questionGuid)
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }   
            // $surveyGuid = $this->doGetSessionSurveyGuid();
            // $questionObject = new question_model();
            // $questionObject->setGuid($questionGuid);
            // $questionObject->getSingleQuestion($questionObject);
            // $questionObject->deleteQuestion($questionObject);
//          
            // $optionObject = new option_model();
            // $optionObject->delete($questionObject->getOptionGuid());
//          
            // $groupOptionModel = new groupoption_model();
            // $groupOptionModel->delete($questionObject->getGroupOptionGuid());
//          
            // $passObjectModel = new pass_model();
            // $passObjectModel->delete($questionObject->getPassGuid());

            $surveyGuid = $this->doGetSessionSurveyGuid();
            $this->doDeleteQuestion($questionGuid);
            $this->updateSurveyUpdateTime($surveyGuid);
            redirect('surveys/editorFull/'.$surveyGuid, 'refresh');
        
        }
        
        function doDeleteQuestion($questionGuid)
        {

            $questionObject = new question_model();
            $questionObject->setGuid($questionGuid);
            $questionObject = $questionObject->getSingleQuestion($questionObject);
            $questionObject->deleteQuestion($questionObject);
            
            $passObject = new pass_model();
            $passObject->delete($questionObject->getPassGuid());
            
            $optionObject = new option_model();
            $optionObject->delete($questionObject->getOptionGuid());
            
            $groupOptionModel = new groupoption_model();
            $groupOptionModel->delete($questionObject->getGroupOptionGuid());
            
            $passObjectModel = new pass_model();
            $passObjectModel->delete($questionObject->getPassGuid());
            $passObjectModel->delete($questionObject->getGroupPassGuid());

            $dyPassObjectModel = new dynamicoptionbyanswer_model();
            $dyPassObjectModel->delete($questionObject->getDynamicOptionAnswerGuid());

            $tipsModel = new tips_model();
            $tipsModel->setTipsGuid($questionObject->getTipsGuid());
            $tipsModel->deleteTips();
        }

        function doUpdateQuestionPriority()
        {
            $surveyGuid = $this->doGetSessionSurveyGuid();
            $action = $this->input->post('action');
            $questionPriorityArray = $this->input->post('itemGuid');
            if($action == 'updateQuestionPriority'){
                $questionModel = new question_model();
                $questionModel->updateQuestionPriority($surveyGuid,$questionPriorityArray);

                echo '問題排序變更完成';
            }
            $this->updateSurveyUpdateTime($surveyGuid);

        }

        public function getOptionArrayJsonForAjax($subjectGuid)
        {
            $question = new question_model();
            $question->setGuid($subjectGuid);
            $question = $question->getSingleQuestion($question);

            $option = new option_model();
            $optionModel = new option_model();
            $option->setGuid($question->getOptionGuid());
            $option = $option->getOptionArray($option);

            $resultData = null;
            $array = array();
            if($question->getType()=='P'){
                $pickerValueArray = $optionModel->getPickerValueArray($option[0]);
                foreach($pickerValueArray as $object){
                    $array = array("optionText"=>$object,"optionValue"=>$object);
                    $resultData[] = $array;
                }
            } else {
                foreach($option as $object){
                    $array = array("optionText"=>$object->getTitle(),"optionValue"=>$object->getOptionIndex());
                    $resultData[] = $array;
                }
            }
            $jsonData = json_encode(array("Data"=>$resultData));
            echo $jsonData;

        }

        public function getQuestionArrayJsonForAjaxForPType($surveyGuid,$subjectGuid)
        {
            $questionModel = new question_model();
            $questionArray = $questionModel->getAllQuestionsNotInThisQuestionBySurveyAndQuestionGuidForPType($surveyGuid,$subjectGuid);

            $resultData = null;
            foreach($questionArray as $object){
                $array = array("QuestionSubjectNumber"=>$object->getSubjectNumber()
                                ,"QuestionTitle"=>$object->getSubject());
                $resultData[] = $array;
            }
            $jsonData = json_encode(array("Data"=>$resultData));
            echo $jsonData;

        }

        public function getQuestionArrayJsonForAjaxForPTypeAdd($surveyGuid)
        {

            echo $this->getQuestionArrayJsonForAjaxForPType($surveyGuid,'');

        }


        public function getOptionArray($subjectGuid)
        {
            $question = new question_model();
            $question->setGuid($subjectGuid);
            $question = $question->getSingleQuestion($question);

            $option = new option_model();
            $option->setGuid($question->getOptionGuid());
            $option = $option->getOptionArray($option);

            if($question->getType()=='P'){
                $resultData = null;
                $pickerValueArray = $option->getPickerValueArray($option[0]);
                foreach($pickerValueArray as $object){
                    $array = array("optionText"=>$object,"optionValue"=>$object);
                    $resultData[] = $array;
                }
            }else {
                $resultData = null;
                $array = array();
                foreach($option as $object){
                    $array = array("optionText"=>$object->getTitle(),"optionValue"=>$object->getOptionIndex());
                    $resultData[] = $array;
                }
            }

            return $resultData;

        }

        public function duplicateSurvey ($surveyGuid){
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $surveyModel = new survey_model();
            $surveyObject = new survey_model();
            $surveyObject->setGuid($surveyGuid);
            $surveyObject = $surveyModel->getSurvey($surveyObject);

            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('','','','','');
            $this->table->add_row('複製調查的新名稱',form_input('surveyTitle',$surveyObject->getTitle().'_複製','size="70"'));

            $hidden = array('surveyGuid' => $surveyObject->getGuid());

            //Set Form
            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "開始複製",
//              'onClick'     => "return confirm('確定刪除選擇的樣本?');"
            );


            $data['title'] = $this->title;
            $data['username'] = $this->doGetUserName();
            $data['headerTitle'] = "修改調查";

            $data['survey'] = $surveyObject;
            $data['message']="";
            $data['form'] = form_open('surveys/doDuplicateSurvey/'.$surveyObject->getGuid());
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['action'] = site_url('surveys/doDuplicateSurvey/');
            $data['link_back'] = anchor('surveys','回到列表',array('class'=>'back'));
            $this->load->view('survey_edit_setting', $data);
        }

        public function doDuplicateSurvey ($surveyGuid){
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $surveyModel = new survey_model();
            $newSurvey = new survey_model();
            $oldSurvey = new survey_model();
            $oldSurvey->setGuid($surveyGuid);
            $oldSurvey = $surveyModel->getSurvey($oldSurvey);

            $newSurvey->setTitle($this->input->post('surveyTitle'));
            $newSurvey->setGreetingText($oldSurvey->getGreetingText());
            $newSurvey->setThankText($oldSurvey->getThankText());
            $newSurvey->setAuthor($oldSurvey->getAuthor());
            $newSurvey->setVersion($oldSurvey->getVersion());
            $newSurvey->addSurvey($newSurvey);

            $this->duplicateQuestion($oldSurvey->getGuid(),$newSurvey->getGuid());

            if ($surveyModel->getSurvey($newSurvey)->getGuid()==$newSurvey->getGuid()) {
                //新增成功，導去新增題目頁
                redirect('surveys/surveys/', 'refresh');
            } else {
                //新增失敗，導去失敗頁
                $data['title'] = "國科會傳播調查資料庫";
                $data['headerTitle'] = "新增樣本";

                $data['survey'] = $newSurvey;
                $data['message']="";
                $data['action'] = site_url('surveys/addSurvey/');
                $data['link_back'] = anchor('surveys/surveyList/','回到列表',array('class'=>'back'));
                $this->load->view('survey_edit_setting', $data);

            }
        }

        function duplicateQuestion ($oldSurveyGuid,$newSurveyGuid){
            $questionModel = new question_model();
            $questionGuidArray = $questionModel->getQuestionListBySurveyGuid($oldSurveyGuid);

            foreach ($questionGuidArray as $questionGuid) {
                $this->doDuplicateQuestion($newSurveyGuid,$questionGuid);
            }
        }

        function doDuplicateQuestion ($newSurveyGuid,$questionGuid){

            $optionGuid = $this->getNewGuid();

            $questionObject = $this->_getQuestion($questionGuid);
            $optionObjectArray = $this->_getOption($questionObject->getOptionGuid());
            foreach ($optionObjectArray as $optionObject){
                $optionObject->setGuid($optionGuid);
                if($optionObject->getPickerValueArrayGuid()!=""){
                    $optionPickerLogicObject = new optionpicker_model();
                    $optionPickerLogicObject->setGuid($optionObject->getPickerValueArrayGuid());
                    $optionPickerLogicObject = $optionPickerLogicObject->getOptionPickerLogic($optionPickerLogicObject);
                    $pickerValueArray = array();
                    $startValue = $optionPickerLogicObject->getStartValue();
                    $endValue = $optionPickerLogicObject->getEndValue();
                    $tickValue = $optionPickerLogicObject->getTickValue();
                    for ($pickerScope = $startValue; $pickerScope <= $endValue ; $pickerScope += $tickValue){
                        $pickerValueArray[] = $pickerScope;
                    }
                    $optionPickerValueArrayGuid = $optionObject->addPickerValueArray($pickerValueArray,$optionPickerLogicObject);
                    $optionObject->setPickerValueArrayGuid($optionPickerValueArrayGuid);
                }else {
                    $optionObject->setPickerValueArrayGuid("");
                }
                $optionObject->setSurveyGuid($newSurveyGuid);
                $optionObject->addOption($optionObject);
            }

            //Single Pass
            if ($questionObject->getPassGuid()!=NULL) {
                $newSinglePassGuid = $this->getNewGuid();
                $singlePassObject = new pass_model();
                $singlePassObject->setGuid($questionObject->getPassGuid());
                $singlePassObject = $singlePassObject->getPass($singlePassObject);
                $singlePassObject->setGuid($newSinglePassGuid);
                $singlePassObject->add($singlePassObject);
                $questionObject->setPassGuid($newSinglePassGuid);
            }

            if ($questionObject->getGroupPassGuid()!=NULL) {
                $newGroupPassGuid = $this->getNewGuid();
                $groupPassObject = new pass_model();
                $groupPassObject->setGuid($questionObject->getGroupPassGuid());
                $groupPassObject = $groupPassObject->getPass($groupPassObject);
                $groupPassObject->setGuid($newGroupPassGuid);
                $groupPassObject->add($groupPassObject);
                $questionObject->setGroupPassGuid($newGroupPassGuid);
            }

            if ($questionObject->getDynamicOptionAnswerGuid()!=NULL) {
                $dynamicOptionAnswerObjectArray = array();
                $dynamicOptionAnswerObject = new dynamicoptionbyanswer_model();
                $dynamicOptionAnswerObject->setGuid($questionObject->getDynamicOptionAnswerGuid());
                $dynamicOptionAnswerObjectArray = $dynamicOptionAnswerObject->getDynamicOptionArrayByAnswer($dynamicOptionAnswerObject);
                foreach($dynamicOptionAnswerObjectArray as $tempDynamicOptionAnswerObject){
                    $tempDynamicOptionAnswerObject = $tempDynamicOptionAnswerObject->add($tempDynamicOptionAnswerObject);
                    $questionObject->setDynamicOptionAnswerGuid($tempDynamicOptionAnswerObject->getGuid());
                }

            }

            $newQuestionGuid = $this->getNewGuid();
            $questionObject->setOptionGuid($optionGuid);
            $questionObject->setGuid($newQuestionGuid);
            $questionObject->addQuestion($questionObject,$newSurveyGuid);


        }

        function getNewGuid(){
            $mkTime = explode(" ", microtime(false));
            $microTime = explode(".",$mkTime[0]);
            $randNumber = rand(0,10000);
            return $randNumber.$microTime[1];
//            return do_hash(microtime(),'md5');
        }
    }



?>
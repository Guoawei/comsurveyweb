<?php
    /**
     * 
     */
    class answer extends CI_Controller {
        
		private $title = "科技部傳播調查資料庫";
		
        function answer() {
            parent::__construct();
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->model("survey_model");		
			$this->load->model("interviewer_model");
			$this->load->model("samplelogic_model");
			$this->load->model("sample_model");
			$this->load->model("surveyanswer_model");
			$this->load->model("outputcsv_model");
        }
		
		public function index()
		{
			$this->surveylist();
		}
		
		function doCheckIsLogin()
		{			
			return $this->session->userdata('admin_logged_in');
		}
		
		function doGetUserName()
		{			
			return $this->session->userdata('username');
		}

		function doSetSessionSurveyGuid($surveyGuid)
		{
			$data = array(
				'surveyGuid' => $surveyGuid
			);
			$this->session->set_userdata($data);
		}
		
		function doGetSessionSurveyGuid()
		{
			return $this->session->userdata('surveyGuid') ;
		}

		public function surveylist()
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading('調查問卷標題','日期時間','修改日期','設計 / 回覆','動作');
			
			$surveyModel = new survey_model();
			$surveyObject = new survey_model();
			$surveyObject->setAuthor("schooladmin");
			$tempArray = $surveyModel->getAllSurveyListByAuthor($surveyObject);
			foreach ($tempArray as $row) {
				$tempSurvey = new survey_model();
				$tempSurvey = $row;
				$surveyListArray['Guid'][] = $row->getGuid();
				$surveyListArray['Title'][] = $row->getTitle();
				$surveyListArray['Author'][] = $row->getAuthor();
				$this->table->add_row(anchor('answer/listInterviewerOfSurvey/'.$row->getGuid(),
				$row->getTitle(),array('class'=>'viewNoImage')),$row->getCDateTime(),$row->getUDateTime()
				,anchor('surveys/editorFull/'.$row->getGuid() ,'設計調查問卷',array('class'=>'edit'))
				.anchor('answer/getCsvSpss/'.$row->getGuid().'/'.$row->getVersion().'/view','下載回覆',array('class'=>'update'))
				,anchor('surveys/editorFull/'.$row->getGuid() ,'刪除',array('class'=>'delete')));
			}
			$data['pagination'] = "";
			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "問卷列表";
			$this->load->view('survey_list_view', $data);
			
			
		}

		//列出所有訪員及完成的樣本數目
		public function listInterviewerOfSurvey($surveyGuid)
		{
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading('訪員名稱','帳號', '已完成樣本數','應調查樣本數','功能');
			
			$interviewer = new interviewer_model();
			$surveyModel = new survey_model();
			$surveyObject = new survey_model();
			$currentSurveyObject = new survey_model();
			$currentSurveyObject->setGuid($surveyGuid);
			$currentSurveyObject = $surveyModel->getSurvey($currentSurveyObject); 
			$surveyObject->setGuid($surveyGuid);
			$surveyObject = $surveyModel->getSurveyInterviewers($surveyObject);
			$interviewerGuidArray = $surveyObject->getInterviewersArray();
			$interviewerDataArray = array();
			$i=0;
			if (count($interviewerGuidArray)>0) {
				foreach ($interviewerGuidArray as $row) {
					$interviewer->setGuid($row);
					$interviewer = $interviewer->getInterviewerByGuid($interviewer);				
					$interviewerDataArray['Guid'][] = $interviewer->getGuid();
					$interviewerDataArray['Name'][] = $interviewer->getName();
					$interviewerDataArray['Email'][] = $interviewer->getEmail();
								
					$sampleLogicModel = new samplelogic_model();
					$sampleModel = new sample_model();
					//取得樣本清單
					$sampleArray = $sampleLogicModel->getSampleInGroup($surveyGuid, $interviewer->getGuid());
					//總調查數
					$sampleTotal = count($sampleArray);
					
					$surveyAnswerModel = new surveyanswer_model();
					$surveyAnswerObject = new surveyanswer_model();
					$surveyAnswerObject->setSurveyGuid($surveyGuid);
					$surveyAnswerObject->setInterviewerGuid($interviewer->getGuid());
					$surveyAnswerAlreadyUploadCount = $surveyAnswerModel->getAlreadyUploadCount($interviewer->getGuid(),$surveyGuid,$currentSurveyObject->getVersion());
					
					$this->table->add_row(
					$interviewer->getName(),
					$interviewer->getEmail(),
					$surveyAnswerAlreadyUploadCount,
					$sampleTotal,
					anchor('answer/listSampleOfInterviewer/'.$interviewer->getGuid().'/'.$surveyGuid,
					'樣本名單',array('class'=>'view')));
					$i++;
				}
			}else {
				$this->table->add_row('無調查結果，請在App中點擊「上傳問卷結果」。');
			}

			$data['pagination'] = "";
            $data['toSampleGroupView'] = "";
			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = $currentSurveyObject->getTitle();
			$data['surveyGuid'] = $this->doGetSessionSurveyGuid();
//			$data['add'] = anchor("answer/getCsvSpss/".$currentSurveyObject->getGuid()."/".$currentSurveyObject->getVersion()."/csv", '<img src="'.base_url().'/style/images/downloadAnswer.png" alt="下載" />');
            $data['add'] = anchor("answer/getAllOptionColumnCSV/".$currentSurveyObject->getGuid()."/".$currentSurveyObject->getVersion(), '<img src="'.base_url().'/style/images/downloadAnswer.png" alt="下載" />');
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            $data['form'] = "";
            $data['formSubmit'] = "";
			$this->load->view('survey_detail_view', $data);

			// $this->load->view('answer_interviewer_view',$data);
		}
		
		//取得調查中該訪員的樣本清單
		public function listSampleOfInterviewer($interviewerGuid,$surveyGuid)
		{
			
			if (!$this->doCheckIsLogin()) {
				redirect('webLogin', 'refresh');
				return 0;
			}
			
			//Set Table
			$this->load->library('table');
        	$this->table->set_empty("&nbsp;");
        	$this->table->set_heading('編號','樣本名稱', '調查狀態', '上傳時間');
			
			//surveyObject
			$surveyModel = new survey_model();
			$currentSurveyObject = new survey_model();
			$currentSurveyObject->setGuid($surveyGuid);
			$currentSurveyObject = $surveyModel->getSurvey($currentSurveyObject); 

            if(!$this->db->table_exists('survey_answer_'.$surveyGuid.'_'.$currentSurveyObject->getVersion()))
            {
                $data['pagination'] = "";
                $data['table'] = $this->table->generate();
                $data['title'] = $this->title;
                $data['username'] = $this->doGetUserName();
                $data['headerTitle'] = "問卷列表";
                $data['surveyGuid'] = $this->doGetSessionSurveyGuid();
                $data['error_msg'] = "訪員未上傳問卷答案";
                $this->load->view('list_view', $data);

                return true;
            }
			
			$sampleLogicModel = new samplelogic_model();
			$sampleModel = new sample_model();
			$sampleDataArray = array();
			//取得樣本清單
			$sampleArray = $sampleLogicModel->getSampleInGroup($surveyGuid, $interviewerGuid);
			$i=0;
			foreach ($sampleArray as $row) {
				$i++;
				$sampleObject = new sample_model();
				$sampleObject->setGuid($row->getGuid());
				$sampleObject = $sampleModel->getSample($sampleObject);
				$sampleDataArray['Guid'][] = $sampleObject->getGuid();
				$sampleDataArray['Name'][] = $sampleObject->getName();

				$surveyAnswerModel = new surveyanswer_model();
				$surveyAnswerObject = new surveyanswer_model();
				$surveyAnswerObject->setSurveyGuid($surveyGuid);
				$surveyAnswerObject->setInterviewerGuid($interviewerGuid);
				$surveyAnswerIsAlreadyUpload = $surveyAnswerModel->getIsAlreadyUpload($interviewerGuid,$row->getGuid(),$surveyGuid,$currentSurveyObject->getVersion());

				$surveyAnswerCDateTime = "";
				if ($surveyAnswerIsAlreadyUpload=="1") {
					$surveyAnswerIsAlreadyUpload="完成";
					$surveyAnswerCDateTime = $surveyAnswerModel->getAnswerCDateTime($interviewerGuid,$row->getGuid(),$surveyGuid,$currentSurveyObject->getVersion());
				}
				$this->table->add_row(
				$i,
				$sampleObject->getName(),
				$surveyAnswerIsAlreadyUpload,
				$surveyAnswerCDateTime
				/*anchor('answer/listSampleOfInterviewer/'.$sampleObject->getGuid().'/'.$surveyGuid,
				'查看問卷答案',array('class'=>'view'))*/);
			}
			$data['pagination'] = "";
			$data['table'] = $this->table->generate();
			$data['title'] = $this->title;
			$data['username'] = $this->doGetUserName();
			$data['headerTitle'] = "問卷列表";
			$data['surveyGuid'] = $this->doGetSessionSurveyGuid();
            $data['error_msg'] = "";
			$this->load->view('list_view', $data);
			// $this->load->view('answer_sample_view',$data);
		}

		public function getCsvSpss($surveyGuid,$surveyVersion,$kind)
		{
			// $list = array
			// (
			// "George,John,Thomas,USA",
			// "James,Adrew,Martin,USA",
			// );
// 			
			// $file = fopen("contacts.csv","w");
// 			
			// foreach ($list as $line)
			  // {
			  // fputcsv($file,split(',',$line));
			  // }
// 			
			// fclose($file);
			
			if ($kind=='csv') {
				$answerModel = new surveyanswer_model();
				$csvModel = new outputcsv_model();
				//訪員id,樣本id,問卷id,問卷版本
				// $answerModel->getAnswerToCSV($surveyGuid, $surveyVersion,1);\
				// $answerModel->getCSVForAllColumn($surveyGuid, $surveyVersion,1);	
				$csvModel->getCSVForOnlyHaveValueColumn($surveyGuid, $surveyVersion);
			}else if($kind=='spss'){
				$answerModel = new surveyanswer_model();
				$answerModel->getAnswerToSPSS($surveyGuid, $surveyVersion);
			}else {
				$surveyModel = new survey_model();
				$survey = new survey_model();
				$survey->setGuid($surveyGuid);
				$survey = $surveyModel->getSurvey($survey);
				$data['pagination'] = "";
				$data['surveyTitle'] = $survey->getTitle();
				$data['surveyGuid'] = $survey->getGuid();
				$data['surveyVersion'] = $survey->getVersion();
				$data['title'] = $this->title;
				$data['headerTitle'] = "問卷列表";
				$this->load->view('answer_csvspss_view',$data);				
			}

		}

        public function getAllOptionColumnCSV($surveyGuid,$surveyVersion)
        {
            $surveyModel = new survey_model();
            $surveyModel->setGuid($surveyGuid);
            $surveyModel = $surveyModel->getSurvey($surveyModel);

            $csvModel = new outputcsv_model();
            $isHaveData = $csvModel->getCSVForFullColumn($surveyGuid,$surveyVersion);
            if(!$isHaveData){
                $data['headerTitle'] = $surveyModel->getTitle();
                $data['username'] = $this->doGetUserName();
                $data['msg'] = "尚未上傳回覆資料，請開啟平板App，點擊上傳答案";
                $this->load->view('answer_nodata_view',$data);
            }

        }

		
		function iconv2big5($str){
				//return iconv("UTF-8",'BIG5//TRANSLIT//IGNORE',$str);
				return mb_convert_encoding($str,"big5","utf-8");
		}
		
		function makesearchcsv($path,$data_fields){
				$sql="SELECT * from schools WHERE";
			
				if($data_fields['start_year']&&$data_fields['start_month']&&$data_fields['end_year']&&$data_fields['end_month']){
					if($data_fields['end_month']==2){
						$endday=28;
					}else if($data_fields['end_month']==4||$data_fields['end_month']==6||$data_fields['end_month']==9||$data_fields['end_month']==11){
						$endday=30;
					}else{
						$endday=31;
					}
					$sql.=" mdate > '".$data_fields['start_year']."-".$data_fields['start_month']."-1' AND  mdate < '".$data_fields['end_year']."-".$data_fields['end_month']."-".$endday."'";
				}else if($data_fields['start_year']&&$data_fields['start_month']&&($data_fields['end_year']=='')&&($data_fields['end_month']=='')){
					if($data_fields['start_month']==2){
						$endday=28;
					}else if($data_fields['start_month']==4||$data_fields['start_month']==6||$data_fields['start_month']==9||$data_fields['start_month']==11){
						$endday=30;
					}else{
						$endday=31;
					}
					$sql.=" mdate > '".$data_fields['start_year']."-".$data_fields['start_month']."-1' AND mdate < '".$data_fields['start_year']."-".$data_fields['start_month']."-".$endday."'";
				}
				$sql.=" ORDER BY code ASC;";
				
				if($result=mysql_query($sql)){
					if(mysql_num_rows($result)>=1){
						$workstation1='代碼,系所名稱,科名稱,最後修改'.'
						';
						while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
							$workstation1.=$row['code'].",".$row['title_b'].",".$row['title_5'].",".$row['mdate'].'
							';
				
						}
					}
				}
				$workstation1=$this->iconv2big5($workstation1);
				file_put_contents($path.'SearchResult.csv',$workstation1);
				$filesdone=$path.'SearchResult.csv';
		
				return $filesdone;
		}


	}
		
    
?>
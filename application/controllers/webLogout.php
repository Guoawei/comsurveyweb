<?php
    /**
     * 
     */
    class webLogout extends CI_Controller {
        
        function webLogout() {
            parent::__construct();
			$this->load->helper('url');
			$this->load->library('session');
        }
		
		function index()
		{
			$this->doLogout();
			redirect('webLogin', 'refresh');
		}
		
		function doLogout()
		{
			$loginData = $this->session->all_userdata();
			$this->session->unset_userdata($loginData);	
		}
    }
    
?>
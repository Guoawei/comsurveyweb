<?php
    /**
     * 
     */
    class updateApp extends CI_Controller {
        
        function updateApp() {
            parent::__construct();
			$this->load->model("updateapp_model");			
        }
		
		function index()
		{
			$update = new updateapp_model();
			$update = $update->getUpdateApp();			


			//Output JSON
			$this->output
	    		 ->set_content_type('application/json')
	    		 ->set_output(json_encode(array('Header' => array('Status' => "0",
	    			 											  'Doc' => '讀取正常'),
												  'Body' => array('updateApp' => array(
																  'version' => $update->getVer(),
																  'url' => $update->getUrl(),
																  'udateTime' => $update->getUdateTime()
																  )))));	

								
			
		}
    }
    
?>
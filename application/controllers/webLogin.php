<?php
    /**
     * 
     */
    class webLogin extends CI_Controller {
        
        function webLogin() {
            parent::__construct();
			$this->load->helper('url');
			$this->load->helper('security');
			$this->load->library('session');
			$this->load->model('account_model');
			$this->load->model('interviewer_model');
						
        }
		
		function index()
		{
			$data['error'] = "";
			$this->load->view('admin',$data);
			
		}
	
		public function doLogin()
		{
			
			// $account = "schooladmin";
			// $pwd = "123456";
			
			//取得參數
			//管理員登入
			$account = new account_model();
			$inputAccount =  $this->input->get_post('username',true);
			$inputPassword =  $this->input->get_post('password',true);			
			$account->setAccount($inputAccount);
			$account->setPassword($inputPassword);
			$account = $account->doLogin($account);
			
			if ($account->getAccount()!=NULL) {
				//帳號密碼正確
				$loginData = array(
                   'username'  => $account->getName(),
                   'account'   => $account->getAccount(),
                   'password'  => $account->getPassword(),
                   'admin_logged_in' => TRUE
              	);
				
				$this->session->set_userdata($loginData);
				
				redirect('surveys', 'refresh');
				
			}else {
				//訪員登入
				$interviewer = new interviewer_model();
				$interviewerAccount = new account_model();
				$interviewerAccount->setAccount($inputAccount);
				$hashPwd = do_hash($inputPassword,'md5');
				$interviewerAccount->setPassword($hashPwd);
				
				$interviewerAccount = $interviewer->user_login($interviewerAccount);
				if ($interviewerAccount->getAccount()!=NULL) {
				//帳號密碼正確
					$interviewer->setEmail($interviewerAccount->getAccount());
					$interviewer = $interviewer->getInterviewerByEmail($interviewer);
				
					//寫入訪員的session
					$loginData = array(
                   		'username'  => $interviewerAccount->getName(),
                   		'account'   => $interviewerAccount->getAccount(),
                   		'password'     => $interviewerAccount->getPassword(),
	               	    'interviewer_logged_in' => TRUE
            		);

					$this->session->set_userdata($loginData);
					redirect('interviewers/iUpdatePwd/'.$interviewer->getGuid(), 'refresh');
					
				}else {
				//帳號密碼錯誤
					//echo "帳號密碼錯誤";
					$data['error'] = "帳號或密碼錯誤";
					$this->load->view('admin',$data);
				}
			} 
			 

			
		}
    }
    
?>
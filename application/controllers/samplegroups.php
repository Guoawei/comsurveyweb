<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Awei
 * Date: 12/12/18
 * Time: 下午10:35
 * To change this template use File | Settings | File Templates.
 */
    class samplegroups extends CI_Controller {
        private $limit = 50;
        private $title = "科技部傳播調查資料庫";

        function samplegroups() {
            parent::__construct();
            $this->load->library('session');
            $this->load->model("survey_model");
            $this->load->model("samplelogic_model");
            $this->load->model("interviewer_logic_model");
            $this->load->model("samplegroup_model");
            $this->load->helper('url');
            $this->load->helper('form');

        }

        function index()
        {
            $this->sampleGroupList();
        }

        function doCheckIsLogin()
        {
            return $this->session->userdata('admin_logged_in');
        }

        function doGetUserName()
        {
            return $this->session->userdata('username');
        }

        function doSetSessionSurveyGuid($surveyGuid)
        {
            $data = array(
                'surveyGuid' => $surveyGuid
            );
            $this->session->set_userdata($data);
        }

        function doGetSessionSurveyGuid()
        {
            return $this->session->userdata('surveyGuid') ;
        }

        function doSetSessionPageRecord($pageRecord)
        {
            $pageData = array(
                'pageRecord' => $pageRecord
            );
            $this->session->set_userdata($pageData);
        }

        function doGetSessionPageRecord()
        {
            return $this->session->userdata('pageRecord') ;
        }

        public function sampleGroupList()
        {
            if (!$this->doCheckIsLogin()) {
                redirect('webLogin', 'refresh');
                return 0;
            }

            $this->load->library('pagination');
            //Set Table
            $this->load->library('table');
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('NO','樣本分群名稱','所屬訪員','功能');

            // offset
            $uri_segment = 4;
            $offset = $this->uri->segment($uri_segment);
            $pageRecord = $this->uri->segment(3);
            if($pageRecord == NULL){
                $pageRecord = 0;
            }
            $this->doSetSessionPageRecord($pageRecord);
            $surveyGuid = $this->doGetSessionSurveyGuid();


            //load Data
            $sampleGroupModel = new samplegroup_model();
            $countAll = $sampleGroupModel->getCountAll($surveyGuid);
            $sampleGroupArray = $sampleGroupModel->getSampleGroupWithPage($surveyGuid,$this->limit, $offset)->result();
            $i = 0 + $offset;

            foreach ($sampleGroupArray as $row) {

                $interviewer = new interviewer_model();
                $interviewer->setGuid($row->interviewerGuid);
                $interviewer = $interviewer->getInterviewerByGuid($interviewer);

                $this->table->add_row(
                    //form_checkbox('sampleGroupSelected['.$row->guid.']', $row->guid, FALSE),
                    ++$i
                    ,$row->sampleGroupName
                    ,$interviewer->getName()
                    ,anchor('samplegroups/update/'.$row->sampleGroupGuid,'編輯',array('class'=>'edit'))
                    .anchor('samplegroups/delete/'.$row->sampleGroupGuid,'刪除',array('class'=>'delete','onclick'=>"return confirm('確定刪除此樣本？')")));

            }

            $submitData = array(
                'name'        => "mysubmit",
                'value'       => "刪除",
                'onClick'     => "return confirm('確定刪除選擇的分群列表?');"
            );

            //分頁功能
            $uri_segment = 4;

            $config['base_url'] = site_url('samplegroups/sampleGroupList/index/');
            $config['total_rows'] = $countAll;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;
            $config['num_links'] = 7;
            $config['first_link'] = '[第1頁]';
            $config['last_link'] = '[最後頁]';
            $config['next_link'] = '下一頁 >';
            $config['prev_link'] = '< 上一頁';
            $this->pagination->initialize($config);

            //目前的調查問卷
            $surveyObject = new survey_model();
            $surveyObject->setGuid($this->doGetSessionSurveyGuid());
            $surveyObject = $surveyObject->getSurvey($surveyObject);

            $data['pagination'] = $this->pagination->create_links();
            $data['formSubmit'] = form_submit($submitData);
            $data['table'] = $this->table->generate();
            $data['title'] = $this->title;
            $data['headerTitle'] = $surveyObject->getTitle();
            $data['surveyGuid'] = $this->doGetSessionSurveyGuid();
            $data['username'] = $this->doGetUserName();
            $data['add'] = anchor("samples/add/", '<img src="'.base_url().'/style/images/addSample.png" alt="Delete" />');
            $data['deleteSurveyAction'] = anchor('surveys/delete/'.$this->doGetSessionSurveyGuid(),'刪除問卷',array('class'=>'survey-left-menu','onclick'=>"return confirm('確定刪除此份調查？')"));
            $data['toSampleGroupView'] = anchor("samplegroups/sampleGroupList/", '<img src="'.base_url().'/style/images/inSampleGroup.png" alt="Delete" />')
                                                ."   ".anchor("samples/sampleList/", '<img src="'.base_url().'/style/images/sample.png" alt="Delete" />')
                                                ."   ".anchor("samplegroups/add/", '<img src="'.base_url().'/style/images/addSampleGroup.png" alt="Delete" />');
            $data['form'] = "";
            $data['formSubmit'] = "";
            $this->load->view('survey_detail_view', $data);
        }

        public function add()
        {

            $sampleGroupObject = new samplegroup_model();
            $sampleArray = $sampleGroupObject->getSampleArray();
            $inListArray = array();
            for($i=0;$i<count($sampleArray);$i++) {
                $sampleObject = new sample_model();
                $sampleObject->setGuid($sampleArray[$i]);
                $sampleObject = $sampleObject->getSample($sampleObject);
                $inListArray[] = $sampleObject;
            }

            $outListArray = $sampleGroupObject->getNotInSampleArrayList($this->doGetSessionSurveyGuid());

            $data['sampleGroupObject'] = $sampleGroupObject;
            $data['sampleArray'] = array();
            $data['inListArray'] = $inListArray;
            $data['outListArray'] = $outListArray;
            $data['title'] = $this->title;
            $data['headerTitle'] = "建立樣本分群";
            $data['message']="";
            $data['action'] = site_url('samplegroups/doAdd/'.$this->doGetSessionSurveyGuid());
            $data['link_back'] = anchor('samplegroups/','回到列表',array('class'=>'back'));
            $data['username'] = $this->doGetUserName();
            $this->load->view('samplegroup_edit', $data);
        }

        public function doAdd($surveyGuid)
        {
            $sampleGroupObject = new samplegroup_model();
            $interviewerLogicObject = new interviewer_logic_model();
            $sampleGroupObject->setSampleGroupName($this->input->post('name'));
            $selectSampleArray = $this->input->post('in_list');

            $sampleGroupObject->setSampleArray($selectSampleArray);
            $sampleGroupObject->addSampleToGroup($sampleGroupObject);
            $interviewerLogicObject->addSampleToInterviewer(null,$surveyGuid,$sampleGroupObject->getSampleGroupGuid());
            redirect('samplegroups/','refresh');
        }

        public function update($sampleGroupGuid)
        {

            $sampleGroupObject = new samplegroup_model();
            $sampleGroupObject = $sampleGroupObject->getSampleGroupBySampleGroupGuid($sampleGroupGuid);
            $sampleArray = $sampleGroupObject->getSampleArray();
            $inListArray = array();
            for($i=0;$i<count($sampleArray);$i++) {
                $sampleObject = new sample_model();
                $sampleObject->setGuid($sampleArray[$i]);
                $sampleObject = $sampleObject->getSample($sampleObject);
                $inListArray[] = $sampleObject;
            }

            $outListArray = $sampleGroupObject->getNotInSampleArrayList($this->doGetSessionSurveyGuid());

            $data['sampleGroupObject'] = $sampleGroupObject;
            $data['sampleArray'] = array();
            $data['inListArray'] = $inListArray;
            $data['outListArray'] = $outListArray;
            $data['title'] = $this->title;
            $data['headerTitle'] = "編輯樣本分群";
            $data['message']="";
            $data['action'] = site_url('samplegroups/doUpdate/'.$this->doGetSessionSurveyGuid().'/'.$sampleGroupObject->getSampleGroupGuid());
            $data['link_back'] = anchor('samplegroups/','回到列表',array('class'=>'back'));
            $data['username'] = $this->doGetUserName();
            $this->load->view('samplegroup_edit', $data);
        }

        public function doUpdate($surveyGuid,$oldSampleGroupGuid)
        {
            $sampleGroupObject = new samplegroup_model();
            $interviewerLogicObject = new interviewer_logic_model();
            $sampleGroupObject->setSampleGroupName($this->input->post('name'));
            $selectSampleArray = $this->input->post('in_list');
            $sampleGroupObject->setSampleArray($selectSampleArray);

            $sampleGroupObject->deleteSampleGroup($oldSampleGroupGuid);
            $sampleGroupObject->deleteSampleGroupData($oldSampleGroupGuid);
            $sampleGroupObject->addSampleToGroup($sampleGroupObject);
            $interviewerLogicObject->updateSampleToInterviewer(null,$surveyGuid,$sampleGroupObject->getSampleGroupGuid(),$oldSampleGroupGuid);
            redirect('samplegroups/','refresh');
        }



        public function delete($sampleGroupGuid)
        {
            $sampleGroupObject = new samplegroup_model();
            $sampleGroupObject->deleteSampleGroup($sampleGroupGuid);
            $sampleGroupObject->deleteSampleGroupData($sampleGroupGuid);
            $sampleGroupObject->deleteSurveyOwnSampleByGroupSimpleGuid($sampleGroupGuid);
            redirect('samplegroups/','refresh');
        }

    }


?>
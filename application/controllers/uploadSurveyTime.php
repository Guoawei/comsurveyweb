<?php
    /**
     * 
     */
    class uploadSurveyTime extends CI_Controller {
        
        function uploadSurveyTime() {
            parent::__construct();
			$this->load->model("sample_model");
			$this->load->model("surveytime_model");
        }
		
		function index()
		{

			$printCode = 0;
			$printCode = $this->input->get_post('test',true); 	 
			
			if ($printCode == 1) {

				//Test Json
				$sampleGuid = "116100311";
				$interviewerGuid = "334cd1a5189c06320b718c2d43c40f19";
				$surveyGuid = "3fba9a60f401e35406c2d78340484caa";
				$index = "2";
				$startDateTime = "2012-06-15 01:32:00";
				$endDateTime = "2012-06-15 03:18:24";
				$index2 = "3";
				$startDateTime2 = "2012-06-16 10:40:50";
				$endDateTime2 = "2012-06-16 10:58:15";
				
				$timeArray1 = array('Sindex'=>$index,'StartDateTime'=>$startDateTime,'EndDateTime'=>$endDateTime);
				$timeArray2 = array('Sindex'=>$index2,'StartDateTime'=>$startDateTime2,'EndDateTime'=>$endDateTime2);
				$timeArray3 = array('Sindex'=>"4",'StartDateTime'=>"2012-06-17 09:18:35",'EndDateTime'=>"2012-06-17 10:58:15");
				$LogDateTime = array($timeArray1,$timeArray2,$timeArray3);
				$json = json_encode(array('SampleGuid' => $sampleGuid,
			    		 				  'InterviewerGuid' => $interviewerGuid,
			    		 				  'SurveyGuid' => $surveyGuid,			
										  'LogDateTime'=> $LogDateTime
														));
			
				//Output JSON
				$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output(json_encode(array('SampleGuid' => $sampleGuid,
			    		 				  'InterviewerGuid' => $interviewerGuid,
			    		 				  'SurveyGuid' => $surveyGuid,			
										  'LogDateTime'=> $LogDateTime
														)));
														
				
			}else {
				$json = file_get_contents('php://input');
				if ($json) {
					$data = json_decode($json);
					$sampleGuid = $data->{'SampleGuid'};
					$interviewerGuid = $data->{'InterviewerGuid'};
					$surveyGuid = $data->{'SurveyGuid'};
					$LogDateTime = $data->{'LogDateTime'}; 
				}else {
					$printCode = 1;
					echo "No JSON";
				}

			}
			

			if ($printCode == 0) {
				//建立答案資料表
				$surveyTime = new surveytime_model();
				$surveyTime->setSampleGuid($sampleGuid);
				$surveyTime->setInterviewerGuid($interviewerGuid);
				$surveyTime->setSurveyGuid($surveyGuid);
				
				foreach ($LogDateTime as $row) {
					// echo $row->Sindex."<br>";
					$surveyTime->setIndex($row->Sindex);
					$surveyTime->setStartDateTime($row->StartDateTime);
					$surveyTime->setEndDateTime($row->EndDateTime);
					
					$sample = new sample_model();
					$sample->setGuid($sampleGuid);
					$sample->getSample($sample);
					
					$isAlreadyHave = $surveyTime->checkSurveyTimeAlreadyHave($surveyTime);
					if ($isAlreadyHave==0) {
						
						$surveyTime->addSurveyTime($surveyTime);
						
						//檢查是否上傳成功，並回傳成功、失敗
						$isAlreadyHave = $surveyTime->checkSurveyTimeAlreadyHave($surveyTime);
						if ($isAlreadyHave==0) {
							echo $sample->getName().":Failure";	
						}else {
							
							echo $sample->getName().":Success";	
						}
					}else {
						//回傳已經有記錄不更新
						// $surveyTime->updateSample($surveyTime);
						echo $sample->getName().":Uploaded";	
					}
				}

				
			}	

		}

		

    }
    
	
?>
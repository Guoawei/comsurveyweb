<?php
    /**
     * 
     */
    class getTipsImage extends CI_Controller {
        
        function getTipsImage() {
            parent::__construct();
			$this->load->helper('html');
        }
		
		function index(){
			$imgString =  $this->input->get_post('img',true);
			$imgArray = explode(".", $imgString);
			//echo $imgArray[0].$imgArray[1];
			$this->output
    				->set_content_type($imgArray[1]) // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
    				->set_output(file_get_contents('images/tips/'.$imgString));
			//echo heading('Welcome!', 3);
			//echo img('images/tips/boy.png');
			// $this->load->view('showImage');
		}
		
    }
    
?>
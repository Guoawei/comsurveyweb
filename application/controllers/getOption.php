<?php
    /**
     * 
     */
    class getOption extends CI_Controller {
        
        function getOption() {
            parent::__construct();
			$this->load->model('option_model');
        }
		
		function index()
		{
			$option = new option_model();
			//取得參數
			$optionGuid =  $this->input->get_post('optionguid',true);
			$option->setGuid($optionGuid);
			
			//getOption
			$optionData = NULL;
			$optionArray = $option->getOptionArray($option);
			if (count($optionArray)>=0) {
				for ($i=0; $i < count($optionArray); $i++) { 
					$tempOption = new option_model();
					$tempOption = $optionArray[$i];
					$tempOptionA = array("optionIndex"=>$tempOption->getOptionIndex()
										,"optionText"=>$tempOption->getTitle()
										,"optionValue"=>$tempOption->getOptionValue()
										,"optionIsShowTextView"=>$tempOption->getIsShowTextView()
										,"optionTextViewPosition"=>$tempOption->getTextViewPosition()
										,"optionDyPickerSubject"=>$tempOption->getDyPickerSubject()
										,"optionDyPickerMin"=>$tempOption->getDyPickerMin()
										,"optionDyPickerMax"=>$tempOption->getDyPickerMax()
										,"optionDyPickerTick"=>$tempOption->getDyPickerTick()
										,"optionPickerValueArray"=>$tempOption->getPickerValueArray($tempOption)
                                        ,"optionTips"=>$tempOption->getOptionTips()
										);
					$optionData[] = $tempOptionA;
				}
			}else {
				$optionData = NULL;
			}
			
			if (count($optionData)>=0){
				//Output JSON
				$this->output
		    		 ->set_content_type('application/json')
		    		 ->set_output(json_encode(array('Header' => array('Status' => "0",
		    			 												  'Doc' => '讀取正常'),
														'Body' => array('getOption' => $optionData
																		 ))));	
				
			} else {
				//帳號密碼錯誤
				//Output JSON
				$this->output
	    			 ->set_content_type('application/json')
	    			 ->set_output(json_encode(array('Header' => array('Status' => "1",
	    			 												  'Doc' => '讀取失敗'),
													'Body' => NULL
																	 )));	
			}
		}
    }
    
?>
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Front answer model
 * 前台問卷作答時用的 model
 * @package Front_answer_model
 * @subpackage
 * @category
 * @author  蔡孟珂
 * @link
 */
class Front_answer_model extends CI_Model
{
    //訪員guid
    private $_interViewerGuid;
    //問卷guid
    private $_surveyGuid;
    //問卷版本
    private $_version;
    //樣本guid
    private $_sampleGuid;
    //題號
    private $_subject_index;
    //題型
    private $_subject_type;
    //答案選項
    private $_subject_option;
    //群組題答案選項
    private $_subject_groupOption;
    //結束情況代碼
    private $_situationCode;
    //結束情況其他文字說明
    private $_situationComment;

    function __construct()
    {
        parent::__construct();

    }

    //設定參數
    public function setInterViewerGuid($value)
    {
        $this->_interViewerGuid = $value;
    }

    public function setSurveyGuid($value)
    {
        $this->_surveyGuid = $value;
    }

    public function setSampleGuid($value)
    {
        $this->_sampleGuid = $value;
    }

    public function setSubject_index($value)
    {
        $this->_subject_index = $value;
    }

    public function setSubject_type($value)
    {
        $this->_subject_type = $value;
    }

    public function setSubject_option($value)
    {
        $this->_subject_option = $value;
    }

    public function setSubject_groupOption($value)
    {
        $this->_subject_groupOption = $value;
    }

    public function setSituationCode($value)
    {
        $this->_situationCode = $value;
    }

    public function setSituationComment($value)
    {
        $this->_situationComment = $value;
    }

    public function setVersion($value)
    {
        $this->_version = $value;
    }

    //取得參數
    public function getInterViewerGuid()
    {
        return $this->_interViewerGuid;
    }

    public function getSurveyGuid()
    {
        return $this->_surveyGuid;
    }

    public function getSampleGuid()
    {
        return $this->_sampleGuid;
    }

    public function getSubject_index()
    {
        return $this->_subject_index;
    }

    public function getSubject_type()
    {
        return $this->_subject_type;
    }

    public function getSubject_option()
    {
        return $this->_subject_option;
    }

    public function getSubject_groupOption()
    {
        return $this->_subject_groupOption;
    }

    public function getSituationCode()
    {
        return $this->_situationCode;
    }

    public function getSituationComment()
    {
        return $this->_situationComment;
    }

    public function getVersion()
    {
        return $this->_version;
    }

    /**
     * 回傳 作答前與中途結束問卷的問題
     *
     * @return Array
     */
    public function getStatusQuestions()
    {
        $statusQuestion[] = array(
            'code'=>110,
            'title'=>'繼續訪問',
            'options'=> array()
        );
        $statusQuestion[] = array(
            'code'=>2,
            'title'=>'沒訪問成功',
            'options'=> array(
                array(
                    'code'=>211,
                    'title'=>'本人拒訪'
                ),
                array(
                    'code'=>212,
                    'title'=>'其他人代為拒訪'
                ),
                array(
                    'code'=>221,
                    'title'=>'受訪者請假，沒上學'
                ),
                array(
                    'code'=>222,
                    'title'=>'受訪者有事不在'
                ),
                array(
                    'code'=>23,
                    'title'=>'其他，請說明'
                )
            )
        );
        $statusQuestion[] = array(
            'code'=>3,
            'title'=>'其他原因',
            'options'=> array(
                array(
                    'code'=>311,
                    'title'=>'身心障礙'
                ),
                array(
                    'code'=>312,
                    'title'=>'語言不通'
                )
            )
        );
        $statusQuestion[] = array(
            'code'=>4,
            'title'=>'中斷訪問',
            'options'=> array( array(
                    'code'=>410,
                    'title'=>'請說明原因'
                ))
        );
        /*
        $statusQuestion[] = array(
            'code'=>1,
            'title'=>'繼續訪問',
            'options'=> array()
        );
        $statusQuestion[] = array(
            'code'=>2,
            'title'=>'需再訪問',
            'options'=> array(
                array(
                    'code'=>21,
                    'title'=>'因故中止訪問(已經訪問一部分了，可以改天再去訪問)'
                ),
                array(
                    'code'=>22,
                    'title'=>'暫時不便接受訪問(還沒開始訪問，可以改天再去訪問)'
                ),
                array(
                    'code'=>23,
                    'title'=>'外出，訪問期間會回來'
                ),
                array(
                    'code'=>24,
                    'title'=>'語言不通(請聯絡輔導員確認是否應使用此代碼)'
                ),
                array(
                    'code'=>25,
                    'title'=>'無人在家'
                ),
                array(
                    'code'=>26,
                    'title'=>'受訪者以外的人代為拒絕(請設法找到受訪者本人)'
                ),
                array(
                    'code'=>27,
                    'title'=>'管理員阻止（請出示所有公文或留本中心聯絡電話）'
                )
            )
        );

        $statusQuestion[] = array(
            'code'=>3,
            'title'=>'不住原址',
            'options'=> array(
                array(
                    'code'=>31,
                    'title'=>'「需再訪」：受訪者不住在戶籍地，有問到新電話及新地址(如因工作、求學、房屋出租等原因)'
                ),
                array(
                    'code'=>32,
                    'title'=>'「需再訪」：受訪者不住在戶籍地，只問到新電話或新地址(如因工作、求學、房屋出租等原因)'
                ),
                array(
                    'code'=>33,
                    'title'=>'「不需再訪」：受訪者不住在戶籍地，沒有問到新電話及新地址(如因工作、求學、房屋出租等原因)'
                ),
            )
        );

        $statusQuestion[] = array(
            'code'=>4,
            'title'=>'地址無法訪問或無此人',
            'options'=> array(
                array(
                    'code'=>41,
                    'title'=>'軍事單位、醫院、療養院、學校、職訓中心、監獄等機構'
                ),
                array(
                    'code'=>42,
                    'title'=>'空屋（如因房子改建/出售/出租等，無人居住，經村里長、警察等證實）'
                ),
                array(
                    'code'=>43,
                    'title'=>'查無此地址（經村里長、警察等證實）'
                ),
                array(
                    'code'=>44,
                    'title'=>'因環境惡劣，無法抵達訪區(如山崩、地震或其他天災等)'
                ),
                array(
                    'code'=>45,
                    'title'=>'該地址查無此人'
                )
            )
        );

        $statusQuestion[] = array(
            'code'=>5,
            'title'=>'受訪者拒訪或無法接受訪問',
            'options'=> array(
                array(
                    'code'=>51,
                    'title'=>'受訪者拒絕接受訪問'
                ),
                array(
                    'code'=>52,
                    'title'=>'受訪者中途拒訪'
                ),
                array(
                    'code'=>53,
                    'title'=>'受訪者以外的人中途拒訪'
                ),
                array(
                    'code'=>54,
                    'title'=>'受訪者因生理/心理問題，無法接受訪問(如生重病、重聽、精神疾病)'
                ),
                array(
                    'code'=>55,
                    'title'=>'外出(如旅遊、遊學、出差等)，訪問期間不會回來'
                ),
                array(
                    'code'=>56,
                    'title'=>'外出不知去向、失蹤'
                ),
                array(
                    'code'=>57,
                    'title'=>'死亡'
                ),
                array(
                    'code'=>58,
                    'title'=>'服刑'
                ),
                array(
                    'code'=>59,
                    'title'=>'受訪者戶籍遷出(因結婚、移民等)'
                ),
                array(
                    'code'=>60,
                    'title'=>'服兵役'
                )
            )
        );
        $statusQuestion[] = array(
            'code'=>6,
            'title'=>'其他',
            'options'=> array( array(
                    'code'=>98,
                    'title'=>'其他無法歸類之原因，請說明'
                ))
        );
        */
        return $statusQuestion;
    }

    /**
     * 儲存問卷情況
     *
     * @return boolean
     */
    public function saveSituation()
    {
        date_default_timezone_set('Asia/Taipei');
        $data = array(
            'interviewerGuid'=>$this->_interViewerGuid,
            'sampleGuid'=>$this->_sampleGuid,
            'surveyGuid'=>$this->_surveyGuid,
            'situationCode'=>$this->_situationCode,
            'comment'=>$this->_situationComment,
            'cDateTime'=>date('Y-m-d H:i:s')
        );

        $this->db->insert('survey_surveysituation', $data);
        if ($this->db->insert_id())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * 儲存問鍵作答時間
     * @param type, 開始 start 或 end 結束時間
     * @return boolean
     */
    public function saveSurveyTime($type = 'start')
    {
        //取得為第幾次記錄時間
        $query = $this->db->get_where('survey_surveytime', array(
            'interviewerGuid'=>$this->_interViewerGuid,
            'sampleGuid'=>$this->_sampleGuid,
            'surveyGuid'=>$this->_surveyGuid
        ));
        //預設為第0次
        $sIndex = 0;
        if ($query->num_rows() > 0)
        {
            //如果超過0次，就已該數量為新的次數，因為從0開始，所以不用+1
            $sIndex = $query->num_rows();
        }
        date_default_timezone_set('Asia/Taipei');
        $data = array(
            'interviewerGuid'=>$this->_interViewerGuid,
            'sampleGuid'=>$this->_sampleGuid,
            'surveyGuid'=>$this->_surveyGuid,
            'sIndex'=>$sIndex
        );

        if ($type == 'start')
        {
            $data['startDateTime'] = $data['endDateTime'] = date('Y-m-d H:i:s');
            $this->db->insert('survey_surveytime', $data);
            if ($this->db->insert_id())
            {
                $this->session->set_userdata('surveyTimeID', $this->db->insert_id());
            }
        }
        elseif ($type == 'update')
        {
            //更新記錄開始
            $this->db->where('id', $this->session->userdata('surveyTimeID'));
            $data['startDateTime'] = date('Y-m-d H:i:s');
            $this->db->update('survey_surveytime', $data);
        }
        else
        {
            //記錄結束時間，並清除 timeID
            $this->db->where('id', $this->session->userdata('surveyTimeID'));
            $data['endDateTime'] = date('Y-m-d H:i:s');
            $this->db->update('survey_surveytime', $data);
            $this->session->unset_userdata('surveyTimeID');
        }
    }

    /**
     * 取得該問卷的題目數量
     * 回傳數字
     * @param
     * @return int
     */
    public function getSurveyQuestionNumber()
    {
        //$query = $this->db->get_where('survey_question',array('surveyGuid' => $this->getSurveyGuid()));
        //return $query->num_rows();

        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            $fields = $this->db->list_fields($table);
            return count($fields) - 4;
        }
        else
        {
            return 0;
        }

    }

    /**
     * 取得該樣本的進度
     * 回傳百分比
     * @param
     * @return int 0~100
     */
    public function getSampleProgressAndTime()
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            //啟用資料庫快取
            $this->db->cache_on();
            $query = $this->db->get_where($table, array(
                'interviewerGuid'=>$this->getInterViewerGuid(),
                'sampleGuid'=>$this->getSampleGuid()
            ));
            //關閉快取
            $this->db->cache_off();

            if ($query->num_rows() == 1)
            {
                //取得所有題目欄位，並根據每個欄位判別是否有值。
                //$fields = $query->list_fields();
                $result = $query->row_array();
                $i = 0;
                $last = 0;
                foreach ($result as $key=>$row)
                {
                    if ($row != '' OR $row != NULL)
                        $last = $i + 1;
                    $i++;
                }

                //回傳的數量，需減掉 id, interviewerGuid, sampleGuid, cDateTime 四個欄位
                $final = $last - 4;
                if ($final >= 0)
                {
                    return array(
                        $final,
                        $result['cDateTime']
                    );
                }
                else
                {
                    return array(
                        0,
                        $result['cDateTime']
                    );
                }
            }
            else
            {
                //還沒有作答，回傳 0
                return array(
                    0,
                    ''
                );
            }
        }
        else
        {
            //還沒有作答，回傳 0
            return array(
                0,
                ''
            );
        }
    }

    /**
     * 鎖定樣本
     * @param $value, 要改變的狀態，資料庫預設0 為不鎖定，此方法預設會以1進行鎖定
     * @return boolean
     */
    public function changeLockSample($value = 1)
    {
        if ($this->_sampleGuid)
        {
            $this->db->where('guid', $this->_sampleGuid);
            $this->db->update('survey_sample', array('locked'=>$value));
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * 建立答案表格
     * @param $field, 題目欄位
     * @return boolean
     */
    public function creatAnswerTable($field)
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();

        //如果該答案表格不在，就建立表格
        if (!$this->db->table_exists($table))
        {
            //載入dbforge
            $this->load->dbforge();
            //基本欄位
            $fields = array(
                'id'=> array(
                    'type'=>'INT',
                    'constraint'=>11,
                    'auto_increment'=>TRUE
                ),
                'interviewerGuid'=> array(
                    'type'=>'VARCHAR',
                    'constraint'=>32
                ),
                'sampleGuid'=> array(
                    'type'=>'VARCHAR',
                    'constraint'=>32
                ),
                'cDateTime'=> array('type'=>'DATETIME')
            );
            

            //新增題目欄位
            foreach ($field as $row)
            {
                $fields["`{$row}`"] = array('type'=>'TEXT','null' => TRUE);
                //$this->db->query("ALTER TABLE  `{$table}` ADD  `{$row}` TEXT NULL ;");
            }
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('id', TRUE);
            //建立表格
            $this->dbforge->create_table($table, TRUE);

        }
        return true;
    }

    /**
     * 儲存答案
     * @param type, 開始 start 或 end 結束時間
     * @return boolean
     */
    public function saveAnswer($field, $answer)
    {
        date_default_timezone_set('Asia/Taipei');
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            //檢查有沒有該樣本已經做答了，有就更新，沒有就新增
            $query = $this->db->get_where($table, array(
                'interviewerGuid'=>$this->getInterViewerGuid(),
                'sampleGuid'=>$this->getSampleGuid()
            ));
            if ($query->num_rows() == 1)
            {
                //更新
                //$this->db->where('interviewerGuid',$this->getInterViewerGuid());
                //$this->db->where('sampleGuid',$this->getSampleGuid());
                //$updatedata = array($field=>join(",", $answer));
                //$this->db->update($table,$updatedata);
                
                $sql = "UPDATE `{$table}` SET `{$field}` = '".join(",", $answer)."' WHERE `interviewerGuid` = '".$this->getInterViewerGuid()."' AND `sampleGuid` = '".$this->getSampleGuid()."'";
                
                $this->db->query($sql);
                //清除答案 cache，讓訪員進度可以重新讀取
                $this->db->cache_delete_all();
                return '更新完畢';
            }
            else
            {
                //新增
                /*
                $inserdata = array(
                    'interviewerGuid'=>$this->getInterViewerGuid(),
                    'sampleGuid'=>$this->getSampleGuid(),
                    'cDateTime'=>date('Y-m-d H:i:s'),
                    $field=>join(",", $answer)
                );
                
                $this->db->insert($table, $inserdata);
                */
                $sql = "INSERT INTO `{$table}` (`interviewerGuid`,`sampleGuid`,`cDateTime`,`{$field}`)
                        VALUE ('".$this->getInterViewerGuid()."','".$this->getSampleGuid()."', '".date('Y-m-d H:i:s')."', '".join(",", $answer)."')";
                $this->db->query($sql);
                //清除答案 cache，讓訪員進度可以重新讀取
                $this->db->cache_delete_all();
                return '新增完畢';
            }
        }
        else 
        {
            return '沒表格可以存答案';      
        }
    }

    /**
     * 檢查目前樣本，正做到目前問卷中的哪個題目
     * @param type, 開始 start 或 end 結束時間
     * @return boolean
     */
    public function checkOnWhichQuestion()
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            $query = $this->db->get_where($table, array(
                'interviewerGuid'=>$this->getInterViewerGuid(),
                'sampleGuid'=>$this->getSampleGuid()
            ));

            //判別有無做答記錄
            if ($query->num_rows() == 1)
            {
                $i = 0;
                $last = 0;
                foreach ($query->row_array() as $row)
                {
                    if ($row != '' OR $row != NULL)
                        $last = $i + 1;
                    $i++;

                }
                //回傳的數量，需減掉 id, interviewerGuid, sampleGuid, cDateTime 四個欄位
                $final = $last - 4;
                if ($final >= 0)
                {
                    return $final;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }

    }

    /**
     * 檢查該樣本在該欄位是否有值
     * @param $field, 要查看的欄位
     * @return boolean
     */
    public function checkHaveValue($field)
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            /*
            $this->db->select("`" . $field . "`");
            $query = $this->db->get_where($table, array(
                'interviewerGuid'=>$this->getInterViewerGuid(),
                'sampleGuid'=>$this->getSampleGuid()
            ));*/
            $sql = "SELECT `{$field}` FROM `{$table}` WHERE `interviewerGuid` = '".$this->getInterViewerGuid()."' AND `sampleGuid` = '".$this->getSampleGuid()."'";
            $query = $this->db->query($sql);
            //判別有無做答記錄
            if ($query->num_rows() == 1)
            {
                $result = $query->row_array();
                if ($result[$field] != '' && $result[$field] != NULL)
                {
                    return $result[$field];
                }
                else
                {
                    return NULL;
                }
            }
            else
            {
                return NULL;
            }
        }
        else
        {
            return NULL;
        }
    }

    /**
     * 取得問題的答案
     * @param $field, array 要查看的題目
     * @return array
     */
    public function gerQuestionAnswer($field)
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            foreach ($field as $key=>$value)
            {
                /*
                $this->db->select("`" . $value . "`");
                $query = $this->db->get_where($table, array(
                    'interviewerGuid'=>$this->getInterViewerGuid(),
                    'sampleGuid'=>$this->getSampleGuid()
                ));*/
                $sql = "SELECT `{$value}` FROM `{$table}` WHERE `interviewerGuid` = '".$this->getInterViewerGuid()."' AND `sampleGuid` = '".$this->getSampleGuid()."'";
                $query = $this->db->query($sql);
                
                //判別有無做答記錄
                if ($query->num_rows() == 1)
                {
                    $result = $query->row_array();
                    if ($result[$value] != '' && $result[$value] != NULL)
                    {
                        $spdot = explode(",", $result[$value]);
                        if(count($spdot) > 1)
                        {
                            foreach ($spdot as $key2 => $value2) {
                                $sp = explode("$", $value2);

                                if (count($sp) == 2)
                                {
                                    $answer[$key][] = $sp[1];
                                }
                                else
                                {
                                    $answer[$key][] = $value2;
                                }
                            }
                        }
                        else
                        {
                            $sp = explode("$", $result[$value]);

                            if (count($sp) == 2)
                            {
                                $answer[$key] = $sp[1];
                            }
                            else
                            {
                                $answer[$key] = $result[$value];
                            }
                        }
                        
                    }
                    else
                    {
                        $answer[] = '';
                    }
                }
                else
                {
                    $answer[] = '';
                }
            }
        }
        else
        {
            $answer[] = '';
        }
        return $answer;
    }
    
    
    /**
     * 取得前問題的答案
     * @param $field, array 要查看的題目
     * @return array
     */
    public function getPrevAnswer($field)
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            /*
            $this->db->select("`" . $field . "`");
            $query = $this->db->get_where($table, array(
                'interviewerGuid'=>$this->getInterViewerGuid(),
                'sampleGuid'=>$this->getSampleGuid()
            ));*/
            $sql = "SELECT `{$field}` FROM `{$table}` WHERE `interviewerGuid` = '".$this->getInterViewerGuid()."' AND `sampleGuid` = '".$this->getSampleGuid()."'";
            $query = $this->db->query($sql);
            //判別有無做答記錄
            if ($query->num_rows() == 1)
            {
                $result = $query->row_array();
                if ($result[$field] != '' && $result[$field] != NULL)
                {
                    $sp_val = explode(":", $result[$field]);
                    if(count($sp_val) == 2){
                        if(substr($sp_val[1], 3,1) == ")")
                        {
                            $sp_val[1] = str_replace(substr($sp_val[1], 0,4), '', $sp_val[1]);
                        }
                        
                        $answer = $sp_val[1];
                        
                    }
                    else 
                    {
                        //取得該答案的字串
                        $this->db->select('survey_option.title');
                        $this->db->from('survey_option');
                        $this->db->join('survey_question','survey_question.optionGuid = survey_option.guid','left');
                        $this->db->where('survey_question.surveyGuid', $this->getSurveyGuid());
                        $this->db->where('survey_question.subjectNumber', $field);
                        $this->db->where('survey_option.optionValue', $result[$field]);
                        $query = $this->db->get();
                        $answer = $query->row_array();
                        if(substr($answer['title'], 3,1) == ")")
                        {
                            $answer['title'] = str_replace(substr($answer['title'], 0,4), '', $answer['title']);
                        }
                        $answer = $answer['title'];
                    }                   
                    //$this->db->get_where('survey_option',array('surveyGuid' => $this->getSurveyGuid(),'optionValue' => ), $limit, $offset)
                }
                else
                {
                    $answer = '';
                }
            }
            else
            {
                $answer = '';
            }
            
        }
        else
        {
            $answer = '';
        }
        return $answer;
    }

    /**
     * 取得前問題的答案
     * @param $field, array 要清除的題目
     * @return array
     */
    public function clearJumpAnswer($field)
    {
        $table = "survey_answer_" . $this->getSurveyGuid() . "_" . $this->getVersion();
        if ($this->db->table_exists($table))
        {
            foreach ($field as $row) {
                //$data[$row] = '';
                $data[] = "`{$row}`= ''";
            }
            /*
            $this->db->where('interviewerGuid',$this->getInterViewerGuid());
            $this->db->where('sampleGuid',$this->getSampleGuid());
            $this->db->update($table,$data);*/
            $sql = "UPDATE `{$table}` SET ".join(", ", $data)."
                    WHERE `interviewerGuid` = '".$this->getInterViewerGuid()."' 
                    AND `sampleGuid` = '".$this->getSampleGuid()."'";
            $this->db->query($sql);
            
            //清除答案 cache，讓訪員進度可以重新讀取
            $this->db->cache_delete_all();
            
            return '更新成功';
        }
        else
        {
            return '答案表格不存在';
        }
        
    }
}

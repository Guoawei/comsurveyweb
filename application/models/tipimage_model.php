<?php
	/**
	 *  No_use
	 */
	class tips_model extends CI_Model {
		private $_id;
		private $_imageFileName;
		private $_imageDesc;
		private $_qtipsId;
		
		function __construct() {
			parent::__construct();
		}
		
		public function setId($id)
		{
			$this->_id = $id;
		}
		
		public function getId()
		{
			return $this->_id;
		}
		
		public function setImageFileName($imageFileName)
		{
			$this->_imageFileName = $imageFileName;
		}
		
		public function getImageFileName()
		{
			return $this->_imageFileName;
		}
		
		public function setImageDesc($imageDesc)
		{
			$this->_imageDesc = $imageDesc;
		}
		
		public function getImageDesc()
		{
			return $this->_imageDesc;
		}
		
		public function setQTipsId($QTipsId)
		{
			$this->_qtipsId = $QTipsId;
		}
		
		public function getQtipsId()
		{
			return $this->_qtipsId;
		}
	}
	
?>
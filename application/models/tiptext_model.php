<?php
	/**
	 *  No Use
	 */
	class tips_model extends CI_Model {
		private $_id;
		private $_text;
		private $_qtipsId;
		
		function __construct() {
			parent::__construct();
		}
		
		public function setId($id)
		{
			$this->_id = $id;
		}
		
		public function getId()
		{
			return $this->_id;
		}
		
		public function setText($text)
		{
			$this->_text = $text;
		}
		
		public function getText()
		{
			return $this->_text;
		}
		
		public function setQTipsId($QTipsId)
		{
			$this->_qtipsId = $QTipsId;
		}
		
		public function getQtipsId()
		{
			return $this->_qtipsId;
		}
	}
	
?>
<?php

    /**
     * 
     */
    class enum {
	    protected $self = array();
	    public function __construct( /*...*/ ) {
	        $args = func_get_args();
	        for( $i=0, $n=count($args); $i<$n; $i++ )
	            $this->add($args[$i]);
	    }
	    
	    public function __get( /*string*/ $name = null ) {
	        return $this->self[$name];
	    }
	    
	    public function add( /*string*/ $name = null, /*int*/ $enum = null ) {
	        if( isset($enum) )
	            $this->self[$name] = $enum;
	        else
	            $this->self[$name] = end($this->self) + 1;
	    }
	}
	
    class qtype_model extends enum {
        
		private $qtype;
		
        function __construct() {
            parent::__construct();
			// $this->load->model("enum_model");
			// $qtypeEnum = new Enum("qtypeT","qtypeS","qtypeM","qtypeF","qtypeO");
			$this->setQtype(new enum("qtypeT","qtypeS","qtypeM","qtypeF","qtypeO"));
        }	
	
		public function setQtype($qtypeEnum)
		{
			$this->qtype = $qtypeEnum;
		}
		public function getQtype()
		{
			return $this->qtype;
		}
		
    }
    
?>
<?php
    /**
     * 
     */
    class sample_model extends CI_Model {
        
		private $_guid;
        private $_surveyGuid;
		private $_name;
		private $_gender;
		private $_birthday;
		private $_address;
		private $_zipcode;
		private $_mobilephone;
		private $_surveyByInterviewer;
        private $_mainPriority;
        private $_subPriority;
		private $_locked;
		
        function __construct() {
            parent::__construct();
			//Initial Guid for md5
			$this->load->helper('security');
			$this->setGuid(do_hash(microtime(),'md5'));
			// echo $this->getGuid();
			
        }
		
		public function getCountAll($surveyGuid)
		{
            $this->db->where('surveyGuid',$surveyGuid);
			return $this->db->count_all_results('survey_sample');
		}
		
		public function getSampleWithPage($surveyGuid, $limit = 10, $offset = 0)
		{
            $this->db->where('surveyGuid',$surveyGuid);
            $this->db->order_by('mainpriority','asc');
            $this->db->order_by('subpriority','asc');
			return $this->db->get("survey_sample", $limit, $offset);
		}

        function setSampleObject($queryResult)
        {
            $sample = new sample_model();
            $sample->setGuid($queryResult->guid);
            $sample->setSurveyGuid($queryResult->surveyGuid); //20131118 珂發現為 getSurveyGuid,所以改成 setSurveyGuid
            $sample->setName($queryResult->name);
            $sample->setGender($queryResult->gender);
            $sample->setBirthday($queryResult->birthday);
            $sample->setAddress($queryResult->address);
            $sample->setZipCode($queryResult->zipcode);
            $sample->setMobilePhone($queryResult->mobilephone);
            $sample->setMainPriority($queryResult->mainpriority);
            $sample->setSubPriority($queryResult->subpriority);
			$sample->setLocked($queryResult->locked);
            return $sample;
        }
		
		public function getList()
		{
			$sql = "SELECT * FROM survey_sample order by zipcode, mainpriority, subpriority ASC";
			$query = $this->db->query($sql);
			$resultArray = array();
			foreach ($query->result() as $row) {
				$resultArray[] = $this->setSampleObject($row);
			}
			return $resultArray;			
		}
		
		public function getListWithPage($limit = 10, $offset = 0)
		{
			$sql = "SELECT * FROM survey_sample order by zipcode, mainpriority, subpriority asc limit ".$offset.",".$limit." ";
			$query = $this->db->query($sql);
			$resultArray = array();
			foreach ($query->result() as $row) {
                $resultArray[] = $this->setSampleObject($row);
			}
			return $resultArray;			
		}
		
		public function getSample($sample)
		{
			$sql = "SELECT * FROM survey_sample WHERE guid ='".$sample->getGuid()."'";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
                $sample = $this->setSampleObject($row);
			}
			
			return $sample;
		}
		
		public function addSample($sample)
		{
			$sql = "SELECT guid from survey_sample where name = '".$sample->getName()."' and surveyGuid = '".$sample->getSurveyGuid()."'";
			$result = $this->db->query($sql);
			if ($result->num_rows()==0) {
					$sql = "INSERT INTO survey_sample (guid, surveyGuid, name, gender, birthday, address, zipcode, mobilephone
					, mainpriority, subpriority)
        			VALUES ('".$sample->getGuid()."', '".$sample->getSurveyGuid()."', '".$sample->getName()."','".$sample->getGender()."'
        			,'".$sample->getBirthday()."','".$sample->getAddress()."','".$sample->getZipCode()."'
        			,'".$sample->getMobilePhone()."','".$sample->getMainPriority()."','".$sample->getSubPriority()."')";
					$this->db->query($sql);
				return true;
			}else {
                return false;
            }
		}
		
		public function updateSample($sample)
		{

            $sql = "SELECT guid from survey_sample where name = '".$sample->getName()."' and surveyGuid = '".$sample->getSurveyGuid()."'
             and guid !='".$sample->getGuid()."'";
            $result = $this->db->query($sql);
            if ($result->num_rows()==0) {

                $sql = "UPDATE survey_sample SET name='".$sample->getName()."'
                , gender='".$sample->getGender()."', birthday='".$sample->getBirthday()."', address='".$sample->getAddress()."'
                , zipcode='".$sample->getZipCode()."', mobilephone='".$sample->getMobilePhone()."', mainpriority='".$sample->getMainPriority()."'
                , subpriority='".$sample->getSubPriority()."'
                 where guid='".$sample->getGuid()."'";
                $this->db->query($sql);

                return true;
            }else {
                return false;
            }


		}
		
		public function deleteSample($sample)
		{
			$sql = "DELETE FROM survey_sample where guid='".$sample->getGuid()."'"; 
			$this->db->query($sql);
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}

        public function getSurveyGuid()
        {
            return $this->_surveyGuid;
        }

        public function setSurveyGuid($surveyGuid)
        {
            $this->_surveyGuid = $surveyGuid;
        }
		
		public function getGender()
		{
			return $this->_gender;
		}
		
		public function setGender($gender)
		{
			$this->_gender = $gender;
		}
		
		public function getName()
		{
			return $this->_name;
		}
		
		public function setName($name)
		{
			$this->_name = $name;
		}
		
		public function getBirthday()
		{
			return $this->_birthday;
		}
		
		public function setBirthday($birthday)
		{
			$this->_birthday = $birthday;			
		}
		
		public function getAddress()
		{
			return $this->_address;
		}
		
		public function setAddress($address)
		{
			$this->_address = $address;
		}
		
		public function getZipCode()
		{
			return $this->_zipcode;
		}
		
		public function setZipCode($zipcode)
		{
			$this->_zipcode = $zipcode;
		}
		
		public function getMobilePhone()
		{
			return $this->_mobilephone;
		}
		
		public function setMobilePhone($mobilePhone)
		{
			$this->_mobilephone = $mobilePhone;
		}
		
		public function getSurveyByInterviewer()
		{
			return $this->_surveyByInterviewer;
		}
		
		public function setSurveyByInterviewer($surveyByInterviewer)
		{
			$this->_surveyByInterviewer = $surveyByInterviewer;
		}

        public function getMainPriority()
        {
            return $this->_mainPriority;
        }

        public function setMainPriority($mainPriority)
        {
            $this->_mainPriority = $mainPriority;
        }

        public function getSubPriority()
        {
            return $this->_subPriority;
        }

        public function setSubPriority($subPriority)
        {
            $this->_subPriority = $subPriority;
        }
		
		 public function getLocked()
        {
            return $this->_locked;
        }

        public function setLocked($locked)
        {
            $this->_locked = $locked;
        }
    }
    
?>
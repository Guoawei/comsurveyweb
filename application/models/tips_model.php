<?php
	/**
	 * 
	 */
	class tips_model extends CI_Model {
		private $_tipsGuid;
		private $_tipsIndex;
		private $_tipsText;
		private $_tipsImage;
		
		function __construct() {
			parent::__construct();
            $this->load->model("guid_model");
            $this->setTipsGuid(guid_model::getNewGuid('Tips'));
		}
		
		public function getTipsArray($tips)
		{
			$tipsArray = array();
			$sql = "SELECT * FROM survey_tips WHERE tipsGuid ='".$tips->getTipsGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				$tempTips = new tips_model();
				$tempTips->setTipsGuid($row->tipsGuid);
				$tempTips->setTipsIndex($row->tipIndex);
				$tempTips->setTipsText($row->tipText);
				$tempTips->setTipsImage($row->tipImage);
				$tipsArray[] = $tempTips;
			}
			return $tipsArray;
		}

        public function getTipsByGuid($tipsGuid)
        {
            $sql = "SELECT * FROM survey_tips WHERE tipsGuid ='".$tipsGuid."'";
            $result = $this->db->query($sql);
            $tempTips = new tips_model();
            if ($result->num_rows() > 0){
                $row = $result->row();
                $tempTips->setTipsGuid($row->tipsGuid);
                $tempTips->setTipsIndex($row->tipIndex);
                $tempTips->setTipsText($row->tipText);
                $tempTips->setTipsImage($row->tipImage);
            }

            return $tempTips;
        }

        public function insertTips()
        {
            $sql = "INSERT INTO survey_tips (tipsGuid, tipIndex, tipImage, tipText)
        	VALUES ('".$this->getTipsGuid()."', ".$this->getTipsIndex().",'".$this->getTipsImage()."'
        	,'".$this->getTipsText()."')";

            $this->db->query($sql);
            return $this->getTipsGuid();
        }

        public function deleteTips()
        {
            $sql1 = "DELETE FROM survey_tips where tipsGuid='".$this->getTipsGuid()."'";
            $this->db->query($sql1);
        }

		public function getTipsGuid()
		{
			return $this->_tipsGuid;
		}
		
		public function setTipsGuid($tipsGuid)
		{
			$this->_tipsGuid = $tipsGuid;
		}
		
		public function getTipsIndex()
		{			
			return $this->_tipsIndex;
		}
		
		public function setTipsIndex($tipsIndex)
		{
			$this->_tipsIndex = $tipsIndex;
		}
		
		public function getTipsText()
		{			
			return $this->_tipsText;
		}
		
		public function setTipsText($tipsText)
		{
			$this->_tipsText = $tipsText;
		}
		
		public function getTipsImage()
		{
			return $this->_tipsImage;
		}
		
		public function setTipsImage($tipsImage)
		{
			$this->_tipsImage = $tipsImage;
		}
		
	}
	
?>
<?php
    /**
     * 
     */
    class pass_model extends CI_Model {
        
		private $_guid;
		private $_answerArray = array();
		private $_subjectNumber; //
		private $_groupSubjectArray = array();
		private $_jumpTo;
        private $_jumpToArray = array(); //給單一跳題，且每個選項有獨立的跳題邏輯用的 by Awei 2013/04/13
		private $_select;
		private $_isGroupPass; //single or group
		
        function __construct() {
            parent::__construct();
			$this->load->helper('security');

            $mkTime = explode(" ", microtime(false));
            $microTime = explode(".",$mkTime[0]);
            $randNumber = rand(0,1000);
            $guid = 'pass'.$randNumber.$microTime[1];
			$this->setGuid($guid);
			
        }
		
		public function getPass($pass)
		{
			$answerArray = array();
			$groupSubjectArray = array();

			$sql = "SELECT * FROM survey_pass WHERE guid ='".$pass->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				$sql = "SELECT * FROM survey_pass_answer WHERE passGuid ='".$row->guid."' order by id";
				$answerResult = $this->db->query($sql);
                foreach ($answerResult->result() as $row2) {
                    $answerArray[] = $row2->answer;
                }
				if ($row->isGroupPass) {
					$sql = "SELECT * FROM survey_pass_groupsubject WHERE passGuid ='".$row->guid."' order by id";
					$groupSubjectResult = $this->db->query($sql);
					foreach ($groupSubjectResult->result() as $row3) {
						$groupSubjectArray[] = $row3->groupSubject;
					}
					$pass->setGroupSubjectArray($groupSubjectArray);
//                    foreach ($answerResult->result() as $row2) {
//                        $answerArray[] = $row2->answer;
//                    }
                }

                $jumpToArray = explode(",",$row->jumpTo);
				$pass->setGuid($row->guid);
				$pass->setAnswerArray($answerArray);
				$pass->setSubjectNumber($row->subjectNumber);
				$pass->setJumpTo($row->jumpTo);
                $pass->setJumpToArray($jumpToArray);
				$pass->setSelect($row->selectStatus);
				$pass->setIsGroupPass($row->isGroupPass);
			}
			return $pass;	
		}
	
		public function add($pass)
		{
			$sql = "INSERT INTO survey_pass (guid, subjectNumber, jumpTo, selectStatus, isGroupPass)
        			VALUES ('".$pass->getGuid()."', '".$pass->getSubjectNumber()."','".$pass->getJumpTo()."'
       				,'".$pass->getSelect()."','".$pass->getIsGroupPass()."'
        		   )";
			$this->db->query($sql);

			if (count($pass->getAnswerArray()) > 0) {
				$answerArray = $pass->getAnswerArray();
				for ($i=0; $i < count($answerArray); $i++) {
                    if($answerArray[$i]!=null){
                        $sql = "INSERT INTO survey_pass_answer (passGuid, answer)
        			VALUES ('".$pass->getGuid()."', '".$answerArray[$i]."')";
                        $this->db->query($sql);
                    }

				}
			}
			
			if (count($pass->getGroupSubjectArray()) > 0 && $pass->getIsGroupPass()=='1') {
				$groupSubjectArray = $pass->getGroupSubjectArray();
				for ($i=0; $i < count($groupSubjectArray); $i++) {
					$sql = "INSERT INTO survey_pass_groupsubject (passGuid, groupSubject)
        			VALUES ('".$pass->getGuid()."', '".$groupSubjectArray[$i]."')";
					$this->db->query($sql);	
				}
			}			
			
		}

		public function delete($passGuid)
		{
			$sql1 = "DELETE FROM survey_pass where guid='".$passGuid."'"; 
			$sql2 = "DELETE FROM survey_pass_answer where passGuid='".$passGuid."'";
			$sql3 = "DELETE FROM survey_pass_groupsubject where passGuid='".$passGuid."'";
			$this->db->query($sql1);
			$this->db->query($sql2);
			$this->db->query($sql3);
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function getAnswerArray()
		{
			return $this->_answerArray;
		}
		
		public function setAnswerArray($answerArray)
		{
			$this->_answerArray= $answerArray;
		}
		
		public function getSubjectNumber()
		{
			return $this->_subjectNumber;
		}
		
		public function setSubjectNumber($subectNumber)
		{
			$this->_subjectNumber = $subectNumber;
		}
		
		public function getGroupSubjectArray()
		{
			return $this->_groupSubjectArray;
		}
		
		public function setGroupSubjectArray($groupSubjectArray)
		{
			$this->_groupSubjectArray = $groupSubjectArray;
		}
		
		public function getJumpTo()
		{
			return $this->_jumpTo;
		}
		
		public function setJumpTo($jumpTo)
		{
			$this->_jumpTo = $jumpTo;
		}

        public function getJumpToArray()
        {
            return $this->_jumpToArray;
        }

        public function setJumpToArray($jumpToArray)
        {
            $this->_jumpToArray = $jumpToArray;
        }

        public function getSelect()
		{
			return $this->_select;
		}
		
		public function setSelect($select)
		{
			$this->_select = $select;
		}
		
		public function getIsGroupPass()
		{
			return $this->_isGroupPass;
		}
		
		public function setIsGroupPass($isGroupPass)
		{
			$this->_isGroupPass = $isGroupPass;
		}

    }
    
?>
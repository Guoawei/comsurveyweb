<?php
    /**
     * 
     */
    class survey_model extends CI_Model {
        
		private $_guid;
		private $_title;
		private $_questionnaireArray = array();
		private $_greetingText;
		private $_thankText;
		private $_author;
		private $_totalRows;
		private $_interviewersArray = array();
		private $_samplesArray = array();
		private $_version;
		private $_cdateTime;
		private $_udateTime;
        private $_isDelete;
				
        function __construct() {
            parent::__construct();
			$this->load->model("samplelogic_model");
			//Initial Guid for md5
			$this->load->helper('security');
			$this->setGuid(do_hash(microtime(),'md5'));
        }
		
		//取得所有問卷資訊-後台
		public function getAllSurveyListByAuthor($survey)
		{
			$sql = "SELECT * FROM survey_surveys WHERE author ='".$survey->getAuthor()."' and isDelete = 0 ORDER BY cdatetime DESC";
			$result = $this->db->query($sql);
			$resultArray = array();
			foreach ($result->result() as $row) {
				$survey = new survey_model();				
				$survey->setGuid($row->guid);
				$survey->setTitle($row->title);	
				$survey->setGreetingText($row->greetingText);
				$survey->setThankText($row->thankText);
				$survey->setAuthor($row->author);
				$survey->setVersion($row->version);
				$survey->setTotalRows($row->totalRows);
				$survey->setCDateTime($row->cdatetime);
				$survey->setUDateTime($row->udatetime);
                $survey->setIsDelete($row->isDelete);
				$resultArray[] = $survey;
			}
			return $resultArray;			
		}
		
		//取得問卷資訊
		public function getSurvey($survey)
		{
			$sql = "SELECT * FROM survey_surveys WHERE guid ='".$survey->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {				
				$survey->setGuid($row->guid);
				$survey->setTitle($row->title);	
				$survey->setGreetingText($row->greetingText);
				$survey->setThankText($row->thankText);
				$survey->setAuthor($row->author);
				$survey->setVersion($row->version);
				$survey->setTotalRows($row->totalRows);
				$survey->setCDateTime($row->cdatetime);
				$survey->setUDateTime($row->udatetime);
                $survey->setIsDelete($row->isDelete);
			}
			return $survey;			
		}
		
		public function getSurveyInterviewers($survey)
		{
			$sql = "SELECT * FROM survey_interviewer_own_surveys WHERE surveyGuid ='".$survey->getGuid()."'";
			$result = $this->db->query($sql);
			$interviewerTempArray = array();
			foreach ($result->result() as $row) {
				$interviewerTempArray[] = $row->interviewerGuid;
			}
			$survey->setInterviewersArray($interviewerTempArray);
			return $survey;			
		}
		
		// //取得問卷內容(所有題目、選項、跳題等資訊)
		// public function getSurveyContent($survey)
		// {
			// $sql = "SELECT * FROM survey_surveys WHERE guid ='".$survey->getGuid()."'";
			// $result = $this->db->query($sql);
			// foreach ($result->result() as $row) {				
				// $survey->setGuid($row->guid);
				// $survey->setTitle($row->title);	
				// $survey->setGreetingText($row->greetingText);
				// $survey->setThankText($row->thankText);
				// $survey->setAuthor($row->author);
				// $survey->setVersion($row->version);
				// $survey->setTotalRows($row->totalRows);
				// $survey->setCDateTime($row->cdatetime);
				// $survey->setUDateTime($row->udatetime);
			// }
			// return $survey;			
		// }
		
		public function addSurvey($survey)
		{
			$sql = "INSERT INTO survey_surveys (guid, title, greetingText, thankText, totalRows, author, version, cdatetime, udatetime, isDelete)
        	VALUES ('".$survey->getGuid()."', '".$survey->getTitle()."','".$survey->getGreetingText()."'
        	,'".$survey->getThankText()."','".$survey->getTotalRows()."','".$survey->getAuthor()."','".$survey->getVersion()."'
        	,now(),now(),'".$survey->getIsDelete()."')";

			$this->db->query($sql);
		}
		
		public function addInterviewers($survey)
		{
			$iarray = $survey->getInterviewersArray();
			for ($i=0; $i < count($iarray); $i++) { 
				$sql2 = "INSERT INTO survey_interviewer_own_surveys (interviewerGuid, surveyGuid) 
	        	VALUES ('".$iarray[$i]."', '".$survey->getGuid()."')";
				
				$this->db->query($sql2);
			}
		}
		
		public function addSamples($survey)
		{
			$iarray = $survey->getInterviewersArray();
			for ($i=0; $i < count($iarray); $i++) { 
				$sampleLogic = new samplelogic_model();
				$sampleLogic->addSampleGroup($survey->getSampleArray(), $survey->getGuid(), $iarray[$i]);	
			}
		}
		
		public function getSamples($surveyGuid,$interviewerGuid)
		{
			$sampleLogic = new samplelogic_model();
			return $sampleLogic->getSampleInGroup($surveyGuid, $interviewerGuid);
		}
		
		public function updateSurvey($survey)
		{
			
			$sql = "UPDATE survey_surveys SET guid='".$survey->getGuid()."', title='".$survey->getTitle()."', greetingText='".$survey->getGreetingText()."'
			, thankText='".$survey->getThankText()."', totalRows='".$survey->getTotalRows()."', author='".$survey->getAuthor().
			"', version='".$survey->getVersion()."', udatetime=now() where guid='".$survey->getGuid()."'";
			$this->db->query($sql);

		}
		
		public function deleteSurvey($survey)
		{
			$sql = "DELETE FROM survey_surveys where guid='".$survey->getGuid()."'"; 
			$this->db->query($sql);
		}

        public function closeSurvey($survey)
        {
            $data = array( 'isDelete'=> 1);
            $this->db->where('guid',$survey->getGuid());
            $this->db->update('survey_surveys',$data);
        }
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getTitle()
		{
			return $this->_title;
		}
		
		public function setTitle($title)
		{
			$this->_title = $title;
		}
		
		public function getQuestionnaireArray()
		{
			return $this->_questionnaireArraye;
		}
		
		public function setQuestionnaireArray($questionnaireArray)
		{
			$this->_questionnaireArray = $questionnaireArray;
		}
		
		public function getGreetingText()
		{
			return $this->_greetingText;
		}
		
		public function setGreetingText($greetingText)
		{
			$this->_greetingText = $greetingText;
		}
		
		public function getThankText()
		{
			return $this->_thankText;
		}
		
		public function setThankText($thankText)
		{
			$this->_thankText = $thankText;
		}
		
		public function getInterviewersArray()
		{
			return $this->_interviewersArray;
		}
		
		public function setInterviewersArray($interviewersArray)
		{
			$this->_interviewersArray = $interviewersArray;
		}
		
		public function getSampleArray()
		{
			return $this->_sampleArray;
		}
		
		public function setSampleArray($sampleArray)
		{
			$this->_sampleArray = $sampleArray;
		}
		
		public function getAuthor()
		{
			return $this->_author;
		}
		
		public function setAuthor($author)
		{
			$this->_author = $author;
		}
		
		public function getVersion()
		{
			return $this->_version;
		}
		
		public function setVersion($version)
		{
			$this->_version = $version;
		}
		
		public function getTotalRows()
		{
			return $this->_totalRows;
		}
		
		public function setTotalRows($totalRows)
		{
			$this->_totalRows = $totalRows;
		}
		
		public function getCDateTime()
		{
			return $this->_cdateTime;
		}
		
		public function setCDateTime($cdateTime)
		{
			$this->_cdateTime = $cdateTime;
		}
		
		public function getUDateTime()
		{
			return $this->_udateTime;
		}
		
		public function setUDateTime($udateTime)
		{
			$this->_udateTime = $udateTime;
		}

        public function getIsDelete()
        {
            return $this->_isDelete;
        }

        public function setIsDelete($isDelete)
        {
            $this->_isDelete = $isDelete;
        }
    }
    
?>
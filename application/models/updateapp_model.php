<?php
    /**
     * 
     */
    class updateapp_model extends CI_Model {
        
		private $_ver;
		private $_url;
		private $_udatetime;
		
        function __construct() {
            parent::__construct();
        }
		
		public function getUpdateApp()
		{
			$update = new updateapp_model();
			$sql = "Select * from app_update order by udatetime desc limit 0,1";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
				$update->setVer($row->ver);
				$update->setUrl($row->url);
				$update->setUdateTime($row->udatetime);
			}
			return $update;
		}
		
		public function getLastVer()
		{
			$sql = "Select ver from app_update order by udatetime desc limit 0,1";
			$query = $this->db->query($sql);
			return $query->row()->ver;
		}
		
		public function addVer($updateAppObject)
		{
			$sql = "INSERT INTO app_update (ver, url, updatetime) 
        	VALUES ('".$updateAppObject->getVer()."', '".$updateAppObject->getUrl()."','now()')";

			$this->db->query($sql);
		}
		
		public function setVer($ver)
		{
			$this->_ver = $ver;
		}
		
		public function getVer()
		{
			return $this->_ver;
		}
		
		public function setUrl($url)
		{
			$this->_url = $url;
		}
		
		public function getUrl()
		{
			return $this->_url;
		}
		
		public function setUdateTime($uDateTime)
		{
			$this->_udatetime = $uDateTime;
		}
		
		public function getUdateTime()
		{
			return $this->_udatetime;
		}
    }
    
?>
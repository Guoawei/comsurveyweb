<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  
/**
 * Sendmail model
 *
 * @package	Sendmail_m
 * @subpackage	
 * @category	Sendmail 發送信件管理
 * @author	蔡孟珂
 * @link	
 */
class Sendmail_m extends CI_Model {
	protected $default_data;
	protected $mailsetting=array();
	
	function __construct()
    {
        parent::__construct();
        /*
		//取得相關預設資訊
		$defaultInfo=array('sitename',
						   'systemmail',
						   'mailconfig');
		foreach ($defaultInfo as $row) {
			$query=$this->db->get_where("system_settings",array('key'=>$row));
			$result=$query->row_array();
			$this->default_data[$row]=$result['value'];
		}
		
		$sp1=explode("|", $this->default_data['mailconfig']);
		
		foreach($sp1 as $key => $value)
		{
			$field=explode(":", $value );
			//print_r($field);
			$this->mailsetting[$field[0]]=$field[1];
		}
		*/
		$this->load->library('email');
		$this->mailsetting['protocol'] = 'smtp';
        $this->mailsetting['host'] = 'mail.sweea.com';
        $this->mailsetting['port'] = '25';
        $this->mailsetting['user'] = 'service@sweea.com';
        $this->mailsetting['pass'] = 'ej03xu3';
        
        $this->default_data['systemmail'] = 'mktsai@sweea.com';
        $this->default_data['sitename'] = '科技部 CAPI 調查系統';
    }
	
	/**
	 * 寄信
	 * 
	 * @param string $tomail 收件者信箱
	 * @param string $subject 標題
	 * @param string $message 內容
	 * @param string $cc 副本寄送郵件 預設為空值
	 * @return json 帶boolean 跟 msg 訊息
	 */
	public function sendmail($tomail,$subject,$message,$cc=null)
	{
		$config['protocol'] = $this->mailsetting['protocol'];
		$config['smtp_host'] = $this->mailsetting['host'];
		$config['smtp_port'] = $this->mailsetting['port'];
		$config['smtp_user'] = $this->mailsetting['user'];
		$config['smtp_pass'] = $this->mailsetting['pass'];
		
		$this->email->initialize($config);
		
		$this->email->from($this->default_data['systemmail'],$this->default_data['sitename']);
		$this->email->reply_to($this->default_data['systemmail'],$this->default_data['sitename']);
		$this->email->to($tomail);
		if($cc) $this->email->cc($cc);
		$this->email->subject($subject);
		$this->email->message($message);
		
		
		if($this->email->send()){
			$value=array('result'=>TRUE,'msg'=>"寄送成功");
		}else{
			$value=array('result'=>false,'msg'=>'#寄送失敗'.$this->email->print_debugger());
		}
		return $value;
	}
	
}
	
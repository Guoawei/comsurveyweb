<?php
	/**
	 * No Use
	 */
	class tip_model extends CI_Model {
		private $_questionGuid;
		private $_tipIndex;
		private $_tipText;
		private $_tipImage;
		
		function __construct() {
			parent::__construct();
			// $this->load->model("tiptext_model");
			// $this->load->model("tipimage_model");
		}
		
		public function getTipsArray($tips)
		{
			$tipsArray = array();
			$sql = "SELECT * FROM survey_tips WHERE questionGuid ='".$tips->getQuestionGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				$tempTips = new tip_model();
				$tempTips->setQuestionGuid($row->questionGuid);
				$tempTips->setTipIndex($row->tipIndex);
				$tempTips->setTipText($row->tipText);
				$tempTips->setTipImage($row->tipImage);
				$tipsArray[] = $tempTips;
			}
			return $tipsArray;
		}
		
		public function getQuestionGuid()
		{
			return $this->_questionGuid;
		}
		
		public function setQuestionGuid($questionGuid)
		{
			$this->_questionGuid = $questionGuid;
		}
		
		public function getTipIndex()
		{			
			return $this->_tipIndex;
		}
		
		public function setTipIndex($tipIndex)
		{
			$this->_tipIndex = $tipIndex;
		}
		
		public function getTipText()
		{			
			return $this->_tipText;
		}
		
		public function setTipText($tipText)
		{
			$this->_tipText = $tipText;
		}
		
		public function getTipImage()
		{
			return $this->_tipImage;
		}
		
		public function setTipImage($tipImage)
		{
			$this->_tipImage = $tipImage;
		}
		
	}
	
?>
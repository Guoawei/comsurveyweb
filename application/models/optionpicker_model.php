<?php
    /**
     * 下拉式題目的邏輯class
     */
    class optionpicker_model extends CI_Model {
    	private $_optionPickerGuid;
		private $_optionPickerStartValue;
		private $_optionPickerEndValue;
		private $_optionPickerTickValue;
	
        function __construct() {
            parent::__construct();
        }
		
		public function getOptionPickerLogic($optionPicker)
		{
			$newOptionPicker = new optionpicker_model();
			$sql = "SELECT * FROM survey_optionpickervaluearray_logic WHERE pguid='".$optionPicker->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				$newOptionPicker->setGuid($row->pguid);
				$newOptionPicker->setStartValue($row->startValue);
				$newOptionPicker->setEndValue($row->endValue);
				$newOptionPicker->setTickValue($row->tick);
			}
			return $newOptionPicker;
		}
		
		public function add($optionPicker)
		{
			$sql = "INSERT INTO survey_optionpickervaluearray_logic (pguid, startValue, endValue, tick) 
			VALUES ('".$optionPicker->getGuid()."','".$optionPicker->getStartValue()."','".$optionPicker->getEndValue()."','".$optionPicker->getTickValue()."')";
			$this->db->query($sql);
		}
		
		public function update($optionPicker)
		{
			
		}
				
		public function getGuid()
		{
			return $this->_optionPickerGuid;
		}
				
		public function getStartValue()
		{
			return $this->_optionPickerStartValue;
		}
		
		public function getEndValue()
		{
			return $this->_optionPickerEndValue;
		}
		
		public function getTickValue()
		{
			return $this->_optionPickerTickValue;
		}
		
		
		public function setGuid($pguid)
		{
			$this->_optionPickerGuid = $pguid;
		}
		
		public function setStartValue($startValue)
		{
			$this->_optionPickerStartValue = $startValue;
		}
		
		public function setEndValue($endValue)
		{
			$this->_optionPickerEndValue = $endValue;
		}
		
		public function setTickValue($tickValue)
		{
			$this->_optionPickerTickValue = $tickValue;
		}
		
    }
    
?>
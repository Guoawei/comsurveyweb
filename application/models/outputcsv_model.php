<?php
    /**
     * 
     */
    
    class outputcsv_model extends CI_Model {
	    public $maxColumnsArray = array();    
        function __construct() {
            parent::__construct();
			
			$this->load->model('question_model');
			$this->load->model('sample_model');
			$this->load->model('surveytime_model');
			$this->load->model('surveyanswer_model');
			$this->load->model('surveysituation_model');
			$this->load->model('groupoption_model');
            $this->load->model('option_model');
        }
		
		function detectMGPType($subjectNumber,$surveyGuid)
		{
			//A1
			$question = new question_model();
			$question->setSubjectNumber($subjectNumber);
			$question = $question->getSingleQuestionBySubjectNumber($question,$surveyGuid);
			$isMGPType = 0;
			$type = $question->getType();
			if ($type == 'M' || $type == 'G' || $type == 'P') {
				$isMGPType = 1;
			}
			return $isMGPType;
		}

        private function _isSMGPType($type)
        {
            if ($type == 'S' || $type == 'M' || $type == 'G' || $type == 'P') {
                return 1;
            }else {
                return 0;
            }
        }
		
		function getMaxColumn($subjectNumber,$tableName)
		{
			//1先搜尋並分割該欄位，比較後，較大的存起來。
			$surveyAnswerModel = new surveyanswer_model();
			$columnAnswerList = $surveyAnswerModel->getAnswerBySubjectNumber($subjectNumber, $tableName);
			$maxCount = 1;
			foreach ($columnAnswerList as $row) {
				
				//分割答案，取得答案數量
				$answer = $row->$subjectNumber;

				// echo 'test'.stristr('1,2', 'p').'<br>';
				if (stristr($answer,',')!=FALSE) {
					// $tempAnswerArray = explode(',', $answer);
					// $answerCount = count($tempAnswerArray);
					$answerCount = strspn($answer,',');
					//取得最大的答案數				
					if ($answerCount > $maxCount) {
						$maxCount = $answerCount;
					}
											
				}
			}
			return $maxCount;
			
		}
		

		public function getCsvHeader($csv,$subjectNumbers,$tableName,$surveyGuid)
		{
			$tempCsv = $csv;
			$subjectNumbersCount = count($subjectNumbers);
			$surveyAnswerModel = new surveyanswer_model();
			for ($i=4; $i < $subjectNumbersCount; $i++) {
				//判斷是否為M G P三種題型
				$subjectNumber = $subjectNumbers[$i];
				$isMGPType = $this->detectMGPType($subjectNumber,$surveyGuid);
				if ($isMGPType) {
					//找出最大值，並分割欄位
					// $maxColumns = $this->getMaxColumn($subjectNumber, $tableName);
					//0827改取最大值的方法
					
					$maxColumns = $surveyAnswerModel->getMaxColumnBySubjectNumber($subjectNumber, $tableName);
					$this->setMaxColumnsArray($maxColumns, $i-4);
					if($maxColumns>1){
						//欄數大於1格才需分割
						for ($j=1; $j <= $maxColumns; $j++) {
							//ex:c1d-1 c1d-2 
							$tempCsv .= ','.$subjectNumber.'-'.($j);
						}			
					}else {
						$tempCsv .= ','.$subjectNumber;	
					}

					// $tempCsv .= ','.$subjectNumber;
					
				}else {
					$tempCsv .= ','.$subjectNumber;
				}
				 
			}
			$tempCsv .= '
			';
			
			return $tempCsv;
		}


        /*
         * 2013/05/19 產生CSV標題的總欄位數
         */
        public function getCSVHeaderWithAllOptionColumn($csv,$allSubjects,$tableName,$surveyGuid)
        {
            $tempCsv = $csv;
            $subjectNumbersCount = count($allSubjects);

            for ($i=0; $i < $subjectNumbersCount; $i++) {
                $subjectObject = $allSubjects[$i];
                $subjectNumber = $subjectObject->getSubjectNumber();
                $subjectType = $subjectObject->getType();

                if($this->_isSMGPType($subjectType)){
                    $tempCsv = $this->_addHeaderColumnBySType($subjectObject, $i, $tempCsv);
                    $tempCsv = $this->_addHeaderColumnByMType($subjectObject, $i, $tempCsv);
                    $tempCsv = $this->_addHeaderColumnByGType($subjectObject, $i, $tempCsv);
                    $tempCsv = $this->_addHeaderColumnByPType($subjectObject, $i, $tempCsv);
                }else if($subjectType == 'T'){
                    $tempCsv = $this->_addHeaderColumnByTType($subjectObject, $i, $tempCsv);
                }else {
                    $tempCsv .= ',' . $subjectNumber;
                }
            }
            $tempCsv .= '
			';

            return $tempCsv;

        }
		
		public function getCSVForOnlyHaveValueColumn($surveyGuid,$surveyVersion)
		{
			$tableName = "survey_answer_".$surveyGuid."_".$surveyVersion;
			
			$questionModel = new question_model();
			$groupOptionModel = new groupoption_model();
			
			//取得欄位名稱
			$subjectNumbers = $this->db->list_fields($tableName);
			$subjectNumbersCount = count($subjectNumbers);
			
			//設定欄位數, 抓取有值欄位，當做最大欄位數。
			$csv = '樣本名稱,樣本地址,調查分鐘數,調查狀況';
			$csv = $this->getCsvHeader($csv, $subjectNumbers, $tableName, $surveyGuid);

			$sql2 = "SELECT * FROM survey_answer_".$surveyGuid."_".$surveyVersion." order by id ASC";
			$query = $this->db->query($sql2);
			$typeArray = $questionModel->getAllQuestionType($surveyGuid);
			foreach ($query->result_array() as $row) {
				$interviewerGuid = $row['interviewerGuid'];
				$sampleGuid = $row['sampleGuid'];
				$sample = new sample_model();
				$sample->setGuid($sampleGuid);
				$sample = $sample->getSample($sample);
				
				$surveyTime = new surveytime_model();
				$surveyTime->setSampleGuid($sampleGuid);
				$surveyTime->setInterviewerGuid($interviewerGuid);
				$surveyTime->setSurveyGuid($surveyGuid);
				
				$surveySituation = new surveysituation_model();
				$surveySituation->setSampleGuid($sampleGuid);
				$surveySituation->setInterviewerGuid($interviewerGuid);
				$surveySituation->setSurveyGuid($surveyGuid);
				
				$sampleName = $sample->getName();
				$sampleAddress = $sample->getAddress();
				$sampleTotalSurveyMinute = $surveyTime->getTotalSurveyMinute($surveyTime); 
				$sampleSituation = $surveySituation->getLastSurveySituationCodeAndComment($surveySituation);
				
				$answer="";
				// if ($sampleGuid == '235104365'){
					//塞答案
					for ($i=4; $i < $subjectNumbersCount; $i++) {
						
						//該題的Type
						$type = $typeArray[$i-4];
						
						$str = "";
						//$str = str_replace (",", ";", $row[$fields[$i]]);
						//$str = str_replace ("T:", "", $str);												
						$str = str_replace ("T:", "", $row[$subjectNumbers[$i]]);
						
						//rtrim移除多餘的空白
						$str = rtrim($str);
						
						
						//將題號和$號拿掉
						$tempAnswerArray = explode(',', $str);
						
						if ($type == 'S' && count($tempAnswerArray)>1){
							$str = $tempAnswerArray[0];
						}
						if ($type == 'FI' && count($tempAnswerArray)>1){
							$str = $tempAnswerArray[0];
						}
						
						if (strpos($str,'$')>0){
							$tempStr = '';
							$str = '';
							$countAnswer = count($tempAnswerArray);
							for ($k=0; $k < $countAnswer; $k++) {
								//如果答案只有一個，只做一次就好。
								if ($countAnswer==1) {
									$pos = strpos($tempAnswerArray[$k],'$');
									$replaceStr = substr_replace($tempAnswerArray[$k], '', 0, $pos+1);
									//如果只有一個，又是G的話，就換成不同的GroupOptionValue
									if ($type =='G' && !(fnmatch('88:*', $replaceStr)) && $replaceStr !=0) {
										$replaceStr = $groupOptionModel->getGroupOptionValueById($replaceStr);
									}
									$str .= $replaceStr;
									break;															
								}
								//找$符號在第幾個位置
								$pos = strpos($tempAnswerArray[$k],'$');
								//從$符號開始回推$pos+1個長度，取代為空白
								$replaceStr = substr_replace($tempAnswerArray[$k], '', 0, $pos+1);
								//若是G題型，答案換成不同groupOptionValue，但88:不換
								if ($type =='G' && !(fnmatch('88:*', $replaceStr)) && $replaceStr !=0) {
									$replaceStr = $groupOptionModel->getGroupOptionValueById($replaceStr);
								}
								//如果是最後一個，就不加逗號
								if($k == $countAnswer-1){
									$str .= $replaceStr;
								}else {
									$str .= $replaceStr.',';
								}
								
							}

						}
												
						//再取一次該欄數然後補空白塞值
						//判斷是否為M G P三種題型
						$subjectNumber = $subjectNumbers[$i];
						// $isMGPType = $this->detectMGPType($subjectNumber,$surveyGuid);						
						//這一段效率差，每次都要去讀Type
						// $isMGPType = 0;
						// $isMGPType = $questionModel->getIsMGPType($subjectNumber, $surveyGuid);	
						
						// if ($isMGPType) {
						
						$surveyAnswerModel = new surveyanswer_model();
						
						if($type == 'M' || $type == 'G' || $type == 'P'){
							//找出最大值，並補所需的空白
							// $maxColumns = $this->getMaxColumn($subjectNumber, $tableName);
							//0827改取最大值的方法
//							echo $subjectNumber.":".$type.":";
							//$maxColumns = $surveyAnswerModel->getMaxColumnBySubjectNumber($subjectNumber, $tableName);
							$maxColumns = $this->getMaxColumnsArray($i-4);
//
							if($maxColumns>1){
								//算出擁有的答案數
								$splitAnswerArray = explode(',', $str);
								$alreadyHaveAnswerCount = count($splitAnswerArray);
								$maxColumns = $maxColumns - $alreadyHaveAnswerCount;
								for ($j=1; $j <= $maxColumns; $j++) {
									//ex:c1d-1 c1d-2 
									$str .= ',';
								}			
							}			
						}
						
						$str = preg_replace( "/\s/", "" , $str );
						$answer .= ','.$str; 
					}

					$csv .= $sampleName.','.$sampleAddress.','.$sampleTotalSurveyMinute.','.$sampleSituation.$answer.'
';
				// }
				// echo $answer;
				
				//break;
			}

			date_default_timezone_set("Asia/Taipei");
			$datestring = "%m%d";
			$time = time();
			$path = 'style/';
			$this->load->helper('date');
			$csv=$this->iconv2big5($csv);
			file_put_contents($path.'CAPI_'.mdate($datestring,$time).'.csv',$csv);
			$filesdone= 'CAPI_'.mdate($datestring,$time).'.csv';
			//echo $filesdone;
			// header('Content-Type: application/csv');
			header('Content-type:application/force-download');
            header('Content-Disposition: attachement; filename="' . $filesdone . '"');
			@readfile($path.$filesdone);
			return $filesdone;
		}

        /*
         * 2013/05/19 產生CSV標題的總欄位數
         */
        public function getCSVForFullColumn($surveyGuid,$surveyVersion)
        {

            ini_set('memory_limit', '512M');

            $tableName = "survey_answer_".$surveyGuid."_".$surveyVersion;
            
            //判斷是否有資料，下載CSV
            $checkTableSql = "SELECT count(table_name) as result FROM information_schema.tables
                              WHERE table_schema = '{$this->db->database}'
                              AND table_name = '".$tableName."';";
            $checkDataSql = "SELECT count(*) as result from ".$tableName."";
            $checkQuery = $this->db->query($checkTableSql);
            $row = $checkQuery->row();
            if($row->result == 0){
                return 0;
            }else {
                $checkQuery = $this->db->query($checkDataSql);
                $row = $checkQuery->row();
                if($row->result == 0){
                    return 0;
                }
            }
			
			
            $questionModel = new question_model();
            $groupOptionModel = new groupoption_model();

            $allSubjects = $questionModel->getAllQuestionsBySurveyGuid($surveyGuid);
            $subjectNumbersCount = count($allSubjects);

            //設定欄位數, 抓取有值欄位，當做最大欄位數。
            $csv = '樣本名稱,樣本地址,調查分鐘數,調查狀況';
            $csv = $this->getCSVHeaderWithAllOptionColumn($csv, $allSubjects, $tableName, $surveyGuid);

            $sql2 = "SELECT a.* FROM survey_answer_".$surveyGuid."_".$surveyVersion." as a ,survey_sample
            as s where a.sampleGuid = s.guid order by s.mainpriority, s.subpriority ASC";
            $query = $this->db->query($sql2);


            foreach ($query->result_array() as $row) {
                $interviewerGuid = $row['interviewerGuid'];
                $sampleGuid = $row['sampleGuid'];
                $sample = new sample_model();
                $sample->setGuid($sampleGuid);
                $sample = $sample->getSample($sample);

                $surveyTime = new surveytime_model();
                $surveyTime->setSampleGuid($sampleGuid);
                $surveyTime->setInterviewerGuid($interviewerGuid);
                $surveyTime->setSurveyGuid($surveyGuid);

                $surveySituation = new surveysituation_model();
                $surveySituation->setSampleGuid($sampleGuid);
                $surveySituation->setInterviewerGuid($interviewerGuid);
                $surveySituation->setSurveyGuid($surveyGuid);

                $sampleName = $sample->getName();
                $sampleAddress = $sample->getAddress();
                $sampleTotalSurveyMinute = $surveyTime->getTotalSurveyMinute($surveyTime);
                $sampleSituation = $surveySituation->getLastSurveySituationCodeAndComment($surveySituation);

                $answer="";
                //塞答案
                for ($i=0; $i < $subjectNumbersCount; $i++) {

                    //init
                    $str = "";
                    $type = $allSubjects[$i]->getType();
                    $subjectNumber = $allSubjects[$i]->getSubjectNumber();
                    $unFormatAnswer = $row[$subjectNumber]; //$row[A1],A1的答案
                    $groupOptionGuid = $allSubjects[$i]->getGroupOptionGuid();

                    //SType
                    if ($type == 'S'){

                        if (strpos($unFormatAnswer,':')){
                            $str = str_replace ("T:", "", $unFormatAnswer);
                            //rtrim移除多餘的空白
                            $str = rtrim($str);
                            $tempAnswerArray = explode(',', $str);
                            $text88 = '88';
                            $tempStr = '';
                            for ($k = 0 ; $k<count($tempAnswerArray) ;$k++){
                                $tempStr .= ','.$tempAnswerArray[$k];
                            }
                            $str = $text88.$tempStr;
                        }else {
                            //rtrim移除多餘的空白
                            $str = rtrim($unFormatAnswer);
                            $tempAnswerArray = explode(',', $str);
                            $maxColumn = $this->getMaxColumnsArray($i);

                            //可能出現$字號的處理狀況
                            if(strpos($tempAnswerArray[0],'$')){
                                $pos = strpos($tempAnswerArray[0],'$');
                                $replaceStr = substr_replace($tempAnswerArray[0], '', 0, $pos+1);
                                $tempStr = $replaceStr;
                            }else {
                                $tempStr = $tempAnswerArray[0];
                            }

                            for ($j = 1 ; $j<$maxColumn; $j++){
                                $tempStr .= ',';
                            }
                            $str = $tempStr;
                        }
                    }

                    //FOType
                    if ($type == 'FO'){
                        $str = $unFormatAnswer;
                    }
                    //FIType
                    if ($type == 'FI'){
                        $str = $unFormatAnswer;
                    }
                    //T Type
                    if ($type == 'T'){
                        $str = rtrim($unFormatAnswer);
                        $tempAnswerArray = explode(',', $str);
                        $tempStr = "";
                        $maxColumn = $this->getMaxColumnsArray($i);
                        $fillEmptyColumnCount = $maxColumn - count($tempAnswerArray);
                        for ($fillCount = 0 ;$fillCount < $fillEmptyColumnCount; $fillCount ++ ){
                            $str .= ',';
                        }
                    }

                    //PType
                    if ($type == 'P'){
                        $str = rtrim($unFormatAnswer);
                        $tempAnswerArray = explode(',', $str);
                        $tempStr = "";
                        $maxColumn = $this->getMaxColumnsArray($i);
                        for ($k = 1 ; $k<=$maxColumn ;$k++){
                            $replaceStr = "";
                            if($maxColumn==count($tempAnswerArray)){
                                $pos = strpos($tempAnswerArray[$k-1],'$');
                                $replaceStr = substr_replace($tempAnswerArray[$k-1], '', 0, $pos+1);
                            }
                            if($k == $maxColumn){
                                $tempStr .= $replaceStr;
                            }else {
                                $tempStr .= $replaceStr.',';
                            }
                        }

                        $str = $tempStr;
                    }

                    //MType
                    if ($type == 'M'){

                        $formatAnswer = rtrim($unFormatAnswer);
                        $tempAnswerArray = explode(',', $formatAnswer);
                        $maxColumn = $this->getMaxColumnsArray($i);

                        $tempStr = "";
                        $temp88Str = "";
                        for ($j = 1 ; $j<=$maxColumn; $j++){
                            $notMatchedAnswer = TRUE;
                            for ($k = 0 ; $k<count($tempAnswerArray) ;$k++){
                                if($subjectNumber=='B2c'){
//                                    echo $subjectNumber.':'.$unFormatAnswer;
//                                    echo count($tempAnswerArray);
//                                    echo "88".$tempAnswerArray[$k];
//                                    echo $maxColumn.';';

                                }

                                $pos = strpos($tempAnswerArray[$k],'$');
                                $noSignAnswerIndex = explode('$',$tempAnswerArray[$k]);
                                $noSignAnswer = substr_replace($tempAnswerArray[$k], '', 0, $pos+1);
                                if($noSignAnswerIndex[0] == $j){
                                    //如果有其它選項
                                    if(strpos($tempAnswerArray[$k],':88')){
                                        $noSign88Answer = substr_replace($noSignAnswer, '',0 ,5);
                                        $temp88Str .= $noSign88Answer;
                                        if($j == $maxColumn){
                                            $tempStr .= $noSign88Answer;
                                        }else {
                                            $tempStr .= '88'.',';
                                        }
                                    }else {
                                        //如果是最後一個，就不加逗號
                                        if($j == $maxColumn){
                                            $tempStr .= $noSignAnswer;
                                        }else {
                                            $tempStr .= $noSignAnswer.',';
                                        }
                                    }
                                    $notMatchedAnswer = FALSE;
                                }
                            }
                            if($notMatchedAnswer){
//                                $optionModel = new option_model();
//                                $subjectObject = $allSubjects[$i];
//                                $max88Columns = $optionModel->getCountByMType88Option($subjectObject->getOptionGuid());
                                if($j == $maxColumn){
                                    if($temp88Str==''){
//                                        $tempStr .= ',@M@';
                                    }else {
                                        $tempStr .= $temp88Str;
                                    }


                                }else {
                                    $tempStr .= ',';
                                }
                            }
                        }
                        $str = $tempStr;
                    }

                    if ($type == 'G'){
                        $str = rtrim($unFormatAnswer);
                        $tempAnswerArray = explode(',', $str);
                        $maxColumn = $this->getMaxColumnsArray($i);
                        $mainColumns = $groupOptionModel->listValueByGroupMainOption($groupOptionGuid);
                        $subColumns = $groupOptionModel->listValueByGroupSubOption($groupOptionGuid);
                        $option88ColumnsCount = $groupOptionModel->getCountByGroup88Option($groupOptionGuid);

                        $tempStr = "";
                        $temp88Str = "";
                        for ($j = 1 ; $j<=$maxColumn; $j++){
                            $notMatchedAnswer = TRUE;
                            for ($k = 0 ; $k<count($tempAnswerArray) ;$k++){

                                if(strpos($tempAnswerArray[$k],":88")!=2){
                                    $pos = strpos($tempAnswerArray[$k],'$');
                                    $noSignAnswerId = substr_replace($tempAnswerArray[$k], '', 0, $pos+1);
                                    $groupOptionObject = $groupOptionModel->getGroupOptionObjectById($noSignAnswerId);
                                    $noSignAnswer = $groupOptionObject->getValue();
                                }

                                if($groupOptionObject->getType()==0 && $j <= count($mainColumns)){
                                    //Main
                                    if($j == $groupOptionObject->getValue()){
                                        if($mainColumns[$j-1] == $groupOptionObject->getValue()){
                                            if(strpos($tempAnswerArray[$k],":88")!=2){
                                                $tempStr .= $noSignAnswer.',';
                                                $notMatchedAnswer = FALSE;
                                            }
                                        }
                                    }
//                                    echo '$j:'.$j.'  $k:'.$k.'  answer:'.$noSignAnswer.'<br>';
                                }else if($groupOptionObject->getType()!=0 && $j > count($mainColumns)){
                                    //Sub
                                    if($j == $maxColumn && $option88ColumnsCount > 0){
                                        if(strpos($tempAnswerArray[$k],':88')){

                                            $pos = strpos($tempAnswerArray[$k],'T:88:');
                                            //will 2014/7/9 修改，有兩個答案的群組題，不能正確在其他欄位加上,區隔以填內容
                                            if(count($tempAnswerArray)==2)
                                            {
                                                $temp88Str = ",".substr_replace($tempAnswerArray[$k], '',0 ,$pos+5);
                                            }
                                            else
                                            {
                                                $temp88Str = substr_replace($tempAnswerArray[$k], '',0 ,$pos+5);
                                            }
                                            
//                                            echo '$j:'.$j.'  $k:'.$k.'  subcolumns:'.$temp88Str.'<br>';
                                        }
                                    }else if($j == $maxColumn-1 && $option88ColumnsCount > 0){
                                        if(strpos($tempAnswerArray[$k],':88')){
//                                            echo '$j:'.$j.'  $k:'.$k.'  subcolumns:'.'88'.'<br>';
                                            $tempStr .= '88'.',';
                                            $notMatchedAnswer = FALSE;
                                        }
                                    }else {
                                        if($subColumns[($j-count($mainColumns))-1] == $groupOptionObject->getValue() && !strpos($tempAnswerArray[$k],':88')){
                                            if($j == $maxColumn){
                                                $tempStr .= $noSignAnswer;
                                            }else {
//                                                echo '$j:'.$j.' '.$k.'  '.$tempAnswerArray[$k].";".$pos.' answer:'.$noSignAnswer.'<br>';
                                                $tempStr .= $noSignAnswer.',';
                                            }
                                            $notMatchedAnswer = FALSE;
                                        }

                                    }

                                }else if(count($tempAnswerArray)==2 && strpos($tempAnswerArray[$k],":88")==2){
                                    $pos = strpos($tempAnswerArray[$k],':88');
                                    $temp88Str = '88'.','.substr_replace($tempAnswerArray[$k], '',0 ,$pos+4);

                                }
                            }
                            if($notMatchedAnswer){
                                if($j == $maxColumn){
                                    if($temp88Str!=''){
                                        if(count($tempAnswerArray)==2 && strpos($tempAnswerArray[1],":88")==2){
                                            $tempStr = substr($tempStr,0,strlen($tempStr)-1);
                                        }
                                        $tempStr .= $temp88Str;
                                    }
                                }else {
                                    $tempStr .= ',';
                                }
                            }
                        }
                        $str = $tempStr;
                    }

                    $str = preg_replace( "/\s/", "" , $str );
                    $answer .= ','.$str;
                }

                $csv .= $sampleName.','.$sampleAddress.','.$sampleTotalSurveyMinute.','.$sampleSituation.$answer.'
';
            }
			
             //取得該調查名稱
            $file_name = 'CAPI_';
            $query = $this->db->get_where('survey_surveys',array('guid' => $surveyGuid));
            if($query->num_rows())
            {
                $survey = $query->row_array();
                $file_name = $survey['title']."_";
            }              
            $file_name = $this->iconv2big5($file_name);
            
            date_default_timezone_set("Asia/Taipei");
            $datestring = "%m%d";
            $time = time();
            $path = 'style/';
            $this->load->helper('date');
            $csv=$this->iconv2big5($csv);
            file_put_contents($path.$file_name.mdate($datestring,$time).'.csv',$csv);
            $filesdone= $file_name.mdate($datestring,$time).'.csv';
            //echo $filesdone;
            // header('Content-Type: application/csv');
            header('Content-type:application/force-download');
            header('Content-Disposition: attachement; filename="' . $filesdone . '"');
            @readfile($path.$filesdone);
            return $filesdone;
        }

		function iconv2big5($str){
			//return iconv("UTF-8",'BIG5//TRANSLIT//IGNORE',$str);
			return mb_convert_encoding($str,"big5","utf-8");
		}

        //S,M,G,P已分割後的暫存答案欄位數
		public function setMaxColumnsArray($count,$index)
		{

			$this->maxColumnsArray[$index] = $count;	
		}	
		
		public function getMaxColumnsArray($index)
		{
//            echo $this->maxColumnsArray[$index].",";
			return $this->maxColumnsArray[$index];
		}

        /**
         * @param $maxColumns
         * @param $max88Columns
         * @param $subjectNumber
         * @param $tempCsv
         * @return string
         */
        private function _addHeaderColumnAnd88ColumnToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv)
        {
//            if ($max88Columns > 0) {
//                $maxColumns = $maxColumns - $max88Columns;
//            }

            for ($j = 1; $j <= $maxColumns; $j++) {
                //ex:c1d-1 c1d-2
                if ($maxColumns==1){
                    $tempCsv .= ',' . $subjectNumber;
                }else {
                    $tempCsv .= ',' . $subjectNumber . '-' . ($j);
                }

            }

            for ($k = 0; $k < $max88Columns; $k++) {
                $tempCsv .= ',' . $subjectNumber . '-其它';
            }

            return $tempCsv;

        }

        private function _addHeaderColumnAnd88ColumnForSTypeOptionToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv)
        {
            for ($j = 1; $j <= $maxColumns; $j++) {
                $tempCsv .= ',' . $subjectNumber;
            }

            for ($k = 0; $k < $max88Columns; $k++) {
                $tempCsv .= ',' . $subjectNumber . '-其它';
            }

            return $tempCsv;

        }

        private function _addHeaderColumnForGroupOptionToCSV($mainColumns,$subColumns,$option88Columns,
                                                             $subjectNumber,$tempCsv)
        {
            for ($i = 1; $i <= count($mainColumns); $i++) {
                $tempCsv .= ',' . $subjectNumber . '大類-' . ($mainColumns[$i-1]);
            }

            for ($j = 1; $j <= count($subColumns); $j++) {
                $tempCsv .= ',' . $subjectNumber . '-' . ($subColumns[$j-1]);
            }

            for ($i = 1; $i <= $option88Columns; $i++) {
                $tempCsv .= ',' . $subjectNumber . '-其它';
            }

            return $tempCsv;
        }

        /**
         * @param $subjectObject
         * @param $i
         * @param $subjectNumber
         * @param $tempCsv
         * @return string
         */
        private function _addHeaderColumnByMType($subjectObject, $i, $tempCsv)
        {
            if($subjectObject->getType() == 'M'){
                $optionModel = new option_model();
                $subjectNumber = $subjectObject->getSubjectNumber();
                $maxColumns = $optionModel->getCountByMTypeOption($subjectObject->getOptionGuid());
                $max88Columns = $optionModel->getCountByMType88Option($subjectObject->getOptionGuid());
                $this->setMaxColumnsArray($maxColumns+$max88Columns, $i);
                $tempCsv = $this->_addHeaderColumnAnd88ColumnToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv);
            }

            return $tempCsv;
        }

        private function _addHeaderColumnBySType($subjectObject, $i, $tempCsv)
        {
            if($subjectObject->getType() == 'S'){
                $optionModel = new option_model();
                $subjectNumber = $subjectObject->getSubjectNumber();
                $maxColumns = 1;
                $max88Columns = $optionModel->getCountBySType88Option($subjectObject->getOptionGuid());
                $this->setMaxColumnsArray($maxColumns+$max88Columns, $i);
                $tempCsv = $this->_addHeaderColumnAnd88ColumnForSTypeOptionToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv);
            }

            return $tempCsv;
        }

        private function _addHeaderColumnByGType($subjectObject, $i, $tempCsv)
        {
            if($subjectObject->getType() == 'G'){
                $groupOptionModel = new groupoption_model();
                $subjectNumber = $subjectObject->getSubjectNumber();
                $groupOptionGuid = $subjectObject->getGroupOptionGuid();
//                $mainColumns = $groupOptionModel->getCountByGroupMainOption($groupOptionGuid);
//                $subColumns = $groupOptionModel->getCountByGroupSubOption($groupOptionGuid);
                $mainColumns = $groupOptionModel->listValueByGroupMainOption($groupOptionGuid);
                $subColumns = $groupOptionModel->listValueByGroupSubOption($groupOptionGuid);
                $option88Columns = $groupOptionModel->getCountByGroup88Option($groupOptionGuid);
                $this->setMaxColumnsArray(count($mainColumns)+count($subColumns)+$option88Columns, $i);
                $tempCsv = $this->_addHeaderColumnForGroupOptionToCSV($mainColumns,$subColumns,$option88Columns,$subjectNumber,$tempCsv);
            }
            return $tempCsv;
        }

        private function _addHeaderColumnByPType($subjectObject, $i, $tempCsv)
        {
            if($subjectObject->getType() == 'P'){
                $optionModel = new option_model();
                $subjectNumber = $subjectObject->getSubjectNumber();
                $maxColumns = $optionModel->getCountByPTypeOption($subjectObject->getOptionGuid());
                $max88Columns = 0;
                $this->setMaxColumnsArray($maxColumns, $i);
                $tempCsv = $this->_addHeaderColumnAnd88ColumnToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv);
            }
            return $tempCsv;
        }

        private function _addHeaderColumnByTType($subjectObject, $i, $tempCsv)
        {
            if($subjectObject->getType() == 'T'){
                $optionModel = new option_model();
                $subjectNumber = $subjectObject->getSubjectNumber();
                $maxColumns = $optionModel->getCountByTTypeOption($subjectObject->getOptionGuid());
                $max88Columns = 0;
                $this->setMaxColumnsArray($maxColumns, $i);
                $tempCsv = $this->_addHeaderColumnAnd88ColumnToCSV($maxColumns, $max88Columns, $subjectNumber, $tempCsv);
            }
            return $tempCsv;
        }

    }
    
	
?>
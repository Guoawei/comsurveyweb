<?php 

/**
 *
 */
class option_model extends CI_Model{

	private $_guid;
	private $_surveygGuid;
	private $_optionIndex;
	private $_title;
	private $_optionValue;
	private $_isShowTextView;	
	private $_textViewPosition;
	private $_pickerValueArrayGuid;
	private $_dyPickerSubject;
	private $_dyPickerMin;
	private $_dyPickerMax;
	private $_dyPickerTick;
	private $_optionTips;

	function __construct() {
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('optionPicker_model');
		$this->setGuid(do_hash(microtime(),'md5'));
	}
	
	public function getOptionArray($option)
	{
		$optionArray = array();
		$sql = "SELECT * FROM survey_option WHERE guid ='".$option->getGuid()."' order by optionIndex";
		$result = $this->db->query($sql);
		foreach ($result->result() as $row) {
			$option = new option_model();				
			$option->setGuid($row->guid);
			$option->setSurveyGuid($row->surveyGuid);
			$option->setOptionIndex($row->optionIndex);
			$option->setTitle($row->title);
			$option->setOptionValue($row->optionValue);
			$option->setIsShowTextView($row->optionIsShowTextView);
			$option->setTextViewPosition($row->optionTextViewPosition);
			$option->setPickerValueArrayGuid($row->optionPickerValueArrayGuid);
			$option->setDyPickerSubject($row->optionDyPickerSubject);
			$option->setDyPickerMin($row->optionDyPickerMin);
			$option->setDyPickerMax($row->optionDyPickerMax);
			$option->setDyPickerTick($row->optionDyPickerTick);
            $option->setOptionTips($row->optionTips);
			$optionArray[] = $option;
		}
		return $optionArray;		
	}
	
	public function getPickerValueArray($option)
	{
		$pickerValueArray = array();
		$sql = "SELECT * FROM survey_optionpickervaluearray WHERE guid='".$option->getPickerValueArrayGuid()."' order by value";
		$result = $this->db->query($sql);
		foreach ($result->result() as $row) {
			$pickerValueArray[] = $row->value;
		}
		if (count($pickerValueArray)<=0)
			$pickerValueArray = NULL;
		return $pickerValueArray;
		
	}
	
	public function addOption($option)
	{
		$optionTextViewPosition = $option->getTextViewPosition();
		$optionPickerValueArrayGuid = $option->getPickerValueArrayGuid();
		$dyPickerSubject = $option->getDyPickerSubject();
		$dyPickerMin = $option->getDyPickerMin();
		$dyPickerMax = $option->getDyPickerMax();
		$dyPickerTick = $option->getDyPickerTick();
		if($optionTextViewPosition==""){
			$optionTextViewPosition = 'NULL';
		}else {
			$optionTextViewPosition = "'".$optionTextViewPosition."'";
		}
		if($optionPickerValueArrayGuid==""){
			$optionPickerValueArrayGuid = 'NULL';
		}else {
			$optionPickerValueArrayGuid = "'".$optionPickerValueArrayGuid."'";
		}
		if($dyPickerSubject==""){
			$dyPickerSubject = 'NULL';
		}else {
			$dyPickerSubject = "'".$dyPickerSubject."'";
		}
		if($dyPickerMin==""){
			$dyPickerMin = 'NULL';
		}else {
			$dyPickerMin = "'".$dyPickerMin."'";
		}
		if($dyPickerMax==""){
			$dyPickerMax = 'NULL';
		}else {
			$dyPickerMax = "'".$dyPickerMax."'";
		}
		if($dyPickerTick==""){
			$dyPickerTick = 'NULL';
		}else {
			$dyPickerTick = "'".$dyPickerTick."'";
		}
        $optionTips = addslashes($option->getOptionTips());
		
		$sql = "INSERT INTO survey_option (guid, surveyGuid, optionIndex, title, optionValue, optionIsShowTextView, optionTextViewPosition, optionPickerValueArrayGuid
				,optionDyPickerSubject, optionDyPickerMin, optionDyPickerMax, optionDyPickerTick, optionTips)
        VALUES ('".$option->getGuid()."', '".$option->getSurveyGuid()."','".(int)$option->getOptionIndex()."','".addslashes(str_replace('"',"'",$option->getTitle()))."','".(int)$option->getOptionValue()."'
        ,'".$option->getIsShowTextView()."',".$optionTextViewPosition.",".$optionPickerValueArrayGuid.",".$dyPickerSubject.",".$dyPickerMin.",".$dyPickerMax."
        ,".$dyPickerTick.",'".addslashes(str_replace('"',"'",$optionTips))."')";

		$this->db->query($sql);

		// echo $this->db->affected_rows();
	}
	
	public function updateOption($option)
	{
		$optionTextViewPosition = $option->getTextViewPosition();
		$optionPickerValueArrayGuid = $option->getPickerValueArrayGuid();
		if($optionTextViewPosition==""){
			$optionTextViewPosition = 'NULL';
		}else {
			$optionTextViewPosition = "'".$optionTextViewPosition."'";
		}
		if($optionPickerValueArrayGuid==""){
			$optionPickerValueArrayGuid = 'NULL';
		}else {
			$optionPickerValueArrayGuid = "'".$optionPickerValueArrayGuid."'";
		}
		$sql = "UPDATE survey_option SET guid='".$option->getGuid()."', surveyGuid='".$option->getSurveyGuid()."', optionIndex='".(int)$option->getOptionIndex()."'
		, title='".addslashes($option->getTitle())."', optionValue='".(int)$option->getOptionValue()."', optionIsShowTextView='".$option->getIsShowTextView()."'
		, optionTextViewPosition=".$optionTextViewPosition.", optionPickerValueArrayGuid=".$optionPickerValueArrayGuid.", optionTips='".addslashes(str_replace('"',"'",$option->getOptionTips()))."'
		 where guid='".$option->getGuid()."'"; 

		$this->db->query($sql);

		// echo $this->db->affected_rows();
	}
	
	public function delete($optionGuid)
	{
		$optionObject = new option_model();
		$optionObject->setGuid($optionGuid);
		$optionArray = $this->getOptionArray($optionObject);
		for ($i=0; $i < count($optionArray); $i++) {
			$tempOptionObject = new option_model();
			$tempOptionObject = $optionArray[$i];
			$this->deletePickerValue($tempOptionObject->getPickerValueArrayGuid());		
		}
	
		$sql = "DELETE FROM survey_option where guid='".$optionGuid."'"; 
		$this->db->query($sql);
	}
	
	public function deleteOptionFromSurveyGuid($suveyGuid)
	{
		$sql = "DELETE FROM survey_option where surveyGuid='".$suveyGuid."'"; 

		$this->db->query($sql);
	}

	public function deletePickerValue($optionPickerGuid)
	{
		$sql1 = "DELETE FROM survey_optionpickervaluearray_logic where pguid='".$optionPickerGuid."'"; 
		$sql2 = "DELETE FROM survey_optionpickervaluearray where guid='".$optionPickerGuid."'";
		$this->db->query($sql1);
		$this->db->query($sql2);
			
	}

	public function addPickerValueArray($pickerValueArray,$optionPickerObject)
	{
//		$pickerValueArrayGuid = (do_hash(microtime(),'md5'));
        $mkTime = explode(" ", microtime(false));
        $microTime = explode(".",$mkTime[0]);
        $randNumber = rand(0,1000);
        $pickerValueArrayGuid = 'PVA'.$randNumber.$microTime[1];
		$opb = new optionPicker_model();
		$opb = $optionPickerObject;
		$opb->setGuid($pickerValueArrayGuid);
		for ($i =0 ;$i<count($pickerValueArray); $i++){
			$sql = "INSERT INTO survey_optionpickervaluearray (guid, value) 
			VALUES ('".$pickerValueArrayGuid."', '".$pickerValueArray[$i]."')";
			$this->db->query($sql);
		}
		$opb->add($opb);
		return $pickerValueArrayGuid;
	}

    /*
     * 2013/05/19 取得某題目的選項數量
     */
    public function getCountByMTypeOption($optionGuid)
    {
        return $this->_getCountByOption($optionGuid);
    }

    /*
     * 2013/05/19 取得某題目的選項數量
     */
    public function getCountByMType88Option($optionGuid)
    {
        return $this->_getCountBy88Option($optionGuid);
    }

    /*
     * 2013/05/19 取得某題目的選項數量
     */
    public function getCountBySType88Option($optionGuid)
    {
        return $this->_getCountBy88Option($optionGuid);
    }

    /*
     * 2013/05/19 取得某題目的選項數量
     */
    public function getCountByPTypeOption($optionGuid)
    {
        return $this->_getCountByOption($optionGuid);
    }

    /*
     * 2013/12/07 取得T題的選項數量
     */
    public function getCountByTTypeOption($optionGuid)
    {
        return $this->_getCountByOption($optionGuid);
    }

	public function setGuid($guid){
		$this->_guid = $guid;
	}
	
	public function getGuid(){
		return $this->_guid;
	}
	
	public function setSurveyGuid($surveyGuid){
		$this->_surveygGuid = $surveyGuid;
	}
	
	public function getSurveyGuid(){
		return $this->_surveygGuid;
	}
	
	public function setTitle($title){
		$this->_title = $title;
	}
	
	public function getTitle(){
		return $this->_title;
	}
	
	public function setOptionIndex($optionIndex){
		$this->_optionIndex = $optionIndex;
	}
	
	public function getOptionIndex(){
		return $this->_optionIndex;
	}
	
	public function setOptionValue($optionValue)
	{
		$this->_optionValue = $optionValue;
	}
	
	public function getOptionValue()
	{
		return $this->_optionValue;
	}
	
	public function setIsShowTextView($isShowTextView)
	{
		$this->_isShowTextView = $isShowTextView;
	}
	
	public function getIsShowTextView()
	{
		return $this->_isShowTextView;
	}
	
	public function setTextViewPosition($textViewPosition)
	{
		$this->_textViewPosition = $textViewPosition;
	}
	
	public function getTextViewPosition()
	{
		return $this->_textViewPosition;
	}
	
	public function setPickerValueArrayGuid($pickerValueArrayGuid)
	{
		$this->_pickerValueArrayGuid = $pickerValueArrayGuid;
	}
	
	public function getPickerValueArrayGuid()
	{
		return $this->_pickerValueArrayGuid;
	}
	
	public function setDyPickerSubject($dyPickerSubject)
	{
		$this->_dyPickerSubject = $dyPickerSubject;
	}
	
	public function getDyPickerSubject()
	{
		return $this->_dyPickerSubject;	
	}
	
	public function setDyPickerMin($dyOptionMin)
	{
		$this->_dyPickerMin = $dyOptionMin;
	}
	
	public function getDyPickerMin()
	{
		return $this->_dyPickerMin;
	}
	
	public function setDyPickerMax($dyOptionMax)
	{
		$this->_dyPickerMax = $dyOptionMax;
	}
	
	public function getDyPickerMax()
	{
		return $this->_dyPickerMax;
	}
	
	public function setDyPickerTick($dyOptionTick)
	{
		$this->_dyPickerTick = $dyOptionTick;
	}
	
	public function getDyPickerTick()
	{
		return $this->_dyPickerTick;
	}

    /**
     * @param $optionGuid
     * @return int
     */
    private function _getCountBy88Option($optionGuid)
    {
        $sql = "SELECT count(guid) as option88Count FROM survey_option WHERE guid = '" . $optionGuid . "' and optionValue = 88 ";
        $result = $this->db->query($sql);
        $optionCount = 0;
        foreach ($result->result() as $row) {
            $optionCount = $row->option88Count;
        }
        return $optionCount;
    }

    /**
     * @param $optionGuid
     * @return int
     */
    private function _getCountByOption($optionGuid)
    {
        $sql = "SELECT count(guid) as optionCount FROM survey_option WHERE guid = '" . $optionGuid . "'";
        $result = $this->db->query($sql);
        $optionCount = 0;
        foreach ($result->result() as $row) {
            $optionCount = $row->optionCount;
        }
        return $optionCount;
    }

    public function getOptionTips()
    {
        return $this->_optionTips;
    }

    public function setOptionTips($optionTips)
    {
        $this->_optionTips = $optionTips;
    }
}
?>
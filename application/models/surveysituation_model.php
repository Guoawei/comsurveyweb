<?php
    /**
     * 
     */
    class surveysituation_model extends CI_Model {
        
		private $_sampleGuid;
		private $_interviewerGuid;
		private $_surveyGuid;
		private $_situationCode;
		private $_comment;
		private $_cDateTime;
		
        function __construct() {
            parent::__construct();
			//Initial Guid for md5
        }
		
		public function getSurveySituation($surveySituation)
		{
			$sql = "SELECT * FROM survey_surveysituation where sampleGuid='".$surveySituation->getSampleGuid()."' and interviewerGuid='"
			.$surveySituation->getInterviewerGuid()."' and surveyGuid='".$surveySituation->getSurveyGuid()."'"; 
			$this->db->query($sql);
			$result = $this->db->query($sql);
			$list = array();
			foreach ($result->result() as $row){
				$surveySituation = new surveysituation_model();
				$surveySituation->setSampleGuid($row->sampleGuid);
				$surveySituation->setInterviewerGuid($row->interviewerGuid);
				$surveySituation->setSurveyGuid($row->surveyGuid);
				$surveySituation->setSituationCode($row->situationCode);
				$surveySituation->setComment($row->comment);
				$surveySituation->setCDateTime($row->cDateTime);
				$list[] = $surveySituation;
			}
			return $list;
		}
				
		public function checkSurveySituationAlreadyHave($surveySituation)
		{
			$sql = "SELECT sampleGuid FROM survey_surveysituation where sampleGuid='".$surveySituation->getSampleGuid()."' and interviewerGuid='"
			.$surveySituation->getInterviewerGuid()."' and surveyGuid='".$surveySituation->getSurveyGuid()."' and cDateTime='".$surveySituation->getCDateTime()."'"; 
			$this->db->query($sql);
			$result = $this->db->query($sql);
			return $result->num_rows();
		}
		
		
		public function addSurveySituation($surveySituation)
		{
			$sql = "INSERT INTO survey_surveysituation (sampleGuid, interviewerGuid, surveyGuid, situationCode, comment, cDateTime) 
        	VALUES ('".$surveySituation->getSampleGuid()."', '".$surveySituation->getInterviewerGuid()."','".$surveySituation->getSurveyGuid()."'
        	,'".$surveySituation->getSituationCode()."','".$surveySituation->getComment()."' ,'".$surveySituation->getCDateTime()."')";

			$this->db->query($sql);
		}
		
		public function updateSurveySituation($surveySituation)
		{
			$sql = "UPDATE survey_surveysituation SET sampleGuid='".$surveySituation->getSampleGuid()."'
			, interviewerGuid='".$surveySituation->getInterviewerGuid()."', surveyGuid='".$surveySituation->getSurveyGuid()."'
			, situationCode='".$surveySituation->getSituationCode()."', comment='".$surveySituation->getComment()."', cDateTime='".$surveySituation->getCDateTime()."'
			where cDateTime='".$surveySituation->getCDateTime()."'";

			$this->db->query($sql);

			// echo $this->db->affected_rows();
		}
		
		public function deleteSurveySituation($surveySituation)
		{
			$sql = "DELETE FROM survey_surveysituation where sampleGuid='".$surveySituation->getGuid()."' and interviewerGuid='".$surveySituation->getInterviewerGuid()."'
			 and surveyGuid='".$surveySituation->getSurveyGuid()."' and sIndex='".$surveySituation->getSituationCode()."'"; 
			$this->db->query($sql);
			// echo $this->db->affected_rows();
		}
		
		public function getLastSurveySituationCodeAndComment($surveySituation)
		{
			$sql = "SELECT situationCode, comment FROM survey_surveysituation where sampleGuid='".$surveySituation->getSampleGuid()."' and surveyGuid='".$surveySituation->getSurveyGuid()."'
			 and interviewerGuid='".$surveySituation->getInterviewerGuid()."' order by cDateTime DESC limit 0,1"; 
			$result = $this->db->query($sql);
			$codeOrComment ='';
			foreach ($result->result() as $row){
				$codeOrComment = $row->situationCode;
				if ($codeOrComment == '98') {
					$codeOrComment = $row->comment;
				}
			}
			return $codeOrComment;
		}
		
		public function getSampleGuid()
		{
			return $this->_sampleGuid;
		}
		
		public function setSampleGuid($sampleGuid)
		{
			$this->_sampleGuid = $sampleGuid;
		}
		
		public function getInterviewerGuid()
		{
			return $this->_interviewerGuid;
		}
		
		public function setInterviewerGuid($interviewerGuid)
		{
			$this->_interviewerGuid = $interviewerGuid;
		}
		
		public function getSurveyGuid()
		{
			return $this->_surveyGuid;
		}
		
		public function setSurveyGuid($surveyGuid)
		{
			$this->_surveyGuid = $surveyGuid;
		}
		
		public function getSituationCode()
		{
			return $this->_situationCode;
		}
		
		public function setSituationCode($situationCode)
		{
			$this->_situationCode = $situationCode;
		}
		
		public function getComment()
		{
			return $this->_comment;
		}
		
		public function setComment($comment)
		{
			$this->_comment = $comment;
		}
		
		public function getCDateTime()
		{
			return $this->_cDateTime;
		}
		
		public function setCDateTime($cDateTime)
		{
			$this->_cDateTime = $cDateTime;			
		}
		
    }
    
?>
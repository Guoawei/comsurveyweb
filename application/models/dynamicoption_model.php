<?php
    /**
     * 
     */
    class dynamicoption_model extends CI_Model {
        
		private $_guid;
		private $_accordingKey;
		private $_subjectIndex;
		private $_accordingValue;
		private $_optionIndex;
		private $_optionText;
		private $_optionValue;
		
        function __construct() {
            parent::__construct();
			$this->load->helper('security');
			$this->setGuid(do_hash(microtime(),'md5'));
        }
		
		public function getDynamicOption($dyOption)
		{
			$accordingKey = "";
			$sql = "SELECT * FROM survey_dynamicoption WHERE guid ='".$dyOption->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				// $dyOption = new dynamicoption_model();
				$dyOption->setAccordingKey($row->accordingKey);
				$dyOption->setSubjectIndex($row->subjectIndex);
			}
			return $dyOption;
		}
		
		public function getDynamicOptionList($dyOption)
		{
			$dyOptionArray = array();
			$sql = "SELECT * FROM survey_dynamicoptionlist WHERE dynamicOptionGuid ='".$dyOption->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {
				$dyOption = new dynamicoption_model();				
				$dyOption->setGuid($row->dynamicOptionGuid);
				$dyOption->setSubjectIndex($dyOption->getSubjectIndex());
				$dyOption->setAccordingValue($row->accordingValue);
				$dyOption->setOptionIndex($row->optionIndex);
				$dyOption->setOptionText($row->optionText);
				$dyOption->setOptionValue($row->optionValue);
				$dyOptionArray[] = $dyOption;
			}
			return $dyOptionArray;		
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setAccordingKey($accordingKey)
		{
			$this->_accordingKey = $accordingKey;
		}
		
		public function getAccordingKey()
		{
			return $this->_accordingKey;
		}
		
		public function setSubjectIndex($subjectIndex)
		{
			$this->_subjectIndex = $subjectIndex;
		}
		
		public function getSubjectIndex()
		{
			return $this->_subjectIndex;
		}
		
		public function setAccordingValue($accordingValue)
		{
			$this->_accordingValue = $accordingValue;
		}
		
		public function getAccordingValue()
		{
			return $this->_accordingValue;
		}
		
		public function setOptionIndex($optionIndex)
		{
			$this->_optionIndex = $optionIndex;
		}
		
		public function getOptionIndex()
		{
			return $this->_optionIndex;
		}
		
		public function setOptionText($optionText)
		{
			$this->_optionText = $optionText;
		}
		
		public function getOptionText()
		{
			return $this->_optionText;
		}
		
		public function setOptionValue($optionValue)
		{
			$this->_optionValue = $optionValue;
		}
		
		public function getOptionValue()
		{
			return $this->_optionValue;
		}
    }
    
?>
<?php 
	/**
	 * 
	 */
	class questionnaire_model extends CI_Model{
		private $_id;
		private $_type;
		private $_subjectNumber;
		private $_subject;
		private $_option;
		private $_pass;
		private $_tips;
		
		function __construct(){
			  parent::__construct();
			  $this->load->model("option_model");
		}
		
		public function setId($id){
			$this->_id = $id;
		}
		
		public function getId(){
			return $this->_id;
		}
		
		public function setType($type){
			$this->_type = $type;
		}
		
		public function getType(){
			return $this->_type;
		}
		
		public function getSubjectNumber(){
			return $this->_subjectNumber;
		}
		
		public function setSubjectNumber($subjectNumber){
			$this->_subjectNumber = $subjectNumber;
		}
		
		public function setSubject($subject){
			$this->_subject = $subject;
		}
		
		public function getSubject(){
			return $this->_subject;
		}
		
		public function setOption(option_model $option){
			$this->_option = $option;
		}
		
		public function getOption(){
			return $this->_option;
		}
		
		public function setPass($pass){
			$this->_pass = $pass;
		}
		
		public function getPass(){
			return $this->_pass;
		}
		
		public function setTips( $tips){
			$this->_tips = $tips;
		}
		
		public function getTips(){
			return $this->_tips;
		}
	}
	
	
?>
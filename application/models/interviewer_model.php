<?php
    /**
     * 
     */
    class interviewer_model extends CI_Model {
        
		private $_guid;
		private $_name;
		private $_email;
		private $_pwd;
		
        function __construct() {
           parent::__construct();  
		   $this->load->helper('security');
		   $this->load->model('account_model');
		   //Initial Guid for md5
		   $this->setGuid(do_hash(microtime(),'md5'));
		   
        }
		
		public function getInterviewerWithPage($surveyGuid ,$limit = 10, $offset = 0)
		{
//			 return $this->db->get_where("survey_interviewer_own_surveys", array('surveyGuid' => $surveyGuid), $limit, $offset);
            $this->db->select('survey_interviewer.guid,survey_interviewer.name,survey_interviewer.email,survey_interviewer.pwd');
//            $this->db->from('survey_interviewer_own_surveys');
            $this->db->join('survey_interviewer','survey_interviewer_own_surveys.interviewerGuid = survey_interviewer.guid','inner');
            $this->db->where('surveyGuid',$surveyGuid);
            $this->db->order_by('email','acs');

            return $this->db->get('survey_interviewer_own_surveys',$limit,$offset);

		}
		
		public function getCountAll($surveyGuid)
		{
            $this->db->like('surveyGuid',$surveyGuid);
            $this->db->from('survey_interviewer_own_surveys');
			 return $this->db->count_all_results();
		}
				
		public function getSurveyList($interviewer)
		{
			$sql = "SELECT surveyGuid FROM survey_interviewer_own_surveys WHERE interviewerGuid ='".$interviewer->getGuid()."'";
			$result = $this->db->query($sql);		
			return $result;
		}
		
		public function user_login($account)
		{
			$resultAccount = new account_model();
			$sql = "SELECT * FROM survey_interviewer WHERE email ='".$account->getAccount()."' and pwd = '".$account->getPassword()."'";
			$result = $this->db->query($sql);	
			foreach ($result->result() as $row) {
				if($account->getPassword() == $row->pwd){
					$resultAccount->setAccount($row->email);
					$resultAccount->setName($row->name);
					$resultAccount->setPassword($row->pwd);
					return $resultAccount;
				}
			}	
			return $resultAccount;
		}
		
		public function getInterviewerByGuid($interviewer)
		{
			$interviewerObject = new interviewer_model();
			$interviewerObject = $interviewer;
			$sql = "SELECT * FROM survey_interviewer WHERE guid ='".$interviewerObject->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {				
				$interviewerObject->setGuid($row->guid);
				$interviewerObject->setName($row->name);
				$interviewerObject->setEmail($row->email);
				$interviewerObject->setPwd($row->pwd);
			}
			return $interviewer;
		}
		
		public function getInterviewerByEmail($interviewer)
		{
			$sql = "SELECT * FROM survey_interviewer WHERE email ='".$interviewer->getEmail()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $row) {				
				$interviewer->setGuid($row->guid);
				$interviewer->setName($row->name);	
				$interviewer->setEmail($row->email);
				$interviewer->setPwd($row->pwd);
			}
			return $interviewer;
		}
		
		public function addInterviewer($interviewer)
		{
			$hashPwd = do_hash($interviewer->getPwd(),'md5');
			$sql = "INSERT INTO survey_interviewer (guid, name, email, pwd) 
        	VALUES ('".$interviewer->getGuid()."', '".$interviewer->getName()."','".$interviewer->getEmail()."','".$hashPwd."')";

			$this->db->query($sql);

			// echo $this->db->affected_rows();
		}
		
		public function updateInterviewer($interviewer)
		{
			$sql = "UPDATE survey_interviewer SET guid='".$interviewer->getGuid()."', name='".$interviewer->getName()."', email='".$interviewer->getEmail()."'
			where guid='".$interviewer->getGuid()."'"; 

			$this->db->query($sql);

			// echo $this->db->affected_rows();
		}
		
		public function deleteInterviewer($interviewer)
		{
			$sql = "DELETE FROM survey_interviewer where guid='".$interviewer->getGuid()."'"; 

			$this->db->query($sql);
			
		}
		
		public function updateInterviewerPwd($interviewer)
		{
			$hashPwd = do_hash($interviewer->getPwd(),'md5');
			$sql = "UPDATE survey_interviewer SET pwd='".$hashPwd."' where guid='".$interviewer->getGuid()."'"; 

			$this->db->query($sql);

			// echo $this->db->affected_rows();
		}
		
		public function exists_user()
		{
			$query = $this->db->get_where('survey_interviewer',array('email' => $this->getEmail()));
			if($query->num_rows() >= 1)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setName($name)
		{
			$this->_name = $name;
		}
		
		public function getName()
		{
			return $this->_name;
		}
		
		public function setEmail($email)
		{
			$this->_email = $email;
		}
	
		public function getEmail()
		{
			return $this->_email;
		}	
		
		public function setPwd($pwd)
		{
			$this->_pwd = $pwd;
		}
		
		public function getPwd()
		{
			return $this->_pwd;
		}
    }
    
?>
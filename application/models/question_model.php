<?php 
	/**
	 * 
	 */
	class question_model extends CI_Model{
		private $_guid;
		private $_type;
        private $_surveyGuid;
		private $_subjectNumber;
		private $_subject;
		private $_optionGuid;
		private $_multiSelectedLimit;
		private $_isRandom;
		private $_optionOutputForm;
		private $_passGuid;
		private $_groupPassGuid;
		private $_tipsGuid;
		private $_dynamicOptionGuid;
		private $_dynamicOptionAnswerGuid;
		private $_groupOptionGuid;
        private $_priority;
		
		function __construct(){
			  parent::__construct();
			  $this->load->model("option_model");
			  $this->load->model("pass_model");
			  $this->load->model("tips_model");
			  $this->load->helper('security');
			  $this->setGuid(do_hash(microtime(),'md5'));
		}
		
		public function getSingleQuestion($question)
		{
			$sql = "SELECT * FROM survey_question WHERE guid ='".$question->getGuid()."'";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
                $question = $this->setQuestionObject($row);
			}
			return $question;
		}
		
		public function getSingleQuestionBySubjectNumber($question,$surveyGuid)
		{
			$sql = "SELECT * FROM survey_question WHERE subjectNumber ='".$question->getSubjectNumber()."' and surveyGuid = '".$surveyGuid."'";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
                $question = $this->setQuestionObject($row);
			}
			return $question;
		}
		
		public function getQuestionListBySurveyGuid($surveyGuid)
		{
			$questionList = array();
			$sql = "SELECT guid FROM survey_question WHERE surveyGuid ='".$surveyGuid."' ORDER BY priority"; //2012-12-31 by Awei 原來是order by subjectNumber
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
				$questionList[] = $row->guid;
			}
			
			return $questionList; //回傳guid列表
		}

		public function getAllQuestionsBySurveyGuid($surveyGuid)
		{
			$questionList = array();
			$sql = "SELECT * FROM survey_question WHERE surveyGuid ='".$surveyGuid."' ORDER BY priority"; //2012-12-31 by Awei 原來是order by subjectNumber
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
                $questionList[] = $this->setQuestionObject($row);
			}
			
			return $questionList; //回傳guid列表
		}

        public function getAllQuestionsNotInThisQuestionBySurveyAndQuestionGuidForPType($surveyGuid,$questionGuid)
        {
            $questionList = array();
            $sql = "SELECT * FROM survey_question WHERE surveyGuid ='".$surveyGuid."' and type = 'P'
             and guid NOT IN ('".$questionGuid."') ORDER BY priority";
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $questionList[] = $this->setQuestionObject($row);
            }
            $questionList = $this->minimizeQuestionTitleWordCount($questionList);
            return $questionList; //回傳guid列表
        }

        function setQuestionObject($queryResult)
        {
            $question = new question_model();
            $question->setGuid($queryResult->guid);
            $question->setSurveyGuid($queryResult->surveyGuid);
            $question->setSubjectNumber($queryResult->subjectNumber);
            $question->setSubject($queryResult->subject);
            $question->setType($queryResult->type);
            $question->setOptionGuid($queryResult->optionGuid);
            $question->setMultiSelectedLimit($queryResult->multiSelectedLimit);
            $question->setIsRandom($queryResult->isRandom);
            $question->setOptionOutputForm($queryResult->optionOutputForm);
            $question->setPassGuid($queryResult->passGuid);
            $question->setGroupPassGuid($queryResult->groupPassGuid);
            $question->setTipsGuid($queryResult->tipsGuid);
            $question->setDynamicOptionGuid($queryResult->dynamicOptionGuid);
            $question->setDynamicOptionAnswerGuid($queryResult->dynamicOptionAnswerGuid);
            $question->setGroupOptionGuid($queryResult->groupOptionGuid);
            $question->setPriority($queryResult->priority);
            return $question;
        }


        /*
         * Add By Awei 2013/05/05
         * 縮短題目,給跳題題目顯示用
         */
        public function getAllQuestionsModifySubjectTitleBySurveyGuid($surveyGuid)
        {
            $allQuestionsArray = $this->getAllQuestionsBySurveyGuid($surveyGuid);
            return $this->minimizeQuestionTitleWordCount($allQuestionsArray);

        }

        function minimizeQuestionTitleWordCount($allQuestionsArray){
            for  ($i =0 ; $i< count($allQuestionsArray) ; $i++)
            {
                $subjectLimit = 20;
                $question = $allQuestionsArray[$i];
                $originalSubject = $question->getSubject();
                $originalSubject = strip_tags($originalSubject);
                $originalSubjectLenth = mb_strlen($originalSubject,"UTF-8");
                if ($originalSubjectLenth > $subjectLimit) {
                    $simpleSubject = mb_substr($originalSubject, 0, $subjectLimit, 'UTF-8');
                    $simpleSubject = $simpleSubject."...";
                    $question->setSubject($simpleSubject);
                    $allQuestionsArray[$i] = $question;
                }
            }
            return $allQuestionsArray;
        }

        public function getIsMGPType($subjectNumber,$surveyGuid)
		{
			$sql = "SELECT type FROM survey_question WHERE surveyGuid = '".$surveyGuid."' and subjectNumber ='".$subjectNumber."'";
			$query = $this->db->query($sql);
			$typeResult = $query->row()->type;
			$result = 0; 
			if ($typeResult == 'M' || $typeResult == 'G' || $typeResult == 'P') {
				$result = 1;
			}
			return $result;
		}
		
		public function getAllQuestionType($surveyGuid)
		{
			$typeArray = array();
			$sql = "SELECT type FROM `survey_question` where surveyGuid = '".$surveyGuid."' order by priority asc";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
				$typeArray[] = $row->type;
			}
			return $typeArray;
			
		}

		public function addQuestion($question,$surveyGuid)
		{
			$sql = "INSERT INTO survey_question (guid, surveyGuid, subjectNumber, subject, type, optionGuid, multiSelectedLimit, isRandom,
			optionOutputForm,passGuid, groupPassGuid, tipsGuid, dynamicOptionGuid, dynamicOptionAnswerGuid, groupOptionGuid, priority)
        	VALUES ('".$question->getGuid()."', '".$surveyGuid."','".$question->getSubjectNumber()."','".addslashes($question->getSubject())."'
        	,'".$question->getType()."','".$question->getOptionGuid()."','".(int)$question->getMultiSelectedLimit()."'
        	,'".(int)$question->getIsRandom()."','".$question->getOptionOutputForm()."','".$question->getPassGuid()."','".$question->getGroupPassGuid()."','".$question->getTipsGuid()."'
        	,'".$question->getDynamicOptionGuid()."','".$question->getDynamicOptionAnswerGuid()."','".$question->getGroupOptionGuid()."'
        	,'".$question->getPriority()."')";

			$this->db->query($sql);
		}
		
		public function updateQuestion($question,$surveyGuid)
		{
			$sql = "UPDATE survey_question SET guid='".$question->getGuid()."', surveyGuid='".$surveyGuid."'
			, subjectNumber='".$question->getSubjectNumber()."', subject='".addslashes($question->getSubject())."', type='".$question->getType()."'
			, optionGuid='".$question->getOptionGuid()."', multiSelectedLimit='".$question->getMultiSelectedLimit()."', isRandom='".$question->getIsRandom()."'
			, optionOutputForm='".$question->getOptionOutputForm()."', passGuid='".$question->getPassGuid()."', groupPassGuid='".$question->getGroupPassGuid()."', tipsGuid='".$question->getTipsGuid()."'
			, dynamicOptionGuid='".$question->getDynamicOptionGuid()."', dynamicOptionAnswerGuid='".$question->getDynamicOptionAnswerGuid()."'
			, groupOptionGuid='".$question->getGroupOptionGuid()."' where guid='".$question->getGuid()."'";
			 
			$this->db->query($sql);
		}

		public function deleteQuestion($question)
		{
			$sql = "DELETE FROM survey_question where guid='".$question->getGuid()."'";
			$this->db->query($sql);
		}

        public function deleteQuestionBySurveyGuid($surveyGuid)
        {
            $sql = "DELETE FROM survey_question where surveyGuid='".$surveyGuid."'";
            $this->db->query($sql);
        }

        public function updateQuestionPriority($surveyGuid,$questionArray)
        {
            $priorityCounter = 1;
            foreach ($questionArray as $guid) {
                $sql = "UPDATE survey_question SET priority = ".$priorityCounter." where surveyGuid = '".$surveyGuid."' and  guid = '".$guid."'";
                $this->db->query($sql);
                $priorityCounter = $priorityCounter + 1;
            }
        }

        public function getNewPriorityNumber($surveyGuid)
        {
            $this->db->like('surveyGuid',$surveyGuid);
            $this->db->from('survey_question');
            return $this->db->count_all_results()+1;
        }

        public function setGuid($guid){
			$this->_guid = $guid;
		}
		
		public function getGuid(){
			return $this->_guid;
		}
		
		public function setType($type){
			$this->_type = $type;
		}
		
		public function getType(){
			return $this->_type;
		}

        public function setSurveyGuid($surveyGuid){
            $this->_surveyGuid = $surveyGuid;
        }

        public function getSurveyGuid(){
            return $this->_surveyGuid;
        }

		public function getSubjectNumber(){
			return $this->_subjectNumber;
		}
		
		public function setSubjectNumber($subjectNumber){
			$this->_subjectNumber = $subjectNumber;
		}
		
		public function setSubject($subject){
			$this->_subject = $subject;
		}
		
		public function getSubject(){
			return $this->_subject;
		}
		
		public function setOptionGuid($optionGuid){
			$this->_optionGuid = $optionGuid;
		}
		
		public function getOptionGuid(){
			return $this->_optionGuid;
		}
		
		/**
		 * 設定複選題選擇限制，預設都是1
		 */
		public function setMultiSelectedLimit($multiSelectedLimit)
		{
			$this->_multiSelectedLimit = $multiSelectedLimit;
		}
		
		/**
		 * 取得複選題選擇限制，預設都是1
		 */		
		public function getMultiSelectedLimit()
		{
			return $this->_multiSelectedLimit;
		}
		
		/**
		 * 設定選項是否亂數出現
		 */
		public function setIsRandom($isRandom)
		{
			$this->_isRandom = $isRandom;
		}
		
		/**
		 * 取得選項是否亂數出現
		 */	
		public function getIsRandom()
		{
			return $this->_isRandom;
		}
		
		public function setOptionOutputForm($optionOutputForm)
		{
			$this->_optionOutputForm = $optionOutputForm;
		}
		
		public function getOptionOutputForm()
		{
			return $this->_optionOutputForm;
		}
	
		public function setPassGuid($passGuid){
			$this->_passGuid = $passGuid;
		}
		
		public function getPassGuid(){
			return $this->_passGuid;
		}
		
		public function setGroupPassGuid($groupPassGuid)
		{
			$this->_groupPassGuid = $groupPassGuid;
		}
		
		public function getGroupPassGuid()
		{
			return $this->_groupPassGuid;
		}
		
		public function setTipsGuid($tipsGuid){
			$this->_tipsGuid = $tipsGuid;
		}
		
		public function getTipsGuid(){
			return $this->_tipsGuid;
		}
		
		public function setDynamicOptionGuid($dynamicOptionGuid){
			$this->_dynamicOptionGuid = $dynamicOptionGuid;
		}
		
		public function getDynamicOptionGuid(){
			return $this->_dynamicOptionGuid;
		}
		
		public function setDynamicOptionAnswerGuid($dynamicOptionAnswerGuid)
		{
			$this->_dynamicOptionAnswerGuid = $dynamicOptionAnswerGuid;
		}
		
		public function getDynamicOptionAnswerGuid()
		{
			return $this->_dynamicOptionAnswerGuid;
		}
		
		public function setGroupOptionGuid($groupOptionGuid)
		{
			$this->_groupOptionGuid = $groupOptionGuid;
		}
		
		public function getGroupOptionGuid()
		{
			return $this->_groupOptionGuid;
		}

        public function setPriority($priority)
        {
            $this->_priority = $priority;
        }

        public function getPriority()
        {
            return $this->_priority;
        }
	}
	
	
?>
<?php
	/**
	 * 
	 */
	class dynamicoptionbyanswer_model extends CI_Model {
		
		private $_guid;
		private $_beforeSubjectNumber;
		private $_targetAnswer;
		private $_targetOption;
        private $_orBeforeSubjectNumber;
        private $_orTargetAnswer;
        private $_orTargetOption;
		private $_isShow;

        function __construct() {
            parent::__construct();
			$this->load->helper('security');
			$this->setGuid(do_hash(microtime(),'md5'));
        }
		
		public function getDynamicOptionArrayByAnswer($dyOptionByAnswer)
		{
			$dyOptionArray = array();
			$sql = "SELECT * FROM survey_dynamicoptionanswer WHERE guid ='".$dyOptionByAnswer->getGuid()."'";
			$result = $this->db->query($sql);
			foreach ($result->result() as $listRow) {
				
				//setGuid
				$sql = "SELECT * FROM survey_dynamicoptionanswer_list WHERE dyanswerGuid ='".$listRow->dyanswerGuid."' order by id";
				$listResult = $this->db->query($sql);
				foreach ($listResult->result() as $row) {
					$dyOptionByAnswer = new dynamicoptionbyanswer_model();				
					$dyOptionByAnswer->setGuid($row->dyanswerGuid);
					$dyOptionByAnswer->setIsShow($row->isShow);
					
					//set beforeSubjectNumber and TargetAnswer
					$sql = "SELECT * FROM survey_dynamicoptionanswer_subjectlist WHERE subjectGuid ='".$row->subjectGuid."' order by id";
					$subjectResult = $this->db->query($sql);
					$subjectArray = array();
					$targetAnswerArray = array();
					foreach ($subjectResult->result() as $subjectRow) {
						$subjectArray[] = $subjectRow->subjectNumber;
						$targetAnswerArray[] = $subjectRow->targetAnswer;					
					}
					$dyOptionByAnswer->setBeforeSubjectNumber($subjectArray);
					$dyOptionByAnswer->setTargetAnswer($targetAnswerArray);

                    //set orBeforeSubjectNumber and orTargetAnswer
                    $sql = "SELECT * FROM survey_dynamicoptionanswer_or_subjectlist WHERE orSubjectGuid ='".$row->orSubjectGuid."' order by id";
                    $orSubjectResult = $this->db->query($sql);
                    $orSubjectArray = array();
                    $orTargetAnswerArray = array();
                    foreach ($orSubjectResult->result() as $subjectRow) {
                        $orSubjectArray[] = $subjectRow->orSubjectNumber;
                        $orTargetAnswerArray[] = $subjectRow->orTargetAnswer;
                    }
                    $dyOptionByAnswer->setOrBeforeSubjectNumber($orSubjectArray);
                    $dyOptionByAnswer->setOrTargetAnswer($orTargetAnswerArray);
					
					//set targetOption
					$sql = "SELECT * FROM survey_dynamicoptionanswer_targetoptionlist WHERE targetOptionGuid ='".$row->targetOptionGuid."' order by id";
					$optionResult = $this->db->query($sql);
					$targetOptionArray = array();
					foreach ($optionResult->result() as $optionRow) {
						$targetOptionArray[] = $optionRow->targetOptionIndex;					
					}
					$dyOptionByAnswer->setTargetOption($targetOptionArray);								
					
					$dyOptionArray[] = $dyOptionByAnswer;
				}
			}	
			
			return $dyOptionArray;		
			
		}

        public function add($dynamicOptionAnswer)
        {
            $dyTargetSubjectArray = $dynamicOptionAnswer->getBeforeSubjectNumber();
            $dyTargetAnswerArray = $dynamicOptionAnswer->getTargetAnswer();
            $dyOrTargetSubjectArray = $dynamicOptionAnswer->getOrBeforeSubjectNumber();
            $dyOrTargetAnswerArray = $dynamicOptionAnswer->getOrTargetAnswer();
            $dyTargetOptionArray = $dynamicOptionAnswer->getTargetOption();

            //這裡改採用時間+題目編號的方式加識別碼做Guid，方便開發者觀察
            $dyGuidPrefix = $dyTargetOptionArray[0];
            $dyGuidPrefix = date('YmdHis').'@'.$dyGuidPrefix;
            $targetOptionGuid = $dyGuidPrefix."-T";

            foreach($dyTargetOptionArray as $targetOption){
                $insertTargetOptionSQL = "INSERT INTO survey_dynamicoptionanswer_targetoptionlist (targetOptionGuid, targetOptionIndex)
        	                    VALUES ('".$targetOptionGuid."', '".$targetOption."')";
                $this->db->query($insertTargetOptionSQL);
            }

            $targetSubjectGuid = $dyGuidPrefix."-S";
            for($i = 0; $i<count($dyTargetAnswerArray); $i++){
                $insertTargetSubjectSQL = "INSERT INTO survey_dynamicoptionanswer_subjectlist (subjectGuid, subjectNumber, targetAnswer)
        	                    VALUES ('".$targetSubjectGuid."','".$dyTargetSubjectArray[$i]."', '".$dyTargetAnswerArray[$i]."')";
                $this->db->query($insertTargetSubjectSQL);
            }

            $orTargetSubjectGuid = $dyGuidPrefix."-OR";
            for($k = 0; $k<count($dyOrTargetAnswerArray); $k++){
                $insertOrTargetSubjectSQL = "INSERT INTO survey_dynamicoptionanswer_or_subjectlist (orSubjectGuid, orSubjectNumber, orTargetAnswer)
        	                    VALUES ('".$orTargetSubjectGuid."','".$dyOrTargetSubjectArray[$k]."', '".$dyOrTargetAnswerArray[$k]."')";
                $this->db->query($insertOrTargetSubjectSQL);
            }

            $dyListGuid = $dyGuidPrefix."-L";
            $insertDyListSQL = "INSERT INTO survey_dynamicoptionanswer_list (dyanswerGuid, isShow, subjectGuid, orSubjectGuid, targetOptionGuid)
        	                    VALUES ('".$dyListGuid."','".$dynamicOptionAnswer->getIsShow()."', '".$targetSubjectGuid."', '".$orTargetSubjectGuid."','".$targetOptionGuid."')";
            $this->db->query($insertDyListSQL);

            $dyGuid = $dyGuidPrefix."-Dy";
            $insertSQL = "INSERT INTO survey_dynamicoptionanswer (guid, dyanswerGuid)
        	                    VALUES ('".$dyGuid."','".$dyListGuid."')";
            $this->db->query($insertSQL);

            $dynamicOptionAnswer->setGuid($dyGuid);

            return $dynamicOptionAnswer;
        }

        public function delete($dyAnswerGuid)
        {
            $dyGuidExplodeArray = explode('-',$dyAnswerGuid);
            $dyGuidPrefix = $dyGuidExplodeArray[0];

            $guid = $dyGuidPrefix.'-Dy';
            $dyanswerGuid = $dyGuidPrefix.'-L';
            $subjectGuid = $dyGuidPrefix.'-S';
            $OrSubjectGuid = $dyGuidPrefix.'-OR';
            $targetOptionGuid = $dyGuidPrefix.'-T';

            $sql1 = "DELETE FROM survey_dynamicoptionanswer where guid='".$guid."'";
            $sql2 = "DELETE FROM survey_dynamicoptionanswer_list where dyanswerGuid='".$dyanswerGuid."'";
            $sql3 = "DELETE FROM survey_dynamicoptionanswer_subjectlist where subjectGuid='".$subjectGuid."'";
            $sql4 = "DELETE FROM survey_dynamicoptionanswer_or_subjectlist where orSubjectGuid='".$OrSubjectGuid."'";
            $sql5 = "DELETE FROM survey_dynamicoptionanswer_targetoptionlist where targetOptionGuid='".$targetOptionGuid."'";

            $this->db->query($sql1);
            $this->db->query($sql2);
            $this->db->query($sql3);
            $this->db->query($sql4);
            $this->db->query($sql5);

        }
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setBeforeSubjectNumber($beforeSubjectNumber)
		{
			$this->_beforeSubjectNumber = $beforeSubjectNumber;
		}
		
		public function getBeforeSubjectNumber()
		{
			return $this->_beforeSubjectNumber;
		}
		
		public function setTargetAnswer($targetAnswer)
		{
			$this->_targetAnswer = $targetAnswer;
		}
		
		public function getTargetAnswer()
		{
			return $this->_targetAnswer;
		}
		
		public function setTargetOption($targetOption)
		{
			$this->_targetOption = $targetOption;
		}
		
		public function getTargetOption()
		{
			return $this->_targetOption;	
		}
		
		public function setIsShow($isShow)
		{
			$this->_isShow = $isShow;
		}
		
		public function getIsShow()
		{
			return $this->_isShow;
		}

        /**
         * @param mixed $orTargetOption
         */
        public function setOrTargetOption($orTargetOption)
        {
            $this->_orTargetOption = $orTargetOption;
        }

        /**
         * @return mixed
         */
        public function getOrTargetOption()
        {
            return $this->_orTargetOption;
        }

        /**
         * @param mixed $orBeforeSubjectNumber
         */
        public function setOrBeforeSubjectNumber($orBeforeSubjectNumber)
        {
            $this->_orBeforeSubjectNumber = $orBeforeSubjectNumber;
        }

        /**
         * @return mixed
         */
        public function getOrBeforeSubjectNumber()
        {
            return $this->_orBeforeSubjectNumber;
        }

        /**
         * @param mixed $orTargetAnswer
         */
        public function setOrTargetAnswer($orTargetAnswer)
        {
            $this->_orTargetAnswer = $orTargetAnswer;
        }

        /**
         * @return mixed
         */
        public function getOrTargetAnswer()
        {
            return $this->_orTargetAnswer;
        }
	}
	
?>
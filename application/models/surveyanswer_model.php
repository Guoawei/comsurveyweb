<?php
    /**
     * 
     */
    class surveyanswer_model extends CI_Model {
        
		private $_surveyGuid;
		private $_interviewerGuid;
		private $_sampleGuid;
		private $_cDateTime;
		private $_subjectNumberArray;
		private $_answerArray;
		
        function __construct() {
            parent::__construct();
			//Initial Guid for md5
			// $this->load->helper('security');
			// $this->setGuid(do_hash(time(),'md5'));
			$this->load->model('sample_model');
        }
		
		//已完成調查數
		public function getAlreadyUploadCount($interviewerGuid,$surveyGuid,$surveyVersion)
		{
			$result ='';
			$tableName = "survey_answer_".$surveyGuid."_".$surveyVersion;
			if ($this->db->table_exists($tableName)){
				$sql = "SELECT count(sampleGuid) as uploadCount FROM survey_answer_".$surveyGuid."_".$surveyVersion."
					where interviewerGuid ='".$interviewerGuid."'";
				$query = $this->db->query($sql);
				foreach ($query->result() as $row) {
					$result = $row->uploadCount;
				}
			}
			// echo $query->result();
			return $result;
		}
		
		//查詢該樣本是否完成調查上傳
		public function getIsAlreadyUpload($interviewerGuid,$sampleGuid,$surveyGuid,$surveyVersion)
		{
			$sql = "SELECT sampleGuid FROM survey_answer_".$surveyGuid."_".$surveyVersion."
				where interviewerGuid ='".$interviewerGuid."' and sampleGuid = '".$sampleGuid."'";
			$query = $this->db->query($sql);
			$result = "";
			if ($query->num_rows()>0) {
				$result = "1";		
			}
			return $result;
		}
		
		
		public function getAnswerCDateTime($interviewerGuid,$sampleGuid,$surveyGuid,$surveyVersion)
		{
			$sql = "SELECT cDateTime FROM survey_answer_".$surveyGuid."_".$surveyVersion."
				where interviewerGuid ='".$interviewerGuid."' and sampleGuid = '".$sampleGuid."'";
			$query = $this->db->query($sql);
			$result = "";
			if ($query->num_rows()>0) {
				$result = $query->row()->cDateTime;
				// $datetime = strtotime($row->createdate);
// $mysqldate = date("m/d/y g:i A", $datetime);		
			}
			return $result;
		}
		
		public function getAnswer($interviewerGuid,$sampleGuid,$surveyGuid,$surveyVersion)
		{
			
		}
		
		
		//求得某一題目的所有答案
		public function getAnswerBySubjectNumber($subjectNumber,$tableName)
		{
			// $sql = "SELECT ".$subjectNumber." FROM ".$tableName."";
			//只取得一筆最多資料的
			$sql = "SELECT length(".$subjectNumber.") FROM ".$tableName." order by length(".$subjectNumber.") desc limit 0,1";
			$query = $this->db->query($sql);
			$resultArray = array();
			foreach ($query->result() as $row) {
				$resultArray[] = $row;
			}
			// echo $query->result();
			return $resultArray;
		}
		
		public function getMaxColumnBySubjectNumber($subjectNumber,$tableName)
		{
			$sql = "SELECT round((char_length(`".$subjectNumber."`) - char_length(replace(`".$subjectNumber."`, ',' , ''))) / (char_length(',')),0) as result
			 FROM ".$tableName." order by result desc limit 0,1";
			$query = $this->db->query($sql);
			$row = $query->row();

			return $row->result+1;
		}
		
		public function getAnswerToCSV($surveyGuid,$surveyVersion,$isDownload)
		{
			$tableName = "survey_answer_".$surveyGuid."_".$surveyVersion;
			// $sql1 = "SELECT * FROM survey_answer_".$surveyGuid."_".$surveyVersion."
				// where interviewerGuid ='".$interviewerGuid."' and sampleGuid = '".$sampleGuid."'";
			$sql2 = "SELECT * FROM survey_answer_".$surveyGuid."_".$surveyVersion."";
			// $query = $this->db->query($sql);
			// $result = array()
			// if ($query->num_rows()>0) {
				// $result = $query->result();
				// for ($i=0; $i < count($result) ; $i++) { 
					// echo $result[$i];	
				// }
// 				
				// // $datetime = strtotime($row->createdate);
// // $mysqldate = date("m/d/y g:i A", $datetime);		
			// }
			date_default_timezone_set("Asia/Taipei");
			$this->load->helper('csv');
            $query = $this->db->query($sql2);
            $num = $query->num_fields();
            $var =array();
            $i=1;
            $fname="";
			// echo $num;
            while($i <= $num){
                $test = $i;
                $value = $this->input->post($test);

                if($value != ''){
                        $fname= $fname." ".$value;
                        array_push($var, $value);

                    }
                 $i++;
            }

            $fname = trim($fname);

            $fname=str_replace(' ', ',', $fname);
			// $fname=str_replace('T:',' ', $fname);
            $this->db->select($fname);
            $quer = $this->db->get($tableName); 
			$datestring = "%m%d";
			$time = time();
			$this->load->helper('date');
            query_to_csv($quer,TRUE,'CAPI_'.mdate($datestring,$time).'.csv','T:',$isDownload);
            
        
		}
		
		function iconv2big5($str){
			//return iconv("UTF-8",'BIG5//TRANSLIT//IGNORE',$str);
			return mb_convert_encoding($str,"big5","utf-8");
		}
		
		public function getCSVForAllColumn($surveyGuid,$surveyVersion,$isDownload)
		{
			$tableName = "survey_answer_".$surveyGuid."_".$surveyVersion;
			
			$csv = '樣本名稱,樣本地址,調查時數';
			
			//取得欄位名稱
			$fields = $this->db->list_fields($tableName);
			$fieldsCount = count($fields);
			for ($i=4; $i < $fieldsCount; $i++) {
				$csv .= ','.$fields[$i]; 
			}
			$csv .= '
			';
			$sql2 = "SELECT * FROM survey_answer_".$surveyGuid."_".$surveyVersion." order by id ASC";
			$query = $this->db->query($sql2);
			foreach ($query->result_array() as $row) {
				$interviewerGuid = $row['interviewerGuid'];
				$sampleGuid = $row['sampleGuid'];
				$sample = new sample_model();
				$sample->setGuid($sampleGuid);
				$sample = $sample->getSample($sample);
				$sampleName = $sample->getName();
				$sampleAddress = $sample->getAddress();
				
				$answer="";
				// if ($sampleGuid == '235104365'){
					for ($i=4; $i < $fieldsCount; $i++) {
						// $answer .= $row['A1'].','.$row['A2'];
						$str = "";
						$str = str_replace (",", ";", $row[$fields[$i]]);
						$str = str_replace ("T:", "", $str);
						$str = rtrim($str);
						// echo $str;
						$answer .= ','.$str; 
					}
					$csv .= $sampleName.','.$sampleAddress.','.$sampleGuid.$answer.'
				';
				// }
				// echo $answer;
				
				//break;
			}

			date_default_timezone_set("Asia/Taipei");
			$datestring = "%m%d";
			$time = time();
			$path = './';
			$this->load->helper('date');
			$csv=$this->iconv2big5($csv);
			file_put_contents($path.'CAPI_'.mdate($datestring,$time).'.csv',$csv);
			$filesdone= $path.'CAPI_'.mdate($datestring,$time).'.csv';
			echo $filesdone;
			// header('Content-Type: application/csv');
            // header('Content-Disposition: attachement; filename="' . $filesdone . '"');
			return $filesdone;
		}
				
		//Awei 未完成
		public function getAnswerToSPSS($surveyGuid,$surveyVersion)
		{
			$this->load->helper('file');
			$spssFileName = 'CAPI_'.date('dMy').'.sav';    
        	$this->getAnswerToCSV($surveyGuid,$surveyVersion,0);
			
			$csv = 'CAPI_'.date('dMy').'.csv';
			// echo $csv;
			//echo $string = read_file($csv);
			$cmd = "java -jar CsvToSpss.jar ".$csv." ".$spssFileName." ";
			// echo $cmd;
		    // $cmd = mb_convert_encoding($cmd,"big5","utf8");
		    exec($cmd,$output);
			echo $output;
			// header('Content-Type: application/sav');
            // header('Content-Disposition: attachement; filename="' . $spssFileName . '"');
			
		}
		
		public function addAnswer($answer)
		{
			
		}
		
		public function updateAnswer($answer)
		{
			
		}
		
		public function deleteAnswer($answer)
		{
			
		}
		
		public function setSurveyGuid($surveyGuid)
		{
			$this->_surveyGuid = $surveyGuid;
		}
		
		public function getSurveyGuid()
		{
			return $this->_surveyGuid;
		}
		
		public function setInterviewerGuid($interviewerGuid)
		{
			$this->_interviewerGuid = $interviewerGuid;
		}
		
		public function getInterviewerGuid()
		{
			return $this->_interviewerGuid;
		}
		
		public function setSampleGuid($sampleGuid)
		{
			$this->_sampleGuid = $sampleGuid;
		}
		
		public function getSampleGuid()
		{
			return $this->_sampleGuid;
		}
		
		public function setCDateTime($cDateTime)
		{
			$this->_cDateTime = $cDateTime;
		}
		
		public function getCDateTime()
		{
			return $this->_cDateTime;
		}
		
		public function setSubjectNumberArray($subjectNumberArray)
		{
			$this->_subjectNumberArray = $subjectNumberArray;
		}
		
		public function getSubjectNumberArray()
		{
			return $this->_subjectNumberArray;
		}
		
		public function setAnswerArray($answerArray)
		{
			$this->_answerArray = $answerArray;
		}
		
		public function getAnswerArray()
		{
			return $this->_answerArray;
		}
    }
    
?>
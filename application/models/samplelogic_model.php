<?php include 'sample_model.php';
    /**
     * 管理Survey與樣本之間的關係
     */
    class samplelogic_model extends sample_model {
        
		private $_groupGuid;
		
        function __construct() {
            parent::__construct();			
			//Initial Guid for md5
			$this->load->helper('security');
			$this->setGroupGuid(do_hash(microtime(),'md5'));
			
        }
		
		//新增訪員群組
		public function addSampleGroup($surveySampleArray)
		{
			$groupGuid = $this->getGroupGuid();
			for ($i=0; $i < count($surveySampleArray); $i++) {
				
				$sql = "SELECT guid from survey_samplegroup where guid = '".$groupGuid."' and sampleGuid = '".$surveySampleArray[$i]."'";
				$result = $this->db->query($sql);
				if ($result->num_rows()==0) {
					$sql2 = "INSERT INTO survey_samplegroup (guid, sampleGuid) 
		        	VALUES ('".$groupGuid."', '".$surveySampleArray[$i]."')";
				
					$this->db->query($sql2);
										
				}
				 
			}
			return $groupGuid;

		}
		
		//新增訪員群組
		public function addSampleGroupByOldGroupGuid($surveySampleArray,$oldSamplGroupGuid)
		{
			$groupGuid = $oldSamplGroupGuid;
			for ($i=0; $i < count($surveySampleArray); $i++) { 
				$sql = "SELECT guid from survey_samplegroup where sampleGuid = '".$surveySampleArray[$i]."'";
				$result = $this->db->query($sql);
				if ($result->num_rows()==0) {
					$sql2 = "INSERT INTO survey_samplegroup (guid, sampleGuid) 
		        	VALUES ('".$groupGuid."', '".$surveySampleArray[$i]."')";
				
					$this->db->query($sql2);
										
				}
			}
			return $groupGuid;

		}
		
		
		//刪除整個Group
		public function deleteSampleGroup($sampleGroupGuid)
		{
		
			$sql = "DELETE FROM survey_samplegroup where guid = '".$sampleGroupGuid."'"; 		
			$this->db->query($sql);
		
		}
		
		//只刪除某個Group裡的sample
		public function deleteSampleGuid($sampleGuid)
		{
			$sql = "DELETE FROM survey_samplegroup where sampleGuid ='".$sampleGuid."'";
			$this->db->query($sql);	
		}
			
		//取得問卷各訪員的樣本名單
		public function getSampleInGroup($surveyGuid,$interviewerGuid)
		{
			$sql = "select * from survey_survey_own_sample where surveyGuid ='".$surveyGuid."' and interviewerGuid = '".$interviewerGuid."'";
			$result = $this->db->query($sql);
			$gsguid="";
			foreach ($result->result() as $row) {
				$gsguid = $row->groupSampleGuid;	
			}

            $sql2 = "select s.* from survey_samplegroup as g inner join survey_sample as s ON s.`guid` = g.`sampleGuid`
                     where g.`guid` ='".$gsguid."' order by s.`mainpriority`,s.`subpriority` ASC";
            $result = $this->db->query($sql2);
            $sArray = array();
            foreach ($result->result() as $row) {
                $sampleObject = new sample_model();
                $sampleObject = $sampleObject->setSampleObject($row);
                $sArray[] = $sampleObject;
            }

			return $sArray;
		}
		
		//取得問卷各訪員的樣本名單數量
		public function getAllCountSampleInGroup($interviewerGuid)
		{
			$sql = "select * from survey_survey_own_sample where interviewerGuid = '".$interviewerGuid."'";
			$result = $this->db->query($sql);
			$gsguid="";
			foreach ($result->result() as $row) {
				$gsguid = $row->groupSampleGuid;	
			}
			
			$sql = "select * from survey_samplegroup where guid ='".$gsguid."'";
			$result = $this->db->query($sql);
			
			return $result->num_rows();
		}
		
		//取得樣本的訪員名單
		public function getSamplesInterviewer($surveyGuid)
		{
			$sql = "SELECT survey_samplegroup.sampleGuid, survey_survey_own_sample.interviewerGuid 
			FROM survey_samplegroup left join survey_survey_own_sample on 
			survey_samplegroup.guid = survey_survey_own_sample.groupSampleGuid where survey_survey_own_sample.surveyGuid='".$surveyGuid."'";
			$result = $this->db->query($sql);
			$sArray = array();
			foreach ($result->result() as $row) {
				$sample = new sample_model();
				$sample->setGuid($row->sampleGuid);
				$sample = $sample->getSample($sample);
				$sample->setSurveyByInterviewer($row->interviewerGuid);
				$sArray[] = $sample;
				
			}
			return $sArray;
		}
		
		public function getWhoInterviewSetOnSample($sampleGuid)
		{
			$sql = "SELECT survey_survey_own_sample.interviewerGuid FROM survey_samplegroup
                    LEFT JOIN survey_survey_own_sample ON survey_samplegroup.guid = survey_survey_own_sample.groupSampleGuid
                    WHERE survey_samplegroup.sampleGuid =  '".$sampleGuid."'";
			$result = $this->db->query($sql);
			$sArray = array();
			$interviewerGuid = "";
			foreach($result->result() as $row){
				$interviewerGuid = $row->interviewerGuid;
			}
			return $interviewerGuid;
			
		}
		
		public function getGroupGuid()
		{
			return $this->_groupGuid;
		}
		
		public function setGroupGuid($groupGuid)
		{
			$this->_groupGuid = $groupGuid;
		}
		
		
		
	}
?>
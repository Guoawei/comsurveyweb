<?php
    /**
     * 
     */
    class interviewer_logic_model extends CI_Model {

        function __construct() {
           parent::__construct();  
		   $this->load->model('survey_model');
		   $this->load->model('interviewer_model');
        }
		
		//列出該位訪員的所有問卷
		public function getInterviewerOwnSurveyGuid($interviewer)
		{
			$sql = "SELECT surveyGuid FROM survey_interviewer_own_surveys WHERE interviewerGuid ='".$interviewer->getGuid()."'";
			$result = $this->db->query($sql);
			$resultArray = array();
			foreach ($result->result() as $row) {
				$surveyObj = new survey_model();
				$surveyObj->setGuid($row->surveyGuid);
				$surveyObj = $surveyObj->getSurvey($surveyObj);
				$resultArray[] = $surveyObj;
			}
			return $resultArray;
		}
		
		
		public function addInterviewerSurveyGuid($interviewer,$surveyGuid)
		{
			$sql = "SELECT interviewerGuid FROM survey_interviewer_own_surveys where interviewerGuid = '".$interviewer->getGuid()."' and 
			surveyGuid = '".$surveyGuid."'";
			$query = $this->db->query($sql);
			
			if ($query->num_rows == 0){
				$sql = "INSERT INTO survey_interviewer_own_surveys (interviewerGuid, surveyGuid) 
		        VALUES ('".$interviewer->getGuid()."', '".$surveyGuid."')";
				$this->db->query($sql);		
			}
		}
		
		public function updateInterviewerSurveyGuid($interviewer,$surveyGuid)
		{
			$it = new interviewer_model();
			$it = $interviewer;
			$sql = "INSERT INTO survey_interviewer_own_surveys (interviewerGuid, surveyGuid) 
		        VALUES ('".$it->getGuid()."', '".$surveyGuid."')";
			$this->db->query($sql);
		}
		
		public function deleteAllInterviewerSurveyGuid($interviewer)
		{
			$it = new interviewer_model();
			$it = $interviewer;
			$sql = "DELETE FROM survey_interviewer_own_surveys where interviewerGuid='".$it->getGuid()."'";
			$this->db->query($sql);
		}
		
		public function deleteInterviewerSurveyGuid($interviewer,$surveyGuid)
		{
			$it = new interviewer_model();
			$it = $interviewer;
			$sql = "DELETE FROM survey_interviewer_own_surveys where interviewerGuid='".$it->getGuid()."' and surveyGuid = '".$surveyGuid."'";
			$this->db->query($sql);
		}

        //2012-12-23 by Awei 新增null判斷，改為一開始不用給訪員的物件也可以
		public function addSampleToInterviewer($interviewer,$surveyGuid,$sampleGroupGuid)
		{
            if($interviewer==null){
                $sql = "INSERT INTO survey_survey_own_sample (groupSampleGuid, surveyGuid, interviewerGuid)
		        VALUES ('".$sampleGroupGuid."', '".$surveyGuid."', '')";
            }else {
                $it = new interviewer_model();
                $it = $interviewer;
                $sql = "INSERT INTO survey_survey_own_sample (groupSampleGuid, surveyGuid, interviewerGuid)
		        VALUES ('".$sampleGroupGuid."', '".$surveyGuid."', '".$it->getGuid()."')";
            }
			$this->db->query($sql);
		}

        //2012-12-23 by Awei 新增Update,代入新的sampleGroupGuid跟舊的sampleGroupGuid
        public function updateSampleToInterviewer($interviewer,$surveyGuid,$newSampleGroupGuid,$oldSampleGroupGuid)
        {
            if($interviewer==null){
                $updateData = array (
                    'groupSampleGuid' => $newSampleGroupGuid,
                );
                $this->db->where('surveyGuid',$surveyGuid);
                $this->db->where('groupSampleGuid',$oldSampleGroupGuid);
            }else {
                $it = $interviewer;

                $updateData = array(
                    'groupSampleGuid' => $newSampleGroupGuid,
                    'interviewerGuid' => $it->getGuid()
                );
                $this->db->where('surveyGuid',$surveyGuid);
                $this->db->where('groupSampleGuid',$oldSampleGroupGuid);
            }
            $this->db->update('survey_survey_own_sample',$updateData);
        }

        //2012-12-23 by Awei 把訪員拿掉
        public function updateSampleSetInterviewerEmpty($surveyGuid,$SampleGroupGuid)
        {
            $updateData = array (
                'interviewerGuid' => ''
            );
            $this->db->where('surveyGuid',$surveyGuid);
            $this->db->where('groupSampleGuid',$SampleGroupGuid);
            $this->db->update('survey_survey_own_sample',$updateData);
        }

        public function getSampleGroupGuidToInterviewer($interviewer,$surveyGuid = null)
		{
			$query = '';
			if($surveyGuid)
				$query = " and surveyGuid='{$surveyGuid}'";
			$sql = "SELECT groupSampleGuid from survey_survey_own_sample where interviewerGuid='".$interviewer->getGuid()."'{$query}";
			$result = $this->db->query($sql);
			$sampleGroupGuid = '';
			if ($result->num_rows() > 0){
				$row = $result->row();
				$sampleGroupGuid = $row->groupSampleGuid;
			}
			return $sampleGroupGuid;
		}
	
		public function deleteSampleToInterviewer($interviewer)
		{
			$it = new interviewer_model();
			$it = $interviewer;
			$sql = "DELETE FROM survey_survey_own_sample where interviewerGuid='".$it->getGuid()."'";
			$this->db->query($sql);
		}

    }
    
?>
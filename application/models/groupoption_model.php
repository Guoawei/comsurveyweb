<?php
	/**
	 * 
	 */
	class groupoption_model extends CI_Model {

		private $_id;		
		private $_guid;
		private $_type;
		private $_index;
		private $_text;
		private $_value;
		private $_isShowTextView;	
		private $_textViewPosition;
        private $_optionTips;

		
		function __construct() {
			parent::__construct();
			$this->load->helper('security');
			$this->setGuid(do_hash(microtime(),'md5'));
		}
		
		public function getGroupOptionList($groupOption)
		{
			$resultArray = array();
			$sql = "SELECT * FROM survey_groupoption WHERE guid ='".$groupOption->getGuid()."' order by groupOption_type, groupOption_index asc";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row) {
				$groupOptionObject = new groupoption_model();
				$groupOptionObject->setId($row->id);//到時候取答案的時候，要拿這欄來找出相對應的答案。
				$groupOptionObject->setGuid($row->guid);
				$groupOptionObject->setType($row->groupOption_type);
				$groupOptionObject->setIndex($row->groupOption_index);
				$groupOptionObject->setText($row->groupOption_text);
				$groupOptionObject->setValue($row->groupOption_value);
				$groupOptionObject->setIsShowTextView($row->groupOption_isShowTextView);
				$groupOptionObject->setTextViewPosition($row->groupOption_textViewPosition);
                $groupOptionObject->setOptionTips($row->groupOption_optionTips);
				$resultArray[] = $groupOptionObject;
				
			}
			
			return $resultArray; 
			
		}
		
		public function getGroupOptionValueById($id)
		{
			$resultArray = array();
			$sql = "SELECT groupOption_value FROM survey_groupoption WHERE id ='".$id."'";
			$query = $this->db->query($sql);
			$row = $query->row();
			return $row->groupOption_value;				
			 
		}

        public function getGroupOptionObjectById($groupOptionId)
        {
            $resultArray = array();
            $sql = "SELECT * FROM survey_groupoption WHERE id ='".$groupOptionId."'";
            $query = $this->db->query($sql);
            $groupOptionObject = new groupoption_model();
            foreach ($query->result() as $row) {
                $groupOptionObject->setId($row->id);//到時候取答案的時候，要拿這欄來找出相對應的答案。
                $groupOptionObject->setGuid($row->guid);
                $groupOptionObject->setType($row->groupOption_type);
                $groupOptionObject->setIndex($row->groupOption_index);
                $groupOptionObject->setText($row->groupOption_text);
                $groupOptionObject->setValue($row->groupOption_value);
                $groupOptionObject->setIsShowTextView($row->groupOption_isShowTextView);
                $groupOptionObject->setTextViewPosition($row->groupOption_textViewPosition);
                $groupOptionObject->setOptionTips($row->groupOption_optionTips);

            }

            return $groupOptionObject;

        }
		
		public function add($groupOption)
		{
			$sql = "INSERT INTO survey_groupoption (guid, groupOption_type, groupOption_index, groupOption_text, groupOption_value, groupOption_isShowTextView
			, groupOption_textViewPosition, groupOption_optionTips)
        	VALUES ('".$groupOption->getGuid()."', '".$groupOption->getType()."','".$groupOption->getIndex()."'
        	,'".$groupOption->getText()."','".$groupOption->getValue()."','".$groupOption->getIsShowTextView()."','".$groupOption->getTextViewPosition()."'
        	,'".$groupOption->getOptionTips()."')";

			$this->db->query($sql);
			
		}
		
		public function update($groupOption)
		{
			$sql = "UPDATE survey_groupoption SET guid='".$groupOption->getGuid()."', groupOption_type='".$groupOption->getType()."', groupOption_index='".$groupOption->getIndex()."'
			, groupOption_text='".$groupOption->getText()."', groupOption_value='".$groupOption->getValue()."', groupOption_isShowTextView='".$groupOption->getIsShowTextView().
			"', groupOption_textViewPosition='".$groupOption->getTextViewPosition()."', groupOption_optionTips='".$groupOption->getOptionTips()."'";
			
			$this->db->query($sql);
		}
		
		public function delete($groupOptionGuid)
		{
			$sql = "DELETE FROM survey_groupoption where guid='".$groupOptionGuid."'"; 
	
			$this->db->query($sql);
		}
		
		public function setId($id)
		{
			$this->_id = $id;
		}
		
		public function getId()
		{
			return $this->_id;
		}
		
		public function setGuid($guid)
		{
			$this->_guid = $guid;
		}
		
		public function getGuid()
		{
			return $this->_guid;
		}
		
		public function setType($type)
		{
			$this->_type = $type;
		}
		
		public function getType()
		{
			return $this->_type;
		}
		
		public function setIndex($index)
		{
			$this->_index = $index;
		}
		
		public function getIndex()
		{
			return $this->_index;
		}
		
		public function setText($text)
		{
			$this->_text = $text;
		}
		
		public function getText()
		{
			return $this->_text;
		}
		
		public function setValue($value)
		{
			$this->_value = $value;
		}
		
		public function getValue()
		{
			return $this->_value;
		}
		
		public function setIsShowTextView($isShowTextView)
		{
			$this->_isShowTextView = $isShowTextView;
		}
		
		public function getIsShowTextView()
		{
			return $this->_isShowTextView;
		}
		
		public function setTextViewPosition($textViewPosition)
		{
			$this->_textViewPosition = $textViewPosition;
		}
		
		public function getTextViewPosition()
		{
			return $this->_textViewPosition;
		}

        /**
         * @param $groupOptionGuid
         * @return array
         */
        public function listValueByGroupMainOption($groupOptionGuid)
        {
            $sql = "SELECT groupOption_value FROM survey_groupoption WHERE guid = '" . $groupOptionGuid . "'
             and groupOption_type = 0 order by groupOption_value ASC";
//            $sql = "SELECT count(guid) as mainOptionCount FROM survey_groupoption WHERE guid = '" . $groupOptionGuid . "' and groupOption_type = 0 ";
            $result = $this->db->query($sql);
            $mainOptionArray = array();
            foreach ($result->result() as $row) {
                $mainOptionArray[] = $row->groupOption_value;
            }
            return $mainOptionArray;
        }

        /**
         * @param $groupOptionGuid
         * @return array
         */
        public function listValueByGroupSubOption($groupOptionGuid)
        {
//            $sql = "SELECT count(guid) as subOptionCount FROM survey_groupoption WHERE guid = '" . $groupOptionGuid . "' and groupOption_type != 0 ";
            $sql = "SELECT CONVERT(groupOption_value,UNSIGNED INTEGER) AS subOptionValue FROM survey_groupoption WHERE
             guid = '" . $groupOptionGuid . "' and groupOption_type != 0 order by subOptionValue ASC";
            $result = $this->db->query($sql);
            $subOptionArray = array();
            foreach ($result->result() as $row) {
                $subOptionArray[] = $row->subOptionValue;
            }
            return $subOptionArray;
        }

        /**
         * @param $groupOptionGuid
         * @return int
         */
        public function getCountByGroup88Option($groupOptionGuid)
        {
            $sql = "SELECT count(guid) as option88Count FROM survey_groupoption WHERE guid = '" . $groupOptionGuid . "' and groupOption_value = 88 ";
            $result = $this->db->query($sql);
            $Option88Count = 0;
            foreach ($result->result() as $row) {
                $Option88Count = $row->option88Count;
            }
            return $Option88Count;
        }

        public function setOptionTips($optionTips)
        {
            $this->_optionTips = $optionTips;
        }

        public function getOptionTips()
        {
            return $this->_optionTips;
        }

    }
	
?>
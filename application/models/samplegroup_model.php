<?php
    /**
     * 管理樣本分群
     */
    class samplegroup_model extends CI_Model{
        private $_sampleGroupGuid;
        private $_sampleGroupName;
        private $_sampleArray = array();

        function __construct() {
            parent::__construct();
            //Initial Guid for md5
            $this->load->helper('security');
            $this->setSampleGroupGuid(do_hash(microtime(),'md5'));
        }

        //get All data
        public function getSampleGroupWithPage($surveyGuid ,$limit = 10, $offset = 0)
        {
            $this->db->select('survey_samplegroup_data.sampleGroupGuid,survey_samplegroup_data.sampleGroupName,survey_survey_own_sample.interviewerGuid');
            $this->db->join('survey_samplegroup_data','survey_survey_own_sample.groupSampleGuid = survey_samplegroup_data.sampleGroupGuid','inner');
            $this->db->where('surveyGuid',$surveyGuid);
            $this->db->order_by('id','acs');
            return $this->db->get('survey_survey_own_sample',$limit,$offset);


            $sql = "select survey_samplegroup_data.sampleGroupGuid,survey_samplegroup_data.sampleGroupName,survey_survey_own_sample.interviewerGuid
             from survey_survey_own_sample
             inner join survey_samplegroup_data on survey_survey_own_sample.groupSampleGuid = survey_samplegroup_data.sampleGroupGuid
             where survey_survey_own_sample.surveyGuid = '".$surveyGuid."' order by id asc";
//            return $this->db->query($sql);
        }

        public function getAllSampleGroupData(){
            $sql = "SELECT * from survey_samplegroup_data";
            return $this->db->query($sql);
        }

        //用guid撈sampleGroup資料
        public function getSampleGroupBySampleGroupGuid($sampleGroupGuid){
            $query = $this->db->get_where('survey_samplegroup_data',array('sampleGroupGuid'=>$sampleGroupGuid));
            $row = $query->row();
            $sampleGroupObject = new samplegroup_model();
            $sampleGroupObject->setSampleGroupGuid($row->sampleGroupGuid);
            $sampleGroupObject->setSampleGroupName($row->sampleGroupName);
            $sampleGroupObject->setSampleArray($this->getSampleArrayInSampleGroupFromDB($row->sampleGroupGuid));
            return $sampleGroupObject;
        }

        //撈出sampleGroup裡的所有Sample資料
        function getSampleArrayInSampleGroupFromDB($sampleGroupGuid){
            $query2 = $this->db->get_where('survey_samplegroup',array('guid'=>$sampleGroupGuid));
            $sampleArray = array();
            foreach($query2->result() as $row2){
                $sampleArray[] = $row2->sampleGuid;
            }
            return $sampleArray;

        }

        public function getCountAll($surveyGuid)
        {
            $this->db->like('surveyGuid',$surveyGuid);
            $this->db->from('survey_interviewer_own_surveys');
            return $this->db->count_all_results();
        }

        public function getCountAllSampleGroup($surveyGuid)
        {
            $this->db->select('survey_samplegroup_data.sampleGroupGuid,survey_samplegroup_data.sampleGroupName,survey_survey_own_sample.interviewerGuid');
            $this->db->join('survey_samplegroup_data','survey_survey_own_sample.groupSampleGuid = survey_samplegroup_data.sampleGroupGuid','inner');
            $this->db->where('surveyGuid',$surveyGuid);
            $this->db->order_by('id','acs');
            return $this->db->count_all_results('survey_survey_own_sample');
        }

        public function deleteSampleGroupData($sampleGroupGuid)
        {
            $data = array(
                'sampleGroupGuid' => $sampleGroupGuid
            );
            $this->db->delete('survey_samplegroup_data',$data);
        }

        //刪除整個Group
        public function deleteSampleGroup($sampleGroupGuid)
        {

            $sql = "DELETE FROM survey_samplegroup where guid = '".$sampleGroupGuid."'";
            $this->db->query($sql);

        }

        //只刪除某個Group裡的sample
        public function deleteSampleGuid($sampleGuid)
        {
            $sql = "DELETE FROM survey_samplegroup where sampleGuid ='".$sampleGuid."'";
            $this->db->query($sql);
        }

        public function deleteSurveyOwnSampleByGroupSimpleGuid($groupSampleGuid)
        {
            $sql = "DELETE FROM survey_survey_own_sample where groupSampleGuid='".$groupSampleGuid."'";
            $this->db->query($sql);
        }

    //新增樣本在新的樣本分群
        public function addSampleToGroup($sampleGroupObject)
        {
            $sampleGroupGuid = $sampleGroupObject->getSampleGroupGuid();
            $sampleGroupName = $sampleGroupObject->getSampleGroupName();
            $sampleArray = $sampleGroupObject->getSampleArray();

            //Group Data
            $data = array(
                'sampleGroupGuid' => $sampleGroupGuid ,
                'sampleGroupName' => $sampleGroupName
            );
            $this->db->insert('survey_samplegroup_data',$data);

            //SampleGroup
            for ($i=0; $i < count($sampleArray); $i++) {

                $sql = "SELECT guid from survey_samplegroup where guid = '".$sampleGroupGuid."' and sampleGuid = '".$sampleArray[$i]."'";
                $result = $this->db->query($sql);
                if ($result->num_rows()==0) {
                    if($sampleArray[$i]!=""){
                        $sql2 = "INSERT INTO survey_samplegroup (guid, sampleGuid) VALUES ('".$sampleGroupGuid."', '".$sampleArray[$i]."')";
                        $this->db->query($sql2);
                    }
                }
            }
            return $sampleGroupGuid;
        }  

        //查詢未被挑選走的樣本清單
        public function getNotInSampleArrayList($surveyGuid)
        {
            $notInArray = array();
            $sql = "select * from survey_sample where surveyGuid = '".$surveyGuid."'and guid not in (select sampleGuid from survey_samplegroup)";
            $query = $this->db->query($sql);
            foreach($query->result() as $row){
                $sampleObject = new sample_model();
                $sampleObject->setGuid($row->guid);
                $sampleObject->setSurveyGuid($row->surveyGuid);
                $sampleObject->setName($row->name);
                $sampleObject->setGender($row->gender);
                $sampleObject->setBirthday($row->birthday);
                $sampleObject->setAddress($row->address);
                $sampleObject->setZipCode($row->zipcode);
                $sampleObject->setMobilePhone($row->mobilephone);
                $notInArray[] = $sampleObject;
            }
            return $notInArray;
        }



        public function getSampleGroupGuid(){
            return $this->_sampleGroupGuid;
        }

        public function setSampleGroupGuid($sampleGroupGuid){
            $this->_sampleGroupGuid = $sampleGroupGuid;
        }

        public function getSampleGroupName(){
            return $this->_sampleGroupName;
        }

        public function setSampleGroupName($sampleGroupName){
            $this->_sampleGroupName = $sampleGroupName;
        }

        public function getSampleArray(){
            return $this->_sampleArray;
        }

        public function setSampleArray($sampleArray){
            $this->_sampleArray = $sampleArray;
        }

    }
?>
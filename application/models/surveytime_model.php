<?php
    /**
     * 
     */
    class surveytime_model extends CI_Model {
        
		private $_sampleGuid;
		private $_interviewerGuid;
		private $_surveyGuid;
		private $_index;
		private $_startDateTime;
		private $_endDateTime;
		
        function __construct() {
            parent::__construct();
			//Initial Guid for md5
        }
				
		public function getList($surveyTime)
		{
			
			$sql = "SELECT * FROM survey_surveytime where sampleGuid='".$surveyTime->getSampleGuid()."' and interviewerGuid='".$surveyTime->getInterviewerGuid()."'
			 and surveyGuid='".$surveyTime->getSurveyGuid()."'";
			$query = $this->db->query($sql);
			$resultArray = array();
			foreach ($query->result() as $row) {
				$surveyTime = new surveytime_model();
				$surveyTime->setSampleGuid($row->surveyGuid);
				$surveyTime->setInterviewerGuid($row->interviewerGuid);
				$surveyTime->setSurveyGuid($row->surveyGuid);
				$surveyTime->setIndex($row->sIndex);
				$surveyTime->setStartDateTime($row->startDateTime);
				$surveyTime->setEndDateTime($row->endDateTime);

				$resultArray[] = $surveyTime;	
			}
			return $resultArray;			
		}
		
		public function checkSurveyTimeAlreadyHave($surveyTime)
		{
			$sql = "SELECT sampleGuid FROM survey_surveytime where sampleGuid='".$surveyTime->getSampleGuid()."' and interviewerGuid='".$surveyTime->getInterviewerGuid()."'
			 and surveyGuid='".$surveyTime->getSurveyGuid()."' and sIndex='".$surveyTime->getIndex()."'"; 
			$this->db->query($sql);
			$result = $this->db->query($sql);
			return $result->num_rows();
		}
		
		
		public function addSurveyTime($surveyTime)
		{
			$sql = "INSERT INTO survey_surveytime (sampleGuid, interviewerGuid, surveyGuid, sIndex, startDateTime, endDateTime) 
        	VALUES ('".$surveyTime->getSampleGuid()."', '".$surveyTime->getInterviewerGuid()."','".$surveyTime->getSurveyGuid()."'
        	,'".$surveyTime->getIndex()."','".$surveyTime->getStartDateTime()."','".$surveyTime->getEndDateTime()."')";

			$this->db->query($sql);
		}
		
		public function updateSample($surveyTime)
		{
			$sql = "UPDATE survey_surveytime SET sampleGuid='".$surveyTime->getSampleGuid()."'
			, interviewerGuid='".$surveyTime->getInterviewerGuid()."', surveyGuid='".$surveyTime->getSurveyGuid()."'
			, sIndex='".$surveyTime->getIndex()."', startDateTime='".$surveyTime->getStartDateTime()."'
			, endDateTime='".$surveyTime->getEndDateTime()."' where sIndex = '".$surveyTime->getIndex()."'"; 

			$this->db->query($sql);

			// echo $this->db->affected_rows();
		}
		
		public function deleteSample($surveyTime)
		{
			$sql = "DELETE FROM survey_surveytime where sampleGuid='".$surveyTime->getGuid()."' and interviewerGuid='".$surveyTime->getInterviewerGuid()."'
			 and surveyGuid='".$surveyTime->getSurveyGuid()."' and sIndex='".$surveyTime->getIndex()."'"; 
			$this->db->query($sql);
			// echo $this->db->affected_rows();
		}
		
		public function getTotalSurveyMinute($surveyTime)
		{
			$list = $this->getList($surveyTime);
			$minute = 0;
			date_default_timezone_set('Asia/Taipei');
			foreach ($list as $row) {
				$startDate = $row->getStartDateTime();
				$endDate = $row->getEndDateTime();
				$strEndDate = strtotime($endDate);
				$strStartDate = strtotime($startDate);
				if ($strEndDate > $strStartDate) {
					$minute += ($strEndDate - $strStartDate)/60; //算相差幾分鐘	
				}else {
					$minute += 1; //算相差幾分鐘
				}
			}
			//四捨五入
			$minute = round($minute,0);
			return $minute;
		}
		
		public function getSampleGuid()
		{
			return $this->_sampleGuid;
		}
		
		public function setSampleGuid($sampleGuid)
		{
			$this->_sampleGuid = $sampleGuid;
		}
		
		public function getInterviewerGuid()
		{
			return $this->_interviewerGuid;
		}
		
		public function setInterviewerGuid($interviewerGuid)
		{
			$this->_interviewerGuid = $interviewerGuid;
		}
		
		public function getSurveyGuid()
		{
			return $this->_surveyGuid;
		}
		
		public function setSurveyGuid($surveyGuid)
		{
			$this->_surveyGuid = $surveyGuid;
		}
		
		public function getIndex()
		{
			return $this->_index;
		}
		
		public function setIndex($index)
		{
			$this->_index = $index;
		}
		
		public function getStartDateTime()
		{
			return $this->_startDateTime;
		}
		
		public function setStartDateTime($startDateTime)
		{
			$this->_startDateTime = $startDateTime;			
		}
		
		public function getEndDateTime()
		{
			return $this->_endDateTime;
		}
		
		public function setEndDateTime($endDateTime)
		{
			$this->_endDateTime = $endDateTime;
		}
		
    }
    
?>
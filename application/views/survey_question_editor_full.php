<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>style/tableStyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css" />
<!-- Favicons --> 
<link rel="shortcut icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="apple-touch-icon" HREF="<?php echo base_url(); ?>style/admin/img/favicons/apple.png" />
<!-- Main Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/style.css" type="text/css" />
<!-- Colour Schemes
Default colour scheme is blue. Uncomment prefered stylesheet to use it.
<link rel="stylesheet" href="css/brown.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/gray.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/green.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/pink.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/red.css" type="text/css" media="screen" />
-->
<!-- Your Custom Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/custom.css" type="text/css" />
<!--swfobject - needed only if you require <video> tag support for older browsers -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/swfobject.js"></script>
<!-- jQuery with plugins -->
<!-- <script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-1.4.2.min.js"></script> -->

<script type="text/javascript" SRC="<?php echo base_url(); ?>style/fancybox2/lib/jquery-1.7.2.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-ui-1.7.3.custom.min.js"></script>
<!-- Could be loaded remotely from Google CDN : <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.tabs.min.js"></script>
<!-- jQuery tooltips -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.tipTip.min.js"></script>
<!-- Superfish navigation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.superfish.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.supersubs.min.js"></script>
<!-- jQuery form validation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.validate_pack.js"></script>
<!-- jQuery popup box -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.nyroModal.pack.js"></script>
<!-- jQuery graph plugins -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/tablednd/jquery.tablednd.0.6.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/tablednd/jquery.tablednd.0.7.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/tablednd/jquery.tablednd.js"></script>
<!--[if IE]><script type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>style/calendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>style/modal.popup.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>style/editorPopup.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
<!-- Internet Explorer Fixes --> 
<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
<script src="js/html5.js"></script>
<![endif]-->
<!--Upgrade MSIE5.5-7 to be compatible with MSIE8: http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE8.js -->
<!--[if lt IE 8]>
<script src="js/IE8.js"></script>
<![endif]-->
	<style>
		body { font-size: 62.5%; }
		label, input {/* display:block;*/ }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		div#users-contain { width: 350px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		a.deleteOptionLink:link {color: #BBBBBB; text-decoration:none;}
		a.deleteOptionLink:hover {color: #DD0000; text-decoration:none;}
		a.deleteOptionLink:visited {color: #BBBBBB; text-decoration:none;}
        #questionContent {float: left; width: 790px; text-align: left}
        #questionContent li {
            list-style: none;
            margin: 0 0 4px 0;
            padding: 10px;
            background-color:#00CCCC;
            border: #CCCCCC solid 1px;
            color:#fff;
        }

    </style>
<script type = "text/javascript">
var sSn = 1;
var mSn = 1;
var tSn = 1;
var pSn = 1;
var gSn = 1;
$(document).ready(function() {
	var align = 'center';									//Valid values; left, right, center
	var top = 50; 											//Use an integer (in pixels)
	var width = 750; 										//Use an integer (in pixels)
	var padding = 10;										//Use an integer (in pixels)
	var backgroundColor = '#FFFFFF'; 						//Use any hex code
	var guid = '<?php echo $guid;?>';
	var source = "/surveyapp/surveys/questionBuilder/"; 								//Refer to any page on your server, external pages are not valid e.g. http://www.google.co.uk
	var borderColor = '#333333'; 							//Use any hex code
	var borderWeight = 4; 									//Use an integer (in pixels)
	var borderRadius = 5; 									//Use an integer (in pixels)
	var fadeOutTime = 300; 									//Use any integer, 0 = no fade
	var disableColor = '#666666'; 							//Use any hex code
	var disableOpacity = 40; 								//Valid range 0-100
	var loadingImage = 'loading.gif';		//Use relative path from this page
	//This method initialises the modal popup
	$(".editorPopup").bind("click", { guid: '<?php echo $guid;?>' }, function(event){
		setUrl(event.data.guid);
		source = getUrl();
        modalPopup(align, top, width, padding, disableColor, disableOpacity, backgroundColor, borderColor, borderWeight, borderRadius, fadeOutTime, source, loadingImage);
	});

	function setUrl(aGuid){
		guid = aGuid;
	}

	function getUrl () {
		source = '/surveyapp/surveys/questionBuilder/'+guid;
	  	return source;
	}

    var fixHelperModified = function(e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function(index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            updateIndex = function(e, ui) {
                $('td.index', ui.item.parent()).each(function (i) {
                    $(this).html(i + 1);
                });
            };


    $("#sort tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    //修改題目順序
    $(function() {
        $("#sort tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
            var order = $(this).sortable("serialize")+'&action=updateQuestionPriority';
            $.post("/surveyapp/surveys/doUpdateQuestionPriority", order, function(theResponse){
                $(".message").html(theResponse);
                setTimeout(function() {
                    $('.message').fadeOut(1000);
                }, 1000);
                setTimeout(function() {
                    $('.message').html('');
                    $('.message').fadeIn();
                }, 2000);
            });
        }
        });
    });

 });


  	function cancelCreate() {
		closePopup(1);
	}

  	function closeTypeDiv (){
  		$("#type-S-div").fadeOut('medium');
  		$("#type-M-div").fadeOut('medium');
  		$("#type-P-div").fadeOut('medium');
  		$("#type-G-div").fadeOut('medium');
  		$("#type-T-div").fadeOut('medium');
  		$("#type-FO-div").fadeOut('medium');
  		$("#type-FI-div").fadeOut('medium');
  		$('#passLogic-div').fadeOut('medium');
  		$('#pass-div').fadeOut('medium');
  	}

 	function detectType () {
		// document.write('test');
		$("#isOptionPassLogic").attr("checked",false);
		closeTypeDiv();
		var selectValue = $("#q_type").find(":selected").val();
		if (selectValue!='Empty'){
			$("#option-div").fadeIn('medium');
		}else {
			$("#option-div").fadeOut('medium');
		}
		switch(selectValue){
			case 'S':
				$("#type-S-div").fadeIn('medium');
                $('#type-S-column-div').remove();
                $('#addSButton').remove();
                $("#type-S-div").append('<div id="type-S-column-div"></div><input type="BUTTON" value="新增" id="addSButton" name="addSButton" onClick="addSOption();">');
                $('#passLogic-div').fadeIn('medium');
                sSn = 0;
                addSOption();
				break;
			case 'M':
				$("#type-M-div").fadeIn('medium');
                $('#type-M-column-div').remove();
                $('#addMButton').remove();
                $("#type-M-div").append('<div id="type-M-column-div"></div><input type="BUTTON" value="新增" id="addMButton" name="addMButton" onClick="addMOption();">');
                $('#passLogic-div').fadeIn('medium');
                mSn = 0;
                addMOption();
				break;
			case 'P':
				$("#type-P-div").fadeIn('medium');
				$('#type-P-column-div').remove();
				$('#addPButton').remove();
				$("#type-P-div").append('<div id="type-P-column-div"></div><input type="BUTTON" value="新增" id="addPButton" name="addPButton" onClick="addPOption();">');
				$('#passLogic-div').fadeIn('medium');
				pSn = 0;
				addPOption();
				break;
			case 'G':
				$("#type-G-div").fadeIn('medium');
				$('#type-G-column-div').remove();
				$('#addGButton').remove();
				$("#type-G-div").append('<div id="type-G-column-div"></div><input type="BUTTON" value="新增主分類" id="addGButton" name="addGButton" onClick="addGOption();">');
				gSn = 0;
				addGOption();
				break;
			case 'T':
				$("#type-T-div").fadeIn('medium');
				$('#type-T-column-div').remove();
				$('#addTButton').remove();
				$("#type-T-div").append('<div id="type-T-column-div"></div><input type="BUTTON" value="新增" id="addTButton" name="addTButton" onClick="addTOption();">');
				$('#passLogic-div').fadeIn('medium');
				tSn = 0;
				addTOption();
				break;
			case 'FO':
				$("#type-FO-div").fadeIn('medium');
				$('#passLogic-div').fadeIn('medium');
				break;
			case 'FI':
				$("#type-FI-div").fadeIn('medium');
				$('#passLogic-div').fadeIn('medium');
				break;

		}
	}

	function detectPassLogic(){
		if ($("#isOptionPassLogic").attr("checked")){
			$("#pass-div").fadeIn('medium');
		}else {
			$("#pass-div").fadeOut('medium');
		}
	}

    function detectIsSelectShowTextViewCheckBox(type,sn) {
        typeSn = type+sn;
        if ($("#optionIsShowTextViewCKBox"+typeSn).attr("checked")){
            $("#optionValue"+typeSn).val('88');
            $("#optionValue"+typeSn).prop("readonly", true);
        }else {
            $("#optionValue"+typeSn).val(sn);
            $("#optionValue"+typeSn).removeAttr("readonly");
        }
    }


	function splitOptionFromTextArea(optionArea) {
		var option = optionArea;
		option = option.rtrim();
		option = option.replace(/([^>])\n/g, '$1<br/>\n');
		option = option.split('<br/>');
		return option;
	}

    function addSOption(){
        if (sSn>=50) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            sSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+sSn+'">' +
                    '選項'+sSn+'<input type="text" size="60" id="optionAreaS'+sSn+'" name="optionAreaS'+sSn+'">' +
                    '選項值<input type="text" size="3" id="optionValueS'+sSn+'" name="optionValueS'+sSn+'" value="'+sSn+'">' +
                    '顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxS'+sSn+'" name="optionIsShowTextViewCKBoxS'+sSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'S'"+','+sSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+sSn+');">   x</a>';
            divEnd = '</div>';
            if(sSn > 1){
                resultAppend = appendForm+deleteOptionLink+divEnd;
            }else {
                resultAppend = appendForm+divEnd;
            }
            $("#type-S-column-div").append(resultAppend);
        }
    }

    function addMOption(){
        if (mSn>=50) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            mSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+mSn+'">' +
                    '選項'+mSn+'<input type="text" size="60" id="optionAreaM'+mSn+'" name="optionAreaM'+mSn+'">' +
                    '選項值<input type="text" size="3" id="optionValueM'+mSn+'" name="optionValueM'+mSn+'" value="'+mSn+'">' +
                    '顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxM'+mSn+'" name="optionIsShowTextViewCKBoxM'+mSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'M'"+','+mSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+mSn+');">   x</a>';
            divEnd = '</div>';
            if(mSn > 1){
                resultAppend = appendForm+deleteOptionLink+divEnd;
            }else {
                resultAppend = appendForm+divEnd;
            }
            $("#type-M-column-div").append(resultAppend);
        }
    }

    function addTOption(){
		if (tSn>=10) {
			//$("#error_msg").fadeIn('medium');
			//$("#error_msg").text("分類限制20以下");
		}else {
			tSn++;
			appendForm = '<div class="optionForm-div" id="optionForm-div'+tSn+'">選項'+tSn+'<input type="text" size="60" id="optionAreaT'+tSn+'" name="optionAreaT'+tSn+'"></input>';
			deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+tSn+');">   x</a>';
			divEnd = '</div>';
			if(tSn > 1){
				resultAppend = appendForm+deleteOptionLink+divEnd;
			}else {
				resultAppend = appendForm+divEnd;
			}
			$("#type-T-column-div").append(resultAppend);
		}
	}

	function addPOption(){
		if (pSn>=10) {
			//$("#error_msg").fadeIn('medium');
			//$("#error_msg").text("選項限制10以下");
		}else {
			pSn++;
			appendForm = '<div class="optionForm-div" id="optionForm-div'+pSn+'">選項'+pSn+'<input type="text" size="50" id="optionAreaP'+pSn+'" name="optionAreaP'+pSn+'">'+
			 '範圍:<input type="text" size="2" id="type-P-value-start'+pSn+'" name="type-P-value-start'+pSn+'" >'+
			 '~<input type="text" size="2" id="type-P-value-end'+pSn+'" name="type-P-value-end'+pSn+'">'+
			 '間隔:<input type="text" size="2" id="type-P-value-tick'+pSn+'" name="type-P-value-tick'+pSn+'">';
			deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+pSn+');">   x</a>';
			divEnd = '</div>';
			if(pSn > 1){
				resultAppend = appendForm+deleteOptionLink+divEnd;
			}else {
				resultAppend = appendForm+divEnd;
			}

			$("#type-P-column-div").append(resultAppend);
		};

	}

	function addGOption(){
		if (gSn>=20) {
			//$("#error_msg").fadeIn('medium');
			//$("#error_msg").text("分類限制20以下");
		}else {
			gSn++;
			$("#type-G-column-div").append
			('<br>主分類'+gSn+'：<input type="text" id="type-G-main-group'+gSn+'" name="type-G-main-group'+gSn+'"><br>'+
			'</input> 選項：<textarea rows="6" cols="50" id="type-G-sub-group'+gSn+'" name="type-G-sub-group'+gSn+'" Wrap="Physical">'+
			'</textarea>');
		};

	}

	function removeOption(sn){
		$("#optionForm-div"+sn).remove();
        sSn--;
        mSn--;
		gSn--;
		tSn--;
		pSn--;
	}

	function submitCheck() {
		var validate = true;
		var msg = "";

		//判斷空白未填
		if($("#q_subjectNumber").val()==""){
			validate = false;
			msg = "請輸入題號";
		}else if($("#q_subject").val()==""){
			validate = false;
			msg = "請輸入標題";
		}else if($("#q_type").val()=="Empty"){
			validate = false;
			msg = "請設定題目類型";
		}

		//判斷選項空白
		var selectValue = $("#q_type").find(":selected").val();
		switch(selectValue){
			case 'FO':
				// var option = $("#optionArea").val();
				// var optionArray = new Array();
				// option = option.replace(/([^>])\n/g, '$1<br/>\n');
				// optionArray = option.split('<br/>');
				// if (optionArray.length!=4){
					// validate = false;
					// msg = "選項應為4則";
				// }
				var option1 = $("#optionAreaFO1").val();
				var option2 = $("#optionAreaFO2").val();
				var option3 = $("#optionAreaFO3").val();
				var option4 = $("#optionAreaFO4").val();
				if (option1 == "" || option2 == "" || option3 == "" || option4 == ""){
					validate = false;
					msg = "請完整輸入4則選項";
				}
				break;
			case 'FI':
				// var option = $("#optionArea").val();
				// var optionArray = new Array();
				// option = option.replace(/([^>])\n/g, '$1<br/>\n');
				// optionArray = option.split('<br/>');
				// if (optionArray.length!=5){
					// validate = false;
					// msg = "選項應為5則";
				// }
				var option1 = $("#optionAreaFI1").val();
				var option2 = $("#optionAreaFI2").val();
				var option3 = $("#optionAreaFI3").val();
				var option4 = $("#optionAreaFI4").val();
				var option5 = $("#optionAreaFI5").val();
				if (option1 == "" || option2 == "" || option3 == "" || option4 == "" || option5 == ""){
					validate = false;
					msg = "請完整輸入5則選項";
				}
				break;
			case 'S':
                for (i=1;i<=sSn;i++){
                    if($("#optionAreaS"+i).val()=="" || $("#optionValueS"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
				break;
			case 'M':
                for (i=1;i<=mSn;i++){
                    if($("#optionAreaM"+i).val()=="" || $("#optionValueM"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
				break;
			case 'T':
				for (i=1;i<=tSn;i++){
					if($("#optionAreaT"+i).val()==""){
						validate = false;
						msg = "請完整輸入選項"+i+"的描述";
					}
				}
				break;
			case 'P':
				for (i=1;i<=pSn;i++){
					if($("#optionAreaP"+i).val()=="" || $("#type-P-value-start"+i).val()=="" || $("#type-P-value-end"+i).val()=="" || $("#type-P-value-tick"+i).val()==""){
						validate = false;
						msg = "請完整輸入選項"+i+"，的描述、範圍與間隔";
					}
				}

				break;

		}

		if (validate){
			$("#form").submit();
		}else {
			$("#error_msg").fadeIn('medium');
			$("#error_msg").text(msg);
		}


		// document.write('test');
		// return false;

		return validate;
	}


</script>
</head>
<body>
		<!-- Header -->
	<header id="top">
		<div class="wrapper">
			<!-- Title/Logo - can use text instead of image -->
			
			<div id="title">
<!-- 				<?php echo $title; ?> -->
<!-- 				<img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" alt="Administry" /><!--<span>Administry</span> demo-->
			<img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" />
			</div>
			<!-- Top navigation -->
			<div id="topnav">
				<!-- <a href="#"><img class="avatar" SRC="<?php echo base_url(); ?>style/admin/img/user_32.png" alt="" /></a> -->
				您好      <b><?php echo $username;?></b>

				<span>|</span> <a href="/surveyapp/webLogout">登出</a><br />
			</div>
			<!-- End of Top navigation -->
			<!-- Main navigation -->
			<nav id="menu">
				<ul class="sf-menu">
					<li class="current"><a HREF="/surveyapp/surveys">調查管理</a></li>
					</li>
				</ul>
			</nav>
		</div>
	</header>
	<!-- End of Header -->
	<!-- Page title -->
	<div id="pagetitle">
		<div class="wrapper">
			<h1><?php echo $headerTitle; ?></h1>
		</div>
	</div>
		<!-- End of Page title -->
    
    <div id="container">
    	<div class="content">
	    	<div id="leftColumn">
	    		<ul class="survey-left-menu">
	    			<li>
	    				<a href="/surveyapp/surveys/editorFull/<?php echo $surveyGuid;?>">設計問卷</a> 
	    			</li>
	    			<li>
	    				<a href="/surveyapp/interviewers/interviewerList">訪員設定</a>
	    				
	    			</li>
	    			<li >
	    				<a href="/surveyapp/samplegroups/sampleGroupList">樣本設定</a>
	    				
	    			</li>
	    			<li >
                        <a href="/surveyapp/samples/sampleImport">匯入訪員及樣本</a>
                    </li>
	    			<li>
	    				<a href="/surveyapp/answer/listInterviewerOfSurvey/<?php echo $surveyGuid; ?>">回覆</a>
	    				
	    			</li>
	    			<li>
	    				<?php echo $deleteSurveyAction;?>
	    			</li>
	    		</ul>
			</div>
	    	<div id="rightColumn">
				<a class="" href="/surveyapp/surveys/newQuestionView" ><img SRC="<?php echo base_url(); ?>style/images/addSubject.png"/></a>
				<div class="message"></div>
				<div class="data" width=95%><?php //echo $table; ?>	</div>
                <div class="data" width=95%>
                <table id="sort" class="grid" title="">
                    <col width="2%">
                    <col width="7%">
                    <col width="8%">
                    <col width="43%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <thead>
                    <tr><th></th><th class="index">順序</th><th>題號</th><th>問題標題</th><th>題型</th><th>編輯</th><th>刪除</th></tr>
                    </thead>
                    <tbody>

                    <?php for($i=0;$i<count($allQuestionsArray);$i++){?>
                    <tr id="itemGuid_<?php echo $allQuestionsArray[$i]->getGuid();?>">
                        <td><img SRC="<?php echo base_url(); ?>style/images/changePriority.png"/></td>
                        <td class="index"><?php echo $i+1;?></td>
                        <td><?php echo $allQuestionsArray[$i]->getSubjectNumber()?></td>
                        <td><?php echo $allQuestionsArray[$i]->getSubject()?></td>
                        <td width="60px"><?php echo $allQuestionChiTypeArray[$i]?></td>
                        <td><a class="edit" href="/surveyapp/surveys/editQuestion/<?php echo $allQuestionsArray[$i]->getGuid()?>">編輯</a></td>
                        <td><a class="delete" onclick="return confirm('確定刪除題目？')" href="/surveyapp/surveys/deleteQuestion/<?php echo $allQuestionsArray[$i]->getGuid()?>">刪除</a></td>

                    </tr>
                    <?php }?>
                    </tbody>
                </table>
                    </div>

<!--                <div id="questionContent">-->
<!--                    <ul>-->
<!--                        --><?php //foreach ($allQuestionsArray as $row){?>
<!--                                <li id="--><?php //echo $row->getGuid()?><!--">--><?php //echo $row->getSubject()?><!--</li>-->
<!--                         --><?php //} ?>
<!--                    </ul>-->
<!--                </div>-->
			</div>
	    	
    	</div>
    </div>


</body>
</html>
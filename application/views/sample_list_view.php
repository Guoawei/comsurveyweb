<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>style/tableStyle.css" rel="stylesheet" type="text/css" />
<!-- Favicons --> 
<link rel="shortcut icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="apple-touch-icon" HREF="<?php echo base_url(); ?>style/admin/img/favicons/apple.png" />
<!-- Main Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/style.css" type="text/css" />
<!-- Colour Schemes
Default colour scheme is blue. Uncomment prefered stylesheet to use it.
<link rel="stylesheet" href="css/brown.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/gray.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/green.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/pink.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/red.css" type="text/css" media="screen" />
-->
<!-- Your Custom Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/custom.css" type="text/css" />
<!--swfobject - needed only if you require <video> tag support for older browsers -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/swfobject.js"></script>
<!-- jQuery with plugins -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-1.4.2.min.js"></script>
<!-- Could be loaded remotely from Google CDN : <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.tabs.min.js"></script>
<!-- jQuery tooltips -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.tipTip.min.js"></script>
<!-- Superfish navigation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.superfish.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.supersubs.min.js"></script>
<!-- jQuery form validation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.validate_pack.js"></script>
<!-- jQuery popup box -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.nyroModal.pack.js"></script>
<!-- jQuery graph plugins -->
<!--[if IE]><script type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/flot/jquery.flot.min.js"></script>
<!-- Internet Explorer Fixes --> 
<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
<script src="js/html5.js"></script>
<![endif]-->
<!--Upgrade MSIE5.5-7 to be compatible with MSIE8: http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE8.js -->
<!--[if lt IE 8]>
<script src="js/IE8.js"></script>
<![endif]-->

</head>
<body>
		<!-- Header -->
	<header id="top">
		<div class="wrapper">
			<!-- Title/Logo - can use text instead of image -->
			<div id="title">
<!-- 				<img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" alt="Administry" /><!--<span>Administry</span> demo-->
				</div>
			<!-- Top navigation -->
			<div id="topnav">
				<a href="#"><img class="avatar" SRC="<?php echo base_url(); ?>style/admin/img/user_32.png" alt="" /></a>
				您好      <b>schooladmin</b>
<!-- 				<span>|</span> <a href="#">Settings</a> -->
				<span>|</span> <a href="/surveyapp/webLogout">Logout</a><br />
<!-- 				<small>You have <a href="#" class="high"><b>1</b> new message!</a></small> -->
			</div>
			<!-- End of Top navigation -->
			<!-- Main navigation -->
			<nav id="menu">
				<ul class="sf-menu">
					<li class="current"><a HREF="/surveyapp/answer">問卷列表</a></li>
					<li class="current"><a HREF="/surveyapp/samples">樣本列表</a>
						<ul>
							<li>
								<a HREF="/surveyapp/samples/add">新增樣本</a>
							</li>
						</ul>
					</li>
					<li class="current"><a HREF="/surveyapp/interviewers">訪問員列表</a>
						<ul>
							<li>
								<a HREF="/surveyapp/interviewers/add">新增訪問員</a>
							</li>
						</ul>
					</li>
<!-- 					<li>
						<a HREF="styles.html">Styles</a>
						<ul>
							<li>
								<a HREF="styles.html">Basic Styles</a>
							</li>
							<li>
								<a href="#">Sample Pages...</a>
								<ul>
									<li><a HREF="samples-files.html">Files</a></li>
									<li><a HREF="samples-products.html">Products</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li><a HREF="tables.html">Tables</a></li>
					<li><a HREF="forms.html">Forms</a></li>	
					<li><a HREF="graphs.html">Graphs</a></li>	 -->				</ul>
			</nav>
			<!-- End of Main navigation -->
			<!-- Aside links -->
<!-- 			<aside><b>English</b> &middot; <a href="#">Spanish</a> &middot; <a href="#">German</a></aside> -->
			<!-- End of Aside links -->
		</div>
	</header>
	<!-- End of Header -->
	<!-- Page title -->
	<div id="pagetitle">
		<div class="wrapper">
			<h1><?php echo $headerTitle; ?></h1>
			<!-- Quick search box -->
<!-- 			<form action="" method="get"><input class="" type="text" id="q" name="q" /></form> -->
		</div>
	</div>
	<!-- End of Page title -->
    <div class="content">

<div class="paging"><?php echo $pagination; ?></div>
    	<?php echo $form; ?>
<!-- 		<?php for($i=0 ;$i<count($Title);$i++){?>
			<a href='<?php $url = "answer/listInterviewerOfSurvey/".$Guid[$i]; echo site_url($url)?>'>
				<?php echo $Title[$i];?>  By<?php echo $Author[$i];?> 
			</a>
			<br>
		<?php }?> -->
		

	<div class="data"><?php echo $table; ?>	</div>
	<?php echo $formSubmit;?>
	</form>
	<div class="paging"><?php echo $pagination; ?></div>

	</div>
</body>
</html>

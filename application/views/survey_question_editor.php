<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>

	<style>
		body { font-size: 62.5%; }
		label, input { white-space:nowrap; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		div#option-div,div#type-S-div,div#type-M-div,div#type-T-div,div#type-P-div,div#type-G-div,div#type-FO-div,div#type-FI-div{display:none;width:700px;}
		.optionForm-div{ margin:10px;}
		div#passLogic-div,div#pass-div {display:none;width:500px;}
		div#error_msg{display:none;width:500px;color:red;}
		div#users-contain { width: 350px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		.button-contain {float:left; display:block; margin:5 10px; padding-top: 10px;}
	
	</style>

</head>
<body>
	
	<div id="error_msg">
		
	</div>
<form id="form" name ="form" onsubmit="return submitCheck();" action="/surveyapp/surveys/addNewQuestion/<?php echo $guid;?>" method="post">
<!-- 		<form id="form" name ="form" onsubmit="return submitQ();" method="post"> -->
	<div id="form_open">
	<p>題號：</p>
	<input id="q_subjectNumber" type="text" name="q_subjectNumber" size="10">
	<p>問題標題：</p>
	<input id="q_subject" type="text" name="q_subject" size = "100">
	<p>問題類型：</p>
	<select id="q_type" name="q_type" width="" onchange="detectType();">
			<option value="Empty" selected>--選擇問題類型--</option>
			<option value="S">單選題</option>
			<option value="M">多選題</option>
			<option value="T">文字輸入題</option>
			<option value="FO">四分題</option>
			<option value="FI">五分題</option>
			<option value="P">下拉式清單</option>
			<option value="G">群組題</option>
		</select>

	
	</div>
	<div id="option-div">
		<div id="type-S-div">
			<p>答案選擇:在個別行中輸入每一個選擇。</p>
<!--			<textarea rows="10" cols="50" id="optionAreaS" name="optionAreaS" Wrap="Physical"></textarea>-->
		</div>
		<div id="type-M-div">
			<p>答案選擇:在個別行中輸入每一個選擇。</p>
<!--			<textarea rows="10" cols="50" id="optionAreaM" name="optionAreaM" Wrap="Physical"></textarea>-->
		</div>
		<div id="type-T-div">
			<p>填入的答案描述：ex:你的地址、你的電話號碼。</p>
		</div>
		<div id="type-FO-div">
			<p>答案選擇:</p>
			<input type="text" id="optionAreaFO1" name="optionAreaFO1">
			<input type="text" id="optionAreaFO2" name="optionAreaFO2">
			<input type="text" id="optionAreaFO3" name="optionAreaFO3">
			<input type="text" id="optionAreaFO4" name="optionAreaFO4">		
		</div>
		<div id="type-FI-div">
			<p>答案選擇:</p>
			<input type="text" id="optionAreaFI1" name="optionAreaFI1">
			<input type="text" id="optionAreaFI2" name="optionAreaFI2">
			<input type="text" id="optionAreaFI3" name="optionAreaFI3">
			<input type="text" id="optionAreaFI4" name="optionAreaFI4">
			<input type="text" id="optionAreaFI5" name="optionAreaFI5">
		</div>
		<p><div id="type-P-div">
			<p>填入的選項的描述，範圍與間隔。例如：如年齡，1,2,3~59,60，在範圍的輸入框填1與60，在階層的輸入框填1。</p>
		</div>
		</p><div id="type-G-div">
			<div id="type-G-column-div">
			</div>
		</div>
		
		<div id="passLogic-div">
		<p>問題邏輯</p>
		<input type="checkbox" id="isOptionPassLogic" name="isOptionPassLogic" onchange="detectPassLogic();">
		<div id="pass-div">
			<p>輸入選擇的答案，請輸入選項的項目數字，例如：1</p>
			<input type="text" id="passAnswer" name="passAnswer">
			<p>跳題至：</p>
			<select id="passSubjectNumber" name="passSubjectNumber" width="">
				<?php foreach ($allQuestionArray as $questionRow){?>
						<option value="<?php echo $questionRow->getSubjectNumber(); ?>"><?php echo $questionRow->getSubjectNumber().':'.$questionRow->getSubject(); ?></option>
					<?php } ?>	
			</select>
		</div>
		</div>
	</div>
	<table>
		<tr>
			<td></td>
			<td></td>
		</tr>
		
	</table>
	<div class="button-contain">
		<input type="submit" value="儲存"/><!--按下送出後，會導去survey_question_editor_full.php這支檔案的submitCheck() -->
	</div>
	<div class="button-contain">
		<input type="BUTTON" value="取消" name="cancel" onClick="cancelCreate();"/>
	</div>
	</form>
<!-- 	<input type="submit"></input> -->

	
</body>
</html>
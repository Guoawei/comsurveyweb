<div id='answer'>
	<?php if($this->uri->segment(2)== 'answering'):?>
		<!-- 回答問卷題目 -->
		<div class='surveyIntro'>
			<?php echo ($survey['greetingText'])?$survey['greetingText']:'';?>
			<a href='javascript:void(0);' class='continueSurvey btn' data-sguid='<?php echo $survey['guid']?>'>開始(繼續)作答</a>
		</div>
		<div class='ending'>
			<?php echo ($survey['thankText'])?$survey['thankText']:'';?>
			<a href='javascript:void(0);' class='endSurvey btn'>結束問卷，回到樣本列表。</a>
		</div>
		<div class='question'>
			<h3 class='title'></h3>
			<div class='options'>
				
			</div>
			
		</div>
		<div class="nav_buttons clearfix">
			<input type='hidden' id="currentIndex" value=''/>
			<button type='button' class='prev_question left width20' rel='' data-questionNumber='' data-questionIndex='' data-sguid='<?php echo $survey['guid']?>'>上一題</button>
			<button type='button' class='next_question right width20' rel='' data-questionNumber='' data-questionIndex='' data-sguid='<?php echo $survey['guid']?>'>下一題</button>
		</div>
		<div id='tips'></div>
	<?php else:?>
		<!-- 回答開始與中斷的題目-->
		<?php echo $situation;?>
	<?php endif;?>
</div>

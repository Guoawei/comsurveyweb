<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>style/tableStyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css" />
<!-- Favicons --> 
<link rel="shortcut icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="apple-touch-icon" HREF="<?php echo base_url(); ?>style/admin/img/favicons/apple.png" />
<!-- Main Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/style.css" type="text/css" />
<!-- Colour Schemes
Default colour scheme is blue. Uncomment prefered stylesheet to use it.
<link rel="stylesheet" href="css/brown.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/gray.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/green.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/pink.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/red.css" type="text/css" media="screen" />
-->
<!-- Your Custom Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/custom.css" type="text/css" />
<!--swfobject - needed only if you require <video> tag support for older browsers -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/swfobject.js"></script>
<!-- jQuery with plugins -->
<!--123 <script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-1.4.2.min.js"></script> -->

<script type="text/javascript" SRC="<?php echo base_url(); ?>style/fancybox2/lib/jquery-1.7.2.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-ui-1.7.3.custom.min.js"></script>
<!-- Could be loaded remotely from Google CDN : <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.tabs.min.js"></script>
<!-- jQuery tooltips -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.tipTip.min.js"></script>
<!-- Superfish navigation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.superfish.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.supersubs.min.js"></script>
<!-- jQuery form validation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.validate_pack.js"></script>
<!-- jQuery popup box -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.nyroModal.pack.js"></script>
<!-- jQuery graph plugins -->
<!--[if IE]><script type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->

    <!-- OptionTips plugins 2014/04/25-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/ui-lightness/jquery-ui-1.10.4.custom.min.css"/>
    <script type="text/javascript" SRC="<?php echo base_url(); ?>style/optionTips_Input_JS.js"></script>
    <!-- OptionTips plugins 2014/04/25-->


<!-- Internet Explorer Fixes --> 
<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
<script src="js/html5.js"></script>
<![endif]-->
<!--Upgrade MSIE5.5-7 to be compatible with MSIE8: http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE8.js -->
<!--[if lt IE 8]>
<script src="js/IE8.js"></script>
<![endif]-->
	<style>
		body { font-size: 62.5%; }
		label, input { display:nowrap; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
        div#form_open{width:760px;}
		div#option-div,div#type-S-div,div#type-M-div,div#type-T-div,div#type-P-div,div#type-G-div,div#type-FO-div,div#type-FI-div{display:none;width:760px;}
		div#passLogic-div,div#pass-div {display:none;width:500px;}
		div#error_msg{display:none;width:500px;color:red;}
		div#users-contain { width: 550px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		a.deleteOptionLink:link {color: #BBBBBB; text-decoration:none;}
		a.deleteOptionLink:hover {color: #DD0000; text-decoration:none;}
		a.deleteOptionLink:visited {color: #BBBBBB; text-decoration:none;} 

		#rightColumn {
		/*	background-color:#ddd;*/
		  	float:right;
		  	width:790px;     /* 最小行高 */
		  	text-align: left;
		}

	</style>
	<script type = "text/javascript">
    var sSn = 1;
    var mSn = 1;
    var tSn = 1;
    var pSn = 1;
    var gSn = 1;
    var gSub_Sn = 1;

    $(document).ready(function() {
		detectType();
		detectPassLogic();
	});
	
	function closeTypeDiv (){
  		$("#type-S-div").fadeOut('medium');
  		$("#type-M-div").fadeOut('medium');
  		$("#type-P-div").fadeOut('medium');
  		$("#type-G-div").fadeOut('medium');
  		$("#type-T-div").fadeOut('medium');
  		$("#type-FO-div").fadeOut('medium');
  		$("#type-FI-div").fadeOut('medium');
  		$('#passLogic-div').fadeOut('medium');
  		$('#pass-div').fadeOut('medium');
  	}

    function detectType () {
        // document.write('test');
        $("#isOptionPassLogic").attr("checked",false);
        closeTypeDiv();
        var selectValue = $("#q_type").find(":selected").val();
        $("#option-div").fadeIn('medium');
        switch(selectValue){
            case 'S':
                $("#type-S-div").fadeIn('medium');
                $('#type-S-column-div').remove();
                $('#addSButton').remove();
                $("#type-S-div").append('<div id="type-S-column-div"></div><input type="BUTTON" value="新增" id="addSButton" name="addSButton" onClick="addSOption();">');
                $('#passLogic-div').fadeIn('medium');
                sSn = 0;
                addSOption();
                break;
            case 'M':
                $("#type-M-div").fadeIn('medium');
                $('#type-M-column-div').remove();
                $('#addMButton').remove();
                $("#type-M-div").append('<div id="type-M-column-div"></div><input type="BUTTON" value="新增" id="addMButton" name="addMButton" onClick="addMOption();">');
                $('#passLogic-div').fadeIn('medium');
                mSn = 0;
                addMOption();
                break;
            case 'P':
                $("#type-P-div").fadeIn('medium');
                $('#type-P-column-div').remove();
                $('#addPButton').remove();
                $("#type-P-div").append('<div id="type-P-column-div"></div><input type="BUTTON" value="新增" id="addPButton" name="addPButton" onClick="addPOption();">');
                $("#type-P-div").append('<div id="type-P-column-div"></div>');
                $('#passLogic-div').fadeIn('medium');
                pSn = 0;
                addPOption();
                break;
            case 'G':
                $("#type-G-div").fadeIn('medium');
                $('#type-G-column-div').remove();
                $('#addGButton').remove();
                $("#type-G-div").append('<div id="type-G-column-div"></div><input type="BUTTON" value="新增主分類" id="addGButton" name="addGButton" onClick="addGOption();">');
                gSn = 0;
                addGOption();
                break;
            case 'T':
                $("#type-T-div").fadeIn('medium');
                $('#type-T-column-div').remove();
                $('#addTButton').remove();
                $("#type-T-div").append('<div id="type-T-column-div"></div><input type="BUTTON" value="新增" id="addTButton" name="addTButton" onClick="addTOption();">');
                $('#passLogic-div').fadeIn('medium');
                tSn = 0;
                addTOption();
                break;
            case 'FO':
                $("#type-FO-div").fadeIn('medium');
                $('#passLogic-div').fadeIn('medium');
                break;
            case 'FI':
                $("#type-FI-div").fadeIn('medium');
                $('#passLogic-div').fadeIn('medium');
                break;

        }
    }

	function detectPassLogic(){ 
		if ($("#isOptionPassLogic").attr("checked")){
			$("#pass-div").fadeIn('medium');
		}else {
			$("#pass-div").fadeOut('medium');
		}
	}

    function detectIsSelectShowTextViewCheckBox(type,sn) {
        typeSn = type+sn;
        if ($("#optionIsShowTextViewCKBox"+typeSn).attr("checked")){
            $("#optionValue"+typeSn).val('88');
            $("#optionValue"+typeSn).prop("readonly", true);
        }else {
            $("#optionValue"+typeSn).val(sn);
            $("#optionValue"+typeSn).removeAttr("readonly");
        }
    }

    function detectGTypeIsSelectShowTextViewCheckBox(mainSn,subSn) {
        typeSn = mainSn +'_'+ subSn;
        if ($("#optionIsShowTextViewCKBoxG"+typeSn).attr("checked")){
            $("#optionValueG"+typeSn).val('88');
            $("#optionValueG"+typeSn).prop("readonly", true);
        }else {
            $("#optionValueG"+typeSn).val('');
            $("#optionValueG"+typeSn).removeAttr("readonly");
        }
    }

    function addSOption(){
        if (sSn>=100) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            sSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+sSn+'">' +
                    '選項'+sSn+'<input type="text" size="60" id="optionAreaS'+sSn+'" name="optionAreaS'+sSn+'">' +
                    ' 選項值<input type="text" size="3" id="optionValueS'+sSn+'" name="optionValueS'+sSn+'" value="'+sSn+'">' +
                    ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxS'+sSn+'" name="optionIsShowTextViewCKBoxS'+sSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'S'"+','+sSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+sSn+');">   x</a>';
            tipsButton = '<input id="optionS-Tips'+sSn+'" type="hidden" name="optionS-Tips'+sSn+'" value="">' +
                         ' <button id="createButton-optionS-Tips'+sSn+'" onclick="return showCreateOptionTipsView(\'optionS-Tips'+sSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(sSn > 1){
                resultAppend = appendForm+deleteOptionLink+tipsButton+divEnd;
            }else {
                resultAppend = appendForm+tipsButton+divEnd;
            }
            $("#type-S-column-div").append(resultAppend);
        }
    }

    function addMOption(){
        if (mSn>=100) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            mSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+mSn+'">' +
                    '選項'+mSn+'<input type="text" size="60" id="optionAreaM'+mSn+'" name="optionAreaM'+mSn+'">'+
                    ' 選項值<input type="text" size="3" id="optionValueM'+mSn+'" name="optionValueM'+mSn+'" value="'+mSn+'">'+
                    ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxM'+mSn+'" name="optionIsShowTextViewCKBoxM'+mSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'M'"+','+mSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+mSn+');">   x</a>';
            tipsButton = '<input id="optionM-Tips'+mSn+'" type="hidden" name="optionM-Tips'+mSn+'" value="">' +
                         ' <button id="createButton-optionM-Tips'+mSn+'" onclick="return showCreateOptionTipsView(\'optionM-Tips'+mSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(mSn > 1){
                resultAppend = appendForm + deleteOptionLink + tipsButton + divEnd;
            }else {
                resultAppend = appendForm + tipsButton + divEnd;
            }
            $("#type-M-column-div").append(resultAppend);
        }
    }

    function addTOption(){
		if (tSn>=10) {
			//$("#error_msg").fadeIn('medium');
			//$("#error_msg").text("分類限制20以下");
		}else {
			tSn++;
			appendForm = '<div class="optionForm-div" id="optionForm-div'+tSn+'">選項'+tSn+'<input type="text" size="60" id="optionAreaT'+tSn+'" name="optionAreaT'+tSn+'">';
            textViewPosition = '答案描述位置在<select id="optionTextViewPosition'+tSn+'" name="optionTextViewPosition'+tSn+'"><option value="back" selected="selected">前</option><option value="front">後</option></select>';
			deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+tSn+');">   x</a>';
            tipsButton = '<input id="optionT-Tips'+tSn+'" type="hidden" name="optionT-Tips'+tSn+'" value="">' +
                         ' <button id="createButton-optionT-Tips'+tSn+'" onclick="return showCreateOptionTipsView(\'optionT-Tips'+tSn+'\')";>輸入提示</button>';
			divEnd = '</div>';
			if(tSn > 1){
				resultAppend = appendForm + textViewPosition + deleteOptionLink + tipsButton + divEnd;
			}else {
				resultAppend = appendForm + textViewPosition + tipsButton + divEnd;
			}
			$("#type-T-column-div").append(resultAppend);
		}
	}

	function addPOption(){
        surveyGuid = $("#p_surveyGuid").val();
		if (pSn>=10) {
			//$("#error_msg").fadeIn('medium');
			//$("#error_msg").text("選項限制10以下");
		}else {
			pSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+pSn+'">選項'+pSn+'<input type="text" size="50" id="optionAreaP'+pSn+'" name="optionAreaP'+pSn+'">'+
                ' 範圍:<input type="text" size="2" id="type-P-value-start'+pSn+'" name="type-P-value-start'+pSn+'" >'+
                ' ~<div id="dyPickerSetting'+pSn+'" style="display:inline"><input type="text" size="2" id="type-P-value-end'+pSn+'" name="type-P-value-end'+pSn+'"></div>'+
                ' 間隔:<input type="text" size="2" id="type-P-value-tick'+pSn+'" name="type-P-value-tick'+pSn+'">'+
                ' 檢誤功能<input type="checkbox" id="isDyPickerCheck'+pSn+'" name="isDyPickerCheck'+pSn+'" onchange="checkDyPicker(\''+surveyGuid+'\',\'\',\''+pSn+'\',\'\');">';
			deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+pSn+');">   x</a>';
            tipsButton = '<input id="optionP-Tips'+pSn+'" type="hidden" name="optionP-Tips'+pSn+'" value="">' +
                         ' <button id="createButton-optionP-Tips'+pSn+'" onclick="return showCreateOptionTipsView(\'optionP-Tips'+pSn+'\')";>輸入提示</button>';
			divEnd = '</div>';
			if(pSn > 1){
				resultAppend = appendForm + deleteOptionLink + tipsButton + divEnd;
			}else {
				resultAppend = appendForm + tipsButton + divEnd;
			}
			$("#type-P-column-div").append(resultAppend);
		}
		
	}

    function addGOption(){
        if (gSn>20) {
            $("#error_msg").fadeIn('medium');
            $("#error_msg").text("分類限制20以下");
        }else {
            gSn++;
            appendForm =
                '<div id="type-G-Area'+gSn+'-div">'+
                    '<br>主分類'+gSn+'：<input type="text" id="type-G-main-group'+gSn+'" name="type-G-main-group'+gSn+'"></input>' +
                    '<input type="BUTTON" value="新增選項" name="addGSubButton" onClick="addGSubOption('+gSn+');">'+
                    '<input id="optionGMain-Tips'+gSn+'" type="hidden" name="optionGMain-Tips'+gSn+'" value="">' +
                    '<button id="createButton-optionGMain-Tips'+gSn+'" onclick="return showCreateOptionTipsView(\'optionGMain-Tips'+gSn+'\')";>輸入提示</button>'+
                    '<a class="deleteOptionLink" href="javascript:removeGMainOption('+gSn+');">   x </a>'+
                    '<br/>'+
                    '<div class="optionLogicG-div" id="optionLogicG_'+gSn+'-div">'+
                    '<div class="optionFormG-div" id="optionFormG_'+gSn+'_1-div">'+
                    '選項'+gSub_Sn+'： '+'<input type="text" size="50" id="optionAreaG'+gSn+'_1" name="optionAreaG'+gSn+'_1">'+
                    ' 選項值 <input type="text" size="3" id="optionValueG'+gSn+'_1" name="optionValueG'+gSn+'_1" value="">'+
                    ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxG'+gSn+'_1" name="optionIsShowTextViewCKBoxG'+gSn+'_1" value=1 onchange="detectGTypeIsSelectShowTextViewCheckBox('+gSn+',1);">' +
                    '<input id="optionGSub-Tips'+gSn+'_1" type="hidden" name="optionGSub-Tips'+gSn+'_1" value="">' +
                    ' <button id="createButton-optionGSub-Tips'+gSn+'_1" onclick="return showCreateOptionTipsView(\'optionGSub-Tips'+gSn+'_1\')";>輸入提示</button>';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeGSubOption(\''+gSn+'_1\');">   x</a></br>';
            hiddenComponet = '<input type="hidden" id="g_sub_sn'+gSn+'" name="g_sub_sn'+gSn+'" value="1" />'
            divEnd = '</div>'+hiddenComponet+'</div></div>';
            resultAppend = appendForm+deleteOptionLink+divEnd;
            $("#type-G-column-div").append(resultAppend);
        };

    }

    function addGSubOption(inputGsn){
        gSub_Sn = $("#g_sub_sn"+inputGsn).val();
        gSub_Sn++;
        serialString = inputGsn+'_'+gSub_Sn;
        appendForm = '<div class="optionFormG-div" id="optionFormG_'+serialString+'-div">'+
            '選項'+gSub_Sn+'： '+'<input type="text" size="50" id="optionAreaG'+serialString+'" name="optionAreaG'+serialString+'">'+
            ' 選項值 <input type="text" size="3" id="optionValueG'+serialString+'" name="optionValueG'+serialString+'" value="">'+
            ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxG'+serialString+'" name="optionIsShowTextViewCKBoxG'+serialString+'" value=1 onchange="detectGTypeIsSelectShowTextViewCheckBox('+inputGsn+','+gSub_Sn+');">';
        deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeGSubOption(\''+serialString+'\');">   x</a></br>';
        tipsButton = '<input id="optionGSub-Tips'+serialString+'" type="hidden" name="optionGSub-Tips'+serialString+'" value="">' +
                     ' <button id="createButton-optionGSub-Tips'+serialString+'" onclick="return showCreateOptionTipsView(\'optionGSub-Tips'+serialString+'\')";>輸入提示</button>';
        divEnd = '</div>';
        resultAppend = appendForm+ tipsButton + deleteOptionLink + divEnd;
        $("#optionLogicG_"+inputGsn+"-div").append(resultAppend);
        $("#g_sub_sn"+inputGsn).val(gSub_Sn);
    }

	function removeOption(sn){
		$("#optionForm-div"+sn).remove();
        sSn--;
        mSn--;
		tSn--;
		pSn--;
	}

    function removeGMainOption(sn){
        $("#type-G-Area"+sn+"-div").remove();
        gSn--;
    }

    function removeGSubOption(snText){
        $("#optionFormG_"+snText+"-div").remove();
    }

    function submitCheck() {
		var validate = true;
		var msg = "";
	
		//判斷空白未填
		if($("#q_subjectNumber").val()==""){
			validate = false;
			msg = "請輸入題號";
		}else if($("#q_subject").val()==""){
			validate = false;
			msg = "請輸入標題";
		}else if($("#q_type").val()=="Empty"){
			validate = false;
			msg = "請設定題目類型";
		}
		
		//判斷選項空白
		var selectValue = $("#q_type").find(":selected").val();
		switch(selectValue){
			case 'FO':
				// var option = $("#optionArea").val();
				// var optionArray = new Array();			
				// option = option.replace(/([^>])\n/g, '$1<br/>\n');
				// optionArray = option.split('<br/>');
				// if (optionArray.length!=4){
					// validate = false;
					// msg = "選項應為4則";
				// }
				var option1 = $("#optionAreaFO1").val();
				var option2 = $("#optionAreaFO2").val();
				var option3 = $("#optionAreaFO3").val();
				var option4 = $("#optionAreaFO4").val();
				if (option1 == "" || option2 == "" || option3 == "" || option4 == ""){
					validate = false;
					msg = "請完整輸入4則選項";
				}
				break;
			case 'FI':
				// var option = $("#optionArea").val();
				// var optionArray = new Array();			
				// option = option.replace(/([^>])\n/g, '$1<br/>\n');
				// optionArray = option.split('<br/>');
				// if (optionArray.length!=5){
					// validate = false;
					// msg = "選項應為5則";
				// }
				var option1 = $("#optionAreaFI1").val();
				var option2 = $("#optionAreaFI2").val();
				var option3 = $("#optionAreaFI3").val();
				var option4 = $("#optionAreaFI4").val();
				var option5 = $("#optionAreaFI5").val();
				if (option1 == "" || option2 == "" || option3 == "" || option4 == "" || option5 == ""){
					validate = false;
					msg = "請完整輸入5則選項";
				}
				break;
            case 'S':
                for (i=1;i<=sSn;i++){
                    if($("#optionAreaS"+i).val()=="" || $("#optionValueS"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
                break;
            case 'M':
                for (i=1;i<=mSn;i++){
                    if($("#optionAreaM"+i).val()=="" || $("#optionValueM"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
                break;
			case 'T':
				for (i=1;i<=tSn;i++){
					if($("#optionAreaT"+i).val()==""){
						validate = false;
						msg = "請完整輸入選項"+i+"的描述";
					}
				}
				break;
			case 'P':
				for (i=1;i<=pSn;i++){
					if($("#optionAreaP"+i).val()=="" || $("#type-P-value-start"+i).val()=="" || $("#type-P-value-end"+i).val()=="" || $("#type-P-value-tick"+i).val()==""){
						validate = false;
						msg = "請完整輸入選項"+i+"，的描述、範圍與間隔";
					}	
				}
				
				break;
		
		}
		
		if (validate){
			$("#form").submit();	
		}else {
			$("#error_msg").fadeIn('medium');
			$("#error_msg").text(msg);
		}
		
		
		// document.write('test');
		// return false;
		
		return validate;
	}

    function checkOnlyNumber(o) {
        v=o.value.replace(/^\s+|\s+$/,''); // remove any whitespace
        if(o=='') {
            return;
        }
        v=v.substr(v.length-1);
        if(v.match(/\d/g)==null) {
            o.value=o.value.substr(0,o.value.length-1).replace(/^\s+|\s+$/,'');
        }
    }

    function checkDyPicker(surveyGuid,subjectGuid,subjectSn,answer) {
        if ($("#isDyPickerCheck"+subjectSn).attr("checked")){
            $('#type-P-value-end'+subjectSn).remove();
            $('#dyPickerSetting'+subjectSn).append(' <select id="dyPickerSelect'+subjectSn+'" name="dyPickerSelect'+subjectSn+'"></select>');
            $.ajax({
                //有2個值用'@'隔開
                url: '../surveys/getQuestionArrayJsonForAjaxForPTypeAdd/'+ surveyGuid ,
                data: {},
                error: function(xhr) {
                    alert('Ajax request 發生錯誤'+xhr);
                },
                success: function(response) {
                    var resultArray = jQuery.parseJSON(response);
                    $('#dyPickerSelect'+subjectSn)
                        .find('option')
                        .remove()
                        .end();
                    $.each(resultArray, function(i, object) {
                        $.each(object, function(property, value) {
                            if(answer == "") {

                                $('#dyPickerSelect'+subjectSn).append($('<option>', { value : value.QuestionSubjectNumber }).text(value.QuestionSubjectNumber+' - '+value.QuestionTitle));
                            }else {
                                $('#dyPickerSelect'+subjectSn).append($('<option>', { value : value.QuestionSubjectNumber }).text(value.QuestionSubjectNumber+' - '+value.QuestionTitle));

                                if(answer == value.optionValue){

                                    $('#dyPickerSelect'+subjectSn).children().attr("selected","true");
                                }
                            }

                        });
                    });
                }
            });
        }else {
            $('#dyPickerSelect'+subjectSn).remove();
            $('#dyPickerSetting'+subjectSn).append(' <input type="text" size="2" id="type-P-value-end'+subjectSn+'" name="type-P-value-end'+subjectSn+'">');
        }

    }
	
	</script>
<body>
<div id="dialog-form" title="輸入提示">
    <form>
        <fieldset>
            <label for="optionTips">Tips</label>
            <!--            <input type="text" name="optionTips" id="optionTips" class="text ui-widget-content ui-corner-all">-->
            <textarea id="optionTips" type="text" name="optionTips" cols="62" rows="5"></textarea>
        </fieldset>
    </form>
</div>
		<header id="top">
		<div class="wrapper">
			<!-- Title/Logo - can use text instead of image -->
			
			<div id="title">
<!-- 				<?php echo $title; ?> -->
				<img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" />
				</div>
			<!-- Top navigation -->
			<div id="topnav">
				<!-- <a href="#"><img class="avatar" SRC="<?php echo base_url(); ?>style/admin/img/user_32.png" alt="" /></a> -->
				您好      <b><?php echo $username;?></b>
<!-- 				<span>|</span> <a href="#">Settings</a> -->
				<span>|</span> <a href="/surveyapp/webLogout">登出</a><br />
<!-- 				<small>You have <a href="#" class="high"><b>1</b> new message!</a></small> -->
			</div>
			<!-- End of Top navigation -->
			<!-- Main navigation -->
			<nav id="menu">
				<ul class="sf-menu">
					<li class="current"><a HREF="/surveyapp/surveys">調查管理</a></li>
				</ul>
			</nav>
		</div>
	</header>
		<!-- End of Header -->
	<!-- Page title -->
	<div id="pagetitle">
		<div class="wrapper">
			<h1><?php echo $headerTitle; ?></h1>			
			<!-- Quick search box -->
<!-- 			<form action="" method="get"><input class="" type="text" id="q" name="q" /></form> -->

		</div>
	</div>
		<!-- End of Page title -->
    
    <div id="container">
    	<div class="content">
	    	<div id="leftColumn">
	    		<ul class="survey-left-menu">
	    			<li>
	    				<a href="/surveyapp/surveys/editorFull/<?php echo $surveyGuid;?>">回上頁</a> 
	    			</li>
	    		</ul>
			</div>
	    	<div id="rightColumn">
				<div id="error_msg">
		
				</div>
                <div id="error_msg">

                </div>
                <form id="form" name ="form" onsubmit="return submitCheck();" action="/surveyapp/surveys/addNewQuestion/<?php echo $surveyGuid;?>" method="post">
                    <!-- 		<form id="form" name ="form" onsubmit="return submitQ();" method="post"> -->
                    <div id="form_open">
<!--                        <p>題號：<input id="q_subjectNumber" type="text" name="q_subjectNumber" size="10" onkeyup="value=value.replace(/[^a-zA-Z0-9]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^a-zA-Z0-9]/g,''))"></p>-->
                        <p>題號：<input id="q_subjectNumber" type="text" name="q_subjectNumber" size="10"></p>
                        <p>問題標題：<input id="q_subject" type="text" name="q_subject" size = "100"></p>
                        <p>問題小幫手：<textarea id="q_tipsText" type="text" name="q_tipsText" cols="100" rows="5"></textarea></p>
                    </div>
                    <div id="option-div">
                        <p>問題類型：
                        <select id="q_type" name="q_type" width="" onchange="detectType();">
                            <option value="Empty" selected>--選擇問題類型--</option>
                            <option value="S">單選題</option>
                            <option value="M">多選題</option>
                            <option value="T">文字輸入題</option>
                            <option value="FO">四分題</option>
                            <option value="FI">五分題</option>
                            <option value="P">下拉式清單</option>
                            <option value="G">群組題</option>
                        </select>
                        </p>
                        <div id="type-S-div">
                            <p>App欄位數：
                                <input type="radio" name="q_optionOutputFormS" value="Vertical" checked>1欄
                                <input type="radio" name="q_optionOutputFormS" value="Horizontal" >2欄</p>
                            <p>答案選擇:在個別行中輸入每一個選擇。</p>
                            <!--			<textarea rows="10" cols="50" id="optionAreaS" name="optionAreaS" Wrap="Physical"></textarea>-->
                        </div>
                        <div id="type-M-div">
                            <p>App欄位數：
                                <input type="radio" name="q_optionOutputFormM" value="Vertical" checked>1欄
                                <input type="radio" name="q_optionOutputFormM" value="Horizontal" >2欄</p>
                            <p>答案選擇:在個別行中輸入每一個選擇。</p>
                            <!--			<textarea rows="10" cols="50" id="optionAreaM" name="optionAreaM" Wrap="Physical"></textarea>-->
                        </div>
                        <div id="type-T-div">
                            <p>填入的答案描述：ex:你的地址、你的電話號碼。</p>
                        </div>
                        <div id="type-FO-div">
                            <p>答案選擇:</p>
                            <input type="text" id="optionAreaFO1" name="optionAreaFO1">
                            <input id="optionFO-Tips1" type="hidden" name="optionFO-Tips1" value="">
                            <button id="createButton-optionFO-Tips1" name="button-optionFO-Tips1" onclick="return showCreateOptionTipsView('optionFO-Tips1');">輸入提示</button><br>

                            <input type="text" id="optionAreaFO2" name="optionAreaFO2">
                            <input id="optionFO-Tips2" type="hidden" name="optionFO-Tips" value="">
                            <button id="createButton-optionFO-Tips2" name="button-optionFO-Tips2" onclick="return showCreateOptionTipsView('optionFO-Tips2');">輸入提示</button><br>

                            <input type="text" id="optionAreaFO3" name="optionAreaFO3">
                            <input id="optionFO-Tips3" type="hidden" name="optionFO-Tips3" value="">
                            <button id="createButton-optionFO-Tips3" name="button-optionFO-Tips3" onclick="return showCreateOptionTipsView('optionFO-Tips3');">輸入提示</button><br>

                            <input type="text" id="optionAreaFO4" name="optionAreaFO4">
                            <input id="optionFO-Tips4" type="hidden" name="optionFO-Tips4" value="">
                            <button id="createButton-optionFO-Tips4" name="button-optionFO-Tips4" onclick="return showCreateOptionTipsView('optionFO-Tips4');">輸入提示</button><br>

                        </div>
                        <div id="type-FI-div">
                            <p>答案選擇:</p>
                            <input type="text" id="optionAreaFI1" name="optionAreaFI1">
                            <input id="optionFI-Tips1" type="hidden" name="optionFI-Tips1" value="">
                            <button id="createButton-optionFI-Tips1" name="button-optionFI-Tips1" onclick="return showCreateOptionTipsView('optionFI-Tips1');">輸入提示</button><br>

                            <input type="text" id="optionAreaFI2" name="optionAreaFI2">
                            <input id="optionFI-Tips2" type="hidden" name="optionFI-Tips2" value="">
                            <button id="createButton-optionFI-Tips2" name="button-optionFI-Tips2" onclick="return showCreateOptionTipsView('optionFI-Tips2');">輸入提示</button><br>

                            <input type="text" id="optionAreaFI3" name="optionAreaFI3">
                            <input id="optionFI-Tips3" type="hidden" name="optionFI-Tips3" value="">
                            <button id="createButton-optionFI-Tips3" name="button-optionFI-Tips3" onclick="return showCreateOptionTipsView('optionFI-Tips3');">輸入提示</button><br>

                            <input type="text" id="optionAreaFI4" name="optionAreaFI4">
                            <input id="optionFI-Tips4" type="hidden" name="optionFI-Tips4" value="">
                            <button id="createButton-optionFI-Tips4" name="button-optionFI-Tips4" onclick="return showCreateOptionTipsView('optionFI-Tips4');">輸入提示</button><br>

                            <input type="text" id="optionAreaFI5" name="optionAreaFI5">
                            <input id="optionFI-Tips5" type="hidden" name="optionFI-Tips5" value="">
                            <button id="createButton-optionFI-Tips5" name="button-optionFI-Tips5" onclick="return showCreateOptionTipsView('optionFI-Tips5');">輸入提示</button><br>

                        </div>
                        <div id="type-P-div">
                            <input type="hidden" id="p_surveyGuid" name="p_surveyGuid" value="<?php echo $surveyGuid ?>" />
                            <p>填入的選項的描述，範圍與間隔。例如：如年齡，1,2,3~59,60，在範圍的輸入框填1與60，在階層的輸入框填1。</p>
                        </div>

                        <div id="type-G-div">
                            <input type="hidden" id="g_sn" name="g_sn" value="1" />
                            <div id="type-G-column-div">
                            </div>
                        </div>

                    </div>
                    <table>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>

                    </table>
                    <div class="button-contain">
                        <input type="submit" value="儲存"/><!--按下送出後，會導去survey_question_editor_full.php這支檔案的submitCheck() -->
                    </div>
                    <div class="button-contain">
<!--                        <input type="BUTTON" value="取消" name="cancel" onClick="cancelCreate();"/>-->
                    </div>
                </form>

			</div>
	    	
    	</div>
    </div>


	
</body>
</html>
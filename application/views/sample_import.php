<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>style/tableStyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css" />
<!-- Favicons --> 
<link rel="shortcut icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="apple-touch-icon" HREF="<?php echo base_url(); ?>style/admin/img/favicons/apple.png" />
<!-- Main Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/style.css" type="text/css" />
<!-- Colour Schemes
Default colour scheme is blue. Uncomment prefered stylesheet to use it.
<link rel="stylesheet" href="css/brown.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/gray.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/green.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/pink.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/red.css" type="text/css" media="screen" />
-->
<!-- Your Custom Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/custom.css" type="text/css" />
<!--swfobject - needed only if you require <video> tag support for older browsers -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/swfobject.js"></script>
<!-- jQuery with plugins -->
<!--<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-1.4.2.min.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Could be loaded remotely from Google CDN : <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.tabs.min.js"></script>
<!-- jQuery tooltips -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.tipTip.min.js"></script>
<!-- Superfish navigation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.superfish.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.supersubs.min.js"></script>
<!-- jQuery form validation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.validate_pack.js"></script>
<!-- jQuery popup box -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.nyroModal.pack.js"></script>
<!-- jQuery graph plugins -->
<!--[if IE]><script type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>style/calendar.js"></script>
<!-- 匯入訪員與樣本 -->
<script>
    var CI = {'base_url': '<?php echo base_url(); ?>'};
</script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/import.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>style/uploadify/css/uploadify.css" type="text/css" />
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/uploadify/js/jquery.uploadify.js"></script>



<!-- Internet Explorer Fixes --> 
<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
<script src="js/html5.js"></script>
<![endif]-->
<!--Upgrade MSIE5.5-7 to be compatible with MSIE8: http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE8.js -->
<!--[if lt IE 8]>
<script src="js/IE8.js"></script>
<![endif]-->

</head>
<body>
        <!-- Header -->
    <header id="top">
        <div class="wrapper">
            <!-- Title/Logo - can use text instead of image -->
            <div id="title">
<!--                <?php echo $title; ?> -->
<!--                <img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" alt="Administry" /><!--<span>Administry</span> demo-->
                <img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" />
            </div>
            <!-- Top navigation -->
            <div id="topnav">
                <!-- <a href="#"><img class="avatar" SRC="<?php echo base_url(); ?>style/admin/img/user_32.png" alt="" /></a> -->
                您好      <b><?php echo $username;?></b>
<!--                <span>|</span> <a href="#">Settings</a> -->
                <span>|</span> <a href="/surveyapp/webLogout">登出</a><br />
<!--                <small>You have <a href="#" class="high"><b>1</b> new message!</a></small> -->
            </div>
            <!-- End of Top navigation -->
            <!-- Main navigation -->
            <nav id="menu">
                <ul class="sf-menu">
                    <li class="current"><a HREF="/surveyapp/surveys">調查管理</a></li>
<!--                    <li class="current"><a HREF="/surveyapp/samples">樣本列表</a>
                        <ul>
                            <li>
                                <a HREF="/surveyapp/samples/add">新增樣本</a>
                            </li>
                        </ul>
                    </li>
                    <li class="current"><a HREF="/surveyapp/interviewers">訪問員列表</a>
                        <ul>
                            <li>
                                <a HREF="/surveyapp/interviewers/add">新增訪問員</a>
                            </li>
                        </ul>
                    </li> --><!--                   <li>
                        <a HREF="styles.html">Styles</a>
                        <ul>
                            <li>
                                <a HREF="styles.html">Basic Styles</a>
                            </li>
                            <li>
                                <a href="#">Sample Pages...</a>
                                <ul>
                                    <li><a HREF="samples-files.html">Files</a></li>
                                    <li><a HREF="samples-products.html">Products</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a HREF="tables.html">Tables</a></li>
                    <li><a HREF="forms.html">Forms</a></li> 
                    <li><a HREF="graphs.html">Graphs</a></li>    -->
                </ul>
            </nav>
            <!-- End of Main navigation -->
            <!-- Aside links -->
<!--            <aside><b>English</b> &middot; <a href="#">Spanish</a> &middot; <a href="#">German</a></aside> -->
            <!-- End of Aside links -->
        </div>
    </header>
    <!-- End of Header -->
    <!-- Page title -->
    <div id="pagetitle">
        <div class="wrapper">
            
            <h1><?php echo $headerTitle; ?></h1>
            <!-- Quick search box -->
<!--            <form action="" method="get"><input class="" type="text" id="q" name="q" /></form> -->
        </div>
    </div>
    <div id="container">
    <div class="content">
<!--        <h1><?php echo $title; ?></h1> -->
        <div class="message"><?php echo $message; ?></div>

        <form method="post" >
        <div class="data">
        <table>
            <tr>
                <td>
                    ，
                    欄位對照：<span class='red'>紅色</span>必填，其餘選填<br/>
                    "<span class='red'>訪員姓名</span>","<span class='red'>訪員帳號</span>","<span class='red'>訪員密碼</span>","<span class='red'>樣本分群名稱</span>","<span class='red'>樣本姓名</span>","<span class='red'>性別</span>","生日","郵遞區號","地址","<span class='red'>排序1</span>","<span class='red'>排序2</span>"<br/>
                    "<span class='red'>interViewerName</span>","<span class='red'>interViewerAcc</span>","<span class='red'>interViewerPwd</span>","<span class='red'>sampleGroupName</span>","<span class='red'>sampleName</span>","<span class='red'>gender</span>","birthday","zipcode","address","<span class='red'>mainpriority</span>","<span class='red'>subpriority</span>"<br/>
                    <p>
                        注意事項:<br/>
                        interViewerName 只能擁有一個 sampleGroupName，sampleGroupName 可擁有多個 sampleName。<br/>
                        訪員姓名(字串),<br/>訪員帳號(字串，<span class='red'>不可重複</span>),<br/>訪員密碼(字串),<br/>樣本分群名稱(字串),<br/>樣本姓名(字串，<span class='red'>不可重複</span>),<br/>性別(整數 1=男;0=女;),<br/>生日(字串 ex:民國年月 650324),<br/>郵遞區號(字串),<br/>地址(字串),<br/>排序1(整數),<br/>排序2(整數)<br/>
                        ex:<br/>
                    </p>
                    <span class='red'>＊＊<a href='http://capi.nctu.edu.tw/video/?k=1&autostart=true' target="_blank">csv處理教學影片</a>＊＊</span>
                    <p>
<pre class='code'>
"interViewerName","interViewerAcc","interViewerPwd","sampleGroupName","sampleName","gender","birthday","zipcode","address","mainpriority","subpriority"
"A018","A018","A018","福安國中","福安國中001","1","","","","1","1"
"A018","A018","A018","福安國中","福安國中002","1","","","","1","2"
"A018","A018","A018","福安國中","福安國中003","1","","","","1","3"
"A018","A018","A018","福安國中","福安國中004","1","","","","1","4"
"A018","A018","A018","福安國中","福安國中005","1","","","","1","5"
"A018","A018","A018","福安國中","福安國中006","1","","","","1","6"
"A018","A018","A018","福安國中","福安國中007","1","","","","1","7"
"A018","A018","A018","福安國中","福安國中008","1","","","","1","8"
"A018","A018","A018","福安國中","福安國中009","1","","","","1","9"
"A018","A018","A018","福安國中","福安國中010","1","","","","1","10"
"A019","A019","A019","金華國中","金華國中001","1","","","","2","1"
"A019","A019","A019","金華國中","金華國中002","1","","","","2","2"
"A019","A019","A019","金華國中","金華國中003","1","","","","2","3"
"A019","A019","A019","金華國中","金華國中004","1","","","","2","4"
"A019","A019","A019","金華國中","金華國中005","1","","","","2","5"
"A019","A019","A019","金華國中","金華國中006","1","","","","2","6"
"A019","A019","A019","金華國中","金華國中007","1","","","","2","7"
"A019","A019","A019","金華國中","金華國中008","1","","","","2","8"
"A019","A019","A019","金華國中","金華國中009","1","","","","2","9"
"A019","A019","A019","金華國中","金華國中010","1","","","","2","10"

</pre>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <a href='<?php echo base_url("csv/sample.csv");?>'>csv範例檔案下載</a>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="file" id="upload_csv" />
                </td>
            </tr>
            <tr>
                <td>
                    <div class='result_msg'></div>
                </td>
            </tr>
        </table>
        </div>
        </form>
        <br />
        <?php  echo $link_back; ?>
<!--        <a href="javascript:history.go(-1);">回上一頁</a>-->
    </div>
    </div>
</body>
</html>
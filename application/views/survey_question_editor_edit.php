<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>style/tableStyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>style/calendar.css" rel="stylesheet" type="text/css" />
<!-- Favicons --> 
<link rel="shortcut icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="icon" type="image/png" HREF="<?php echo base_url(); ?>style/admin/img/favicons/favicon.png"/>
<link rel="apple-touch-icon" HREF="<?php echo base_url(); ?>style/admin/img/favicons/apple.png" />
<!-- Main Stylesheet --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/style.css" type="text/css" />
<!-- Colour Schemes
Default colour scheme is blue. Uncomment prefered stylesheet to use it.
<link rel="stylesheet" href="css/brown.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/gray.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/green.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/pink.css" type="text/css" media="screen" />  
<link rel="stylesheet" href="css/red.css" type="text/css" media="screen" />
-->
<!-- Your Custom Stylesheet -->

<link rel="stylesheet" href="<?php echo base_url(); ?>style/admin/css/custom.css" type="text/css" />
<!--swfobject - needed only if you require <video> tag support for older browsers -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/swfobject.js"></script>
<!-- jQuery with plugins -->
<!--123 <script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-1.4.2.min.js"></script> -->

<script type="text/javascript" SRC="<?php echo base_url(); ?>style/fancybox2/lib/jquery-1.7.2.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery-ui-1.7.3.custom.min.js"></script>

<!-- Could be loaded remotely from Google CDN : <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.ui.tabs.min.js"></script>
<!-- jQuery tooltips -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.tipTip.min.js"></script>
<!-- Superfish navigation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.superfish.min.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.supersubs.min.js"></script>
<!-- jQuery form validation -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.validate_pack.js"></script>
<!-- jQuery popup box -->
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/admin/js/jquery.nyroModal.pack.js"></script>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/questionView_Edit_JS.js"></script>
<!-- jQuery graph plugins -->

<!-- OptionTips plugins 2014/04/25-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/ui-lightness/jquery-ui-1.10.4.custom.min.css"/>
<script type="text/javascript" SRC="<?php echo base_url(); ?>style/optionTips_Input_JS.js"></script>
<!-- OptionTips plugins 2014/04/25-->

<!-- <script type="text/javascript" src="<?php echo base_url(); ?>style/editorPopup.js"></script> -->
	
<!-- Add mousewheel plugin (this is optional) -->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--style/fancybox2/lib/jquery.mousewheel-3.0.6.pack.js"></script>-->

<!-- Add fancyBox main JS and CSS files -->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--style/fancybox2/source/jquery.fancybox.pack.js?v=2.0.6"></script>-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo base_url(); ?><!--style/fancybox2/source/jquery.fancybox.css?v=2.0.6" media="screen" />-->
<!---->
<!-- Add Button helper (this is optional) -->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo base_url(); ?><!--style/fancybox2/source/helpers/jquery.fancybox-buttons.css?v=1.0.2" />-->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--style/fancybox2/source/helpers/jquery.fancybox-buttons.js?v=1.0.2"></script>-->
<!-- Add Thumbnail helper (this is optional) -->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo base_url(); ?><!--style/fancybox2/source/helpers/jquery.fancybox-thumbs.css?v=1.0.2" />-->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--style/fancybox2/source/helpers/jquery.fancybox-thumbs.js?v=1.0.2"></script>-->

<!-- Add Media helper (this is optional) -->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--style/fancybox2/source/helpers/jquery.fancybox-media.js?v=1.0.0"></script>-->

<!-- Internet Explorer Fixes --> 
<!--[if IE]>
<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
<script src="js/html5.js"></script>
<![endif]-->
<!--Upgrade MSIE5.5-7 to be compatible with MSIE8: http://ie7-js.googlecode.com/svn/version/2.1(beta3)/IE8.js -->
<!--[if lt IE 8]>
<script src="js/IE8.js"></script>

<![endif]-->
	<style>
		body { font-size: 62.5%; }
		label, input { display:nowrap; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-top:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
        div#form_open{width:760px;}
		div#option-div,div#type-S-div,div#type-M-div,div#type-T-div,div#type-P-div,div#type-G-div,div#type-FO-div,div#type-FI-div{display:none;width:760px;}
		div#passLogic-div,div#pass-div {display:none;width:500px;}
		div#error_msg{display:none;width:500px;color:red;}
		div#users-contain { width: 550px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		a.deleteOptionLink:link {color: #BBBBBB; text-decoration:none;}
		a.deleteOptionLink:hover {color: #DD0000; text-decoration:none;}
		a.deleteOptionLink:visited {color: #BBBBBB; text-decoration:none;} 

		#rightColumn {
		/*	background-color:#ddd;*/
		  	float:right;
		  	width:790px;     /* 最小行高 */
		  	text-align: left;
		}

	</style>
<body>
<div id="dialog-form" title="輸入提示">
    <form>
        <fieldset>
            <label for="optionTips">Tips</label>
<!--            <input type="text" name="optionTips" id="optionTips" class="text ui-widget-content ui-corner-all">-->
            <textarea id="optionTips" type="text" name="optionTips" cols="62" rows="5"></textarea>
        </fieldset>
    </form>
</div>
		<header id="top">
		<div class="wrapper">
			<!-- Title/Logo - can use text instead of image -->
			
			<div id="title">
<!-- 				<?php echo $title; ?> -->
				<img SRC="<?php echo base_url(); ?>style/admin/img/logo.png" />
				</div>
			<!-- Top navigation -->
			<div id="topnav">
				<!-- <a href="#"><img class="avatar" SRC="<?php echo base_url(); ?>style/admin/img/user_32.png" alt="" /></a> -->
				您好      <b><?php echo $username;?></b>
<!-- 				<span>|</span> <a href="#">Settings</a> -->
				<span>|</span> <a href="/surveyapp/webLogout">登出</a><br />
<!-- 				<small>You have <a href="#" class="high"><b>1</b> new message!</a></small> -->
			</div>
			<!-- End of Top navigation -->
			<!-- Main navigation -->
			<nav id="menu">
				<ul class="sf-menu">
					<li class="current"><a HREF="/surveyapp/surveys">調查管理</a></li>
				</ul>
			</nav>
			<!-- End of Main navigation -->
			<!-- Aside links -->
<!-- 			<aside><b>English</b> &middot; <a href="#">Spanish</a> &middot; <a href="#">German</a></aside> -->
			<!-- End of Aside links -->
		</div>
	</header>
		<!-- End of Header -->
	<!-- Page title -->
	<div id="pagetitle">
		<div class="wrapper">
			<h1><?php echo $headerTitle; ?></h1>			
			<!-- Quick search box -->
		</div>
	</div>
		<!-- End of Page title -->
    
    <div id="container">
    	<div class="content">
	    	<div id="leftColumn">
	    		<ul class="survey-left-menu">
	    			<li>
	    				<a href="/surveyapp/surveys/editorFull/<?php echo $surveyGuid;?>">回上頁</a> 
	    			</li>
	    			<li>
<!-- 	    				<a href="<?php echo $deleteSurveyAction; ?>" onClick="return confirm('確定刪除此份調查?');">刪除</a> -->
	    			
	    				
	    			</li>
	    		</ul>
			</div>
	    	<div id="rightColumn">
				<div id="error_msg">
		
				</div>
				<form id="form" name ="form" onsubmit="return submitCheck();" action="/surveyapp/surveys/updateQuestion/<?php echo $questionObject->getGuid();?>" method="post">
				<!-- 		<form id="form" name ="form" onsubmit="return submitQ();" method="post"> -->
					<div id="form_open">
                    <p><H2>第<?php echo $questionObject->getPriority();?>題</H2></p>
                    <input type="hidden" id="priority" name="priority" value="<?php echo $questionObject->getPriority(); ?>" />
					<p>題號：<input id="q_subjectNumber" type="text" name="q_subjectNumber" size="10" value = "<?php echo $questionObject->getSubjectNumber() ?>"></p>
<!--                    <p>題號：<input id="q_subjectNumber" type="text" name="q_subjectNumber" size="10" value = "--><?php ////echo $questionObject->getSubjectNumber() ?><!--" onkeyup="value=value.replace(/[^a-zA-Z0-9]/g,'') "onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^a-zA-Z0-9]/g,''))"></p>-->
					<p>問題標題：<input id="q_subject" type="text" name="q_subject" size="100" value="<?php echo $questionObject->getSubject() ?>"  ></p>
                    <p>問題小幫手：<textarea id="q_tipsText" type="text" name="q_tipsText" cols="100" rows="5"><?php echo $tipsObject->getTipsText() ?></textarea></p>

					<?php //echo $questionType_chi; ?>

<!--                    Todo Change QuestionType-->
<!--					<input type="hidden" id="q_type" name="q_type" value="--><?php //echo $questionObject->getType(); ?><!--" />-->




                    </div>
					<div id="option-div">
                    <p>問題類型：
                    <input type="hidden" id="q_o_type" name="q_o_type" value="<?php echo $questionObject->getType(); ?>" />
                    <select id="q_type" name="q_type" width="" onchange="detectType();">
                        <option value="Empty">--選擇問題類型--</option>
                        <option value="S" <?php if($questionObject->getType()=='S'){ ?> selected<?php } ?>>單選題</option>
                        <option value="M" <?php if($questionObject->getType()=='M'){ ?> selected<?php } ?>>多選題</option>
                        <option value="T" <?php if($questionObject->getType()=='T'){ ?> selected<?php } ?>>文字輸入題</option>
                        <option value="FO" <?php if($questionObject->getType()=='FO'){ ?> selected<?php } ?>>四分題</option>
                        <option value="FI" <?php if($questionObject->getType()=='FI'){ ?> selected<?php } ?>>五分題</option>
                        <option value="P" <?php if($questionObject->getType()=='P'){ ?> selected<?php } ?>>下拉式清單</option>
                        <option value="G" <?php if($questionObject->getType()=='G'){ ?> selected<?php } ?>>群組題</option>
                    </select>
                    </p>
						<div id="type-S-div">
                            <input type="hidden" id="s_sn" name="s_sn" value="<?php echo count($optionObjectArray) ?>" />
                            <p>App欄位數：
                            <input type="radio" name="q_optionOutputFormS" value="Vertical" <?php if ($questionObject->getOptionOutputForm()=='Vertical'){?>checked<?php }  ?>>1欄
                            <input type="radio" name="q_optionOutputFormS" value="Horizontal" <?php if ($questionObject->getOptionOutputForm()=='Horizontal' || $questionObject->getOptionOutputForm()==''){?>checked<?php }  ?>>2欄</p>
							<p>答案選擇:在個別行中輸入每一個選擇。</p>
                            <div id="type-S-column-div">
                            <?php
                                if (count($optionObjectArray)>0 && $questionObject->getType() == 'S') {
                                    for ($i=0; $i<count($optionObjectArray); $i++){
                                            $tempOption = new option_model();
                                            $tempOption = $optionObjectArray[$i];
                            ?>
                                <div class="optionForm-div" id="optionForm-div<?php echo $i+1?>">選項<?php echo $i+1?><input type="text" size="60" id="optionAreaS<?php echo $i+1?>" name="optionAreaS<?php echo $i+1?>" value="<?php echo $tempOption->getTitle(); ?>">
                                選項值<input type="text" size="3" id="optionValueS<?php echo $i+1?>" name="optionValueS<?php echo $i+1?>" value="<?php echo $tempOption->getOptionValue(); ?>">
                                顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxS<?php echo $i+1?>" name="optionIsShowTextViewCKBoxS<?php echo $i+1?>" value="1" onchange="detectIsSelectShowTextViewCheckBox('S',<?php echo $i+1?>);"
                                            <?php if($tempOption->getIsShowTextView()=='1'){ ?>checked<?php }?>>
                                   <?php if($i+1!=1){?>
                                   <a class="deleteOptionLink" href="javascript:removeOption(<?php echo $i+1?>);">   x</a>
                                   <?php }?>
                                    <input id="optionS-Tips<?php echo $i+1?>" type="hidden" name="optionS-Tips<?php echo $i+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                    <?php
                                        $buttonText = "";
                                        if($tempOption->getOptionTips()!=""){
                                            $buttonText = "有提示✔";
                                        }else {
                                            $buttonText = "輸入提示";
                                        }
                                    ?>
                                    <button id="createButton-optionS-Tips<?php echo $i+1?>" name="button-optionS-Tips<?php echo $i+1?>" onclick="return showCreateOptionTipsView('optionS-Tips<?php echo $i+1?>');"><?php echo $buttonText;?></button>


                                </div>

                            <?php }} ?>
							</div><input type="BUTTON" value="新增" id="addSButton" name="addSButton" onClick="addSOption();">
						</div>
						<div id="type-M-div">
                            <input type="hidden" id="m_sn" name="m_sn" value="<?php echo count($optionObjectArray) ?>" />
                            <p>App欄位數：
                                <input type="radio" name="q_optionOutputFormM" value="Vertical" <?php if ($questionObject->getOptionOutputForm()=='Vertical' ){?>checked<?php }  ?>>1欄
                                <input type="radio" name="q_optionOutputFormM" value="Horizontal" <?php if ($questionObject->getOptionOutputForm()=='Horizontal' || $questionObject->getOptionOutputForm()==''){?>checked<?php }  ?>>2欄</p>
							<p>答案選擇:在個別行中輸入每一個選擇。</p>
                            <div id="type-M-column-div">
                                <?php
                                if (count($optionObjectArray)>0 && $questionObject->getType() == 'M') {
                                    for ($i=0; $i<count($optionObjectArray); $i++){
                                        $tempOption = new option_model();
                                        $tempOption = $optionObjectArray[$i];
                                        ?>
                                        <div class="optionForm-div" id="optionForm-div<?php echo $i+1?>">選項<?php echo $i+1?><input type="text" size="60" id="optionAreaM<?php echo $i+1?>" name="optionAreaM<?php echo $i+1?>" value="<?php echo $tempOption->getTitle(); ?>">
                                            選項值<input type="text" size="3" id="optionValueM<?php echo $i+1?>" name="optionValueM<?php echo $i+1?>" value="<?php echo $tempOption->getOptionValue(); ?>">
                                            顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxM<?php echo $i+1?>" name="optionIsShowTextViewCKBoxM<?php echo $i+1?>" value="1" onchange="detectIsSelectShowTextViewCheckBox('M',<?php echo $i+1?>);"
                                                <?php if($tempOption->getIsShowTextView()=='1'){ ?>checked<?php }?>>
                                            <?php if($i+1!=1){?>
                                                <a class="deleteOptionLink" href="javascript:removeOption(<?php echo $i+1?>);">   x</a>
                                                <?php }?>

                                            <input id="optionM-Tips<?php echo $i+1?>" type="hidden" name="optionM-Tips<?php echo $i+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                            <?php
                                            $buttonText = "";
                                            if($tempOption->getOptionTips()!=""){
                                                $buttonText = "有提示✔";
                                            }else {
                                                $buttonText = "輸入提示";
                                            }
                                            ?>
                                            <button id="createButton-optionM-Tips<?php echo $i+1?>" name="button-optionM-Tips<?php echo $i+1?>" onclick="return showCreateOptionTipsView('optionM-Tips<?php echo $i+1?>');"><?php echo $buttonText;?></button>
                                        </div>
                                        <?php }} ?>
                            </div><input type="BUTTON" value="新增" id="addMButton" name="addMButton" onClick="addMOption();">
						</div>
						<div id="type-T-div">
							<input type="hidden" id="t_sn" name="t_sn" value="<?php echo count($optionObjectArray) ?>" />
							<p>填入的答案描述：ex:你的地址、你的電話號碼。</p>
							<div id="type-T-column-div">
							<?php
							
								if (count($optionObjectArray)>0 && $questionObject->getType() == 'T') {
									for ($i=0; $i < count($optionObjectArray); $i++) { 
										$tempOption = new option_model();
										$tempOption = $optionObjectArray[$i];										
							?>
								<div class="optionForm-div" id="optionForm-div<?php echo $i+1?>">
									選項<?php echo $i+1?><input type="text" size="60" id="optionAreaT<?php echo $i+1?>" name="optionAreaT<?php echo $i+1?>" value="<?php echo $tempOption->getTitle(); ?>">
                                    答案描述位置在<select id="optionTextViewPosition<?php echo $i+1?>" name="optionTextViewPosition<?php echo $i+1?>">
                                        <?php if($tempOption->getTextViewPosition()=="front"){ ?>
                                            <option value="back">前</option><option value="front" selected="selected">後</option></select>
                                        <?php }else ?>
                                            <option value="back" selected="selected">前</option><option value="front">後</option></select>
									<?php if($i+1!=1){?>
									<a class="deleteOptionLink" href="javascript:removeOption(<?php echo $i+1?>);">   x</a>

									<?php }?>
                                    <input id="optionT-Tips<?php echo $i+1?>" type="hidden" name="optionT-Tips<?php echo $i+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                    <?php
                                    $buttonText = "";
                                    if($tempOption->getOptionTips()!=""){
                                        $buttonText = "有提示✔";
                                    }else {
                                        $buttonText = "輸入提示";
                                    }
                                    ?>
                                    <button id="createButton-optionT-Tips<?php echo $i+1?>" name="button-optionT-Tips<?php echo $i+1?>" onclick="return showCreateOptionTipsView('optionT-Tips<?php echo $i+1?>');"><?php echo $buttonText;?></button>
                                </div>
							<?php }} ?>
							</div><input type="BUTTON" value="新增" id="addTButton" name="addTButton" onClick="addTOption();">
						</div>

						<div id="type-FO-div">
                            <div id="type-FO-column-div">
							<p>答案選擇:</p>
							<?php
								if (count($optionObjectArray) == 4 && $questionObject->getType() == 'FO') {
                                    for ($foIndex=0;$foIndex<count($optionObjectArray);$foIndex++){
                                        $tempOption = $optionObjectArray[$foIndex];
                                    ?>
                                        <input type="text" id="optionAreaFO<?php echo $foIndex+1;?>" name="optionAreaFO<?php echo $foIndex+1;?>" value="<?php  echo $tempOption->getTitle(); ?>">
                                        <input id="optionFO-Tips<?php echo $foIndex+1;?>" type="hidden" name="optionFO-Tips<?php echo $foIndex+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                        <?php
                                        $buttonText = "";
                                        if($tempOption->getOptionTips()!=""){
                                            $buttonText = "有提示✔";
                                        }else {
                                            $buttonText = "輸入提示";
                                        }
                                        ?>
                                        <button id="createButton-optionFO-Tips<?php echo $foIndex+1?>" name="button-optionFO-Tips<?php echo $foIndex+1?>" onclick="return showCreateOptionTipsView('optionFO-Tips<?php echo $foIndex+1?>');"><?php echo $buttonText;?></button><br>

                                <?php }
							?>
							<?php  } ?>
                            </div>
						</div>
						<div id="type-FI-div">
                            <div id="type-FI-column-div">
							<p>答案選擇:</p>
							<?php
								if (count($optionObjectArray) == 5 && $questionObject->getType() == 'FI') {
                                    for ($fiIndex=0;$fiIndex<count($optionObjectArray);$fiIndex++){
                                        $tempOption = $optionObjectArray[$fiIndex];
                                        ?>
                                        <input type="text" id="optionAreaFI<?php echo $fiIndex+1;?>" name="optionAreaFI<?php echo $fiIndex+1;?>" value="<?php  echo $tempOption->getTitle(); ?>">
                                        <input id="optionFI-Tips<?php echo $fiIndex+1;?>" type="hidden" name="optionFI-Tips<?php echo $fiIndex+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                        <?php
                                        $buttonText = "";
                                        if($tempOption->getOptionTips()!=""){
                                            $buttonText = "有提示✔";
                                        }else {
                                            $buttonText = "輸入提示";
                                        }
                                        ?>
                                        <button id="createButton-optionFI-Tips<?php echo $fiIndex+1?>" name="button-optionFI-Tips<?php echo $fiIndex+1?>" onclick="return showCreateOptionTipsView('optionFI-Tips<?php echo $fiIndex+1?>');"><?php echo $buttonText;?></button><br>

                                    <?php }
                                    ?>
							<?php } ?>
                            </div>
						</div>
						<div id="type-P-div">
							<input type="hidden" id="p_sn" name="p_sn" value="<?php echo count($optionObjectArray) ?>" />
                            <input type="hidden" id="p_surveyGuid" name="p_surveyGuid" value="<?php echo $surveyGuid ?>" />
                            <input type="hidden" id="p_subjectGuid" name="p_subjectGuid" value="<?php echo $questionObject->getGuid() ?>" />
							<div id="type-P-column-div">
							<?php
								if (count($optionObjectArray)>0 && $questionObject->getType() == 'P') {
									for ($i=0;$i<count($optionObjectArray);$i++){
										$tempOption = new option_model();
										$tempOption = $optionObjectArray[$i];
										$pickerLogicObject = new optionpicker_model();
										$pickerLogicObject = $pickerLogicArray[$i];
							?>
									<div class="optionForm-div" id="optionForm-div<?php echo $i+1?>">
										<?php if($tempOption->getDyPickerSubject()!=""){ ?>
                                            選項<?php echo $i+1?><input type="text" id="optionAreaP<?php echo $i+1?>" name="optionAreaP<?php echo $i+1?>" size="50" value="<?php echo $tempOption->getTitle(); ?>">
                                            範圍:<input type="text" id="type-P-value-start<?php echo $i+1?>" name="type-P-value-start<?php echo $i+1?>" size="2" value="<?php echo $tempOption->getDyPickerMin(); ?>">
                                            ~<div id="dyPickerSetting<?php echo $i+1?>" style="display:inline">
                                            <select id="dyPickerSelect<?php echo $i+1?>" name="dyPickerSelect<?php echo $i+1?>">
                                                <?php for($k=0;$k<count($questionArrayNoSelf);$k++){ ?>
                                                    <?php if($questionArrayNoSelf[$k]->getSubjectNumber() == $tempOption->getDyPickerSubject()){ ?>
                                                        <option value="<?php echo $questionArrayNoSelf[$k]->getSubjectNumber();?>" selected="true"> <?php echo $questionArrayNoSelf[$k]->getSubjectNumber().$questionArrayNoSelf[$k]->getSubject();?> </option>
                                                    <?php }else {?>
                                                        <option value="<?php echo $questionArrayNoSelf[$k]->getSubjectNumber();?>"> <?php echo $questionArrayNoSelf[$k]->getSubjectNumber().$questionArrayNoSelf[$k]->getSubject();?> </option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            </div>
                                            間隔:<input type="text" id="type-P-value-tick<?php echo $i+1?>" name="type-P-value-tick<?php echo $i+1?>" size="2" value="<?php echo $tempOption->getDyPickerTick(); ?>">
                                            檢誤功能<input type="checkbox" id="isDyPickerCheck<?php echo $i+1?>" name="isDyPickerCheck<?php echo $i+1?>" value="1" onchange="checkDyPicker('<?php echo $surveyGuid;?>','<?php echo $questionObject->getGuid();?>','<?php echo $i+1?>','<?php echo $tempOption->getDyPickerSubject();?>');" checked="Yes">
                                        <?php }else {?>
                                            選項<?php echo $i+1?><input type="text" id="optionAreaP<?php echo $i+1?>" name="optionAreaP<?php echo $i+1?>" size="50" value="<?php echo $tempOption->getTitle(); ?>">
                                            範圍:<input type="text" id="type-P-value-start<?php echo $i+1?>" name="type-P-value-start<?php echo $i+1?>" size="2" value="<?php echo $pickerLogicObject->getStartValue(); ?>">
                                            ~<div id="dyPickerSetting<?php echo $i+1?>" style="display:inline"><input type="text" id="type-P-value-end<?php echo $i+1?>" name="type-P-value-end<?php echo $i+1?>" size="2" value="<?php echo $pickerLogicObject->getEndValue();?>"></div>
                                            間隔:<input type="text" id="type-P-value-tick<?php echo $i+1?>" name="type-P-value-tick<?php echo $i+1?>" size="2" value="<?php echo $pickerLogicObject->getTickValue(); ?>">
                                            檢誤功能<input type="checkbox" id="isDyPickerCheck<?php echo $i+1?>" name="isDyPickerCheck<?php echo $i+1?>" value="1" onchange="checkDyPicker('<?php echo $surveyGuid;?>','<?php echo $questionObject->getGuid();?>','<?php echo $i+1?>','<?php echo $tempOption->getDyPickerSubject();?>');"">
                                        <?php }?>
                                        <?php if($i+1!=1){?>
                                            <a class="deleteOptionLink" href="javascript:removeOption(<?php echo $i+1?>);">   x</a>
                                        <?php }?>
                                        <input id="optionP-Tips<?php echo $i+1;?>" type="hidden" name="optionP-Tips<?php echo $i+1?>" value="<?php echo $tempOption->getOptionTips();?>">
                                        <?php
                                        $buttonText = "";
                                        if($tempOption->getOptionTips()!=""){
                                            $buttonText = "有提示✔";
                                        }else {
                                            $buttonText = "輸入提示";
                                        }
                                        ?>
                                        <button id="createButton-optionP-Tips<?php echo $i+1?>" name="button-optionP-Tips<?php echo $i+1?>" onclick="return showCreateOptionTipsView('optionP-Tips<?php echo $i+1?>');"><?php echo $buttonText;?></button>
									</div>
								<?php }} ?>
							</div>
                            <input type="BUTTON" value="新增" id="addPButton" name="addPButton" onClick="addPOption();">
						</div>
						<div id="type-G-div">
							<input type="hidden" id="g_sn" name="g_sn" value="<?php echo count($groupMainOptionArray) ?>" />
                            <div id="type-G-column-div">
							<?php for ($i=0; $i < count($groupMainOptionArray); $i++) {
								$mainGroupOption = new groupoption_model();
								 $mainGroupOption = $groupMainOptionArray[$i];
							?>
                            <div id="type-G-Area<?php echo $i+1?>-div">
							<br>主分類<?php echo $i+1;?>：<input type="text" size="40" id="type-G-main-group<?php echo $i+1?>" name="type-G-main-group<?php echo $i+1?>" value="<?php echo $mainGroupOption->getText() ?>">
                            <input type="BUTTON" value="新增選項" name="addGSubButton" onClick="addGSubOption(<?php echo $i+1?>);">

                                <!-- Start GroupMainOptionTips -->
                                <input id="optionGMain-Tips<?php echo $i+1;?>" type="hidden" name="optionGMain-Tips<?php echo $i+1?>" value="<?php echo $mainGroupOption->getOptionTips();?>">
                                <?php
                                $buttonText = "";
                                if($mainGroupOption->getOptionTips()!=""){
                                    $buttonText = "有提示✔";
                                }else {
                                    $buttonText = "輸入提示";
                                }
                                ?>
                                <button id="createButton-optionGMain-Tips<?php echo $i+1?>" name="button-optionGMain-Tips<?php echo $i+1?>" onclick="return showCreateOptionTipsView('optionGMain-Tips<?php echo $i+1?>');"><?php echo $buttonText;?></button>
                                <!-- End GroupMainOptionTips -->
                                <a class="deleteOptionLink" href="javascript:removeGMainOption(<?php echo $i+1?>);">   x </a>

                            <br />
                            <?php $subOptionIndex = 1;?>

                                <div class="optionLogicG-div" id="optionLogicG_<?php echo $i+1;?>-div">

                                    <?php for ($j=0; $j < count($groupSubOptionArray); $j++) { ?>
                                        <?php if($groupSubOptionArray[$j]->getType()==$mainGroupOption->getValue()){?>
                                            <div class="optionFormG-div" id="optionFormG_<?php $tempSn = $i+1; echo $tempSn.'_'.$subOptionIndex;?>-div">

                                                選項<?php echo $subOptionIndex;?>：
                                                <input type="text" size="50" id="optionAreaG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" name="optionAreaG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" value="<?php echo $groupSubOptionArray[$j]->getText(); ?>">
                                                選項值
                                                <input type="text" size="3" id="optionValueG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" name="optionValueG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" value="<?php echo $groupSubOptionArray[$j]->getValue(); ?>">
                                                顯示輸入框
                                                <input type="checkbox" id="optionIsShowTextViewCKBoxG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" name="optionIsShowTextViewCKBoxG<?php echo $i+1?>_<?php echo $subOptionIndex;?>" value="1" onchange="detectGTypeIsSelectShowTextViewCheckBox(<?php echo $i+1?>,<?php echo $subOptionIndex;?>);"
                                                       <?php if($groupSubOptionArray[$j]->getIsShowTextView()=='1'){ ?>checked<?php }?>>

                                                <!-- Start GroupSubOptionTips -->
                                                <input id="optionGSub-Tips<?php echo $i+1?>_<?php echo $subOptionIndex;?>" type="hidden" name="optionGSub-Tips<?php echo $i+1?>_<?php echo $subOptionIndex;?>" value="<?php echo $groupSubOptionArray[$j]->getOptionTips();?>">
                                                <?php
                                                $buttonText = "";
                                                if($groupSubOptionArray[$j]->getOptionTips()!=""){
                                                    $buttonText = "有提示✔";
                                                }else {
                                                    $buttonText = "輸入提示";
                                                }
                                                ?>
                                                <button id="createButton-optionGSub-Tips<?php echo $i+1?>_<?php echo $subOptionIndex;?>" name="button-optionGSub-Tips<?php echo $i+1?>_<?php echo $subOptionIndex;?>" onclick="return showCreateOptionTipsView('optionGSub-Tips<?php echo $i+1?>_<?php echo $subOptionIndex;?>');"><?php echo $buttonText;?></button>

                                                <!-- End GroupSubOptionTips -->
                                                <a class="deleteOptionLink" href="javascript:removeGSubOption('<?php $tempSn2 = $i+1; echo $tempSn2."_".$subOptionIndex;?>');">   x</a>
                                                <?php $subOptionIndex ++;?>
                                                </br>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                    <input type="hidden" id="g_sub_sn<?php echo $i+1 ?>" name="g_sub_sn<?php echo $i+1 ?>" value="<?php echo $subOptionIndex ?>" />
                                </div>

                            </div>
                            <?php }?>

							</div>
							<input type="BUTTON" value="新增主分類" name="addGButton" onClick="addGOption();">
							
						</div>
                    </div>
						<div id="singlePass-div">
                        <input type="hidden" id="singlePass_sn" name="singlePass_sn" value="<?php echo count($passAnswerArray); ?>" />
                        <H4><input type="checkbox" id="isSinglePassLogic" name="isSinglePassLogic" onchange="detectSinglePassLogic();" <?php if ($questionHasSinglePass){?>checked="Yes"<?php }  ?>">單題跳題設定</H4>
                            <div id="singlePassLogic-div">
                                <div id="singlePass-ConditionalArea-div">
                                <p><H5>請輸入選項值，例如：1(勿輸入字母或空白)</H5></p>
                                <?php if (count($passAnswerArray)>0) {
                                         for($i = 0; $i<count($passAnswerArray); $i++){
                                            $singlePassSn = $i+1
                                ?>
                                    <div id="singlePass-conditionalForm-div<?php echo $singlePassSn ;?>">
                                        <H5>條件<?php echo $singlePassSn;?><input type="text" id="passAnswer<?php echo $singlePassSn;?>" name="passAnswer<?php echo $singlePassSn;?>" value = "<?php if(count($passAnswerArray)>0 && $questionHasSinglePass){echo $passAnswerArray[$i];} ?>" onkeyup="checkOnlyNumber(this)" onbeforepaste="checkOnlyNumber(this)">
                                        跳題至：
                                        <select id="singleJumpToSubjectNumber<?php echo $singlePassSn;?>" name="singleJumpToSubjectNumber<?php echo $singlePassSn;?>" width="">
                                            <?php foreach ($allQuestionArray as $questionRow){
                                                    if ($questionHasSinglePass==1) {  ?>
                                                        <option value="<?php echo $questionRow->getSubjectNumber(); ?>" <?php if($passJumpToArray[$i] == $questionRow->getSubjectNumber()){echo 'selected';} ?>>
                                                            <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                        </option>
                                                    <?php }else { ?>
                                                        <option value="<?php echo $questionRow->getSubjectNumber(); ?>" >
                                                            <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                        </option>
                                                    <?php }?>
                                                <?php } ?>
                                        </select>
                                        <?php if($singlePassSn != 1){ ?>
                                            <a class="deleteOptionLink" href="javascript:removeSinglePassOption(<?php echo $singlePassSn;?>);">   x</a>
                                        <?php } ?>
                                        </H5>
                                    </div>
                                <?php }} ?>
                                </div>
                                <H5>
                                <input type="radio" name="singlePassSelectStatus" value="1" <?php if ($singlePassSelectStatus){?>checked<?php }  ?>>選取答案以觸發跳題
                                <input type="radio" name="singlePassSelectStatus" value="0" <?php if (!$singlePassSelectStatus){?>checked<?php }  ?>>不選取答案以觸發跳題</H5>
                                <input type="BUTTON" value="新增條件" id="addSinglePassButton" name="addSinglePassButton" onClick="addSinglePassOption();">
                            </div>
						</div>

                        <div id="groupPass-div">
                            <input type="hidden" id="groupPass_sn" name="groupPass_sn" value="<?php echo count($groupPassSubjectArray) ?>" />
                            <input type="hidden" id="groupPassAnswerArrayFormat" name="groupPassAnswerArrayFormat" value="<?php echo $groupPassAnswerArrayFormat ?>" />
                            <H4><input type="checkbox" id="isGroupPassLogic" name="isGroupPassLogic" onchange="detectGroupPassLogic();" <?php if ($questionHasGroupPass){?>checked="Yes"<?php }  ?>">複數條件跳題設定</H4>

                                <div id="groupPassLogic-div">
                                    <div id="groupPassConditionalArea-div">
                                        <?php if (count($groupPassSubjectArray)>0) {
                                                   for($i = 0; $i<count($groupPassSubjectArray); $i++){
                                                        $passSn = $i+1
                                            ?>
                                                <div id="groupPass-conditionalForm-div<?php echo $passSn;?>">
                                                    <p><H5>條件<?php echo $passSn;?>
                                                        <select id="groupPassSubjectNumber<?php echo $passSn;?>" name="groupPassSubjectNumber<?php echo $passSn;?>" width="" onchange="getGroupPassAnswer(<?php echo $passSn;?>);">
                                                            <?php foreach ($allQuestionArray as $index=>$questionRow){
                                                                if($questionHasGroupPass){
                                                                ?>
                                                                <option value="<?php echo $questionRow->getGuid().'@'.$questionRow->getSubjectNumber(); ?>" <?php if($i < count($groupPassSubjectArray) && $groupPassSubjectArray[$i] == $questionRow->getSubjectNumber()){echo 'selected';} ?>>
                                                                    <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                                </option>
                                                            <?php }else {?>
                                                               <option value="<?php echo $questionRow->getGuid().'@'.$questionRow->getSubjectNumber(); ?>" >
                                                                   <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                               </option>
                                                            <?php }}?>
                                                        </select>
                                                        觸發選項
                                                            <select id="groupPassAnswerNumber<?php echo $passSn;?>" name="groupPassAnswerNumber<?php echo $passSn;?>" width="">
                                                            </select>

                                                            <?php if($passSn != 1 && $passSn != 2){ ?>
                                                                <a class="deleteOptionLink" href="javascript:removeGroupPassOption(<?php echo $passSn;?>);">   x</a>
                                                            <?php } ?>
                                                    </H5></p>
                                                </div>

                                            <?php  }} ?>
                                    </div>
                                    <input type="BUTTON" value="新增條件" id="addGroupPassButton" name="addGroupPassButton" onClick="addGroupPassOption();">
                                    <p><H5>跳題至：</H5></p>
                                    <select id="groupJumpToSubjectNumber" name="groupJumpToSubjectNumber" width="">
                                        <?php foreach ($allQuestionArray as $questionRow){
                                            if ($questionHasGroupPass==1) {  ?>
                                                <option value="<?php echo $questionRow->getSubjectNumber(); ?>" <?php if($groupPassObject->getJumpTo() == $questionRow->getSubjectNumber()){echo 'selected';} ?>>
                                                    <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                </option>
                                            <?php }else { ?>
                                                <option value="<?php echo $questionRow->getSubjectNumber(); ?>" >
                                                    <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                </option>
                                            <?php }?>
                                        <?php } ?>
                                    </select>
                                </div>
                        </div>
						
                        <div id="dyPass-div">
                            <input type="hidden" id="dyPass_sn" name="dyPass_sn" value="<?php echo count($dy_SubjectNumber) ?>" />
                            <input type="hidden" id="dyPassAnswerArrayFormat" name="dyPassAnswerArrayFormat" value="<?php echo $dyPassAnswerArrayFormat ?>" />
                            <H4><input type="checkbox" id="isDyPassLogic" name="isDyPassLogic" onchange="detectDyPassLogic();" <?php if ($isDyPassLogic){?>checked="Yes"<?php }  ?>">前置跳題設定</H4>

                            <div id="dyPassLogic-div">
                                <div id="dyPassConditionalArea-div">
                                    <?php if (count($dy_SubjectNumber)>0) {
                                        for($i = 0; $i<count($dy_SubjectNumber); $i++){
                                            $dyPassSn = $i+1
                                            ?>
                                            <div id="dyPass-conditionalForm-div<?php echo $dyPassSn;?>">
                                            	
                                                <p><H5>條件<?php echo $dyPassSn;?>
                                                    <select id="dyPassSubjectNumber<?php echo $dyPassSn;?>" name="dyPassSubjectNumber<?php echo $dyPassSn;?>" width="" onchange="getDyPassAnswer(<?php echo $dyPassSn;?>);">
                                                        <?php foreach ($allQuestionArray as $index=>$questionRow){
                                                            if(isset($dy_SubjectNumber[$i]) && $questionRow->getSubjectNumber() == $dy_SubjectNumber[$i]){
                                                                ?>
                                                                <option value="<?php echo $questionRow->getGuid().'@'.$questionRow->getSubjectNumber(); ?>" <?php if($i < count($dy_SubjectNumber) && $dy_SubjectNumber[$i] == $questionRow->getSubjectNumber()){echo 'selected';} ?>>
                                                                    <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                                </option>
                                                            <?php }else {?>
                                                                <option value="<?php echo $questionRow->getGuid().'@'.$questionRow->getSubjectNumber(); ?>" >
                                                                    <?php echo '第'.$questionRow->getPriority().'題      '.$questionRow->getSubjectNumber().' - '.$questionRow->getSubject(); ?>
                                                                </option>
                                                            <?php }}?>
                                                    </select>
                                                    觸發選項
                                                    <select id="dyPassAnswerNumber<?php echo $dyPassSn;?>" name="dyPassAnswerNumber<?php echo $dyPassSn;?>" width=""></select>

                                                    <?php if(count($isOrAnswer) > 0 && $isOrAnswer[$i]==1 && $dyPassSn != 1){ ?>
                                                        "或"的條件<input type="checkbox" id="isOrAnswer<?php echo $i+1?>" name="isOrAnswer<?php echo $i+1?>" value="1"  checked="Yes" >
                                                    <?php }else if($dyPassSn != 1){ ?>
                                                        "或"的條件<input type="checkbox" id="isOrAnswer<?php echo $i+1?>" name="isOrAnswer<?php echo $i+1?>" value="1" >
                                                    <?php }?>
                                                    </select>
                                                    <?php if($dyPassSn != 1){ ?>
                                                        <a class="deleteOptionLink" href="javascript:removeDyPassOption(<?php echo $dyPassSn;?>);">   x</a>
                                                    <?php } ?>
                                                </H5></p>

                                            </div>

                                        <?php  }} ?>

                                </div>
                                <H5>
                                <input type="radio" name="dyIsShow" value="1" <?php if ($dy_IsShow){?>checked<?php }  ?>>滿足以上條件需回答此題
                                <input type="radio" name="dyIsShow" value="0" <?php if (!$dy_IsShow){?>checked<?php }  ?>>滿足以上條件不需回答此題</H5>
                                <input type="BUTTON" value="新增條件" id="addDyPassButton" name="addDyPassButton" onClick="addDyPassOption();">
                            </div>
                        </div>


					<input type="submit" value="儲存"/>
			    </form>

			</div>
	    	
    	</div>
    </div>
</body>
</html>
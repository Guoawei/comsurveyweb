/**
 * @author 蔡孟珂
 */
$(document).ready(function() {
	/*
	 * 登入
	 */
	$("#loginform").submit(function() {
		//取得按鈕
		var submitBtn = $(this).find('button[type="submit"]');
		submitBtn.hide();
		submitBtn.next().html('<img src="' + CI.base_url + 'style/front/images/base/ajax-loader-small.gif"/>');

		var err = '';
		// @formatter:off
		if ($("#email").val() == '')
			err += '請輸入 帳號<br/>';
		if ($("#pwd").val() == 0)
			err += '請選擇 密碼<br/>';
		// @formatter:on
		if (err != '') {
			showNotify(err, 5000);
			submitBtn.next().html('');
			submitBtn.show();
		} else {
			$.ajax({
				type : "POST",
				url : CI.base_url + "login",
				data : {
					email : $("#email").val(),
					pwd : $.md5($("#pwd").val())
				},
				dataType : 'json'
			}).done(function(data) {
				//console.log(data);

				if (data.Header.Status == 0) {
					//清除問卷
					delStorageSuvery();
					//轉跳
					window.location.href = CI.base_url + "front/survey_list";
				} else {
					showNotify(data.Header.Doc, 5000);
					submitBtn.next().html('');
					submitBtn.show();
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
			});
		}
		return false;
	});

	/*
	 * 登出
	 */
	$("a.logout").click(function() {
		$.ajax({
			type : "POST",
			url : CI.base_url + "webLogout/doLogout"
		}).done(function(data) {
			//清除問卷
			delStorageSuvery();
			window.location.href = CI.base_url;

		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
		});
	});

	/*
	 * 正在讀取中mask
	 */
	$("#mask_loading").height($(document).height());
	$("a.startAnswer,a.mask").click(function() {
		$("#mask_loading").show();
	});

	/**
	 * 取得樣本進度條並產生視覺圖
	 */
	if ($('div.sampleProgress').length > 0) {
		var progressNum = $('div.sampleProgress').length;
		$("#mask_loading").show();
		$('div.sampleProgress').each(function(i) {
			var answerDate = $(this).parent().next().next().next();
			var bar = $(this).find("span");

			$.ajax({
				type : "POST",
				url : CI.base_url + "front/getSampleProgress",
				data : {
					sp : $(this).attr('data-spguid'),
					s : $(this).attr('data-sguid'),
					iv : $(this).attr('data-ivguid'),
					v : $(this).attr('data-ver')
				},
				dataType : 'json'
			}).done(function(data) {
				//產生進度效果
				if (data.percentage != 100) {
					bar.css("background-color", '#e494a3');
				}

				bar.animate({
					'width' : data.percentage + "%"
				}, 300);
				//顯示作答時間
				answerDate.html(data.cDateTime);
				//console.log(data.percentage);
				//清除問卷

				//如果做到最後一個就關閉讀取視窗
				if (i == (progressNum - 1)) {
					$("#mask_loading").hide();
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
			});
		});
	}

	/**
	 * 鎖定問卷
	 */
	$("a.lockSample").click(function() {
		var lockBtn = $(this);
		//警告是否要鎖定該樣本。
		var goLock = confirm("您確定要鎖定此樣本？\n鎖定後將不可以在進行作答或修改答案。");
		if (goLock) {
			$.ajax({
				type : "POST",
				url : CI.base_url + "front/lockSample",
				data : {
					s : lockBtn.attr('data-guid')
				},
				dataType : 'json'
			}).done(function(data) {
				//console.log(data);
				if (data.result) {
					//將該按鈕改成紅色已鎖定，並改變 class 為 locked
					lockBtn.parent().prev().prev().prev().html('<img src="' + CI.base_url + 'style/front/images/base/lock.gif">');
					lockBtn.parent().html("已鎖定，無法作答");
				} else {
					showNotify(data.msg, 5000);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});
		}

		return false;
	});

	/**
	 * 取得題目並存在 localStorge 中
	 */
	$("a.startAnswer").click(function() {
		//先取得 localstorage 判定有沒有該問卷題目 以 surveyGuid 為儲存的鍵值
		var storage = $.localStorage;
		var sguid = $(this).attr('data-sguid');
		var redirect = $(this).attr('href');
		if (storage.isEmpty(sguid)) {
			//該問卷為空，就載入題目
			$.ajax({
				type : "POST",
				url : CI.base_url + "getQuestion",
				data : {
					surveyguid : sguid
				},
				dataType : 'json'
			}).done(function(data) {
				//console.log(data);

				if (data.Header.Status == 0) {
					//取得題目後，將 data.Body.getQuestion.Subject 儲存於 localstorge
					storage.set(sguid, data.Body.getQuestion.Subject);

					//並將題號也抽出來，另外存成陣列，作為建立答案表格用。
					var subject_index = new Array();
					$.each(data.Body.getQuestion.Subject, function(i) {
						subject_index[i] = this.subject_index;
					});
					storage.set(sguid + "_index", subject_index);

					//轉跳 sample_status
					window.location.href = redirect;
				} else {
					showNotify(data.Header.Doc, 5000);
					submitBtn.next().html('');
					submitBtn.show();
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
			});
		} else {
			//如果已經有該問卷了，就直接轉跳 sample_status 頁面
			//轉跳
			window.location.href = redirect;
		}

		return false;
	});

	/**
	 * 題目狀況 li 效果
	 */
	$("a.dropdown").click(function() {
		$("ul.situation ul").not(":hidden").slideUp();
		//顯示下一層
		$(this).parent().find('ul').slideToggle();

		//清除文字方塊內容
		if ($("input[class^='situationComment']").length >= 1)
			$("input[class^='situationComment']").val('');

	});

	/**
	 * 記錄時間點
	 */
	function saveSurveySituation(code,redirect) {
		//redirect 預設為 true
		redirect = typeof redirect !== 'undefined' ? redirect : true;
		comment = '';
		if (code == '23')
			comment = $("input.situationComment23").val();
		if (code == '410')
			comment = $("input.situationComment410").val();

		$.ajax({
			type : "POST",
			url : CI.base_url + "front/uploadSurveySituation",
			data : {
				c : code,
				m : comment
			},
			dataType : 'json'
		}).done(function(data) {
			//console.log(data);
			if (data.result) {
				//轉跳 sample_status
				if(redirect)
					window.location.href = data.redirect;
			} else {
				showNotify(data.msg, 5000);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
	}

	/**
	 * 進入問卷問候畫面
	 */
	$("a.intoAnswer").click(function() {
		$("#mask_loading").show();
		var storage = $.localStorage;
		var qustionsIndex = storage.get($(this).attr('data-sguid') + "_index");
		//檢查並建立答案表格
		$.ajax({
			type : "POST",
			url : CI.base_url + "front/creatAnswerTable",
			data : {
				i : qustionsIndex
			},
			dataType : 'json'
		}).done(function(data) {
			saveSurveySituation(1);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
		return false;
	});
	
		
	
	/**
	 * 停止問卷回到情況頁面
	 */
	$("a.stopAnswer").click(function() {
		window.location.href = CI.base_url + "front/sample_status/?spguid=" + $(this).attr('data-spguid') + "&sguid=" + $(this).attr('data-sguid');
	});
	$("a.stopSurvey").click(function() {
		saveSurveySituation($(this).attr('data-code'));

		return false;
	});
	$("button.stopSurvey").click(function() {
		var input = $(this).prev();
		if (input.val() == '') {
			showNotify('請填入原因', 5000);
		} else {
			saveSurveySituation($(this).attr('data-code'));

		}
		return false;
	});

	/**
	 * 刪除local storage的問卷
	 */
	function delStorageSuvery() {
		var storage = $.localStorage;
		storage.removeAll();
		//var storageKeys = storage.keys();
		//console.log(storageKeys[0]);
	}

	/**
	 * 開始繼續作答
	 */
	$("a.continueSurvey").click(function() {
		//回資料庫取得是否該樣本已經有作答，若有就從該題的下一題開始
		checkCurrentQuestion($(this).attr('data-sguid'));
		$("div.surveyIntro").hide();
		$("div.question,div.nav_buttons").show();
		//重新儲存作答時間開始
		//繼續作答時修正開始時間
		$.ajax({
			type : "POST",
			url : CI.base_url + "front/updateSurveyStartTime",
			dataType : 'json'
		}).done(function(data) {
			
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
		return false;
	});

	/**
	 * 上一題
	 */
	$("button.prev_question").click(function() {
		var storage = $.localStorage;
		var surveyGuid = $(this).attr('data-sguid');
		var qustions = storage.get(surveyGuid + "_index");
		//console.log(qustions);
		//隱藏答案項目
		$("div.options").css({
			'filter' : 'alpha(opacity=0)',
			'-moz-opacity' : 0,
			'opacity' : 0
		});
		//取得上一題的題號
		var prevIndex = $(this).attr('data-questionindex');
		prevQuestion(prevIndex, surveyGuid);
	});

	/**
	 * 取得上一題題目
	 */
	function prevQuestion(prevIndex, surveyGuid) {
		$("div.question h3.title").html('<img src="'+CI.base_url+'style/front/images/base/ajax-loader-small.gif">');
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid + "_index");

		//進行上一題的判斷，如果沒有答案就繼續跳回
		var islast = false;
		$.ajax({
			type : "POST",
			url : CI.base_url + "front/checkHaveSelect",
			data : {
				sn : qustions[prevIndex]
			},
			dataType : 'json'
		}).done(function(data) {
			if (data.result) {
				if (data.value) {
					showQustion(surveyGuid, parseInt(prevIndex), islast);
				} else {
					prevQuestion((prevIndex - 1), surveyGuid);
				}
			} else {
				showNotify(data.msg, 5000);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
	}

	/**
	 * 下一題
	 */
	$("button.next_question").click(function() {

		var storage = $.localStorage;
		var surveyGuid = $(this).attr('data-sguid');
		var qustions = storage.get(surveyGuid);
		var qustionsIndex = storage.get(surveyGuid + "_index");
		//目前題目index
		var currentIndex = $("#currentIndex").val();
		var haveToGo = false;
		var reloadOption = false;
		//檢查有沒有 輸入值
		switch (qustions[currentIndex].subject_type) {
			case "FO":
				//四分題
				if($("div.options input[type='radio']:checked").length == 1)
					haveToGo = true;
				break;
			case "FI":
				//五分題
				if($("div.options input[type='radio']:checked").length == 1)
					haveToGo = true;
				break;
			case "T":
				//文字輸入題
				if($("div.options select").length > 0)
				{
					//給縣市下拉選單用
					haveToGo = true;
					$("div.options select").each(function() {
						if(($(this).val() == undefined || $(this).val() == 'undefined') && !reloadOption)
						{
							reloadOption = true;
						}
						if ($(this).val() == '' || $(this).val() == undefined || $(this).val() == 'undefined')
						{	
							haveToGo = false;
							return false;
						}
					});
				}
				else
				{
					$("div.options input:text").each(function() {
						if ($(this).val() != '')
							haveToGo = true;
					});
				}
				break;
			case "P":
				//下拉式清單
				haveToGo = true;
				$("div.options select").each(function() {
					//如果為undrfined就顯示題目錯誤
					//console.log($(this).attr('data-index'), $(this).val());
					if(($(this).attr('data-index') == undefined || $(this).attr('data-index') == 'undefined' || $(this).val() == undefined || $(this).val() == 'undefined') && !reloadOption)
					{
						reloadOption = true;
					}
					if ($(this).val() == '' || $(this).attr('data-index') == undefined || $(this).attr('data-index') == 'undefined' || $(this).val() == undefined || $(this).val() == 'undefined')
					{	
						haveToGo = false;
						return false;
					}
				});
				break;
			default:
				//"S" 單選題, "M" 多選題, "G"群組題
				if ($("div.options input:checked").length > 0) {
					$("div.options input:checked").each(function(){
						if($(this).parent().parent().find('input:text').length == 1)
						{
							if($(this).parent().parent().find('input:text').val() == '')
							{
								haveToGo = false;
								return false;
							}
							else
							{
								haveToGo = true;
							}
						}
						else
						{
							haveToGo = true;
						}
					});
				}
				break;
		}
		
		if (haveToGo) {
			//console.log(qustions);
			//隱藏答案項目
			$("div.options").css({
				'filter' : 'alpha(opacity=0)',
				'-moz-opacity' : 0,
				'opacity' : 0
			});

			//下一個題目index
			var nextIndex = $(this).attr('data-questionindex');

			//儲存答案
			if(saveAnswer(currentIndex, surveyGuid))
			{
				console.log('問卷結束索引值qustionsIndex.length - 1='+(qustionsIndex.length - 1)+', nextIndex='+nextIndex);
			
				var islast = false;
				if (nextIndex == 'done') {
					//記錄最後完成時間。
					//如果為完成問卷
					showEnd();
				} else {
					$("div.question h3.title").html('<img src="'+CI.base_url+'style/front/images/base/ajax-loader-small.gif">');
					if ((qustionsIndex.length - 1) == nextIndex)
						islast = true;
					nextQuestion(currentIndex, nextIndex, surveyGuid, islast);
				}
			}
			else
			{
				//答案存取錯誤，請回上一題，再按下一題回來作答
				showNotify("答案儲存錯誤，請回上一題，在按下一題回來進行填寫。", 3000);
			}
			
			
		} else {
			if(reloadOption)
			{
				showNotify("答案項目讀取有錯誤，請回上一題，在按下一題回來進行填寫。", 3000);
			}
			else
			{
				showNotify("請作答後，才能下一題<br/>如有選其他，請填入文字。<br/>若單選題，請確定選一答案。", 3000);
			}
		}
		
	});

	/**
	 * 取得下一題題目
	 */
	function nextQuestion(currentIndex, nextIndex, surveyGuid, islast) {
		//$("div.question h3.title").html('<img src="'+CI.base_url+'style/front/images/base/ajax-loader-small.gif">');
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid);
		var qustionsIndex = storage.get(surveyGuid + "_index");
		currentQustionObject = qustions[currentIndex];
		var nextNumber = startIndex = endIndex = null;
		
		//console.log("currentIndex = "+currentIndex + " nextIndex = "+nextIndex + " currentNunver = "+currentQustionObject.subject_index + " subject_singlePass = "+currentQustionObject.subject_singlePass);
		//下一題的邏輯判別
		if (currentQustionObject.subject_singlePass != null) {

			//單題跳題
			if (currentQustionObject.subject_singlePass.select == 1) {
				//為勾選題目才跳題

				switch (currentQustionObject.subject_type) {
					case 'S':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] == $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];

							}
						}

						break;
					case 'M':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] == $("div.options input:checkbox:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'T':
						//文字方塊沒有跳題

						break;
					case 'FO':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] == $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'FI':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] == $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'P':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] == $("div.options select").val()) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'G':
						//群組題沒有跳題

						break;
					default:

				}
			} else {
				//沒勾選這幾題就跳
				switch (currentQustionObject.subject_type) {
					case 'S':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] != $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}

						break;
					case 'M':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] != $("div.options input:checkbox:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'T':
						//文字方塊沒有跳題

						break;
					case 'FO':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] != $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'FI':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] != $("div.options input:radio:checked").attr('data-value')) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'P':
						for (var key in currentQustionObject.subject_singlePass.answer) {
							if (currentQustionObject.subject_singlePass.answer[key] != $("div.options select").val()) {
								nextNumber = currentQustionObject.subject_singlePass.jumpTo[key];
							}
						}
						break;
					case 'G':
						//群組題沒有跳題

						break;
					default:

				}
			}
		}

		if (currentQustionObject.subject_groupPass != null) {
			//群組跳題
			//先取得此群組跳提要的答案
			$.ajax({
				type : "POST",
				url : CI.base_url + "front/getGroupPassAnswer",
				data : {
					n : currentQustionObject.subject_groupPass.subject
				},
				dataType : 'json'
			}).done(function(data) {
				if (data.result) {

					var jump = true;
					
					for (var key in data.anwser) {
						if (data.anwser[key] != currentQustionObject.subject_groupPass.answer[key]) {
							jump = false;
							break;
						}
					}

					if (jump) {
						nextNumber = currentQustionObject.subject_groupPass.jumpTo;
					}

					//取得該 nextNumber 的index值
					for (var keyIndex in qustionsIndex) {
						if (nextNumber == qustionsIndex[keyIndex])
							nextIndex = keyIndex;
					}
					
					//清除跳題，中間不該有的答案
					if ((nextIndex - currentIndex) > 1) {
						//跳超過一題以上就進行中間題的清除
						startIndex = parseInt(currentIndex) + 1;
						endIndex = parseInt(nextIndex) - 1;
						clearAnswer(surveyGuid, startIndex, endIndex);
					}

					//檢查前置跳題後跳下一題
					checkDynamicOptionAnswer(surveyGuid, nextIndex, islast);
					//showQustion(surveyGuid, parseInt(nextIndex), islast);

				} else {
					//showNotify(data.msg, 5000);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});

		} else {
			//取得該 nextNumber 的index值
			if (nextNumber) {
				for (var keyIndex in qustionsIndex) {
					if (nextNumber == qustionsIndex[keyIndex])
						nextIndex = keyIndex;
				}
			}
			
			//清除跳題，中間不該有的答案
			if ((nextIndex - currentIndex) > 1) {
				//跳超過一題以上就進行中間題的清除
				startIndex = parseInt(currentIndex) + 1;
				endIndex = parseInt(nextIndex) - 1;
				clearAnswer(surveyGuid, startIndex, endIndex);
			}
			//檢查前置跳題後跳下一題
			checkDynamicOptionAnswer(surveyGuid, nextIndex, islast);
			//showQustion(surveyGuid, parseInt(nextIndex), islast);
		}
	}

	/**
	 * 檢查前置跳題
	 */
	function checkDynamicOptionAnswer(surveyGuid, nextIndex, islast) {
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid);
		var qustionsIndex = storage.get(surveyGuid + "_index");
		nextQustionObject = qustions[nextIndex];
		var andAnswer = orAnswer = andlogicok = orlogicok = finallogic = null, dynamicOptionByAnswer;

		if (nextQustionObject.subject_dynamicOptionAnswer != '') {
			//console.log("surveyGuid = "+surveyGuid+" nextIndex = "+nextIndex+" getDynamicOptionByAnswer?dyaguid="+nextQustionObject.subject_dynamicOptionAnswer);
			//console.log("nextQustionObject = ",nextQustionObject);
			//前置跳題
			$.ajax({
				type : "POST",
				async : false,
				url : CI.base_url + "getDynamicOptionByAnswer?dyaguid=" + nextQustionObject.subject_dynamicOptionAnswer,
				dataType : 'json'
			}).done(function(data) {

				if (data.Header.Status == 0) {
					var andAnswer = orAnswer = null;
					dynamicOptionByAnswer = data.Body.getDynamicOptionByAnswer[0];
					if (dynamicOptionByAnswer.AndLogic.subjectNumber.length > 0) {
						//先取得此群組跳提要的答案
						$.ajax({
							type : "POST",
							async : false,
							url : CI.base_url + "front/getGroupPassAnswer",
							data : {
								n : dynamicOptionByAnswer.AndLogic.subjectNumber
							},
							dataType : 'json'
						}).done(function(anddata) {
							//console.log("================================================");
							//console.log("anddata = ");
							//console.log(anddata);
							if (anddata.result) {
								andAnswer = anddata.anwser;
							} else {
								showNotify(anddata.msg, 5000);
							}
						}).fail(function(jqXHR, textStatus, errorThrown) {
							//console.log(jqXHR);
							showNotify(jqXHR.responseText, 50000);
						});
					}

					if (dynamicOptionByAnswer.OrLogic.subjectNumber.length > 0) {
						//先取得此群組跳提要的答案
						$.ajax({
							type : "POST",
							async : false,
							url : CI.base_url + "front/getGroupPassAnswer",
							data : {
								n : dynamicOptionByAnswer.OrLogic.subjectNumber
							},
							dataType : 'json'
						}).done(function(ordata) {
							//console.log("ordata = "+ordata);
							if (ordata.result) {
								orAnswer = ordata.anwser;
							} else {
								showNotify(ordata.msg, 5000);
							}
						}).fail(function(jqXHR, textStatus, errorThrown) {
							//console.log(jqXHR);
							showNotify(jqXHR.responseText, 50000);
						});
					}

					//滿足以上條件需回答此題
					if (andAnswer) {						
						for (var key in dynamicOptionByAnswer.AndLogic.targetAnswer) {
							//console.log(andAnswer[key]);
							if($.isArray(andAnswer[key]))
							{
								//console.log("check "+dynamicOptionByAnswer.AndLogic.targetAnswer[key]+" in array");
								//console.log("$.isArray("+dynamicOptionByAnswer.AndLogic.targetAnswer[key]+", "+andAnswer[key]+") = " + $.inArray(dynamicOptionByAnswer.AndLogic.targetAnswer[key], andAnswer[key]));
								if (($.inArray(dynamicOptionByAnswer.AndLogic.targetAnswer[key], andAnswer[key]))>=0) {
									andlogicok = true;
								} else {
									andlogicok = false;
									break;
								}
							}
							else
							{
								if (andAnswer[key] == dynamicOptionByAnswer.AndLogic.targetAnswer[key]) {
									andlogicok = true;
								} else {
									andlogicok = false;
									break;
								}
							}
							//console.log(andlogicok);
						}
						
					}
					if (orAnswer) {
						for (var key in dynamicOptionByAnswer.OrLogic.targetAnswer) {
							if($.isArray(orAnswer[key]))
							{
								if ($.inArray(dynamicOptionByAnswer.OrLogic.targetAnswer[key], orAnswer[key])) {
									orlogicok = true;
								}
							}
							else
							{
								if (orAnswer[key] == dynamicOptionByAnswer.OrLogic.targetAnswer[key]) {
									orlogicok = true;
								}
							}
						}
					}
					//console.log(andlogicok +", "+orlogicok);
					//如果 and 或 or 邏輯比對成功，就依照isshow決定要不要顯示
					if(andlogicok || orlogicok)
					{
						//符合的正常邏輯
						if (dynamicOptionByAnswer.isShow == "1")
						{
							//有符合此and就顯示
							showQustion(surveyGuid, parseInt(nextIndex), islast);
						}
						else
						{
							//要不跳下一題
							nextQuestion(nextIndex, parseInt(nextIndex) + 1, surveyGuid, islast);
						}
					}
					else
					{
						//不符合，就反過來的邏輯。
						if (dynamicOptionByAnswer.isShow == "1")
						{
							//要不跳下一題
							nextQuestion(nextIndex, parseInt(nextIndex) + 1, surveyGuid, islast);
						}
						else
						{
							//有符合此and就顯示
							showQustion(surveyGuid, parseInt(nextIndex), islast);
						}
					}
					
					/*
					if (andlogicok || orlogicok) {
						finallogic = true;
					}

					//console.log("done = "+andAnswer, orAnswer);
					if ((dynamicOptionByAnswer.isShow == "1") && finallogic) {
						//console.log('顯示此題');
						showQustion(surveyGuid, parseInt(nextIndex), islast);
					} else if ((dynamicOptionByAnswer.isShow == "0") && finallogic) {
						//console.log('顯示下一題');
						//顯示下一提前，先清除要跳題的答案

						//載入下一題
						//console.log(nextIndex,parseInt(nextIndex)+1,surveyGuid,islast);
						nextQuestion(nextIndex, parseInt(nextIndex) + 1, surveyGuid, islast);
					} else {
						//正常跳下一題
						showQustion(surveyGuid, parseInt(nextIndex), islast);
					}
					*/
				} else {
					showNotify(data.Header.Doc + "1", 5000);
				}

			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});
		} else {
			showQustion(surveyGuid, parseInt(nextIndex), islast);
		}
	}

	/**
	 * 儲存答案
	 */
	function saveAnswer(currentIndex, surveyGuid) {
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid);
		var qustionsIndex = storage.get(surveyGuid + "_index");
		currentQustionObject = qustions[currentIndex];

		//儲存目前的做答內容，取得答案
		var answer = new Array();
		switch (currentQustionObject.subject_type) {
			case 'S':
				$("div.options input:checked").each(function(i) {
					answer[i] = $(this).attr('data-value');
					if ($(this).parent().parent().find("input:text").length == 1) {
						if ($(this).parent().parent().find("input:text").val() != '') {
							answer[i] = "T:"+$(this).parent().parent().find("input:text").val();
						}
					}
				});
				break;
			case 'M':
				$("div.options input:checked").each(function(i) {
					if ($(this).parent().parent().find("input[type='text']").length == 1) {
						if ($(this).parent().parent().find("input[type='text']").val() != '') {
							answer[i] = $(this).attr('data-index') + "$T:" + $(this).attr('data-value') + ":" + $(this).parent().parent().find("input[type='text']").val();
						}
					} else {
						answer[i] = $(this).attr('data-index') + "$" + $(this).attr('data-value');
					}
				});
				break;
			case 'T':
				if($("div.options select").length >0)
				{
					$("div.options select").each(function(i) {
						if ($(this).val() != '')
							answer[i] = $(this).val();
					});
				}
				else
				{
					$("div.options input[type='text']").each(function(i) {
						if ($(this).val() != '')
							answer[i] = $(this).val();
					});
				}
				
				break;
			case 'FO':
				$("div.options input:checked").each(function(i) {
					answer[i] = $(this).attr('data-value');
				});
				break;
			case 'FI':
				$("div.options input:checked").each(function(i) {
					answer[i] = $(this).attr('data-value');
				});
				break;
			case 'P':
				$("div.options select").each(function(i) {
					answer[i] = $(this).attr('data-index') + "$" + $(this).val();
				});

			case 'G':
				//群組題沒有跳題
				$("div.options input:checked").each(function(i) {
					//console.log($(this));
					if ($(this).parent().parent().find("input[type='text']").length == 1) {
						if ($(this).parent().parent().find("input[type='text']").val() != '') {
							answer[i] = "$T:88:" + $(this).parent().parent().find("input[type='text']").val();
						}
					} else {
						answer[i] = $(this).attr('data-index') + "$" + $(this).attr('data-id');
					}
				});
				break;
			default:
				
				break;
		}
		//console.log('answer.length = '+answer.length);
		//answer = new Array();
		if(parseInt(answer.length) > 0)
		{
			var noUN = true;
			$.each(answer,function(){
				var isun = this.match(/undefined/g);
				if(isun != null)
				{
					if(isun.length >= 1)
					{
						noUN = false;
						return false;
					}
				}
			});
			if(noUN)
			{
				var getresult = false;
				$.ajax({
					type : "POST",
					async: false,
					url : CI.base_url + "front/saveAnswer",
					data : {
						n : currentQustionObject.subject_index,
						a : answer,
						i : qustionsIndex
					},
					dataType : 'json'
				}).done(function(data) {
					getresult = true;
					//console.log(data);
				}).fail(function(jqXHR, textStatus, errorThrown) {
					//console.log(jqXHR.responseText);
					showNotify(jqXHR.responseText, 50000);
					getresult = false;
				});
				return getresult;
			}
			else
			{
				return noUN;
			}
			
		}
		else
		{
			return false;	
		}
	}
	
	
	
	/**
	 * 檢查目前樣本狀況，若都還沒做，就從第一題開始，要不然就是從做到最後一題開始
	 * surveyGuid = 問卷的id
	 */
	function checkCurrentQuestion(surveyGuid) {
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid + "_index");

		$.ajax({
			type : "POST",
			url : CI.base_url + "front/checkSampleCurrentQuestion",
			dataType : 'json'
		}).done(function(data) {
			//console.log(data);
			if (data.result) {
				//顯示目前題目。
				//給予目前題目物件,目前的陣列index,是否為last
				var islast = false;
				if ((qustions.length - 1) == data.subjectArrayIndex)
					islast = true;
				showQustion(surveyGuid, data.subjectArrayIndex, islast);
				//showQustion(qustions[data.subjectArrayIndex],data.subjectArrayIndex,islast);

			} else {
				//showNotify(data.msg, 5000);
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
	}

	/**
	 * 顯示目前題目
	 */
	function showQustion(surveyGuid, index, islast) {
		/**
		 qustionObject.subject_dynamicOption: ""      	動態選項編號
		 qustionObject.subject_dynamicOptionAnswer: ""	動態答案選項編號
		 qustionObject.subject_groupOption: ""			群組題選項編號
		 qustionObject.subject_groupPass: null			群組跳題
		 qustionObject.subject_index: "RB5"				題號
		 qustionObject.subject_isRandom: "0"				選項隨機訓續亂排預設為0
		 qustionObject.subject_multiSelectedLimit: "1"	複選題最大選擇數量
		 qustionObject.subject_option: "16746ebb3f7c9e498b62075db6a4d186" 選項陣列 guid
		 qustionObject.subject_optionOutputForm: ""		判斷直向或橫向顯示 Vertical 直, Horizontal 橫
		 qustionObject.subject_singlePass: null			單題跳題
		 qustionObject.subject_tips: null				提示
		 qustionObject.subject_title: "開始詢問問卷問題後，您覺得受訪者對您的信任程度如何？ " 題目問題
		 qustionObject.subject_type: "FO"				題型 "S" 單選題,
		 "M" 多選題,
		 "T" 文字輸入題,
		 "FO" 四分題,
		 "FI"五分題,
		 "P" 下拉式清單,
		 "G"群組題
		 */
		var storage = $.localStorage;
		var qustions = storage.get(surveyGuid);
		var qustionsNumber = storage.get(surveyGuid + "_index");

		qustionObject = qustions[index];
		var title = qustionObject.subject_title;
		//處理標題是否有前題的答案顯示。
		var spsubject_title = title.split("<%");
		if (spsubject_title.length == 2) {
			var prevNumber = spsubject_title[1].split(">");
			$.ajax({
				type : "POST",
				async : false,
				url : CI.base_url + "front/getPrevAnswer",
				data : {
					sn : prevNumber[0]
				},
				dataType : 'json'
			}).done(function(data) {

				if (data.result) {
					title = title.replace("<%" + prevNumber[0] + ">", data.anwser);
				} else {
					//showNotify(data.msg, 5000);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});
		}
		
		//取得是否有設定提示
		var helper = '';
		$("#tips").val('');
		if(qustionObject.subject_tips)
		{
			if(qustionObject.subject_tips.length > 0)
			{
				if(qustionObject.subject_tips[0].tips_text != '')
				{
					var tips = '';
					helper = "<a href='javascript:void(0);' class='showTips' style='font-size:10pt;color:blue;'> <i class='fa fa-info-circle'></i>作答說明</a>";
					$.each(qustionObject.subject_tips, function(e) {
						tips = tips + this.tips_text;
					});
					$("#tips").html(tips);
				}
			}
		}
		
		//顯示標題
		setTimeout(function(){
			$("div.question h3.title").html(qustionObject.subject_index + "：" + title + helper);
			if(qustionObject.subject_tips)
			{
				$("a.showTips").hover(function(){
					$("#tips").show();
				},function(){
					$("#tips").hide();
				});
			}
		},200);
		
		var activeTWZipcode = false;
		
		$(document).tooltip({
			content: function () {
				return $(this).prop('title');
			}
		});
		
		//取得答案
		if (qustionObject.subject_type != 'G') {
			//"S" 單選題, "M" 多選題,"T" 文字輸入題, "FO" 四分題, "FI"五分題, "P" 下拉式清單
			//取得選項
			$.ajax({
				type : "GET",
				url : CI.base_url + "getOption?optionguid=" + qustionObject.subject_option,
				dataType : 'json'
			}).done(function(data) {
				
				if (data.Header.Status == 0) {
					//顯示目前題目，
					var options = '';
					var optionsTips = '';
					switch (qustionObject.subject_type) {
						case 'S':
							options += "<ul class='S'>";
							$.each(data.Body.getOption, function(i) {
								options += "<li class='" + qustionObject.subject_optionOutputForm + "'><label><input type='radio' name='answers' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "'> " + this.optionText;
								if(this.optionTips != '' && this.optionTips != null)
								{
									options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
								}
								options += "</label>";
								if (this.optionIsShowTextView == 1)
									options += "<input type='text' class='' value='' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "' >";
								options += "</li>";
							});
							options += "</ul>";
							break;
						case 'M':
							options += "<ul class='M'>";
							$.each(data.Body.getOption, function(i) {
								options += "<li class='" + qustionObject.subject_optionOutputForm + "'><label>" + "<input type='checkbox' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "'> " + this.optionText;
								if(this.optionTips != '' && this.optionTips != null)
								{
									options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
								}
								options += "</label>";
								if (this.optionIsShowTextView == 1)
									options += "<input type='text' class='' value='' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "' >";
								options += "</li>";
							});
							options += "</ul>";
							break;
						case 'T':
							options += "<ul class='T'>";
							$.each(data.Body.getOption, function(i) {
								//console.log(this);
								frontText = backText = '';
								if(this.optionTextViewPosition == 'front')
								{
									frontText = this.optionText;
								}
								else
								{
									backText = this.optionText;
								}
								if(this.optionText == '縣(市)')
								{
									options += "<li class='" + qustionObject.subject_optionOutputForm + "'>" + frontText + " <div data-role='county'></div> " + backText;
									if(this.optionTips != '' && this.optionTips != null)
									{
										options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
									}
									options += "</li>";
									activeTWZipcode = true;
								}
								else if(this.optionText == '鄉(鎮、市、區)' || this.optionText == '區(鄉、鎮、市)')
								{
									options += "<li class='" + qustionObject.subject_optionOutputForm + "'>" + frontText + " <div data-role='district'></div> " + backText;
									if(this.optionTips != '' && this.optionTips != null)
									{
										options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
									}
									options += "</li>";
									activeTWZipcode = true;
								}
								else
								{
									options += "<li class='" + qustionObject.subject_optionOutputForm + "'>" + frontText + " <input type='text' class='' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "' > " + backText;
									if(this.optionTips != '' && this.optionTips != null)
									{
										options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
									}
									options += "</li>";
								}
							});
							options += "</ul>";
							break;
						case 'FO':
							options += "<ul class='FO'>";
							$.each(data.Body.getOption, function(i) {
								options += "<li class='" + qustionObject.subject_optionOutputForm + "'><label>" + this.optionText + "";
								if(this.optionTips != '' && this.optionTips != null)
								{
									options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
								}
								options += "<br/><input type='radio' name='answers' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "'>";
								options += "</label>";
								if (this.optionIsShowTextView == 1)
									options += "<input type='text' class='' value='' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "' >";
								options += "</li>";
							});
							options += "</ul>";
							break;
						case 'FI':
							options += "<ul class='FI'>";
							$.each(data.Body.getOption, function(i) {
								options += "<li class='" + qustionObject.subject_optionOutputForm + "'><label>" + this.optionText + "";
								if(this.optionTips != '' && this.optionTips != null)
								{
									options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
								}
								options += "<br/><input type='radio' name='answers' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "'>";
								options += "</label>";
								if (this.optionIsShowTextView == 1)
									options += "<input type='text' class='' value='' data-value='" + this.optionValue + "' data-index='" + this.optionIndex + "' >";
								options += "</li>";
							});
							options += "</ul>";
							break;
						case 'P':
							options += "<ul class='P'>";
							options += "<li class='" + qustionObject.subject_optionOutputForm + "'>";
							$.each(data.Body.getOption, function(i) {
								var optinosValue = new Array();
								var getOption = this;
								//取得檢物的值，如果為null就進行檢誤值的抓取。
								if(!getOption.optionPickerValueArray)
								{
									$.ajax({
										type : "POST",
										async : false,
										url : CI.base_url + "front/checkHaveSelect",
										data : {
											sn : getOption.optionDyPickerMax
										},
										dataType : 'json'
									}).done(function(data) {
										if (data.result) {
											
											if (data.value) {
												var sp_value = data.value.split(",");
												var sp_max = sp_value[i].split("$");
												if(sp_max.length>1)
												{
													max = sp_max[1];
												}
												else
												{
													max = sp_value[i];
												}
												max = parseInt(max);
												
												
												for (min = parseInt(getOption.optionDyPickerMin); min<=max; min = min + parseInt(getOption.optionDyPickerTick))
												{
													optinosValue[min] = min;
												}
												//console.log(optinosValue);
											} else {
												showNotify(getOption.optionDyPickerSubject+" 尚未填答", 50000);
											}
										} else {
											showNotify(data.msg, 5000);
										}
									}).fail(function(jqXHR, textStatus, errorThrown) {
										//console.log(jqXHR);
										showNotify(jqXHR.responseText, 50000);
									});
								}
								else
								{
									optinosValue = getOption.optionPickerValueArray;
								}
								
								options += "<select data-value='" + getOption.optionValue + "' data-index='" + getOption.optionIndex + "'> ";
								options += "<option value=''>-請選擇-</option>";
								$.each(optinosValue, function(i) {
									options += "<option value='" + this + "'>" + this + "</option>";
								});
								options += "</select>";
								options += getOption.optionText + " ";
								if(this.optionTips != '' && this.optionTips != null)
								{
									options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
								}
							});
							options += "</li>";
							options += "</ul>";
							break;
						default:

					}
					//將答案插入
					$("div.options").html(options);
					
					if(activeTWZipcode)
					{
						$('ul.T').twzipcode({
							css: ['addr-county', 'addr-area', 'addr-zip']
						});
						
					}

					//檢查是否已經有輸入答案。
					checkHaveAnswer(qustionObject);

					//給予checkbox radio input 的處理事件
					answerSelectEvent(qustionObject.multiSelectedLimit, qustionObject.isRandom);

				} else {
					showNotify(data.Header.Doc, 5000);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});
		} else {
			//群組題
			//取得選項
			$.ajax({
				type : "GET",
				url : CI.base_url + "getGroupOption?groupoptionguid=" + qustionObject.subject_groupOption,
				dataType : 'json'
			}).done(function(data) {
				if (data.Header.Status == 0) {
					//顯示目前題目，
					var options = '<ul class="G level0">';
					//console.log(data.Body.getGroupOption);
					//取出type = 0 的大類
					$.each(data.Body.getGroupOption, function(i) {
						if (this.type == 0) {
							//console.log(this);
							/*
							options += "<li>";
							options += "<a href='javascript:void(0);' data-index='" + this.index + "' data-id='" + this.id + "'>" + this.text + "</a>";
							var getSubType = this.index;
							options += "<ul class='level1'>";
							$.each(data.Body.getGroupOption, function(i) {
								if (this.type == getSubType) {
									//console.log(this);
									options += "<li class='Horizontal'><label>" + "<input type='checkbox' data-index='" + this.index + "' data-id='" + this.id + "'> " + this.text;
									options += "</label>";
									if (this.isShowTextView == 1)
										options += "<input type='text' class='' value='' data-index='" + this.index + "' data-id='" + this.id + "'>";
									options += "</li>";

								}
							});
							options += "</ul></li>";
							*/
							options += "<li>";
							options += "<table><tr><td>";
							options += "<a href='javascript:void(0);' data-index='" + this.index + "' data-id='" + this.id + "'>" + this.text + "</a>";
							if(this.optionTips != '' && this.optionTips != null)
							{
								options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
							}
							options += "</td><td>";
							var getSubType = this.index;
							options += "<ul class='level1'>";
							$.each(data.Body.getGroupOption, function(i) {
								if (this.type == getSubType) {
									//console.log(this);
									options += "<li class='Horizontal'><label>" + "<input type='checkbox' data-index='" + this.index + "' data-id='" + this.id + "'> " + this.text;
									options += "</label>";
									if(this.optionTips != '' && this.optionTips != null)
									{
										options += '<a href="javascript:void(0);" class="showOptionTips" title="'+this.optionTips+'" style="font-size:10pt;color:blue;"> <i class="fa fa-info-circle"></i></a>';
									}
									if (this.isShowTextView == 1)
										options += "<input type='text' class='' value='' data-index='" + this.index + "' data-id='" + this.id + "'>";
									options += "</li>";

								}
							});
							options += "</ul></td></tr></table></li>";
						}
					});
					options += '</ul>';

					//將答案插入
					$("div.options").html(options);

					//檢查是否已經有輸入答案。
					checkHaveAnswer(qustionObject);

					//給予checkbox radio input 的處理事件
					answerSelectEvent(qustionObject.multiSelectedLimit, qustionObject.isRandom);

				} else {
					showNotify(data.Header.Doc, 5000);
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});

		}

		//更新目前的題目index
		$("#currentIndex").val(index);

		//var storage = $.localStorage;
		//var qustionsNumber = storage.get(surveyGuid + "_index");

		//修改上下題的題號。
		var prevIndex = index - 1, nextIndex = index + 1;
		nextNumber = qustionsNumber[nextIndex];
		if (index == 0) {
			prevIndex = 0;
			$("button.prev_question").hide();
		}
		else
		{
			$("button.prev_question").show();
		}
		if (islast||nextIndex == qustionsNumber.length) {
			nextIndex = 'done';
			nextNumber = '';
		}

		$("button.prev_question").attr('data-questionindex', prevIndex).attr('data-questionnumber', qustionsNumber[prevIndex]);
		$("button.next_question").attr('data-questionindex', nextIndex).attr('data-questionnumber', nextNumber);

	}

	/**
	 * 處理 checkbox radio input 處理事件
	 */
	function answerSelectEvent(multiSelectedLimit, isRandom) {
		//multiSelectedLimit 複選上限管理後台還沒設定，但預設為1 且先不用做該功能
		//isRandom 亂數排答案內容也先不用做

		//當選了radio，就保留此選項中的文字方塊內容，清除其他選項的input val
		$("div.options input[type='radio']").click(function() {

			if ($(this).parent().parent().find("input[type='text']").length > 0) {
				var currentInputText = $(this).parent().parent().find("input[type='text']");
				$("div.options input[type='text']").not(currentInputText).val('');
			} else {
				$("div.options input[type='text']").val('');
			}

		});

		//要是input text點擊了，就自動選取該選項的radio or checkbox
		$("div.options input[type='text']").click(function() {
			$(this).prev().find("input[type=radio]:not(:checked), input[type=checkbox]:not(:checked)").trigger("click");
		});

		//要是checkbox點擊後，若為取消，那要是裡面有input text 就清除
		$("div.options input[type='checkbox']").click(function() {
			if (!$(this).is(":checked")) {
				$(this).parent().parent().find("input[type='text']").val('');
			}
			//如果選取後，該項目內容是「都沒有」或「以上皆無」就清除其他所有已選的checkbox
			if ($(this).is(":checked")) {
				var optionAnswer = $.trim($(this).parent().text());
				if(optionAnswer == '都沒有' || optionAnswer == '以上皆無' || optionAnswer == '以上皆非')
				{
					//alert("!");
					$("div.options input[type='checkbox']").not($(this)).removeAttr('checked');
				}
				else
				{
					//當不是都沒有的時候，就把都沒有清除掉
					$("div.options label:contains('都沒有')").find('input:checkbox').removeAttr('checked');
					$("div.options label:contains('以上皆無')").find('input:checkbox').removeAttr('checked');
					$("div.options label:contains('以上皆非')").find('input:checkbox').removeAttr('checked');
				}
			}
		});
	}

	/**
	 * 檢查是否已經有輸入答案
	 * qustionObject
	 */
	function checkHaveAnswer(qustionObject) {
		//然後查詢資料表看該樣本該題是否已經有填值，有就設定哪些答案已選取
		$.ajax({
			type : "POST",
			url : CI.base_url + "front/checkHaveSelect",
			data : {
				sn : qustionObject.subject_index
			},
			dataType : 'json'
		}).done(function(data) {
			if (data.result) {
				//console.log(data);
				if (data.value) {
					var sp_answer = data.value.split(',');
					var getElm, sp_t;
					switch (qustionObject.subject_type) {
						case 'S':
							//單一答案
							//判別是否為文字方塊的答案
							if (sp_answer[0].substr(0, 5) == 'T:88:')
								sp_t = sp_answer[0].split(':88:');
							else
								sp_t = sp_answer[0].split(':');
							if (sp_t.length == 2) {
								getElm = $("div.options input:text[data-value='88']");
								getElm.val(sp_t[1]);
								getElm.parent().find("input:radio").attr('checked', 'checked');
							} else {
								$("div.options input:radio:eq(" + (sp_answer[0] - 1) + ")").attr('checked', 'checked');
							}
							break;
						case 'M':
							//可能兩個以上
							//下拉可能兩個以上
							for (var key in sp_answer) {
								var sp = sp_answer[key].split("$");
								if (sp[1].substr(0, 5) == 'T:88:')
									sp_t = sp[1].split(':88:');
								else
									sp_t = sp[1].split(':');

								if (sp_t.length == 2) {
									getElm = $("div.options input[data-index='" + sp[0] + "']");
									getElm.attr('checked', 'checked');
									getElm.parent().parent().find("input:text").val(sp_t[1]);
								} else {
									$("div.options input[data-index='" + sp[0] + "']").attr('checked', 'checked');
								}
							}
							break;
						case 'T':
							//可能兩個以上
							if($("div.options select").length > 0)
							{
								for (var key in sp_answer) {
									$("div.options select:eq(" + key + ")").val(sp_answer[key]).trigger('change');
								}
							}
							else
							{
								for (var key in sp_answer) {
									$("div.options input:text:eq(" + key + ")").val(sp_answer[key]);
								}
							}
							
							break;
						case 'FO':
							//單一答案
							$("div.options input:radio[data-value='" + sp_answer[0] + "']").attr('checked', 'checked');
							break;
						case 'FI':
							//單一答案
							$("div.options input:radio[data-value='" + sp_answer[0] + "']").attr('checked', 'checked');
							break;
						case 'P':
							//下拉可能兩個以上
							$.each(sp_answer, function(i) {
								var sp = this.split("$");
								//console.log($("div.options select:eq("+i+")").find("option[value='"+sp[1]+"']"));
								//$("div.options select:eq(" + i + ")").find("option[value='" + sp[1] + "']").attr('selected', 'selected');
								$("div.options select:eq(" + i + ")").val(sp[1]).trigger('change');
							});
							break;
						case 'G':
							//可能兩個以上
							for (var key in sp_answer) {
								var sp = sp_answer[key].split("$");
								if (sp[1].substr(0, 5) == 'T:88:')
									sp_t = sp[1].split(':88:');
								else
									sp_t = sp[1].split(':');

								if (sp_t.length == 2) {
									getElm = $("div.options input:text");
									getElm.val(sp_t[1]);
									getElm.prev().find("input:checkbox").attr('checked', 'checked');
								} else {
									$("div.options input[data-index='" + sp[0] + "'][data-id='" + sp[1] + "']").attr('checked', 'checked');
								}
							}
							break;
						default:

					}
				}
			} else {
				//showNotify(data.msg, 5000);
			}
			//顯示答案項目
			$("div.options").css({
				'filter' : 'alpha(opacity=100)',
				'-moz-opacity' : 1,
				'opacity' : 1
			});
			/*
			$("div.options").animate({
				'filter' : 'alpha(opacity=100)',
				'-moz-opacity' : 1,
				'opacity' : 1,
			}, 400);*/
		}).fail(function(jqXHR, textStatus, errorThrown) {
			//console.log(jqXHR);
			showNotify(jqXHR.responseText, 50000);
		});
	}

	/**
	 * 清圖除題中間空白的答案，設為空字串
	 */
	function clearAnswer(surveyGuid, startIndex, endIndex) {
		var storage = $.localStorage;
		var qustionsNumber = storage.get(surveyGuid + "_index");
		var clearNumbers = new Array();
		if (endIndex - startIndex == 0) {
			clearNumbers[0] = qustionsNumber[startIndex];
		} else {
			var i = 0;
			for (startIndex; startIndex <= endIndex; startIndex++) {
				clearNumbers[i] = qustionsNumber[startIndex];
				i++;
			}
		}
		if(clearNumbers.length > 0)
		{
			$.ajax({
				type : "POST",
				url : CI.base_url + "front/clearJumpAnswer",
				data : {c: clearNumbers},
				dataType : 'json'
			}).done(function(data) {
				
			}).fail(function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR);
				showNotify(jqXHR.responseText, 50000);
			});
		}
	}

	/**
	 * 顯示完成作答
	 * showEnd
	 */
	function showEnd() {
		//記錄最後時間
		saveSurveySituation('done',false);
		$("div.question, div.nav_buttons").hide();
		$("div.ending").show();
		$("a.endSurvey").click(function(){
			$(window).unbind('beforeunload');
			//跳回列表
			history.go(-2);
			//saveSurveySituation('done');
			//return false;
		});
	}

	/**
	 * 離開作答頁面的警告提醒
	 */
	if ($("#answer").length == 1) {
		$(window).bind('beforeunload', function(e) {
			return '正在作答中，確定要離開？';
		});

		$("a.intoAnswer, a.stopAnswer, a.stopSurvey, button.stopSurvey").click(function() {
			$(window).unbind('beforeunload');
		});
	}

});

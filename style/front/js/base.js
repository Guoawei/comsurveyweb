/**
 * @author 蔡 孟珂 sweea.com
 */

var template='theme/v1';

/**
 * 取得網址列的query key 跟值
 * 先 var get=getUrlQuery();
 * 在此使用 get['getkey']
 */
function getUrlQuery(){
	var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
    }
    return vars;
}

/**
 * 產生亂數密碼
 * 
 */
function generator(plength){
	var keylist="abcdefghijklmnopqrstuvwxyz123456789";
	var temp='';
	for (i=0;i<plength;i++){
		temp+=keylist.charAt(Math.floor(Math.random()*keylist.length))
	}
    return temp;
}

/*
 * 將文字轉成 url
 */
function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
}

/*
 * 解決 sortable 表格格式跑掉問題
 */
var fixHelper = function(e, ui)
{
	ui.children().each(function()
	{
		$(this).width($(this).width());
	});
	return ui;
};

$(document).ready(function(){
	//執行瀏覽器偵測
	BrowserDetect.init();
	/**
	 * 給ie的瀏覽器也有placeholder效果。
	 */
	/*
	if(BrowserDetect.browser=='Explorer'){
		$('input, textarea').each(function(){
			var get_placeholder=$(this).attr('placeholder');
			if($(this).val()==''){
				if(get_placeholder){
					$(this).val(get_placeholder).css('color','#AAA');
					$(this).focus(function(){
						if($(this).val()==get_placeholder){
							$(this).val('').css('color','#000');
						}
					});
					$(this).blur(function(){
						if($(this).val()==get_placeholder||$(this).val()==''){
							$(this).val(get_placeholder).css('color','#AAA');
						}
					});
				}
			}
		});
	}*/
		
	/**
	 * 初始通知功能
	 * 顯示通知 showNotify("<h2>123</h2>",5000);
	 * showNotify("html或字串",毫秒數); 5000等於5秒
	 * 
	 */
	ininotify();	
	if((BrowserDetect.browser=='Explorer'&&BrowserDetect.version<=7)||(BrowserDetect.browser=='Opera'&&BrowserDetect.version<10.5)){
		var browser = BrowserDetect.browser;
		if(browser == 'Explorer')
			browser = 'IE';
		var msg="本系統不支援您目前使用的<br/>"+browser+" "+BrowserDetect.version+
				" 瀏覽器<br/>請更新為最新版本，或改使用<a href='http://www.google.com/chrome' target='_blank'>Google Chrome</a>、"+
				"<a href='http://www.apple.com/tw/safari' target='_blank'>Safari</a>、"+
				"<a href='http://moztw.org/firefox' target='_blank'>Firefox</a>等瀏覽器。";
		showNotify(msg,20000);
	}
	
});

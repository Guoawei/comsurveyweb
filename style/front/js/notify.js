/*
 Project:
 vision: V1.0.0
 Program:

 Creat Date:
 Release Date:
 Aauthor: MKTsai
 E-mail: mktsai@sweea.com

 history:

 Copyright (c) 2012 Sweea.com All rights reserved.
 */

/*
 * 插入元素
 */
function ininotify(){
	var notifyhtml='<div id="notify"><div id="notifyInfo"><span></span><a href="javascript:void(0);" class="closeX">X</a></div></div>';
	$("body").append(notifyhtml);
	
	//關閉通知
	$('a.closeX').click(function() {
		closeNotify();
	});
}

/*
 * 顯示通知
 */
function showNotify(info, time) {
	info = info || '';
	time = parseInt(time) || null;
	$("#notifyInfo span").html(info);
	$("#notify").fadeIn();
	if(time) {
		setTimeout(function() {
			$("#notify").fadeOut();
		}, time);
	}
}

/*
 * 關閉通知
 */
function closeNotify() {
	$("#notify").fadeOut();
}




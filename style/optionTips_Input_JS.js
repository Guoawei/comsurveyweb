/**
 * Created by Awei on 2014/4/24.
 * modify by Will on 2014/4/26
 */
var optionTipsID = "";
$(function() {
	var optionTips = $("#optionTips"), tips = $(".validateTips");

	function updateTips(t) {
		tips.text(t).addClass("ui-state-highlight");
		setTimeout(function() {
			tips.removeClass("ui-state-highlight", 1500);
		}, 500);
	}


	$("#dialog-form").dialog({
		autoOpen : false,
		height : 250,
		width : 360,
		modal : true,
		buttons : {
			"確定" : function() {
				var inputID = "#" + optionTipsID;
				var buttonID = "#" + "createButton-" + optionTipsID;
				$(inputID).val(optionTips.val());
				checkHasTips(buttonID);

				$(this).dialog("close");
			},
			"取消" : function() {
				$(this).dialog("close");
			}
		}
	});

	function checkHasTips(buttonID) {
		if (optionTips.val()) {
			$(buttonID).text('有提示✔');
		} else {
			$(buttonID).text('輸入提示');
		}
	}

});

function showCreateOptionTipsView(aOptionTipsID) {
	optionTipsID = aOptionTipsID;
	var tempInputID = "#" + aOptionTipsID;
	$("#optionTips").val($(tempInputID).val());
	$("#dialog-form").dialog("open");
	return false;
}
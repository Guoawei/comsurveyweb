/**
 * Created with JetBrains PhpStorm.
 * User: Awei
 * Date: 13/6/7
 * Time: 上午11:03
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function() {
    hideAction()
    $("#selectAll").click(function() {
        if($("#selectAll").attr("checked"))
        {
            $("input[name='sampleSelected[]']").each(function() {
                $(this).attr("checked", true);
            });
            showAction();
        }
        else
        {
            $("input[name='sampleSelected[]']").each(function() {
                $(this).attr("checked", false);
            });
            hideAction();
        }
    });

    $("input[name='sampleSelected[]']").click(function() {
        $("input[name='sampleSelected[]']").each(function() {
            if($(this).attr("checked"))
            {
                showAction();
                return false;
            }else {
                hideAction();
            }
        });
    });

    function showAction(){
        $("#showOrHideTop").show();
        $("#showOrHideBottom").show();
    }

    function hideAction(){
        $("#showOrHideTop").hide();
        $("#showOrHideBottom").hide();
    }

});

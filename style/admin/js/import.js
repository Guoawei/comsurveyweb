/**
 * @author 斯威亞資訊科技 蔡孟珂
 */

$(document).ready(function() {
	//上傳csv
	upload_path = 'csv/upload';
	$("#upload_csv").uploadify(
	{
		buttonText: '上傳csv並匯入',
		height: 24,
		width: 120,
		fileTypeDesc: 'csv 檔',
		fileTypeExts: "*.csv",
		swf: CI.base_url + 'style/uploadify/uploadify.swf',
		checkExisting: CI.base_url + 'uploader/uploadCheckExists/?targetFolder=' + upload_path,
		uploader: CI.base_url + 'uploader/uploadify?not_change_name=1',
		method: 'post',
		formData:
		{
			'targetFolder': upload_path
		},
		auto: true,
		multi: false,
		removeTimeout: 0,
		removeCompleted: true,
		onUploadStart: function(){
			$("div.result_msg").html("上傳中<img src='"+CI.base_url+"style/uploadify/images/ajax-loader.gif'/>");
		},
		onUploadSuccess: function(file, data, response)
		{
			var getdata = $.parseJSON(data);
			if (getdata.result)
			{
				//$("img.photo").attr("src", CI.base_url + getdata.original);
				$("div.result_msg").html("處理中<img src='"+CI.base_url+"style/uploadify/images/ajax-loader.gif'/>");
				
				//處理上傳的檔案
				//刪除照片檔案
				$.ajax({
					type : "POST",
					async: false,
					url : CI.base_url + "samples/doSampleImport",
					data : {
						file: getdata.original
					},
					dataType : 'json'
				}).done(function(data) {
					$("div.result_msg").html(data.msg);
					//console.log(data.msg);
				}).fail(function(jqXHR, textStatus, errorThrown) {
					console.log(jqXHR.responseText);
				});
			}
			else
			{
				showNotify(getdata.msg, 5000);
			}
		}
	});

});
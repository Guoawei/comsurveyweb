/**
 * @author 蔡孟珂 2014/4/26
 */

$(document).ready(function(){
	$("a.delete_result").click(function(){
		if(confirm('你確定要刪除所有以做的訪問答案嗎？將會無法復原唷。'))
		{
			$.post($(this).attr('href'),{},function(data){
				alert(data.msg);
			},'json');
		}
		return false;
	});
});

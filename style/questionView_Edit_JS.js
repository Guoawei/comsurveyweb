/**
 * Created with JetBrains PhpStorm.
 * User: Awei
 * Date: 13/3/27
 * Time: 上午8:07
 * To change this template use File | Settings | File Templates.
 */
var sSn = 1;
var mSn = 1;
var tSn = 1;
var pSn = 1;
var gSn = 1;
var gSub_Sn = 1;
var singlePassSn = 1;
var groupPassSn = 1;
var dyPassSn = 1;

$(document).ready(function() {
    detectType();
    detectSinglePassLogic();
    detectGroupPassLogic();
    detectDyPassLogic();

    });

function closeTypeDiv (){
    $("#type-S-div").fadeOut('fast');
    $("#type-M-div").fadeOut('fast');
    $("#type-P-div").fadeOut('fast');
    $("#type-G-div").fadeOut('fast');
    $("#type-T-div").fadeOut('fast');
    $("#type-FO-div").fadeOut('fast');
    $("#type-FI-div").fadeOut('fast');
    $('#singleassLogic-div').fadeOut('fast');
    $('#groupPassLogic-div').fadeOut('fast');
    $('#dyPassLogic-div').fadeOut('fast');
    $('#pass-div').fadeOut('fast');
    }

function detectType () {
    // document.write('test');
    // $("#isOptionPassLogic").attr("checked",false);
    closeTypeDiv();
    var selectValue = $("#q_type").val();
    var originalSelectValue = $("#q_o_type").val();
    $("#option-div").fadeIn('fast');
switch(selectValue){
    case 'S':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'S'){
            $("#type-S-div").fadeIn('fast');
            $('#type-S-column-div').remove();
            $('#addSButton').remove();
            $("#type-S-div").append('<div id="type-S-column-div"></div><input type="BUTTON" value="新增" id="addSButton" name="addSButton" onClick="addSOption();">');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
            sSn = 0;
            addSOption();
        }else {
            $("#type-S-div").fadeIn('fast');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
            sSn = $("#s_sn").val();
        }
    break;
    case 'M':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'M'){
            $("#type-M-div").fadeIn('fast');
            $('#type-M-column-div').remove();
            $('#addMButton').remove();
            $("#type-M-div").append('<div id="type-M-column-div"></div><input type="BUTTON" value="新增" id="addMButton" name="addMButton" onClick="addMOption();">');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
            mSn = 0;
            addMOption();
        }else {
            $("#type-M-div").fadeIn('fast');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
            mSn = $("#m_sn").val();
        }
        break;
    case 'P':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'P'){
            $("#type-P-div").fadeIn('fast');
            $('#type-P-column-div').remove();
            $('#addPButton').remove();
            $("#type-P-div").append('<div id="type-P-column-div"></div><input type="BUTTON" value="新增" id="addPButton" name="addPButton" onClick="addPOption();">');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
            pSn = 0;
            addPOption();

        }else {
            $("#type-P-div").fadeIn('fast');
            //$("#type-P-column-div").after('<input type="BUTTON" value="新增" id="addPButton" name="addPButton" onClick="addPOption();">');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
            pSn = $("#p_sn").val();
        }
        break;
    case 'G':
        $('#singlePass-div').fadeOut('fast');
        $('#groupPass-div').fadeOut('fast');
        if(originalSelectValue != 'G'){
            $("#type-G-div").fadeIn('fast');
            $('#type-G-column-div').remove();
            $('#addGButton').remove();
            $("#type-G-div").append('<div id="type-G-column-div"></div><input type="BUTTON" value="新增主分類" id="addGButton" name="addGButton" onClick="addGOption();">');
            gSn = 0;
            detectDyPassLogic();
            addGOption();

        }else {
            $("#type-G-div").fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');

            gSn = $("#g_sn").val();
        }
        break;
    case 'T':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'T'){
            $("#type-T-div").fadeIn('fast');
            $('#type-T-column-div').remove();
            $('#addTButton').remove();
            $("#type-T-div").append('<div id="type-T-column-div"></div><input type="BUTTON" value="新增" id="addTButton" name="addTButton" onClick="addTOption();">');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
            tSn = 0;
            addTOption();
        }else {
            $("#type-T-div").fadeIn('fast');
            //$('#type-T-column-div').after('<input type="BUTTON" value="新增" id="addTButton" name="addTButton" onClick="addTOption();">');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
            tSn = $("#t_sn").val();
        }
        break;
    case 'FO':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'FO'){
            $("#type-FO-div").fadeIn('fast');
            $('#type-FO-column-div').remove();
            $("#type-FO-div").append('<div id="type-FO-column-div"><input type="text" id="optionAreaFO1" name="optionAreaFO1">' +
            '<input type="text" id="optionAreaFO2" name="optionAreaFO2">' +
            '<input type="text" id="optionAreaFO3" name="optionAreaFO3">' +
            '<input type="text" id="optionAreaFO4" name="optionAreaFO4"></div>');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
        }else {
            $("#type-FO-div").fadeIn('fast');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
        }
        break;
    case 'FI':
        $('#singlePass-div').fadeIn('fast');
        $('#groupPass-div').fadeIn('fast');
        if(originalSelectValue != 'FI'){
            $("#type-FI-div").fadeIn('fast');
            $('#type-FI-column-div').remove();
            $("#type-FI-div").append('<div id="type-FI-column-div"><input type="text" id="optionAreaFI1" name="optionAreaFI1">' +
            '<input type="text" id="optionAreaFI2" name="optionAreaFI2">' +
            '<input type="text" id="optionAreaFI3" name="optionAreaFI3">' +
            '<input type="text" id="optionAreaFI4" name="optionAreaFI4">' +
            '<input type="text" id="optionAreaFI5" name="optionAreaFI5"></div>');
            detectSinglePassLogic();
            detectGroupPassLogic();
            detectDyPassLogic();
        }else {
            $("#type-FI-div").fadeIn('fast');
            $('#singlePass-div').fadeIn('fast');
            $('#groupPass-div').fadeIn('fast');
            $('#dyPassLogic').fadeIn('fast');
         }
        break;
    }
}

    function detectSinglePassLogic(){
        if ($("#isSinglePassLogic").attr("checked")){
            $("#singlePassLogic-div").fadeIn('medium');
            singlePassSn = $("#singlePass_sn").val();
        }else {
            $("#singlePassLogic-div").fadeOut('medium');
        }
    }

    function detectGroupPassLogic(){
        if ($("#isGroupPassLogic").attr("checked")){
            $("#groupPassLogic-div").fadeIn('medium');
            groupPassSn = $("#groupPass_sn").val();
            var answerArray = $('#groupPassAnswerArrayFormat').val();
            for(var i=1;i<=groupPassSn;i++){
                checkGroupPassAnswer(i,answerArray);
            }
        }else {
            $("#groupPassLogic-div").fadeOut('medium');
        }
    }

    function detectDyPassLogic(){
        if ($("#isDyPassLogic").attr("checked")){
            $("#dyPassLogic-div").fadeIn('medium');
            dyPassSn = $("#dyPass_sn").val();
            var answerArray = $('#dyPassAnswerArrayFormat').val();
            for(var i=1;i<=dyPassSn;i++){
                checkDyPassAnswer(i,answerArray);
            }
        }else {
            $("#dyPassLogic-div").fadeOut('medium');
        }
    }

    function detectIsSelectShowTextViewCheckBox(type,sn) {
        typeSn = type + sn;
        if ($("#optionIsShowTextViewCKBox"+typeSn).attr("checked")){
        $("#optionValue"+typeSn).val('88');
        $("#optionValue"+typeSn).prop("readonly", true);
        }else {
        $("#optionValue" + typeSn).val(sn);
        $("#optionValue"+typeSn).removeAttr("readonly");
        }
    }

    function detectGTypeIsSelectShowTextViewCheckBox(mainSn,subSn) {
        typeSn = mainSn +'_'+ subSn;
        if ($("#optionIsShowTextViewCKBoxG"+typeSn).attr("checked")){
            $("#optionValueG"+typeSn).val('88');
            $("#optionValueG"+typeSn).prop("readonly", true);
        }else {
            $("#optionValueG"+typeSn).val('');
            $("#optionValueG"+typeSn).removeAttr("readonly");
        }
    }

    function addSOption(){
        if (sSn>=100) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            sSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+sSn+'">' +
            '選項'+sSn+'<input type="text" size="60" id="optionAreaS'+sSn+'" name="optionAreaS'+sSn+'">' +
            ' 選項值<input type="text" size="3" id="optionValueS'+sSn+'" name="optionValueS'+sSn+'" value="'+sSn+'">' +
            ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxS'+sSn+'" name="optionIsShowTextViewCKBoxS'+sSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'S'"+','+sSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+sSn+');">   x</a>';
            tipsButton = '<input id="optionS-Tips'+sSn+'" type="hidden" name="optionS-Tips'+sSn+'" value="">' +
            ' <button id="createButton-optionS-Tips'+sSn+'" onclick="return showCreateOptionTipsView(\'optionS-Tips'+sSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(sSn > 1){
                resultAppend = appendForm + deleteOptionLink + tipsButton + divEnd;
            }else {
                resultAppend = appendForm + tipsButton + divEnd;
            }
            $("#type-S-column-div").append(resultAppend);
        }
    }

    function addMOption(){
        if (mSn>=100) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            mSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+mSn+'">' +
            '選項'+mSn+'<input type="text" size="60" id="optionAreaM'+mSn+'" name="optionAreaM'+mSn+'">'+
            ' 選項值<input type="text" size="3" id="optionValueM'+mSn+'" name="optionValueM'+mSn+'" value="'+mSn+'">'+
            ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxM'+mSn+'" name="optionIsShowTextViewCKBoxM'+mSn+'" value=1 onchange="detectIsSelectShowTextViewCheckBox('+"'M'"+','+mSn+');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+mSn+');">   x</a>';
            tipsButton = '<input id="optionM-Tips'+mSn+'" type="hidden" name="optionM-Tips'+mSn+'" value="">' +
                         ' <button id="createButton-optionM-Tips'+mSn+'" onclick="return showCreateOptionTipsView(\'optionM-Tips'+mSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(mSn > 1){
                resultAppend = appendForm + deleteOptionLink + tipsButton + divEnd;
            }else {
                resultAppend = appendForm + tipsButton +divEnd;
            }
            $("#type-M-column-div").append(resultAppend);
        }
    }

    function addTOption(){
        if (tSn>=10) {
        //$("#error_msg").fadeIn('medium');
        //$("#error_msg").text("分類限制20以下");
        }else {
            tSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+tSn+'">選項'+tSn+'<input type="text" size="60" id="optionAreaT'+tSn+'" name="optionAreaT'+tSn+'">';
            textViewPosition = '答案描述位置在<select id="optionTextViewPosition'+tSn+'" name="optionTextViewPosition'+tSn+'"><option value="back" selected="selected">前</option><option value="front">後</option></select>';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+tSn+');">   x</a>';
            tipsButton = '<input id="optionT-Tips'+tSn+'" type="hidden" name="optionT-Tips'+tSn+'" value="">' +
                         ' <button id="createButton-optionT-Tips'+tSn+'" onclick="return showCreateOptionTipsView(\'optionT-Tips'+tSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(tSn > 1){
                resultAppend = appendForm + textViewPosition + deleteOptionLink + tipsButton + divEnd;
            }else {
                resultAppend = appendForm + textViewPosition + tipsButton + divEnd;
            }
            $("#type-T-column-div").append(resultAppend);
        }
    }

    function addPOption(){
        surveyGuid = $("#p_surveyGuid").val();
        subjectGuid = $("#p_subjectGuid").val();
        if (pSn>=10) {
        //$("#error_msg").fadeIn('medium');
        //$("#error_msg").text("選項限制10以下");
        }else {
            pSn++;
            appendForm = '<div class="optionForm-div" id="optionForm-div'+pSn+'">選項'+pSn+'<input type="text" size="50" id="optionAreaP'+pSn+'" name="optionAreaP'+pSn+'">'+
            ' 範圍:<input type="text" size="2" id="type-P-value-start'+pSn+'" name="type-P-value-start'+pSn+'" >'+
            ' ~<div id="dyPickerSetting'+pSn+'" style="display:inline"><input type="text" size="2" id="type-P-value-end'+pSn+'" name="type-P-value-end'+pSn+'"></div>'+
            ' 間隔:<input type="text" size="2" id="type-P-value-tick'+pSn+'" name="type-P-value-tick'+pSn+'">'+
            ' 檢誤功能<input type="checkbox" id="isDyPickerCheck'+pSn+'" name="isDyPickerCheck'+pSn+'" value="1" onchange="checkDyPicker(\''+surveyGuid+'\',\''+subjectGuid+'\',\''+pSn+'\',\'\');">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeOption('+pSn+');">   x</a>';
            tipsButton = '<input id="optionP-Tips'+pSn+'" type="hidden" name="optionP-Tips'+pSn+'" value="">' +
                         ' <button id="createButton-optionP-Tips'+pSn+'" onclick="return showCreateOptionTipsView(\'optionP-Tips'+pSn+'\')";>輸入提示</button>';
            divEnd = '</div>';
            if(pSn > 1){
                resultAppend = appendForm + deleteOptionLink + tipsButton + divEnd;
            }else {
                resultAppend = appendForm + tipsButton + divEnd;
            }
            $("#type-P-column-div").append(resultAppend);
        }

    }

    function addGOption(){
        if (gSn>20) {
            $("#error_msg").fadeIn('medium');
            $("#error_msg").text("分類限制20以下");
        }else {
            gSn++;
            appendForm =
                '<div id="type-G-Area'+gSn+'-div">'+
                '<br>主分類'+gSn+'：<input type="text" id="type-G-main-group'+gSn+'" name="type-G-main-group'+gSn+'"></input>' +
                '<input type="BUTTON" value="新增選項" name="addGSubButton" onClick="addGSubOption('+gSn+');">'+
                '<input id="optionGMain-Tips'+gSn+'" type="hidden" name="optionGMain-Tips'+gSn+'" value="">' +
                '<button id="createButton-optionGMain-Tips'+gSn+'" onclick="return showCreateOptionTipsView(\'optionGMain-Tips'+gSn+'\')";>輸入提示</button>'+
                '<a class="deleteOptionLink" href="javascript:removeGMainOption('+gSn+');">   x </a>'+
                '<br/>'+
                '<div class="optionLogicG-div" id="optionLogicG_'+gSn+'-div">'+
                '<div class="optionFormG-div" id="optionFormG_'+gSn+'_1-div">'+
                '選項'+gSub_Sn+'： '+'<input type="text" size="50" id="optionAreaG'+gSn+'_1" name="optionAreaG'+gSn+'_1">'+
                ' 選項值 <input type="text" size="3" id="optionValueG'+gSn+'_1" name="optionValueG'+gSn+'1" value="">'+
                ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxG'+gSn+'_1" name="optionIsShowTextViewCKBoxG'+gSn+'_1" value=1 onchange="detectGTypeIsSelectShowTextViewCheckBox('+gSn+',1);">'+
                '<input id="optionGSub-Tips'+gSn+'_1" type="hidden" name="optionGSub-Tips'+gSn+'_1" value="">' +
                ' <button id="createButton-optionGSub-Tips'+gSn+'_1" onclick="return showCreateOptionTipsView(\'optionGSub-Tips'+gSn+'_1\')";>輸入提示</button>';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeGSubOption(\''+gSn+'_1\');">   x</a></br>';
            hiddenComponet = '<input type="hidden" id="g_sub_sn'+gSn+'" name="g_sub_sn'+gSn+'" value="1" />'
            divEnd = '</div>'+hiddenComponet+'</div></div>';
            resultAppend = appendForm+divEnd;
            $("#type-G-column-div").append(resultAppend);
        };

    }

    function addGSubOption(inputGsn){
        gSub_Sn = $("#g_sub_sn"+inputGsn).val();
        gSub_Sn++;
        serialString = inputGsn+'_'+gSub_Sn;
        appendForm = '<div class="optionFormG-div" id="optionFormG_'+serialString+'-div">'+
           '選項'+gSub_Sn+'： '+'<input type="text" size="50" id="optionAreaG'+serialString+'" name="optionAreaG'+serialString+'">'+
            ' 選項值 <input type="text" size="3" id="optionValueG'+serialString+'" name="optionValueG'+serialString+'" value="">'+
            ' 顯示輸入框<input type="checkbox" id="optionIsShowTextViewCKBoxG'+serialString+'" name="optionIsShowTextViewCKBoxG'+serialString+'" value=1 onchange="detectGTypeIsSelectShowTextViewCheckBox('+inputGsn+','+gSub_Sn+');">';
        deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeGSubOption(\''+serialString+'\');">   x</a>';
        tipsButton = '<input id="optionGSub-Tips'+serialString+'" type="hidden" name="optionGSub-Tips'+serialString+'" value="">' +
            ' <button id="createButton-optionGSub-Tips'+serialString+'" onclick="return showCreateOptionTipsView(\'optionGSub-Tips'+serialString+'\')";>輸入提示</button>';
        divEnd = '</div>';
        resultAppend = appendForm + tipsButton + deleteOptionLink + divEnd;
        $("#optionLogicG_"+inputGsn+"-div").append(resultAppend);
        $("#g_sub_sn"+inputGsn).val(gSub_Sn);
    }

    function removeOption(sn){
        $("#optionForm-div" + sn).remove();
        sSn--;
        mSn--;
        tSn--;
        pSn--;
    }

    function removeGMainOption(sn){
        $("#type-G-Area"+sn+"-div").remove();
        gSn--;
    }

    function removeGSubOption(snText){
        $("#optionFormG_"+snText+"-div").remove();
    }

    function removeSinglePassOption(sn){
        $("#singlePass-conditionalForm-div" + sn).remove();
        singlePassSn--;
    }

    function removeGroupPassOption(sn){
        $("#groupPass-conditionalForm-div" + sn).remove();
        groupPassSn--;
    }

    function removeDyPassOption(sn){
        $("#dyPass-conditionalForm-div" + sn).remove();
        dyPassSn--;
    }

    function submitCheck() {
        var validate = true;
        var msg = "";

        //判斷空白未填
        if($("#q_subjectNumber").val()==""){
            validate = false;
            msg = "請輸入題號";
        }else if($("#q_subject").val()==""){
            validate = false;
            msg = "請輸入標題";
        }else if($("#q_type").val()=="Empty"){
            validate = false;
            msg = "請設定題目類型";
        }else if($("#isSinglePassLogic").attr("checked")){
            for(var i=0;i<=singlePassSn;i++){
                if($('#passAnswer'+i).val()==""){
                    validate = false;
                    msg = "請輸入單一邏輯跳題選項值";
                }
            }
        }else if($("#isGroupPassLogic").attr("checked")){
            var loadGroupPassSn = $("#groupPass_sn").val();
            if (groupPassSn < 2 ){
                validate = false;
                msg = "多邏輯跳題設定最少設定2個條件";
            }
        }

        //判斷選項空白
        var selectValue = $("#q_type").find(":selected").val();
        switch(selectValue){
            case 'FO':
            var option1 = $("#optionAreaFO1").val();
            var option2 = $("#optionAreaFO2").val();
            var option3 = $("#optionAreaFO3").val();
            var option4 = $("#optionAreaFO4").val();
            if (option1 == "" || option2 == "" || option3 == "" || option4 == ""){
                validate = false;
                msg = "請完整輸入4則選項";
            }
        break;
        case 'FI':
            var option1 = $("#optionAreaFI1").val();
            var option2 = $("#optionAreaFI2").val();
            var option3 = $("#optionAreaFI3").val();
            var option4 = $("#optionAreaFI4").val();
            var option5 = $("#optionAreaFI5").val();
            if (option1 == "" || option2 == "" || option3 == "" || option4 == "" || option5 == ""){
                validate = false;
                msg = "請完整輸入5則選項";
            }
        break;
        case 'S':
            for (i=1;i<=sSn;i++){
                if($("#optionAreaS"+i).val()=="" || $("#optionValueS"+i).val()==""){
                validate = false;
                msg = "請完整輸入選項"+i+"的描述";
                }
            }
            break;
            case 'M':
                for (i=1;i<=mSn;i++){
                    if($("#optionAreaM"+i).val()=="" || $("#optionValueM"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
            break;
            case 'T':
                for (i=1;i<=tSn;i++){
                    if($("#optionAreaT"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"的描述";
                    }
                }
                break;
            case 'P':
                for (i=1;i<=pSn;i++){
                    if($("#optionAreaP"+i).val()=="" || $("#type-P-value-start"+i).val()=="" || $("#type-P-value-end"+i).val()=="" || $("#type-P-value-tick"+i).val()==""){
                        validate = false;
                        msg = "請完整輸入選項"+i+"，的描述、範圍與間隔";
                    }
                }
                break;
        }

        if (validate){
            $("#form").submit();
        }else {
            $("#error_msg").fadeIn('medium');
            $("#error_msg").text(msg);
        }
//        $("#form").submit();
		return validate;
	}

    function checkOnlyNumber(o) {
        v=o.value.replace(/^\s+|\s+$/,''); // remove any whitespace
        if(o=='') {
            return;
        }
        v=v.substr(v.length-1);
        if(v.match(/\d/g)==null) {
            o.value=o.value.substr(0,o.value.length-1).replace(/^\s+|\s+$/,'');
        }
    }

    function addSinglePassOption() {
        var minimunOption = 1;
        if (singlePassSn>=10) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            singlePassSn++;
            appendForm = '<div id="singlePass-conditionalForm-div'+singlePassSn+'" >'+
                '<H5>條件'+singlePassSn+'<input type="text" id="passAnswer'+singlePassSn+'" name="passAnswer'+singlePassSn+'" value = "" onkeyup="checkOnlyNumber(this)" onbeforepaste="checkOnlyNumber(this)">'+
                ' 跳題至： <select id="singleJumpToSubjectNumber'+singlePassSn+'" name="singleJumpToSubjectNumber'+singlePassSn+'" width="">'+
                '<option value="" >' +
                '</option></select>';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeSinglePassOption('+singlePassSn+');">   x</a></H5>';
            divEnd = '</div>';
            if(singlePassSn > minimunOption){
                resultAppend = appendForm + deleteOptionLink + divEnd;
            }else {
                resultAppend = appendForm + divEnd;
            }
            $('#singlePass-ConditionalArea-div').append(resultAppend);

            //Clone Option to new select
            $('#singleJumpToSubjectNumber1 option').clone().appendTo('#singleJumpToSubjectNumber'+singlePassSn);

        }
    }

    function addGroupPassOption(){
        var minimunOption = 2;
        if (groupPassSn>=10) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            groupPassSn++;
            appendForm = '<div id="groupPass-conditionalForm-div'+groupPassSn+'" >' +
                '<p><H5>條件'+groupPassSn+' <select id="groupPassSubjectNumber'+groupPassSn+'" name="groupPassSubjectNumber'+groupPassSn+'" width="" onchange="getGroupPassAnswer('+groupPassSn+');">'+
                '<option value="" ></option></select>'+
                ' 觸發選項'+' <select id="groupPassAnswerNumber'+groupPassSn+'" name="groupPassAnswerNumber'+groupPassSn+'" width="">' +
                    '<option value="<?php echo $questionRow->getSubjectNumber(); ?>" >' +
                '</option></select>';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeGroupPassOption('+groupPassSn+');">   x</a></H5></p>';
            divEnd = '</div>';
            if(groupPassSn > minimunOption){
                resultAppend = appendForm + deleteOptionLink + divEnd;
            }else {
                resultAppend = appendForm + divEnd;
            }
            $('#groupPassConditionalArea-div').append(resultAppend);

            //Clone Option to new select
            $('#groupPassSubjectNumber1 option').clone().appendTo('#groupPassSubjectNumber'+groupPassSn);

        }
    }

    function addDyPassOption() {
        var minimunOption = 1;
        if (dyPassSn>=10) {
            //$("#error_msg").fadeIn('medium');
            //$("#error_msg").text("分類限制20以下");
        }else {
            dyPassSn++;
            appendForm = '<div id="dyPass-conditionalForm-div'+dyPassSn+'" >' +
                '<p><H5>條件'+dyPassSn+' <select id="dyPassSubjectNumber'+dyPassSn+'" name="dyPassSubjectNumber'+dyPassSn+'" width="" onchange="getDyPassAnswer('+dyPassSn+');">'+
                '<option value="" ></option></select>'+
                ' 觸發選項'+' <select id="dyPassAnswerNumber'+dyPassSn+'" name="dyPassAnswerNumber'+dyPassSn+'" width="">' +
                '<option value="<?php echo $questionRow->getSubjectNumber(); ?>" >' +
                '</option></select>' +
                ' "或"的條件'+'<input type="checkbox" id="isOrAnswer'+dyPassSn+'" name="isOrAnswer'+dyPassSn+'" value="1">';
            deleteOptionLink = '<a class="deleteOptionLink" href="javascript:removeDyPassOption('+dyPassSn+');">   x</a></H5></p>';
            divEnd = '</div>';
            if(dyPassSn > minimunOption){
                resultAppend = appendForm + deleteOptionLink + divEnd;
            }else {
                resultAppend = appendForm + divEnd;
            }
            $('#dyPassConditionalArea-div').append(resultAppend);

            //Clone Option to new select
            $('#dyPassSubjectNumber1 option').clone().appendTo('#dyPassSubjectNumber'+dyPassSn);

        }
    }

    function getGroupPassAnswer(subjectSn) {
        $.ajax({
            //groupPassSubjectNumber的value有2個值用'@'隔開
            url: '../getOptionArrayJsonForAjax/'+ $('#groupPassSubjectNumber'+subjectSn).val().split("@")[0],
            data: {},
            error: function(xhr) {
                alert('Ajax request 發生錯誤'+xhr);
            },
            success: function(response) {
                var resultArray = jQuery.parseJSON(response);
                $('#groupPassAnswerNumber'+subjectSn)
                    .find('option')
                    .remove()
                    .end();

                $.each(resultArray, function(i, object) {
                    $.each(object, function(property, value) {
                        $('#groupPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));
                    });
                });
            }
        });
    }

    function checkGroupPassAnswer(subjectSn,answerArray) {
        var answerArrayFormat = answerArray.split("@");
        $.ajax({
            //groupPassSubjectNumber的value有2個值用'@'隔開
            url: '../getOptionArrayJsonForAjax/'+ $('#groupPassSubjectNumber'+subjectSn).val().split("@")[0],
            data: {},
            error: function(xhr) {
                alert('Ajax request 發生錯誤'+xhr);
            },
            success: function(response) {
                var resultArray = jQuery.parseJSON(response);
                $('#groupPassAnswerNumber'+subjectSn)
                    .find('option')
                    .remove()
                    .end();

                $.each(resultArray, function(i, object) {
                    $.each(object, function(property, value) {
                        if(answerArray == "") {
                            $('#groupPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));
                        }else {
                            $('#groupPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));
                            if(answerArrayFormat[subjectSn-1] == value.optionValue){
                                $('#groupPassAnswerNumber'+subjectSn).children().attr("selected","true");
                            }
                        }

                    });
                });
            }
        });
    }

    function getDyPassAnswer(subjectSn) {
        $.ajax({
            //groupPassSubjectNumber的value有2個值用'@'隔開
            url: '../getOptionArrayJsonForAjax/'+ $('#dyPassSubjectNumber'+subjectSn).val().split("@")[0],
            data: {},
            error: function(xhr) {
                alert('Ajax request 發生錯誤'+xhr);
            },
            success: function(response) {
                var resultArray = jQuery.parseJSON(response);
                $('#dyPassAnswerNumber'+subjectSn)
                    .find('option')
                    .remove()
                    .end();

                $.each(resultArray, function(i, object) {
                    $.each(object, function(property, value) {
                        $('#dyPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));
                    });
                });
            }
        });
    }

    function checkDyPassAnswer(subjectSn,answerArray) {
        var answerArrayFormat = answerArray.split("@");
        $.ajax({
            //有2個值用'@'隔開
            url: '../getOptionArrayJsonForAjax/'+ $('#dyPassSubjectNumber'+subjectSn).val().split("@")[0],
            data: {},
            error: function(xhr) {
                alert('Ajax request 發生錯誤'+xhr);
            },
            success: function(response) {
                var resultArray = jQuery.parseJSON(response);
                $('#dyPassAnswerNumber'+subjectSn)
                    .find('option')
                    .remove()
                    .end();
                $.each(resultArray, function(i, object) {
                    $.each(object, function(property, value) {
                        if(answerArray == "") {

                            $('#dyPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));
                        }else {
                            $('#dyPassAnswerNumber'+subjectSn).append($('<option>', { value : value.optionValue }).text(value.optionText));

                            if(answerArrayFormat[subjectSn-1] == value.optionValue){

                                $('#dyPassAnswerNumber'+subjectSn).children().attr("selected","true");
                            }
                        }

                    });
                });
            }
        });
    }

    function checkDyPicker(surveyGuid,subjectGuid,subjectSn,answer) {
        if ($("#isDyPickerCheck"+subjectSn).attr("checked")){
            $('#type-P-value-end'+subjectSn).remove();
            $('#dyPickerSetting'+subjectSn).append(' <select id="dyPickerSelect'+subjectSn+'" name="dyPickerSelect'+subjectSn+'"></select>');
            $.ajax({
                //有2個值用'@'隔開
                url: '../getQuestionArrayJsonForAjaxForPType/'+ surveyGuid + '/' + subjectGuid + '/' ,
                data: {},
                error: function(xhr) {
                    alert('Ajax request 發生錯誤'+xhr);
                },
                success: function(response) {
                    var resultArray = jQuery.parseJSON(response);
                    $('#dyPickerSelect'+subjectSn)
                        .find('option')
                        .remove()
                        .end();
                    $.each(resultArray, function(i, object) {
                        $.each(object, function(property, value) {
                            if(answer == "") {

                                $('#dyPickerSelect'+subjectSn).append($('<option>', { value : value.QuestionSubjectNumber }).text(value.QuestionSubjectNumber+' - '+value.QuestionTitle));
                            }else {
                                $('#dyPickerSelect'+subjectSn).append($('<option>', { value : value.QuestionSubjectNumber }).text(value.QuestionSubjectNumber+' - '+value.QuestionTitle));

                                if(answer == value.optionValue){

                                    $('#dyPickerSelect'+subjectSn).children().attr("selected","true");
                                }
                            }

                        });
                    });
                }
            });
        }else {
            $('#dyPickerSelect'+subjectSn).remove();
            $('#dyPickerSetting'+subjectSn).append(' <input type="text" size="2" id="type-P-value-end'+subjectSn+'" name="type-P-value-end'+subjectSn+'">');
        }

    }
